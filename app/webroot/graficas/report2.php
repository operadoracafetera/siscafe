<?php
  require_once("Models/comulatedVehiculeDownloadMonth.php");
  require_once("Models/comulatedCoffeeDownloadMonth.php");
  require_once("Models/coffeePackagingModeDaysByUnits.php");
  require_once("Models/comulatedContainerMonth.php");

  
?>

<html>
  <head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
    <script type="text/javascript">    
    	setInterval(function() {
                  window.location.reload();
                }, 30000); 
    </script>
    <title>Estadisticas C.O.P.C. S.A.</title>
  </head>

        <br>
        <a href="./index.php">1</a>
        <div class="container" style="width: 100%;">
          <br>
          
          <div class="row">
                <div class="col-sm-3">
                    <div style="margin-left: -25px;" id="chart_div_vehicleDownloadComulate"></div>
                </div>
                <div class="col-sm-3">
                    <div style="margin-left: -40px;" id="chart_div_coffeeDownloadComulate"></div>
                </div>
                <div class="col-sm-3">
                    <div style="margin-left: -40px;" id="chart_div_coffeePackagingModeDaysByUnits"></div>
                </div>
                <div class="col-sm-3">
                    <div style="margin-left: -40px;" id="chart_div_coffeePackagingMonthsByMode"></div>
                </div>
          </div>

          <br>
          <div class="row">
                <div class="col-sm-4">
                    <div id="chart_div_vehicleDownloadWeek"></div>
                </div>
                <div class="col-sm-4">
                    <div id="chart_div_coffeeDownloadBodWeek"></div>
                </div>
                <div class="col-sm-4">
                    <div id="chart_div_coffeePackagingModeWeek"></div>
                </div>
          </div>

          <br>
          <div class="row">
                <div class="col-sm-6">
                    <div id="chart_div_vehicleDownloadMonths"></div>
                </div>
                <div class="col-sm-6">
                    <div style="margin-left: -20px;" id="chart_div_coffeePackagingModeMonths"></div>
                </div>
          </div>         

        </div>
  </body>
</html>