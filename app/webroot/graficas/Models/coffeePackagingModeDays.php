<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT distinct vp.packaging_mode AS modalidad, COUNT(vp.bic_ctn) AS cantidad  
                        FROM view_packaging AS vp
                        WHERE date(vp.packaging_date) = date(now())
			AND (estado_embalaje = 'EMBALAJE COMPLETADO' OR estado_embalaje = 'EMBALAJE LLENADO-VACIADO')
                        GROUP BY vp.packaging_mode ORDER BY vp.packaging_date DESC  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Modalidad', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();

      $temp[] = array('v' => 'Contenedores '.(string) $r['modalidad'].' ('.(int) $r['cantidad'].')'); 

      $temp[] = array('v' => (int) $r['cantidad']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">

      google.load('visualization', '1', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);   

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          var options = {
                title: 'Modalidad de embalaje diaria',
                width: 400,
                is3D: 'true',
            };
           
          var chart = new google.visualization.PieChart(document.getElementById('chart_div_coffeePackagingModeDays'));
          chart.draw(data, options);
      }

    </script>

