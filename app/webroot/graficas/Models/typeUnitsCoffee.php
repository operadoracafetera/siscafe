<?php
  
  require_once "Conection/database.php";

  $sql = mysql_query(" SELECT tu.name AS tipo_unidad, sum(rc.quantity_radicated_bag_in) AS cantidad 
                        FROM remittances_caffee rc 
                        INNER JOIN type_units tu ON tu.id = rc.type_units_id
                        GROUP  BY tu.name ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Tipo de Unidad', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysql_fetch_assoc($sql)) {
      $temp = array();
      // the following line will be used to slice the Pie chart
      $temp[] = array('v' => (string) $r['tipo_unidad']); 

      // Values of each slice
      $temp[] = array('v' => (int) $r['cantidad']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                             { calc: "stringify",
                               sourceColumn: 1,
                               type: "string",
                               role: "annotation" }]);
            var options = {
              title: "Tipo de Unidad descargado",
              bar: {groupWidth: "80%"},
              legend: { position: "none" },
              isStacked: true,
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_typeUnitsCoffee"));
            chart.draw(view, options);
      }

    </script>

