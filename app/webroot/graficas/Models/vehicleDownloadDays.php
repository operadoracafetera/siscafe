<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT  rc.created_date AS fecha, count(distinct rc.vehicle_plate) AS total_vehiculos, 
                                DATE_FORMAT(rc.created_date, '%p') AS jornada
                        FROM remittances_caffee AS rc
                        WHERE date(rc.created_date) = date(now()) and rc.jetty=1
                        GROUP BY DATE_FORMAT(rc.created_date, '%p')
                        ORDER BY rc.created_date DESC   ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Fecha descargue', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                        );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      
      $temp[] = array('v' => 'Jornada '.(string) $r['jornada'].' Vehiculos ('.(int) $r['total_vehiculos'].')'); 

      $temp[] = array('v' => (int) $r['total_vehiculos']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">

      google.load('visualization', '1', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);   

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          var options = {
                title: 'Vehiculos Descargados diario',
                width: 400,
                is3D: 'true',
            };
           
          var chart = new google.visualization.PieChart(document.getElementById('chart_div_vehicleDownloadDays'));
          chart.draw(data, options);
      }

    </script>


    <script type="text/javascript">
    /**  
      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          
           var options = {
              title : 'Vehiculos Descargados diarios',
              vAxis: {title: 'Cantidad'},
              width: 360,
              seriesType: 'bars',
              series: {3: {type: 'line'}},
              legend: { position: 'top', maxLines: 3 },
              isStacked: true,
            };

            
          var chart = new google.visualization.ComboChart(document.getElementById('chart_div_vehicleDownloadDays'));
          chart.draw(data, options);
      }
    **/      

    </script>

