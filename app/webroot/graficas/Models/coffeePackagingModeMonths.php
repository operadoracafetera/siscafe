<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT  distinct count(vp.bic_ctn) AS contenedores,  vp.packaging_date AS fecha, 
                                DATE_FORMAT(vp.packaging_date, '%d') AS dias,                               
                                DATE_FORMAT(vp.packaging_date, '%b') AS mes  
                        FROM view_packaging AS vp
                        WHERE MONTH(vp.packaging_date) = MONTH(CURDATE()) AND YEAR(vp.packaging_date) = YEAR(CURDATE()) 
			AND (estado_embalaje = 'EMBALAJE COMPLETADO' OR estado_embalaje = 'EMBALAJE LLENADO-VACIADO') AND vp.jetty='BUN'
                        GROUP BY date(vp.packaging_date)
                        ORDER BY vp.packaging_date ASC  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Fecha', 'type' => 'string'),
                          array('label' => 'Contenedores', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysqli_fetch_assoc($sql)) {
      $temp = array();

      $temp[] = array('v' => (string) $r['mes'].'-'.(string) $r['dias']); 

      $temp[] = array('v' => (int) $r['contenedores']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);

           var view = new google.visualization.DataView(data);
                      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" }]);
          
           var options = {
              title: 'Contenedores embalados en el MES',
              vAxis: {title: 'Cantidad'},
              legend: { position: "none" },
              //seriesType: 'bars',
              series: {3: {type: 'line'}},
              width: 720,
            };

            
          var chart = new google.visualization.AreaChart(document.getElementById('chart_div_coffeePackagingModeMonths'));
          chart.draw(view, options);
      }     


    </script>

