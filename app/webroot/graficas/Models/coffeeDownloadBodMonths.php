<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con,"SELECT ss.stores_caffee_id AS bodega, sum(rc.quantity_radicated_bag_in) AS radicados,
                                DATE_FORMAT(rc.created_date, '%d') AS dias,                               
                                DATE_FORMAT(rc.created_date, '%b') AS mes  
                        FROM remittances_caffee AS rc
                        INNER JOIN slot_store AS ss on ss.id=rc.slot_store_id 
                        WHERE MONTH(rc.created_date) = MONTH(CURDATE()) AND YEAR(rc.created_date) = YEAR(CURDATE()) 
                        GROUP BY date(rc.created_date), ss.stores_caffee_id
                        ORDER BY rc.created_date DESC");

  $datos[0] = array('bodega','radicados');
  $i=1;
  while($row = mysqli_fetch_assoc($sql)) {
   $bodega = $row['bodega'];
   $radicados = $row['radicados']; 
   $datos[$i] = array($bodega, (int) $radicados);
   $i++;
 }

?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var jsonTable1 = <?php echo json_encode($datos); ?>;
          var data = google.visualization.arrayToDataTable(jsonTable1);
          
           var options = {
              title: 'Descargue por bodega mensual',
              vAxis: {title: 'Cantidad'},
              legend: { position: "none" },
              //seriesType: 'bars',
              series: {3: {type: 'line'}},
              width: 480,
            };

            
          var chart = new google.visualization.AreaChart(document.getElementById('chart_div_coffeeDownloadBodMonths'));
          chart.draw(data, options);
      }     


    </script>

