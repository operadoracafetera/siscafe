<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con,"SELECT  MONTHNAME(rc.created_date) as mes,sum(rc.quantity_radicated_bag_in) AS total_cafe
                        FROM remittances_caffee AS rc
                        WHERE MONTH(rc.created_date) =  MONTH(CURDATE()) AND YEAR(rc.created_date) = YEAR(CURDATE()) AND rc.jetty=1
                        ORDER BY rc.created_date DESC  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Fecha descargue', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                        );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      $temp[] = array('v' => (string) $r['mes']); 
      $temp[] = array('v' => (int) $r['total_cafe']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);
?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          console.log(data);

            var view = new google.visualization.DataView(data);
                view.setColumns ([0,1,
                               { calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation" }]);

             var options = {
                title: 'Acomulados Mes '+<?=date("Y.m");?>+' Unidades Café Descargado',
                vAxis: {title: 'Cantidad'},
                bars: 'vertical',
                seriesType: 'bars',
                series: {1: {type: 'line'}},
                legend: { position: "none" },
                width: 370,
              };

            
          var chart = new google.visualization.ComboChart(document.getElementById('chart_div_coffeeDownloadComulate'));
          chart.draw(view, options);
      }     


    </script>


    

