<?php
  
  require_once "Conection/database.php";

  $sql = mysql_query(" SELECT  'Descargados', sum(quantity_radicated_bag_in) AS radicados FROM remittances_caffee 
                            UNION ALL SELECT 'Existentes', sum(quantity_bag_in_store) AS existentes FROM  remittances_caffee 
                            UNION ALL SELECT 'Embalados', sum(quantity_bag_out_store) AS embalados FROM remittances_caffee ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Estado', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysql_fetch_assoc($sql)) {
      $temp = array();
      // the following line will be used to slice the Pie chart
      $temp[] = array('v' => (string) $r['Descargados']); 

      // Values of each slice
      $temp[] = array('v' => (int) $r['radicados']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);


?>   

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">

      google.load('visualization', '1', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);   

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          var options = {
               title: 'Estado del CAFÉ',
              is3D: 'true',
            };
           
          var chart = new google.visualization.PieChart(document.getElementById('chart_div_stateQuantityCoffee'));
          chart.draw(data, options);
      }

    </script>

