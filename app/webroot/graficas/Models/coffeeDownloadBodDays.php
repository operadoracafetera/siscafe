<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con," SELECT ss.stores_caffee_id AS bodega, sum(rc.quantity_radicated_bag_in) AS radicados
                        FROM remittances_caffee AS rc
                        INNER JOIN slot_store ss ON ss.id=rc.slot_store_id 
                        WHERE date(rc.created_date) =date(now()) and rc.jetty=1
                        GROUP BY ss.stores_caffee_id
                        ORDER BY rc.created_date DESC  ");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Bodega', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                    );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      // the following line will be used to slice the Pie chart
      $temp[] = array('v' => 'Bodega '.(string) $r['bodega'].' ('.(int) $r['radicados'].')'); 

      // Values of each slice
      $temp[] = array('v' => (int) $r['radicados']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);

?>   

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">

      google.load('visualization', '1', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);   

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          var options = {
                title: 'Descargue por bodega diario',
                width: 400,
                is3D: 'true',
            };
           
          var chart = new google.visualization.PieChart(document.getElementById('chart_div_coffeeDownloadBodDays'));
          chart.draw(data, options);
      }

    </script>

