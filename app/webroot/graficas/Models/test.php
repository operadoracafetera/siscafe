
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2013',  1000,      400],
          ['2014',  1170,      460],
          ['2015',  660,       1120],
          ['2016',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        var chart1 = new google.visualization.AreaChart(document.getElementById('chart_div1'));
        var chart2 = new google.visualization.AreaChart(document.getElementById('chart_div2'));
        var chart3 = new google.visualization.AreaChart(document.getElementById('chart_div3'));
        var chart4 = new google.visualization.AreaChart(document.getElementById('chart_div4'));
        var chart5 = new google.visualization.BarChart(document.getElementById('chart_div5'));
        var chart6 = new google.visualization.AreaChart(document.getElementById('chart_div6'));
		var chart7 = new google.visualization.AreaChart(document.getElementById('chart_div7'));
		var chart8 = new google.visualization.AreaChart(document.getElementById('chart_div8'));
		var chart9 = new google.visualization.AreaChart(document.getElementById('chart_div9'));
        chart.draw(data, options);
        chart1.draw(data, options);
        chart2.draw(data, options);
        chart3.draw(data, options);
        chart4.draw(data, options);
        chart5.draw(data, options);
        chart6.draw(data, options);
		chart7.draw(data, options);
		chart8.draw(data, options);
        chart9.draw(data, options);
      }
    </script>
  