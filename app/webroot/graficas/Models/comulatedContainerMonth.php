<?php
  
  require_once "Conection/database.php";

  $sql = mysqli_query($con,"SELECT vp.packaging_mode as modalidad, count(vp.oie) AS cantidad  
                            FROM view_packaging AS vp
                            WHERE MONTH(vp.packaging_date) = MONTH(CURDATE()) AND YEAR(vp.packaging_date) = YEAR(CURDATE())
                            AND (estado_embalaje = 'EMBALAJE COMPLETADO' OR estado_embalaje = 'EMBALAJE LLENADO-VACIADO') AND vp.jetty='BUN'
                            GROUP BY vp.packaging_mode ORDER BY vp.packaging_date DESC");

  $flag = true;
  $table = array();
  $table['cols'] = array(
                          array('label' => 'Fecha Embalaje', 'type' => 'string'),
                          array('label' => 'Cantidad', 'type' => 'number')
                        );

  $rows = array();
  while($r = mysqli_fetch_array($sql)) {
      $temp = array();
      $temp[] = array('v' => (string) $r['modalidad']); 
      $temp[] = array('v' => (int) $r['cantidad']); 
      $rows[] = array('c' => $temp);
  }

  $table['rows'] = $rows;
  $jsonTable = json_encode($table);
?>   

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      google.charts.load('visualization', '1', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = new google.visualization.DataTable(<?=$jsonTable?>);
          console.log(data);

            var view = new google.visualization.DataView(data);
                view.setColumns ([0, 1,
                               { calc: "stringify",
                                 sourceColumn: 1,
                                 type: "string",
                                 role: "annotation" }]);

             var options = {
                title: 'Acomulados Mes '+<?=date("Y.m");?>+' Contenedores X Modalidad',
                vAxis: {title: 'Cantidad'},
                bars: 'vertical',
                seriesType: 'bars',
                series: {1: {type: 'line'}},
                legend: { position: "none" },
                width: 370,
              };

            
          var chart = new google.visualization.ComboChart(document.getElementById('chart_div_coffeePackagingMonthsByMode'));
          chart.draw(view, options);
      }     


    </script>


    

