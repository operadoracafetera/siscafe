<div class="basculeVerifications form">
    <?php $this->Html->addCrumb('Pesos padrones', 'index'); ?>
    <?php $this->Html->addCrumb('Verificar bascula');?>
<?php 
echo $this->Html->script('jquery.min');
echo $this->Form->create('BasculeVerification'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Verificación Bascula'); ?></legend>
	<?php
		//echo $this->Form->input('datetime_verification');
		echo $this->Form->input('diferency',array('label'=>'Diferencia','type' => 'hidden'));
                echo $this->Form->input('diferencia',array('label'=>'Diferencia','disabled' => 'disabled'));
		echo $this->Form->input('tolerancy',array('label'=>'Pasa tolerancia?','disabled' => 'disabled'));
                echo $this->Form->input('tolerancyHidden',array('type' => 'hidden'));
                echo $this->Form->input('mjs',array('label'=>'Mjs','disabled' => 'disabled'));
                echo $this->Form->input('basc',array('label'=>'Mjs','type' => 'hidden'));
		echo $this->Form->input('basc_id',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."")));
		echo $this->Form->input('weight',array('label'=>'Peso en Bascula','disabled' => 'disabled'));
                echo $this->Form->input('weight_basc',array('label'=>'Peso en Bascula','type' => 'hidden'));
                echo $this->Form->input('weight_ref',array('label'=>'Peso Patron','value'=>'1821','disabled' => 'disabled'));
                echo $this->Form->input('weight_patron',array('label'=>'Peso Patron','value'=>'1821','type' => 'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
    <script type="text/javascript">
            $("#BasculeVerificationBascId").change(function () {
                var valorLista = $("#BasculeVerificationBascId").val();
                var idSelector= "#BasculeVerificationBascId option[value='"+valorLista+"']";
                var textoLista = $(idSelector).text();
                $("#BasculeVerificationBasc").val(textoLista);
                setInterval(function () {
                    $.ajax({
                        url: $("#BasculeVerificationBascId").val(),
                        crossDomain: true,
                        success: function (response) {
                            $("#BasculeVerificationWeight").val(parseInt(response));
                            $("#BasculeVerificationWeightBasc").val(parseInt(response));
                            var valorLista = $("#BasculeVerificationBascId").val();
                            var idSelector= "#BasculeVerificationBascId option[value='"+valorLista+"']";
                            var textoLista = $(idSelector).text();
                            $("#BasculeVerificationBasc").val(textoLista);
                            setValueDiferency(parseInt(response));
                        }
                    });
                }, 500); //medio segundo
            });
            
            function setValueDiferency(capPeso){
                var diferenciaPesaje = (capPeso-1821);
                $("#BasculeVerificationDiferencia").val(parseInt(diferenciaPesaje));
                $("#BasculeVerificationDiferency").val(parseInt(diferenciaPesaje));
                var diferencia = Math.abs(diferenciaPesaje);
                if(diferencia >1){
                    $("#BasculeVerificationTolerancyHidden").val(false);
                    $("#BasculeVerificationTolerancy").attr('checked', false);
                    $("#BasculeVerificationMjs").val("REPROBO TOLERANCIA ");
                }
                else{
                    $("#BasculeVerificationTolerancyHidden").val(true);
                    $("#BasculeVerificationTolerancy").attr('checked', true);
                    $("#BasculeVerificationMjs").val("TOLERANCIA ACEPTADA");
                    
                }
            }
        </script>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Lista Verificación Bascula'), array('action' => 'index')); ?></li>
	</ul>
</div>
