<div class="viewLotCoffees view">
<h2><?php echo __('Información Lote de Café'); ?></h2>
	<?php
                $var_total_bag=0;
                foreach ($remittancesCaffees as $remittancesCaffee){
                    $var_total_bag+=$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'];
                }
                echo $this->Form->input('lot_coffe',array('label'=>'Lote Café','value' => '3-'.$remittancesCaffees[0]['Client']['exporter_code'].'-'.$remittancesCaffees[0]['RemittancesCaffee']['lot_caffee'], 'disabled' => 'disabled'));
		echo $this->Form->input('exporter',array('label'=>'Exportador','value' => $remittancesCaffees[0]['Client']['exporter_code'].' - '.$remittancesCaffees[0]['Client']['business_name'],'disabled' => 'disabled'));
		echo $this->Form->input('qta_bags',array('label'=>'Total Sacos Lote','value' =>$var_total_bag,'disabled' => 'disabled'));
                ?>
                <br>
                <h2><?php echo __('Listado de remesas que consolidan el lote de café'); ?></h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('create_date', 'Fecha Descargue'); ?></th>
                            <th><?php echo $this->Paginator->sort('remittance_id', 'Remesa'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_caffee', 'Lote'); ?></th>
                            <th><?php echo $this->Paginator->sort('remittance_id', 'Placa Vehiculo'); ?></th>
                            <th><?php echo $this->Paginator->sort('available_bags', 'Sacos disponibles'); ?></th>
                            <th><?php echo $this->Paginator->sort('actions', 'Acciones'); ?></th>
                        </tr>
                    </thead>
                    <?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
                        <tr>
                            <td><?php 
                            //debug($remittancesCaffee);exit;
                            echo ($remittancesCaffee['RemittancesCaffee']['download_caffee_date'] == null) ? "Sin descargar":$remittancesCaffee['RemittancesCaffee']['download_caffee_date']; ?>&nbsp;</td>
                            <td><?php echo h($this->Session->read('User.centerId').'-'.$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                            <td><?php echo h('3-'.$remittancesCaffee['Client']['exporter_code'].'-'.$remittancesCaffee["RemittancesCaffee"]['lot_caffee']); ?>&nbsp;</td>
                            <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                            <td><?php echo h(($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] - $remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'])); ?>&nbsp;</td>
                            <td><?php echo $this->Html->link(__('Ver'), array('controller'=>'RemittancesCaffees','action' => 'weights', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => '_blank',)); ?>&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <br>
                <br>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));;?></li>
		<li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'colaborador')); ?></li>
	</ul>
</div>
