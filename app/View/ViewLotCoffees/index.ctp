<div class="viewLotCoffees index">
	<h2><?php echo ($option_index == 2) ? 'Asignación de Muestras - lotes descargados':'Listado Lotes descargados'; ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('lot_caffee','Lote Café'); ?></th>
			<th><?php echo $this->Paginator->sort('qta_fraction','Cta Remesas'); ?></th>
			<th><?php echo $this->Paginator->sort('qta_bags','Sacos descargados'); ?></th>
			<th><?php echo $this->Paginator->sort('weight_lot','Peso Lote'); ?></th>
			<th><?php echo $this->Paginator->sort('business_name','Exportador'); ?></th>
			<th><?php //echo $this->Paginator->sort('download_caffee_date','Fecha descargue'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($viewLotCoffees as $viewLotCoffee): ?>
	<tr>
		<td><?php echo h('3-'.$viewLotCoffee['ViewLotCoffee']['exporter_code'].'-'.$viewLotCoffee['ViewLotCoffee']['lot_caffee']); ?>&nbsp;</td>
		<td><?php echo h($viewLotCoffee['ViewLotCoffee']['qta_fraction']); ?>&nbsp;</td>
		<td><?php echo ($viewLotCoffee['ViewLotCoffee']['qta_bags_in_store'] == null) ? "0":$viewLotCoffee['ViewLotCoffee']['qta_bags_in_store']; ?>&nbsp;</td>
		<td><?php echo h($viewLotCoffee['ViewLotCoffee']['weight_lot']); ?>&nbsp;</td>
		<td><?php echo h($viewLotCoffee['ViewLotCoffee']['business_name']); ?>&nbsp;</td>
		<td><?php //echo h($viewLotCoffee['ViewLotCoffee']['download_caffee_date']); ?>&nbsp;</td>
		<td class="actions">
                    <?php 
                            if($departaments_id ==1){
                                echo ($option_index == 2) ? $this->Html->link(__('Asignar muestra'), array('controller'=>'CoffeeSamples','action' => 'add', $viewLotCoffee['ViewLotCoffee']['lot_caffee'].'-'.$viewLotCoffee['ViewLotCoffee']['client_id']."-0")):"";
                                echo ($option_index == 1) ? $this->Html->link(__('Ver lote'), array('controller'=>'ViewLotCoffees','action' => 'view', $viewLotCoffee['ViewLotCoffee']['lot_caffee'].'-'.$viewLotCoffee['ViewLotCoffee']['client_id']."-0")):"";                                                                
                            }
                            
                        ?>
			</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo ($option_index == 2) ? $this->Html->link(__('Pre-asignar muestra'), array('controller'=>'CoffeeSamples','action' => 'preassignment')):""; ?></li>
                <li><?php echo ($option_index == 2) ? $this->Html->link(__('Listado de muestras'), array('controller'=>'CoffeeSamples','action' => 'index',1)):""; ?></li>
                <li><?php echo ($option_index != 2) ? $this->Html->link(__('Listado maestro remesas'), array('controller'=>'RemittancesCaffees','action' => 'index')):""; ?></li>
                <li><?php //echo $this->Html->link(__('Mis remesas'), array('action' => 'index',$this->Session->read('User.id')));?></li>
                <li><?php echo $this->Html->link(__('Buscar'), array('controller'=>'RemittancesCaffees','action' => 'search'));;?></li>
                <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'colaborador')); ?></li>
	</ul>
</div>
