<div class="viewLotCoffees form">
<?php echo $this->Form->create('ViewLotCoffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit View Lot Coffee'); ?></legend>
	<?php
		echo $this->Form->input('lot_caffee');
		echo $this->Form->input('count(rc.id)');
		echo $this->Form->input('sum(rc.quantity_bag_in_store)');
		echo $this->Form->input('sum(rc.total_weight_net_nominal)');
		echo $this->Form->input('business_name');
		echo $this->Form->input('user_register');
		echo $this->Form->input('departaments_id');
		echo $this->Form->input('state_operation_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ViewLotCoffee.n')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ViewLotCoffee.n')))); ?></li>
		<li><?php echo $this->Html->link(__('List View Lot Coffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
