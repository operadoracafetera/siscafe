<div class="adictionalElements form">
<?php echo $this->Form->create('AdictionalElement'); ?>
	<fieldset>
		<legend><?php echo __('Edit Adictional Element'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('code_ext');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('created_date');
		echo $this->Form->input('updated_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AdictionalElement.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('AdictionalElement.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Adictional Elements'), array('action' => 'index')); ?></li>
	</ul>
</div>
