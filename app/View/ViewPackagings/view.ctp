<div class="viewPackagings view">
    <style>
        dt{ width: 220px; }
        dd{ width: 350px; margin-left: 220px;}
    </style>        
<h2><?php echo __('Tarja Embalaje CTNR ' . strtoupper($viewPackaging['ViewPackaging']['bic_ctn'])); ?></h2>
	<dl>
		<dt><?php 
//			debug($packagingCaffee);exit;
			echo __('Oie'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['oie']); ?>
			&nbsp;
		</dd>
                <dt><?php if($this->Session->read('User.opera') != 1) echo __('Num Autorización'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') != 1){
                            echo h($viewPackaging['ViewPackaging']['autorization']);
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lotes'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') == 1){
                            echo h($viewPackaging['ViewPackaging']['lotes']); 
                        }
                        else{
                            echo h($viewPackaging['ViewPackaging']['observation']);
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Booking'); ?></dt>
		<dd>
			<?php 
			echo h($viewPackaging['ViewPackaging']['booking']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proforma'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['proforma']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Motonave'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['motorship_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Travel Num'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['travel_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Business Name'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['business_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exportador'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') == 1){
                            echo h($viewPackaging['ViewPackaging']['exportador']);
                        }
                        else{
                            echo h($viewPackaging['ViewPackaging']['client2_name']);
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expotador Code'); ?></dt>
		<dd>
			<?php 
                         if($this->Session->read('User.opera') == 1){
                             echo h($viewPackaging['ViewPackaging']['expotador_code']); 
                         }
                         else{
                             echo h($viewPackaging['ViewPackaging']['exporter_code2']); 
                         }
                            ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Agente Naviero'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['agente_naviero']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Puerto Terminal'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['jetty']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Sacos'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') == 1){
                            echo h($viewPackaging['ViewPackaging']['total_sacos']); 
                        }
                        else{
                            echo h($viewPackaging['ViewPackaging']['qta_bags2']); 
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Packaging Mode'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['packaging_mode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo embalaje'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['packaging_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sello 1'); ?></dt>
		<dd>
			<?php echo $viewPackaging['ViewPackaging']['seal_1']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sello 2'); ?></dt>
		<dd>
			<?php echo $viewPackaging['ViewPackaging']['seal_2']; ?>

			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Embalaje'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['packaging_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Iso Ctn'); ?></dt>
		<dd>
			<?php echo h($viewPackaging['ViewPackaging']['iso_ctn']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Cafe Empaque'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') == 1){
                            echo h($viewPackaging['ViewPackaging']['total_cafe_empaque']); 
                        }
                        else{
                            echo h($viewPackaging['ViewPackaging']['weight_net2']); 
                        }
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Elementos Adicionales'); ?></dt>
		<dd>
			<?php 
                        if($this->Session->read('User.opera') == 1){
                            echo h($viewPackaging['ViewPackaging']['elementos_adicionales']); 
                        }
                        else{
                            echo h($viewPackaging['ViewPackaging']['elements_adicional']); 
                        }?>
			&nbsp;
		</dd>
                <dt><?php echo __('Estado de Operación'); ?></dt>
		<dd>
                    <?php echo h($viewPackaging['ViewPackaging']['estado_embalaje']); ?>
                    &nbsp
		</dd>
                <dt>
                    <?php if($viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE LLENADO-VACIADO' || $viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE VACIADO'){
                            echo __('Observación del Vaciodo');
                    }?>
                </dt>
		<dd>
			<?php 
                        if($viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE LLENADO-VACIADO' || $viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE VACIADO'){
                            echo h($packagingCaffee['PackagingCaffee']['observation']);  
                        }
                        ?>
			&nbsp;
		</dd>
                <dt>
                    <?php if($viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE VACIADO'){
                            echo __('Fecha Vaciado');
                    }?>
                </dt>
		<dd>
			<?php 
                        if($viewPackaging['ViewPackaging']['estado_embalaje'] == 'EMBALAJE VACIADO'){
                            echo h($packagingCaffee['PackagingCaffee']['empty_date']);  
                        }
                        ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
                <li><?php echo $this->Html->link(__('Imprimir Tarja'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=10&oie=".$viewPackaging['ViewPackaging']['oie']);?> </li>
                <li><?php echo $this->Html->link(__('Trazabilidad Fotografica'), array('controller' => 'OperationTrackings', 'action' =>"addctns",$viewPackaging['ViewPackaging']['oie']),array('target'=>'_blank'));?> </li>
                <li><?php echo $this->Html->link(__('PD'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=20&oie=".$viewPackaging['ViewPackaging']['oie']);?> </li>
                <li><?php echo $this->Html->link(__('RCIS'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=21&oie=".$viewPackaging['ViewPackaging']['oie']);?> </li>
                <li><?php echo $this->Html->link(__('EIR'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=22&oie=".$viewPackaging['ViewPackaging']['oie']);?> </li>
        </ul>
</div>
