<div class="clients index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('Client');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda de Clientes'); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('exporter_code',array('label' => 'Código Exportador:'));?></td>
                <td><?php echo $this->Form->input('business_name',array('label' => 'Razón Social o Nombre:'));?></td>
                <td><?php echo $this->Form->input('nit',array('label' => 'NIT'));?></td>
                <td><?php echo $this->Form->end(__('Buscar')); ?></td>
            </tr>  
        </table>
        <br>         
    </fieldset>
    <table>
        <tr>
            <td>
                <table>
                    <thead>
                        <tr>
                        <tr>
                            <th><?php echo $this->Paginator->sort('exporter_code','Código Exportador'); ?></th>
                            <th><?php echo $this->Paginator->sort('business_name','Razón Social o Nombre'); ?></th>
                            <th><?php echo $this->Paginator->sort('nit','NIT'); ?></th>
                            <th><?php echo $this->Paginator->sort('bank_account','Cuenta Bancaria'); ?></th>
                            <th><?php echo $this->Paginator->sort('emails','Email'); ?></th>
                            <th><?php echo $this->Paginator->sort('contact_name','Nombre Contacto'); ?></th>
                            <th><?php echo $this->Paginator->sort('phone','Teléfono'); ?></th>
                            <th><?php echo $this->Paginator->sort('city_location','Ciudad'); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>                   
                        <?php
                        if($searchClients){
                            foreach ($searchClients as $searchClient): ?>
                            <tr>
                                <td><?php echo h($searchClient['Client']['exporter_code']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['business_name']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['nit']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['bank_account']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['emails']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['contact_name']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['phone']); ?>&nbsp;</td>
                                <td><?php echo h($searchClient['Client']['city_location']); ?>&nbsp;</td>
                                <td class="actions">
                                    <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $searchClient['Client']['id'])); ?>
                                    <?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $searchClient['Client']['id'])); ?>
                                </td>
                            </tr>
                        <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            
        </tr>
    </table>

</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>

