<style>      
          dt{
            width: 200px;
          }
		  dd{
            width: auto;
			margin-left: 200px;
          }
</style>	

<div class="clients view">
<h2><?php echo __('Detalle del Cliente'); ?></h2>
	<dl>		
		<dt><?php echo __('Código exportador'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['exporter_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Razón social o Nombre'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['business_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NIT'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['nit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cuenta Bancaria'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['bank_account']); ?>
			&nbsp;
		</dd>		
		<dt><?php echo __('Nombre Contacto'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['contact_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teléfono'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Correo Electrónico'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['emails']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ciudad'); ?></dt>
		<dd>
			<?php echo h($clients['Client']['city_location']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Modificar Cliente'), array('action' => 'edit', $clients['Client']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
        </ul>
</div>
 
 