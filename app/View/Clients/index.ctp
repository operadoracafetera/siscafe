
<div class="clients index">
<?php 
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('jquery-ui.min');
	echo $this->Html->css('jquery-ui.min');
?>

	<h2><?php echo __('Listado Clientes'); ?></h2>
		<h1><?php echo $this->Html->link(__('Nuevo Cliente'), array('action' => 'add')); ?></h1>
	<table cellpadding="0" cellspacing="0">
	
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('business_name'); ?></th>
			<th><?php echo $this->Paginator->sort('exporter_code'); ?></th>
			<th><?php echo $this->Paginator->sort('nit'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>

	<tbody>
	<?php foreach ($clients as $client): ?>
		<tr>
					<td><?php echo h($client['Client']['id']); ?>&nbsp;</td>
					<td><?php echo h($client['Client']['business_name']); ?>&nbsp;</td>
					<td><?php echo h($client['Client']['exporter_code']); ?>&nbsp;</td>
					<td><?php echo h($client['Client']['nit']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $client['Client']['id'])); ?>
						<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $client['Client']['id'])); ?>
					</td>
		</tr>
	<?php endforeach; ?>	
	</tbody>

	</table>

	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} de un registro total de {:count}, iniciando con {:start}, finalizando con el {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Antes'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Buscar Cliente'), array('action' => 'search')); ?></li>
		<br>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>

