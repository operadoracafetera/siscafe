
<div class="clients form">
<?php echo $this->Form->create('Client',array('novalidate' => false)); ?>

	<fieldset>
		<legend><?php echo __('Crear nuevo Cliente'); ?></legend>
	<?php
		echo $this->Form->input('exporter_code', array('label'=>'Código Exportador:','maxlength'=>'4','required' => true));
		echo $this->Form->input('business_name', array('label'=>'Razón Social o Nombre:','required' => true));
		echo $this->Form->input('nit', array('label'=>'Nit:','required' => true));
		echo $this->Form->input('bank_account', array('label'=>'Cuenta Bancaria:','required' => true));
		echo $this->Form->input('emails', array('label'=>'Correo Electrónico:'));
		echo $this->Form->input('contact_name', array('label'=>'Nombre Contacto:'));
		echo $this->Form->input('phone', array('label'=>'Teléfono:'));
		echo $this->Form->input('city_location', array('label'=>'Ciudad:'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Clientes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Menú '), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>


