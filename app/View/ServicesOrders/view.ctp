    <style>
    dd{
        width: 90%;
    }
</style>
<div class="servicesOrders view">
    <h2><?php 
    echo __('Resumen Orden de Servicio'); ?></h2>
    <dl>
        <dt><?php echo __('ID'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['id']); ?>
        </dd>
        <dt><?php echo __('Cliente'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['exporter_code'] ." - ".$dataClients[$servicesOrder['ServicesOrder']['exporter_code']]); ?>
        </dd>
        <dt><?php echo __('Completado'); ?></dt>
        <dd>
            <?php echo h(($servicesOrder['ServicesOrder']['closed']==1)?'SI':'NO'); ?>
        </dd>
        <dt><?php echo __('Fecha de Creación'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['created_date']); ?>
        </dd>
        
        <dt><?php echo __('Valor Total'); ?></dt>
        <dd>
            <?php echo h($servicesOrder['ServicesOrder']['total_valor']); ?>
        </dd>
        <dt><?php echo __('Fecha de Cierre'); ?></dt>
        <dd>
            <?php echo h(($servicesOrder['ServicesOrder']['closed_date']=='')?"Por definir":$servicesOrder['ServicesOrder']['closed_date']); ?>
        </dd>
        <dt><?php echo __('Instrucciones del Cliente: '); ?></dt>
        <dd>
            <?php echo h(($servicesOrder['ServicesOrder']['instructions']=='')?"N/A":$servicesOrder['ServicesOrder']['instructions']); ?>
        </dd>
    </dl>
    <br>
    <h2><?php echo __('Paquetes de orden de servicio'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Id'); ?></th>
                <th><?php echo $this->Paginator->sort('Paquete de Servicio'); ?></th>
                <th><?php echo $this->Paginator->sort('CO - Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Completado'); ?></th>
                <th><?php echo $this->Paginator->sort('Observaciones'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha creación'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha actualización'); ?></th>
                <th><?php echo $this->Paginator->sort('Documento'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detailsServicesPackage as $servicesPackage): ?>
                <tr>
                    <td><?php echo h($servicesPackage['ServicePackage']['id']); ?>&nbsp;</td>
                    <td><?php echo h($servicesPackage['ServicePackage']['name_package']); ?>&nbsp;</td>
                    
                    <td><?php 
                    if($servicesPackage['DetailsServicesToCaffee']['remittances_caffee_id'] != NULL)
                        echo h($this->Session->read('User.centerId')."-".$servicesPackage['DetailsServicesToCaffee']['remittances_caffee_id']); 
                    else
                        echo h("NA")?>&nbsp;</td>
                    <td><?php echo h(($servicesPackage['DetailsServicesToCaffee']['completed'] == 0) ? 'NO' : 'SI'); ?>&nbsp;</td>
                    <td><?php echo h($servicesPackage['DetailsServicesToCaffee']['observation']); ?>&nbsp;</td>
                    <td><?php echo h($servicesPackage['DetailsServicesToCaffee']['created_date']); ?>&nbsp;</td>
                    <td><?php echo h($servicesPackage['DetailsServicesToCaffee']['updated_date']); ?>&nbsp;</td>
                    <td><?php echo ($servicesPackage['DetailsServicesToCaffee']['document'] == true ) ? $this->Html->link(__('Ver Documento'), '/' . $servicesPackage['DetailsServicesToCaffee']['document'],array('target' => '_blank')): "N/A"; ?>&nbsp;</td>
                    <td class="actions">
                        <?php if($servicesPackage['DetailsServicesToCaffee']['completed'] == 0){
                            echo $this->Form->postLink(__('Borrar'), array('controller' => 'DetailsServicesToCaffees', 'action' => 'delete', $servicesPackage['DetailsServicesToCaffee']['id'],$servicesOrder['ServicesOrder']['id']), array('confirm' => __('¿Esta seguro de quitar este trabajo?')));
                            echo $this->Form->postLink(__('Completar'), array('controller' => 'DetailsServicesToCaffees', 'action' => 'completed', $servicesPackage['DetailsServicesToCaffee']['id'],$servicesOrder['ServicesOrder']['id'],$servicesPackage['ServicePackage']['id']), array('confirm' => __('¿Esta seguro de completar este trabajo ?')));
                        } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <h2><?php echo __('Detalles de paquetes'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Id Transacción'); ?></th>
                <th><?php echo $this->Paginator->sort('Cant. despachada'); ?></th>
                <th><?php echo $this->Paginator->sort('Id Paquete Servicio'); ?></th>
                <th><?php echo $this->Paginator->sort('ID Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Valor'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detailsServicesToCaffees as $detailsServicesToCaffee): ?>
                <tr>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['id']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['qta_bag_to_work']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['ServicePackage']['id']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['ItemsService']['unoee_ref']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['ItemsService']['services_name']); ?>&nbsp;</td>
                    <td><?php echo h($detailsServicesToCaffee['DetailsServicesToCaffee']['value']); ?>&nbsp;</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?> </li>
        <li><?php 
        if($servicesOrder['ServicesOrder']['closed'] == false)
            echo $this->Html->link(__('Gestionar'), array('controller'=>'DetailsServicesToCaffees','action' => 'add',$servicesOrder['ServicesOrder']['id'],$servicesOrder['ServicesOrder']['exporter_code'])); 
        else {
            if(Configure::read('siscafe.mode_app') == '1')
                echo $this->Html->link(__('Ver Remisión'), Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=9&soid=".$servicesOrder['ServicesOrder']['id']);
            else
                echo $this->Html->link(__('Ver Remisión'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=9&soid=".$servicesOrder['ServicesOrder']['id']);
        }
        ?> </li>
    </ul>
</div>
