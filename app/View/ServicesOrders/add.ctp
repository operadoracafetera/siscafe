<div class="servicesOrders form">
    <?php echo $this->Html->script('jquery.min');
    echo $this->Form->create('ServicesOrder'); ?>
    <fieldset>
        <legend><?php echo __('Agregar Orden de Servicios'); ?></legend>
        <?php
        echo $this->Form->input('exporter_code', array('label' => 'Orden relacionada al Cliente', 'type' => 'text'));
        echo $this->Form->input('client_name', array('label' => 'Nombre Cliente', 'disabled' => 'disabled'));
        //echo $this->Form->input('valor', ['disabled' => 'true','label'=>'Total Valor']);
        echo $this->Form->input('total_valor', ['type' => 'hidden']);
        ?>
    </fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<script>
    $("#ServicesOrderExporterCode").keyup(function () {
        findInfoRemittancesByClientCode($(this).val());
    });

    function findInfoRemittancesByClientCode(exportCode) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByExportCode/" + exportCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var clientData = JSON.parse(data)['clientData'];
                    var remittancesData = JSON.parse(data)['remittancesData'];
                    $("#ServicesOrderClientName").val(clientData['Client']['business_name']);
                }
            }
        });
    }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
