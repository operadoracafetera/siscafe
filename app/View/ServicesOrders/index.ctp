<div class="servicesOrders index">
    <h2><?php echo __('Ordenes de Servicio'); ?></h2>
    <?php echo $this->Html->link(__('Crear Orden de Servicio'), array('action' => 'add')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('ID'); ?></th>
                <th><?php echo $this->Paginator->sort('Servicio prestado al Cliente'); ?></th>
                <th><?php echo $this->Paginator->sort('Completada'); ?></th>
                <th><?php echo $this->Paginator->sort('Autorizada'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha de Creación'); ?></th>
                <th><?php echo $this->Paginator->sort('Fecha Cierre'); ?></th>
                <th><?php echo $this->Paginator->sort('Relación'); ?></th>
                <th><?php echo $this->Paginator->sort('Valor Total'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($servicesOrders as $servicesOrder): ?>
                <tr>
                    <td><?php 
                    //debug($servicesOrder);exit;
                    echo h($servicesOrder['ServicesOrder']['id']); ?>&nbsp;</td>
                    <td><?php echo h($dataClients[$servicesOrder['ServicesOrder']['exporter_code']]); ?>&nbsp;</td>
                    <td><?php 
                    if($servicesOrder['ServicesOrder']['active'] == 0) echo 'CANCELADA'; 
                    else{ echo ($servicesOrder['ServicesOrder']['closed']==1)?'SI':'NO';}
                    ?>&nbsp;</td>
                    <td><?php echo ($servicesOrder['ServicesOrder']['authorized']==1) ? 'AUTORIZADA':'RECHAZADA'; ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['ServicesOrder']['created_date']); ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['ServicesOrder']['closed_date']); ?>&nbsp;</td>
                    <td><?php 
                    $observation="";
                    $tmp="";
                    $tmp2="";
                    foreach($servicesOrder['DetailsServicesToCaffee'] as $detailsServices){
                        $tmp = $detailsServices['observation'];
                        if($tmp != $tmp2){
                            $observation.=$detailsServices['observation']." | ";
                        }
                        $tmp2 = $detailsServices['observation'];
                    }
                    echo h($observation);
                    ?>&nbsp;</td>
                    <td><?php echo h($servicesOrder['ServicesOrder']['total_valor']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php 
                              echo $this->Html->link(__('Ver'), array('action' => 'view', $servicesOrder['ServicesOrder']['id']));
                              if($servicesOrder['ServicesOrder']['closed']==0 & $servicesOrder['ServicesOrder']['active']==1)
                              {
                                  echo $this->Html->link(__('Agegar Servicio'), array('controller' => 'DetailsServicesToCaffees', 'action' => 'add', $servicesOrder['ServicesOrder']['id'], $servicesOrder['ServicesOrder']['exporter_code']));
                                  echo $this->Form->postLink(__('Cancelar'), array('action' => 'delete', $servicesOrder['ServicesOrder']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicesOrder['ServicesOrder']['id'])));
                                  if(!in_array($servicesOrder['ServicesOrder']['id'], $detailsServicesToCaffee, true)){
                                      echo $this->Html->link(__('Editar'), array('action' => 'edit', $servicesOrder['ServicesOrder']['id'])); 
                                  }
                              }
                              else{
                                  if(Configure::read('siscafe.mode_app') == '1')
                                    echo $this->Html->link(__('Remisión'), Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=9&soid=".$servicesOrder['ServicesOrder']['id']);
                                  else
                                    echo $this->Html->link(__('Remisión'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=9&soid=".$servicesOrder['ServicesOrder']['id']);
                              }
                            ?>
                        <?php  ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        
        <li><?php echo $this->Html->link(__('Fumicafe'), array('controller' => 'FumigationServices', 'action' => 'index')); ?> </li>
        <li><?php //echo $this->Html->link(__('Buscar Ordenes Servicios'), array('controller' => 'ServicesOrders', 'action' => 'search')); ?> </li>
        <li><?php echo $this->Html->link(__('Paquetes Servicios'), array('controller' => 'ServicePackages', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
    </ul>
</div>
