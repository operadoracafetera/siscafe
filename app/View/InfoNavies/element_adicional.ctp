<div class="infoNavies form">
<?php echo $this->Form->create('InfoNavy'); ?>
    <fieldset>
        <legend><?php echo __('Agregar Elementos Adicionales a la Proforma (Información Naviera)'); ?></legend>
        
        <table>
            <tr>
                <?php echo $this->Form->input('id'); ?>
                <td><?php echo $this->Form->input('proforma',array('disabled' => 'disabled')); ?></td>
                <td><?php echo $this->Form->input('booking',array('disabled' => 'disabled')); ?></td>
                <td><?php echo $this->Form->input('motorship_name',array('disabled' => 'disabled'));  ?></td>              
            </tr>
        </table>
        
        <?php echo $this->Form->input('type_papper',array('label'=>'Tipo de Papel','required' => true, 'type' => 'select',
           'options' =>array('KRAFT'=>'PAPEL KRAFT','KRAFT2'=>'PAPEL KRAFT2','CARTON'=>'PAPEL CARTON','CARTON2'=>'PAPEL CARTON2', 'REF-CTN'=>'CTN REF'),
           'empty'=>'Seleccione un  tipo de papel...','style'=>'width:100%; font-size:16px; height:40px; color:#E32;'));  ?>
		<?php echo $this->Form->input('num_ctn_real',array('label'=>'Ctns Embalados','type'=>'number','required' => true,'default'=>1));  ?>
        <?php echo $this->Form->input('adictional_dry_bags',array('label'=>'Cantidad de Dry Bags','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_kraft_20',array('label'=>'Cantidad de capa kraft de 20','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_kraft_40',array('label'=>'Cantidad de capa kraft de 40','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_carton_20',array('label'=>'Cantidad de capa carton de 20','required' => true));  ?>
        <?php echo $this->Form->input('adictional_layer_carton_40',array('label'=>'Cantidad de capa carton de 40','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_label',array('label'=>'Número de etiqueta','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_stiker',array('label'=>'Número de stiker','required' => true));  ?>
        <?php echo $this->Form->input('adictional_num_manta',array('label'=>'Número V-DRY 20','required' => true));  ?>
        <?php echo $this->Form->input('mantas_vdry_40',array('label'=>'Número V-DRY 40','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('tsl',array('label'=>'TSL','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('hc_qta',array('label'=>'HC','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('seals',array('label'=>'Sellos','required' => true,'default'=>'0'));  ?>
	<?php echo $this->Form->input('tls_desiccant',array('label'=>'TSL con Desecantes','required' => true,'default'=>'0'));  ?>
        <?php echo $this->Form->input('rejillas',array('label'=>'Rejillas','required' => true));  ?>
	<?php echo $this->Form->input('dataloger',array('label'=>'Data Logger','required' => true));  ?>
	<?php echo $this->Form->input('absortes',array('label'=>'Absorbentes','required' => true));  ?>
	<?php echo $this->Form->input('pallets',array('label'=>'Pallets','required' => true));  ?>
        
    </fieldset>
<?php echo $this->Form->end(__('Actualizar')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
