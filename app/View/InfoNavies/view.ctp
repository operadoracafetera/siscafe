<div class="infoNavies view">
    <h2><?php echo __('Info Navy'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Proforma'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['proforma']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Packaging Type'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['packaging_type']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Motorship Name'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['motorship_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Packaging Mode'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['packaging_mode']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Booking'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['booking']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Travel Num'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['travel_num']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Destiny'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['destiny']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Navy Agent Id'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['navy_agent_id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Shipping Lines Id'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['shipping_lines_id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Customs Id'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['customs_id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Jornally'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['jornally']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Observation'); ?></dt>
        <dd>
            <?php echo h($infoNavy['InfoNavy']['observation']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Info Navy'), array('action' => 'edit', $infoNavy['InfoNavy']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Info Navy'), array('action' => 'delete', $infoNavy['InfoNavy']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $infoNavy['InfoNavy']['id']))); ?> </li>
        <li><?php echo $this->Html->link(__('List Info Navies'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Info Navy'), array('action' => 'add')); ?> </li>
    </ul>
</div>
