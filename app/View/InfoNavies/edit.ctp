<div class="infoNavies form">
<?php echo $this->Form->create('InfoNavy'); ?>
    <fieldset>
        <legend><?php echo __('Modifcar Proforma (Información Naviera)'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('proforma');
		echo $this->Form->input('booking');
		echo $this->Form->input('travel_num');
                echo $this->Form->input('long_container',array('label' => 'Longitud Contenedor','empty'=>'Seleccione...', 'required' => true,
                'options' => array('20' => '22G1', '40' => '42G1', '20-RF' => '20 RF', '40-RF' => '40 RF')));
		echo $this->Form->input('destiny');
                echo $this->Form->input('motorship_name');
                echo $this->Form->input('packaging_type',array('label' => 'Tipo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('FCL' => 'FCL', 'LCL' => 'LCL')));
            	echo $this->Form->input('packaging_mode',array('label' => 'Modo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('Sacos' => 'Sacos','Granel' => 'Granel','CACAO' => 'CACAO','COMBINADO(SACOS - CAJAS)' => 'COMBINADO(SACOS - CAJAS)','COMBINADO(SACOS - BIG BAGS)' => 'COMBINADO(SACOS - BIG BAGS)','BIG BAGS' => 'BIG BAGS','Cajas' => 'Cajas')));
                echo $this->Form->input('navy_agent_id',array('label' => 'Código Agente Marítimo', 'type'=>'text', 'required' => true));
                echo $this->Form->input('navy_agent_name',array('label' => 'Nombre Agente Marítimo','disabled' => 'disabled', 'value'=>$this->request->data['NavyAgent']['name']));
                echo $this->Form->input('shipping_lines_id',array('label' => 'Código Línea Naviera', 'type'=>'text', 'required' => true));
                echo $this->Form->input('shipping_lines_name',array('label' => 'Nombre Línea Naviera','disabled' => 'disabled', 'value'=>$this->request->data['ShippingLine']['business_name']));
                echo $this->Form->input('customs_id',array('label' => 'Código Agencia de Aduanas', 'type'=>'text', 'required' => true));
                echo $this->Form->input('customs_name',array('label' => 'Nombre Agencia de Aduanas','disabled' => 'disabled', 'value'=>$this->request->data['Custom']['cia_name']));
		echo $this->Form->input('observation');
	?>
    </fieldset>
    <script type="text/javascript">
        $("#InfoNavyNavyAgentId").keyup(function () {
            findNavyAgent($(this).val());
        });

        $("#InfoNavyShippingLinesId").keyup(function () {
            findShippingLine($(this).val());
        });

        $("#InfoNavyCustomsId").keyup(function () {
            findCustom($(this).val());
        });

        function findNavyAgent(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/NavyAgents/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyNavyAgentName").val(json.NavyAgent.name);
                    } else {
                        $("#InfoNavyNavyAgentName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
        
        function findShippingLine(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ShippingLines/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyShippingLinesName").val(json.ShippingLine.business_name);
                    } else {
                        $("#InfoNavyShippingLinesName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
        
        function findCustom(id) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/Customs/findById/" + id,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#InfoNavyCustomsName").val(json.Custom.cia_name);
                    } else {
                        $("#InfoNavyCustomsName").val("NO ARROJO RESULTADOS");
                    }
                }});
        }
    </script>
<?php echo $this->Form->end(__('Actualizar')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
