<div class="infoNavies index">
    <h2><?php echo __('Master Información Naviera'); ?></h2>
    <?php echo $this->Html->link(__('Crear Proforma'), array('action' => 'add')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                 <th><?php echo $this->Paginator->sort('', 'Código'); ?></th>
                            <th><?php echo $this->Paginator->sort('proforma','Proforma'); ?></th>
                            <th><?php echo $this->Paginator->sort('booking','Booking'); ?></th>
                            <th><?php echo $this->Paginator->sort('motorship_name','Motonave'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_type','Tipo'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_mode','Modo Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('travel_num','Viaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fceha Embalaje'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($infoNavies as $infoNavy): ?>
            <tr>
                <td><?php echo h($infoNavy['InfoNavy']['id']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['proforma']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['booking']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['motorship_name']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['packaging_type']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['packaging_mode']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['travel_num']); ?>&nbsp;</td>
                <td><?php echo h($infoNavy['InfoNavy']['created_date']); ?>&nbsp;</td>
                <td class="actions">
			<?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $infoNavy['InfoNavy']['id'])); ?>
                        <!--?php echo $this->Html->link(__('Elementos adicionales'), array('controller' => 'AdictionalElementsHasPackagingCaffees', 'action' => 'addelementadicional','?' => ['info_navy_id' => $infoNavy['InfoNavy']['id'],'proforma' => $infoNavy['InfoNavy']['proforma']])); ?-->
                        <?php echo $this->Html->link(__('Elementos Adicionales'), array('action' => 'elementAdicional', $infoNavy['InfoNavy']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'Pages', 'action' => 'colaborador')); ?></li>
        <li><?php echo $this->Html->link(__('Buscar Naviera'), array('controller'=>'InfoNavies', 'action' => 'searchNavies')); ?></li>
    </ul>
</div>
