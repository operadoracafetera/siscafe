<div class="vehiculeInspections view">
<h2><?php echo __('Información Inspección Vehiculo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Inspection'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['date_inspection']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mark Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['mark_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['place_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['color_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Driver Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['driver_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Identification Driver'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['identification_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date In Reten'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['date_in_reten']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Check Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['time_check_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Exit Vehicule'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['date_exit_vehicule']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q1'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q2'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q3'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q4'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q5'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q6'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q7'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q7']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q8'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q8']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q9'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q9']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q10'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q10']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q11'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q11']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q12'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q12']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q13'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q13']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q14'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q14']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q15'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q15']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q16'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q16']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q17'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q17']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q18'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q18']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Q19'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['q19']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Users Id'); ?></dt>
		<dd>
			<?php echo h($vehiculeInspection['VehiculeInspection']['users_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Vehicule Inspections'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicule Inspection'), array('action' => 'add')); ?> </li>
	</ul>
</div>
