<div class="vehiculeInspections index">
	<h2><?php echo __('Listado Inspección de Vehiculos'); ?></h2>
        <?php echo $this->Html->link(__('Registrar nueva inspección'), array('action' => 'add'));?>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('date_inspection','fecha inspección'); ?></th>
			<th><?php echo $this->Paginator->sort('mark_vehicule','marca vehicule'); ?></th>
			<th><?php echo $this->Paginator->sort('place_vehicule','placa vehicule'); ?></th>
			<th><?php echo $this->Paginator->sort('driver_vehicule', 'Conductor'); ?></th>
			<th><?php echo $this->Paginator->sort('identification_driver','Cedula'); ?></th>
			<th><?php echo $this->Paginator->sort('date_in_reten','Fecha ingreso Reten'); ?></th>
			<th><?php echo $this->Paginator->sort('time_check_vehicule','Hora Chequeo'); ?></th>
			<th><?php echo $this->Paginator->sort('date_exit_vehicule','Fecha salida'); ?></th>
			<th><?php echo $this->Paginator->sort('users_id','Responsable revisión'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($vehiculeInspections as $vehiculeInspection): ?>
	<tr>
		<td><?php 
                echo h($vehiculeInspection['VehiculeInspection']['id']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['date_inspection']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['mark_vehicule']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['place_vehicule']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['driver_vehicule']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['identification_driver']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['date_in_reten']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['time_check_vehicule']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['VehiculeInspection']['date_exit_vehicule']); ?>&nbsp;</td>
		<td><?php echo h($vehiculeInspection['User']['username']); ?>&nbsp;</td>
		<td class="actions">
                <?php 
                if(Configure::read('siscafe.mode_app') == '1')
                    echo $this->Html->link(__('Ver inspección'), Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=12&inspection=".$vehiculeInspection['VehiculeInspection']['id']); 
                else
                    echo $this->Html->link(__('Ver inspección'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=12&inspection=".$vehiculeInspection['VehiculeInspection']['id']); 
                ?>	</tr>
                
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
