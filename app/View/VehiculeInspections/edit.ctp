<div class="vehiculeInspections form">
<?php echo $this->Form->create('VehiculeInspection'); ?>
	<fieldset>
		<legend><?php echo __('Edit Vehicule Inspection'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('date_inspection');
		echo $this->Form->input('mark_vehicule');
		echo $this->Form->input('place_vehicule');
		echo $this->Form->input('color_vehicule');
		echo $this->Form->input('driver_vehicule');
		echo $this->Form->input('identification_driver');
		echo $this->Form->input('date_in_reten');
		echo $this->Form->input('time_check_vehicule');
		echo $this->Form->input('date_exit_vehicule');
		echo $this->Form->input('q1');
		echo $this->Form->input('q2');
		echo $this->Form->input('q3');
		echo $this->Form->input('q4');
		echo $this->Form->input('q5');
		echo $this->Form->input('q6');
		echo $this->Form->input('q7');
		echo $this->Form->input('q8');
		echo $this->Form->input('q9');
		echo $this->Form->input('q10');
		echo $this->Form->input('q11');
		echo $this->Form->input('q12');
		echo $this->Form->input('q13');
		echo $this->Form->input('q14');
		echo $this->Form->input('q15');
		echo $this->Form->input('q16');
		echo $this->Form->input('q17');
		echo $this->Form->input('q18');
		echo $this->Form->input('q19');
		echo $this->Form->input('observation');
		echo $this->Form->input('users_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VehiculeInspection.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('VehiculeInspection.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vehicule Inspections'), array('action' => 'index')); ?></li>
	</ul>
</div>
