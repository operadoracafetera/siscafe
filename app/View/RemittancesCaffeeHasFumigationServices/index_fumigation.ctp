<div class="fumigationServices index">
	<h2><?php echo __('Listado de servicio de Fumigación'); ?></h2>
        <table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
            <th><?php echo $this->Paginator->sort('Id Fumigación'); ?></th>
            <th><?php echo $this->Paginator->sort('Ref Lote'); ?></th>
			<th><?php echo $this->Paginator->sort('Cantidad'); ?></th>
			<th><?php echo $this->Paginator->sort('Exportador'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha registro'); ?></th>
            <th><?php echo $this->Paginator->sort('Ciudad'); ?></th>
            <th><?php echo $this->Paginator->sort('Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('Lotes Descargados'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($fumigationServices as $fumigationService): ?>
	<tr>
		<td><?php echo h($fumigationService['FumigationService']['id']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['ScheduleCoffeeFumigation']['lot_coffee']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['ScheduleCoffeeFumigation']['qta_coffee']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['FumigationService']['Client']['business_name']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['FumigationService']['created_date']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['FumigationService']['Departament']['name']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['HookupStatus']['status_name']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['RemittancesCaffeeHasFumigationService']['state']) ? "SI":"NO"; ?>&nbsp;</td>
		<td class="actions">
			<?php
                echo $this->Html->link(__('Fotografias'), array('controller' => 'OperationTrackings','action' => 'addfumigation', $fumigationService['RemittancesCaffeeHasFumigationService']['id'])); 
				if($fumigationService['RemittancesCaffeeHasFumigationService']['state'] == 0){
					echo $this->Html->link(__('Fumigar'), array('controller' => 'RemittancesCaffeeHasFumigationServices','action' => 'completeLotCoffee', $fumigationService['RemittancesCaffeeHasFumigationService']['id'])); 
				}
				
            ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Ordenes Servicios'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Servicios'), array('controller'=>'FumigationServices', 'action' => 'searchServices')); ?></li>
	</ul>
</div>
