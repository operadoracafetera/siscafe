<div class="fumigationServices index">
	<h2><?php echo __('Listado de Lotes descargados para fumigar - (Completar por lotes)'); ?></h2>
        <table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
            <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
            <th><?php echo $this->Paginator->sort('Lotes Cafe'); ?></th>
			<th><?php echo $this->Paginator->sort('Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('Cantidad'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha descargado'); ?></th>
            <th><?php echo $this->Paginator->sort('Id Fumigacion'); ?></th>
            <th><?php echo $this->Paginator->sort('Ciudad'); ?></th>
            <th><?php echo $this->Paginator->sort('Completado?'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($fumigationServices as $fumigationService): ?>
	<tr>
		<td><?php echo h($fumigationService['RemittancesCaffee']['id']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['RemittancesCaffee']['Client']['exporter_code']."-".$fumigationService['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['RemittancesCaffee']['StateOperation']['name']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['RemittancesCaffeeHasFumigationService']['qta_coffee']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['FumigationService']['id']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['Departament']['name']); ?>&nbsp;</td>
        <td><?php echo h($fumigationService['RemittancesCaffeeHasFumigationService']['state']) ? "SI":"NO"; ?>&nbsp;</td>
		<td class="actions">
			<?php
                echo $this->Html->link(__('Fotografias'), array('controller' => 'OperationTrackings','action' => 'addfumigation', $fumigationService['RemittancesCaffeeHasFumigationService']['id'])); 
				if($fumigationService['RemittancesCaffeeHasFumigationService']['state'] == 0){
					echo $this->Html->link(__('Fumigar'), array('controller' => 'RemittancesCaffeeHasFumigationServices','action' => 'completeLotCoffee', $fumigationService['RemittancesCaffeeHasFumigationService']['id'])); 
				}
				
            ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Ordenes Servicios'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Servicios'), array('controller'=>'FumigationServices', 'action' => 'searchServices')); ?></li>
	</ul>
</div>
