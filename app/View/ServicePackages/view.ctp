<div class="servicePackages view">
<h2><?php echo __('Service Package'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name Package'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['name_package']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated Date'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['updated_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Per Bag'); ?></dt>
		<dd>
			<?php echo h($servicePackage['ServicePackage']['price_per_bag']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Service Package'), array('action' => 'edit', $servicePackage['ServicePackage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Service Package'), array('action' => 'delete', $servicePackage['ServicePackage']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicePackage['ServicePackage']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Packages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Package Has Items Services'), array('controller' => 'service_package_has_items_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package Has Items Service'), array('controller' => 'service_package_has_items_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Service Package Has Items Services'); ?></h3>
	<?php if (!empty($servicePackage['ServicePackageHasItemsService'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Service Package Id'); ?></th>
		<th><?php echo __('Items Services Id'); ?></th>
		<th><?php echo __('Qta'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($servicePackage['ServicePackageHasItemsService'] as $servicePackageHasItemsService): ?>
		<tr>
			<td><?php echo $servicePackageHasItemsService['service_package_id']; ?></td>
			<td><?php echo $servicePackageHasItemsService['items_services_id']; ?></td>
			<td><?php echo $servicePackageHasItemsService['qta']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'service_package_has_items_services', 'action' => 'view', $servicePackageHasItemsService['service_package_id,items_services_id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'service_package_has_items_services', 'action' => 'edit', $servicePackageHasItemsService['service_package_id,items_services_id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'service_package_has_items_services', 'action' => 'delete', $servicePackageHasItemsService['service_package_id,items_services_id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicePackageHasItemsService['service_package_id,items_services_id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Service Package Has Items Service'), array('controller' => 'service_package_has_items_services', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
