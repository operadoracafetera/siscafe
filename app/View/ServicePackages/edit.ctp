<div class="servicePackages form">
<?php echo $this->Form->create('ServicePackage'); ?>
	<fieldset>
		<legend><?php echo __('Editar Paquete de servicio'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name_package');
		echo $this->Form->input('active', array('label'=>'Activo'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listar Paquetes de Servicios'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Menu'), array('controller' => 'service_package_has_items_services', 'action' => 'index')); ?> </li>
	</ul>
</div>
