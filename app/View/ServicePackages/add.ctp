<div class="servicePackages form">
<?php echo $this->Form->create('ServicePackage'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Paquete de Servicio'); ?></legend>
	<?php
		echo $this->Form->input('name_package',array('label'=>'Nombre del Paquete'));
		echo $this->Form->input('active',array('label'=>'Activo','value'=>true));
		//echo $this->Form->input('created_date');
		//echo $this->Form->input('updated_date');
		//echo $this->Form->input('price_per_bag',array('label'=>'Precio por saco'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menu'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Listado Paquetes Servicios'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
