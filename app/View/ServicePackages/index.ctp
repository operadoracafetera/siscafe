<div class="servicePackages index">
	<h2><?php echo __('Listado de Paquetes de Servicios'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name_package','Nombre del Paquete'); ?></th>
			<th><?php echo $this->Paginator->sort('active','Activo'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha creado'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_date','Fecha actualizado'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($servicePackages as $servicePackage): ?>
	<tr>
		<td><?php echo h($servicePackage['ServicePackage']['id']); ?>&nbsp;</td>
		<td><?php echo h($servicePackage['ServicePackage']['name_package']); ?>&nbsp;</td>
		<td><?php echo h($servicePackage['ServicePackage']['active']==true ? "SI": "NO"); ?>&nbsp;</td>
		<td><?php echo h($servicePackage['ServicePackage']['created_date']); ?>&nbsp;</td>
		<td><?php echo h($servicePackage['ServicePackage']['updated_date']); ?>&nbsp;</td>
		<td class="actions">
                        <?php echo $this->Html->link(__('Ver'), array('controller' => 'ServicePackageHasItemsServices','action' => 'add', $servicePackage['ServicePackage']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $servicePackage['ServicePackage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo registro'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Regresar'), array('controller' => 'ServicesOrders', 'action' => 'index')); ?> </li>
	</ul>
</div>
