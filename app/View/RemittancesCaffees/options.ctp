

<div class="remittancesCaffees view">
<?php 
	header('Content-Type: text/html; charset=UTF-8');
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('RemittancesCaffees');?>
    <fieldset>
        <legend><?php 
        if($this->Session->read('User.opera') == 1)
            echo __('Paletizar Remesa Descargue'); 
        else
            echo __('Información Remesa (Número de servicio) - Descargue'); 
        ?></legend>
	<?php
		echo $this->Form->input('remittances_caffee',array('value'=>$idRemittanceCoffee,'type' => 'hidden'));
		echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
		echo $this->Form->input('remesa',array('label' => 'CO - Remesa','value'=>$this->Session->read('User.centerId')."-".$idRemittanceCoffee, 'disabled' => 'disabled'));
                if($remittancesCaffee['RemittancesCaffee']['coffee_with_packging_anormal']){
                    echo $this->Form->input('tare_packing_coffee_anormal',array('type' => 'number','step'=>'any','required' => true,'label' => 'Peso Tara Empaque (Kg)','value'=>$remittancesCaffee['RemittancesCaffee']['tare_packing_coffee_anormal']));   
                    echo $this->Form->input('packing_cafee_id',array('type' => 'hidden','value'=>$remittancesCaffee['RemittancesCaffee']['packing_cafee_id']));
                    echo $this->Form->input('packing_cafee_id1',array('type' => 'select','disabled'=>'disabled','label' => 'Empaque Cafe - Gramaje','options' => $packingCaffee,'value'=>$remittancesCaffee['RemittancesCaffee']['packing_cafee_id']));
                }
                else{
                    echo $this->Form->input('packing_cafee_id',array('type' => 'select','label' => 'Empaque Cafe - Gramaje','options' => $packingCaffee,'required' => true,'value'=>$remittancesCaffee['RemittancesCaffee']['packing_cafee_id'],'multiple' => false));
                    echo $this->Form->input('tare_packing_coffee_anormal',array('type' => 'hidden','step'=>'any','required' => true,'label' => 'Peso Tara Empaque (Kg)','value'=>$remittancesCaffee['RemittancesCaffee']['tare_packing_coffee_anormal'])); 
                }
                echo $this->Form->input('nota_entrega',array('label' => 'Nota entrega', 'value' => $remittancesCaffee['RemittancesCaffee']['nota_entrega']));             
                echo $this->Form->input('ref_driver',array( 'label' => 'Zona Bodega', 'value' => $remittancesCaffee['RemittancesCaffee']['ref_driver']));
	        echo $this->Form->input('type_units_id',array( 'label' => 'Tipo de unidad', 'options' => $typesUnits, 'required' => true,'value' => $remittancesCaffee['TypeUnit']['id']) );
                echo $this->Form->input('material',array('type' => 'select','label' => 'Material Cafe','options' => $materialCoffee,'required' => true,'value'=>$remittancesCaffee['MaterialCoffee']['id'],'multiple' => false));
		echo $this->Form->input('desc_material',array('label' => 'Descripción Material','disabled' => 'disabled','value'=>$remittancesCaffee['MaterialCoffee']['id'].'-'.$remittancesCaffee['MaterialCoffee']['name'])); 
                echo $this->Form->input('coffee_year',array('type' => 'select','label' => 'Year Coffee','disabled' => 'enable','required' => true,'value'=>$remittancesCaffee['RemittancesCaffee']['coffee_year'], 'options'=>['1'=>'2021 cafetero','0'=>'2020 cafetero']));        
		echo $this->Form->input('staff_sample_id',array('type' => 'select','label' => 'Muestreador','options' => $usersSamplers,'value'=>$remittancesCaffee['RemittancesCaffee']['staff_sample_id']));
		echo $this->Form->input('staff_driver_id',array('type' => 'select','label' => 'Motorista','options' => $usersDriver,'value'=>$remittancesCaffee['RemittancesCaffee']['staff_driver_id']));
                if($this->Session->read('User.opera') != 1){
                   $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
                   echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
                   echo $this->Form->input('sacos',array('label' => 'Cant. Sacos','value'=>$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'], 'disabled' => 'disabled'));
                   echo $this->Form->input('driver',array( 'label' => 'Motorista', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['Users']['first_name']." ".  $remittancesCaffee['Users']['last_name']) );
                   echo $this->Form->input('fecha_descargue',array( 'label' => 'Fecha descargue ', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['download_caffee_date']) );
                   echo $this->Form->input('details_weight',array( 'label' => 'Detalle de peso', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['details_weight']) );
                   echo $this->Form->input('observation',array( 'label' => 'Observación', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['observation']) );
                   echo $this->Form->input('empaque',array( 'label' => 'Empaque Café ingreso', 'disabled' => 'disabled', 'value' => $remittancesCaffee['PackingCaffee']['name']." - (".$remittancesCaffee['PackingCaffee']['weight'].") Kg x Saco") );
                   echo $this->Form->input('type_unit',array( 'label' => 'Tipo de unidad', 'disabled' => 'disabled', 'value' => $remittancesCaffee['TypeUnit']['name']) );
                   echo $this->Form->input('unit_caffe',array( 'label' => 'Unidad Cafe', 'disabled' => 'disabled', 'value' => $remittancesCaffee['UnitsCaffee']['name']) );
                   
                }
                	?> 
        
	<?php
            echo $this->Form->input('observation',array( 'label' => 'Observaciones' , 'value' => $remittancesCaffee['RemittancesCaffee']['observation']) );
            echo $this->Form->input('details_weight',array( 'label' => 'Detalles del Peso' , 'value' => $remittancesCaffee['RemittancesCaffee']['details_weight']) );

            ?>
            <?php echo $this->Form->end(__('Guardar')); ?>
            <?php echo $this->Html->link(__('Pesar'),array('controller'=>'RemittancesCaffees','action' => 'view',$idRemittanceCoffee)) ?>

        <script type="text/javascript">

            $("#RemittancesCaffeesMaterial").click(function () {
                var idMaterial = $(this).val();
                findMaterialCode(idMaterial);
            });
            
            function findMaterialCode(idMaterial){
                console.log(idMaterial);
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/MaterialCoffees/getMaterialCode/"+idMaterial,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         console.log(json);
                         if(JSON.stringify(json) !== "[]"){
                             $("#RemittancesCaffeesDescMaterial").val(json['MaterialCoffee']['cod_material']+"-"+json['MaterialCoffee']['description']);
                         }
                         else{
                             $("#RemittancesCaffeesDescMaterial").val("NO ARROJO RESULTADOS");
                         }
                  }});
            }

        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php
        if($this->Session->read('User.opera') != 1){
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'index')); 
            echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));
        }
        else
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'download_caffee')); 
        ?></li>
    </ul>
</div>