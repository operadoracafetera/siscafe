<div class="remittancesCaffees index">
    <style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>
    <h2><?php echo __('Listado de Remesas a Descargar'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','CO-Remesa'); ?></th>
		<th><?php //echo $this->Paginator->sort('cargolot_id','CargoLot ID'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Lote a Descargar'); ?></th>
                <th><?php echo $this->Paginator->sort('stores_caffee_id','Pos Bod'); ?></th>
		<th><?php echo $this->Paginator->sort('vehicle_plate','Placa vehicule'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_in_pallet_caffee','Pallet In'); ?></th>
                <th><?php //echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php //echo $this->Paginator->sort('total_weight_net_real','Peso Real'); ?></th>
                <th><?php echo $this->Paginator->sort('tare_download','Tara Descargue'); ?></th>
                <th><?php //echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha Radicado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //debug($remittancesCaffee);exit;
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
		<td><?php //echo h($remittancesCaffee['RemittancesCaffee']['cargolot_id']); ?>&nbsp;</td>
                <td style="background-color: #f16542;"><span style="font-size: 15px ;color: black;"><?php echo h('3-'.$remittancesCaffee['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</span></td>
                <td><?php echo h($remittancesCaffee['SlotStore']['name_space']); ?>&nbsp;</td>
		<td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td style="background-color: #f16526;"><span style="font-size: 15px ;color: black;"><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</span></td>
                <td style="background-color: #9dd4f5;"><span style="font-size: 15px; color: black;"><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</span></td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee']); ?>&nbsp;</td>
                <td><?php //echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                <td><?php //echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_real']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['tare_download']); ?>&nbsp;</td>
                <td><?php //echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>

                <td class="actions">
			<?php echo $this->Html->link(__('Procesar'), array('action' => 'register_download',$remittancesCaffee['RemittancesCaffee']['id'])); ?>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
    <script type="text/javascript">
    
    function endWeight(e){
        var target = $( event.target );
        if ( target.is( "a.end" ) ) {
          var remesaId = target.attr('id');
          completeWeightingCoffeeSISCAFE(remesaId);
        }
    }
    
    function exeTxMIGOFNC(remesaId){
        $.ajax({
            url:"http://siscafe.copcsa.com:8181/SISCAFE-IntegrationWS/WS/integration/exportingDataCoffeeCOPCToFNC?idCoffee="+remesaId,
            crossDomain: true,
            error: function(xhr, status, error){
                console.log(xhr.responseText);
                window.location.href = "http://siscafe.copcsa.com/remittances_caffees/download_caffee";
                alert("Se realizó transacción MIGO FNC al descargue con ID "+remesaId);
            },
            success: function (data) {
                console.log(data);
            }
        });
    }
    
    function completeWeightingCoffeeSISCAFE(remesaId){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/completeDownloadCoffee/" + remesaId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    var result = JSON.parse(data);
                    console.log(result);
                    exeTxMIGOFNC(remesaId);
                }
                else{
                    alert("No se puede finalizar porque hay sacos por pesar de la remesa "+remesaId);
                }
            }
        });
    }
    
    </script>
</div>


<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'basculero')); ?></li>
        <li><?php //echo $this->Html->link(__('Buscar Remesa'), array('controller'=>'pages','action' => 'motorista')); ?></li>
    </ul>
</div>
