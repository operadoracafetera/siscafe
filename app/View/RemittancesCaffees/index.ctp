<div class="remittancesCaffees index">
<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>

    <h2><?php echo (!isset($idColaborate)) ? __('Listado de remesas radicadas'):__('Mis remesas radicadas'); ?></h2>
    <?php echo $this->Html->link(__('Registrar descargue de cafe'), array('action' => 'add'));?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','C.O-Remesa ID'); ?></th>
		<th><?php echo $this->Paginator->sort('cargolot_id','CargoLot ID'); ?></th>
		<th><?php echo $this->Paginator->sort('created_date','Fecha Rad'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Numero Lote'); ?></th>
                <th><?php 
                if($this->Session->read('User.centerId') == 1 || $this->Session->read('User.centerId') == 10)
                    echo $this->Paginator->sort('slotStore','Ubica'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th><?php 
                if($this->Session->read('User.centerId') == 1 || $this->Session->read('User.centerId') == 10)
                echo $this->Paginator->sort('state_operation_id','Estado Operativo'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Final Pesaje (FNC)'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Cierre descargue (SPB)'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
		<td><?php echo h($remittancesCaffee['RemittancesCaffee']['cargolot_id']); ?>&nbsp;</td>
				<td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php echo h("3-".$remittancesCaffee['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php 
                if($this->Session->read('User.opera') == 1)
                    echo h($remittancesCaffee['SlotStore']['name_space']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td><?php 
                if($this->Session->read('User.opera') == 1)
                echo h($remittancesCaffee['StateOperation']['name']); ?>&nbsp;</td>
                <td><?php echo (!isset($remittancesCaffee['RemittancesCaffee']['sync_date_sapmigo_fnc'])) ? "Sin finalizar el pasaje":"<span>".$remittancesCaffee['RemittancesCaffee']['sync_date_sapmigo_fnc']."</span>"; ?>&nbsp;</td>
                <td><?php echo (!isset($remittancesCaffee['RemittancesCaffee']['download_caffee_date'])) ? "Sin cerrar el descargue":$remittancesCaffee['RemittancesCaffee']['download_caffee_date']; ?>&nbsp;</td>
                <td class="actions">
                    <div class="dropdown">
                    <button class="dropbtn"><strong>Menú</strong></button>
                    <div class="dropdown-content">
			<?php 
                        if($remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 1 || $remittancesCaffee['RemittancesCaffee']['jetty'] != 1){
                            echo $this->Html->link(__('Editar'), array('action' => 'edit', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => "_blank"));
                        }
                        else if($remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 1 && $remittancesCaffee['RemittancesCaffee']['slot_store_id'] == null){
                            echo $this->Html->link(__('Almacenar'), array('action' => 'store', $remittancesCaffee['RemittancesCaffee']['id']));
                        }
                        if($remittancesCaffee['RemittancesCaffee']['slot_store_id'] != null && $remittancesCaffee['RemittancesCaffee']['state_operation_id'] != 4){
                          if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id')==1 || $this->Session->read('User.profiles_id')==5)
                            if(Configure::read('siscafe.mode_app') == '1')
                              echo $this->Html->link(__('Papeleta'), Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=1&toPdf=true&idPrinter=1");
                            else
                              echo $this->Html->link(__('Papeleta'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=1&toPdf=true&idPrinter=1");
                          if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id')==1){
                            echo $this->Html->link(__('Stiker'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=4&toPdf=true&idPrinter=1"); 
                          }
                          if($remittancesCaffee['RemittancesCaffee']['sync_date_sapmigo_fnc'] != null && $remittancesCaffee['RemittancesCaffee']['download_caffee_date'] == null){
                            echo $this->Html->link(__('Finalizar Descargue'), 'javascript:void(0)',array('class'=>'end','id'=>$remittancesCaffee['RemittancesCaffee']['id'],'onclick'=> 'endWeight();')); 
                          }
						  if($remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 12 || $remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 2){
							if($remittancesCaffee['RemittancesCaffee']['jetty'] == 1){
								echo $this->Html->link(__('Repesar'), array('action' => 'reopening', $remittancesCaffee['RemittancesCaffee']['id']));
							}
						  }
                          if($this->Session->read('User.profiles_id')==1 && $remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 1 || $remittancesCaffee['RemittancesCaffee']['state_operation_id'] == 8)
                          if($this->Session->read('User.opera') != 1){
                              echo $this->Html->link(__('Ver'), array('action' => 'weights', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => "_blank"));
                              echo $this->Html->link(__('Editar'), array('action' => 'edit', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => "_blank"));
                          }
                          if($this->Session->read('User.profiles_id')==1){
                            if($remittancesCaffee['RemittancesCaffee']['novelty_in_caffee_id'] == null){
                                echo $this->Html->link(__('Novedad'), array('controller' => 'NoveltyInCaffees','action' => 'add', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => "_blank"));
                            }
                            else{
                                if(Configure::read('siscafe.mode_app') == 1)
                                    $url = Configure::read('siscafe.url_dev')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=5&toPdf=true&idPrinter=1";
                                else{
                                    $url = Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=5&toPdf=true&idPrinter=1";
                                    echo $this->Html->link(__('Ver Novedad'),$url);
                                    echo $this->Html->link(__('F.N'), array('action' => 'zip', $remittancesCaffee['RemittancesCaffee']['id']));
                                }
                            }
                          }
                        }
                        echo $this->Html->link(__('Ver Pesos'), array('action' => 'weights', $remittancesCaffee['RemittancesCaffee']['id']),array('target' => "_blank"));
                        ?>
                    </div>
                    </div>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
        
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
    <script type="text/javascript">

        function endWeight(e){
            var target = $( event.target );
            if ( target.is( "a.end" ) ) {
              var remesaId = target.attr('id');
              completeWeightingCoffeeSISCAFE(remesaId);
            }
        }

        function weightCargoLotIdToN4(caragolot, weight, remesaId){
            $.ajax({
                url: "http://siscafe.copcsa.com:8181/SISCAFE-IntegrationWS/WS/integration/getWeightByCargoLotId?cargolotid="+remesaId+"&weight="+weight,
                crossDomain: true,
                error: function(xhr, status, error){
                    console.log(xhr.responseText);
                    window.location.href = "http://siscafe.copcsa.com/RemittancesCaffees";
                    alert("Se finalizó el proceso de descargue de la remesa "+remesaId);
                },
                success: function (data) {
                    console.log(data);
                }
            });
        }

        function completeWeightingCoffeeSISCAFE(remesaId){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/completeDownloadCoffeeSPB/" + remesaId,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        var result = JSON.parse(data);
                        var cargolot = result.cargolot;
                        var weight = result.weight;
                        validateCrossdocking(remesaId);
                        weightCargoLotIdToN4(cargolot,weight,remesaId);
                    }
                    else{
                        alert("No se puede finalizar porque hay sacos por pesar de la remesa "+remesaId);
                    }
                }
            });
        }

        function validateCrossdocking(remesaId){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/validateCrossdockingByRemittance/" + remesaId,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data !== "") {
                        console.log(data);
                    }
                }
            });
        }


    </script>
</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php 
        if($this->Session->read('User.profiles_id')==1){
            //echo $this->Html->link(__('Lotes maestro descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));
            //echo $this->Html->link(__('Mis remesas'), array('action' => 'index',$this->Session->read('User.id')));
	    echo $this->Html->link(__('Cargue Masivo'), array('controller'=>'BulkUploadRemmittancesCoffees','action' => 'index'));
            echo $this->Html->link(__('Buscar'), array('action' => 'search'));
	    echo $this->Html->link(__('Listado maestro remesas'), array('action' => 'index'));
        }
        ?></li>
    </ul>
</div>
