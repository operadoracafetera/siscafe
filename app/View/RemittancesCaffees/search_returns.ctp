<div class="returnsCaffees index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('ReturnsCaffee');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda de Devoluciones'); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('codigo',array('label' => 'Código'));?></td>
                <td><?php echo $this->Form->input('remesa',array('label' => 'Remesa'));?></td>
                <td><?php echo $this->Form->input('lote',array('label' => 'Exportador - Lote'));?></td>
                <td><?php echo $this->Form->input('vehiculo',array('label' => 'Vehiculo'));?></td>
            </tr>  
        </table>
        <?php echo $this->Form->end(__('Buscar')); ?>
                    <br>

    </fieldset>
    <table>
        <tr>
            <td>
                <table id="tbDataOie">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('Código'); ?></th>
                            <th><?php echo $this->Paginator->sort('Fecha devolución'); ?></th>
                            <th><?php echo $this->Paginator->sort('Observación'); ?></th>
                            <th><?php echo $this->Paginator->sort('Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('Placa del vehiculo'); ?></th>
                            <th><?php echo $this->Paginator->sort('Responsable'); ?></th>
                            <th><?php echo $this->Paginator->sort('Orden del Servicio'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($searchInfoList){
                        //debug($searchInfoList);
                        foreach ($searchInfoList as $returnsCaffee): ?>
                            <tr>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['id']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['return_date']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['observation']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['state_return']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['vehicle_plate']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['user_register']); ?>&nbsp;</td>
                                <td><?php echo h($returnsCaffee['ReturnsCaffee']['order_service']); ?>&nbsp;</td>
                                <td class="actions">
					<?php echo $this->Html->link(__('Ver'), array('action' => 'return_lot', $returnsCaffee['ReturnsCaffee']['id']), array('target' => "_blank")); ?>
                        		<?php 
                        		if($returnsCaffee['ReturnsCaffee']['return_type'] != 'FIQUE'){
                        		    echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=14&returnId=".$returnsCaffee['ReturnsCaffee']['id']);
                        		}
                        		else{
                            			echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=23&returnId=".$returnsCaffee['ReturnsCaffee']['id']);
                        		}
                        		?>
                                </td>
                            </tr>
                    <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            <td>
 
            </td>
        </tr>
    </table>
    
</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
