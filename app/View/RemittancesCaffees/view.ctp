<div class="remittancesCaffees view">
<?php 
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('WeighingDownloadCaffee', array('url' => 'savePallet')); ?>
    <fieldset>
        <legend><?php 
        if($this->Session->read('User.centerId') == 1)
            echo __('Paletizar Remesa Descargue'); 
        else
            echo __('Información Remesa (Número de servicio) - Descargue'); 
        ?></legend>
	<?php
		echo $this->Form->input('remittances_caffee',array('value'=>$this->request->data,'type' => 'hidden'));
		echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
		echo $this->Form->input('remesa',array('label' => 'CO - Remesa','value'=>$this->Session->read('User.centerId')."-".$this->request->data, 'disabled' => 'disabled'));

		echo $this->Form->input('desc_material',array('label' => 'Descripción Material','disabled' => 'disabled','value'=>$remittancesCaffee['MaterialCoffee']['id'].'-'.$remittancesCaffee['MaterialCoffee']['name'])); 
        echo $this->Form->input('nota_entrega',array('label' => 'Nota entrega', 'disabled' => 'disabled','value' => $remittancesCaffee['RemittancesCaffee']['nota_entrega']));
                
                if($this->Session->read('User.opera') != 1){
                   $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
                   echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
                   echo $this->Form->input('sacos',array('label' => 'Cant. Sacos','value'=>$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'], 'disabled' => 'disabled'));
                   echo $this->Form->input('driver',array( 'label' => 'Motorista', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['Users']['first_name']." ".  $remittancesCaffee['Users']['last_name']) );
                   echo $this->Form->input('fecha_descargue',array( 'label' => 'Fecha descargue ', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['download_caffee_date']) );
                   echo $this->Form->input('details_weight',array( 'label' => 'Detalle de peso', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['details_weight']) );
                   echo $this->Form->input('observation',array( 'label' => 'Observación', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['observation']) );
                   echo $this->Form->input('empaque',array( 'label' => 'Empaque Café ingreso', 'disabled' => 'disabled', 'value' => $remittancesCaffee['PackingCaffee']['name']." - (".$remittancesCaffee['PackingCaffee']['weight'].") Kg x Saco") );
                   echo $this->Form->input('type_unit',array( 'label' => 'Tipo de unidad', 'disabled' => 'disabled', 'value' => $remittancesCaffee['TypeUnit']['name']) );
                   echo $this->Form->input('unit_caffe',array( 'label' => 'Unidad Cafe', 'disabled' => 'disabled', 'value' => $remittancesCaffee['UnitsCaffee']['name']) );
                   
                }
                if($this->Session->read('User.opera') == 1){
                    //echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."","MANUAL"=>"MANUAL")));
					echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => $bascules));
                    echo $this->Form->input('weight',array( 'label' => 'Peso','type' => 'number', 'disabled' => 'disabled','required' => true) );
                    echo $this->Form->input('weight_pallet',array( 'type' => 'hidden') );
                    echo $this->Form->input('quantity_bag_pallet',array('type' => 'hidden') );
                    echo $this->Form->input('bags',array( 'label' => 'Cantidad de sacos','type' => 'number','disabled' => 'disabled', 'max' => 700 ,'min' => 1 ,'required' => true) );
                }
                //echo $this->Form->input('weighing_date',array( 'label' => 'Fecha','type' => 'datetime' ) );
	?> 
        <?php
            if($this->Session->read('User.opera') == 1):?>
                <div class="row actions">
                    <?php echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addBag();' ) );?>
                    <?php echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removeBag();') );?>
                    <?php if($this->Session->read('User.opera') == 1) echo $this->Form->end(__('Guardar Peso')); ?>

                </div>
                <?php echo $this->Html->link(__('Opciones'),array('controller'=>'RemittancesCaffees','action' => 'options',$this->request->data)) ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                        <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                        <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                        <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                        <th class="actions"><?php echo __('Acciones'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $quantityPallet = 0;
                    $quantityBags = 0;
                    foreach ($weighingDownloadCaffees as $weighingDownloadCaffee): 
                            $quantityPallet = $quantityPallet +1;?>
                    <tr>
                        <td><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['seq_weight_pallet']); ?>&nbsp;</td>
                        <td><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['weight_pallet']); ?>&nbsp;</td>
                        <td><?php 
                            $quantityBags = $quantityBags + $weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet'];
                            echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                        <td ><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['weighing_date']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('Repesar'),'javascript:void(0)',array('id'=>'reweigh','value'=>$weighingDownloadCaffee['WeighingDownloadCaffee']['id']."-".$weighingDownloadCaffee['WeighingDownloadCaffee']['remittances_caffee_id'])); ?>&nbsp;
                            <?php //echo $this->Html->link(__('Fraccionar'), array('action' => 'split',$weighingDownloadCaffee['WeighingDownloadCaffee']['id']), array('confirm' => __('¿Esta seguro de fraccionar este pallet?'))); ?>&nbsp;
                        </td>
                    </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
	<br>
		<div id="divTableNoveltys">
        <h2><?php echo __('Listado de bloqueos a la remesa: (Novedades al Cafe)'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('noveltys_caffee_id','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
			<th><?php echo $this->Paginator->sort('active','Estado'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($remittancesCaffeeHasNoveltysCaffee as $coffeeHasNovelty): ?>
	<tr>
		<td><?php 
                //debug($adictionalElement);exit;
                echo h($coffeeHasNovelty["NoveltysCoffee"]['name']); ?>&nbsp;</td>
		<td><?php echo h($coffeeHasNovelty["RemittancesCaffeeHasNoveltysCaffee"]['created_date']); ?>&nbsp;</td>
		<td><?php echo ($coffeeHasNovelty['RemittancesCaffeeHasNoveltysCaffee']['active'] == 1) ? "ACTIVO":"DESACTIVADO"; ?>&nbsp;</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
        </div>
        
	<?php
            echo $this->Form->input('type_unit',array( 'label' => 'Tipo de unidad', 'disabled' => 'disabled', 'value' => $remittancesCaffee['TypeUnit']['name']) );
            echo $this->Form->input('unit_caffe',array( 'label' => 'Unidad Cafe', 'disabled' => 'disabled', 'value' => $remittancesCaffee['UnitsCaffee']['name']) );
            echo $this->Form->input('quantity_pallet',array('label' => 'Pallets ingresados', 'disabled' => 'disabled', 'value' => $quantityPallet));
            echo $this->Form->input('quantity_bags',array( 'label' => 'Sacos ingresados','type' => 'number', 'disabled' => 'disabled', 'value' => $quantityBags ) );
            echo $this->Form->input('total_bags',array('type' => 'hidden','value' => $quantityBags ) );
            echo $this->Form->input('tare_download',array( 'label' => 'Tara ingreso','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['tare_download']) );
            echo $this->Form->input('total_weight_net_real',array( 'label' => 'Peso Ingresado','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['total_weight_net_real']) );
            $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
            echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
            
            echo $this->Form->input('seals',array( 'label' => 'Sellos vehiculo', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['seals']) );
            echo $this->Form->input('bags_in',array( 'label' => 'Sacos Rad In', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']) );
            echo $this->Form->input('cargolotid',array( 'label' => 'Cargo Lot Id', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['cargolot_id']) );
	    echo $this->Form->input('details_weight',array( 'label' => 'Detalles del Peso' , 'value' => $remittancesCaffee['RemittancesCaffee']['details_weight']) );
            ?>
	<?php endif;?>

        <script type="text/javascript">
            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            var weigthPallet = 0;

            function addBag() {
                var pallet = parseInt($("#WeighingDownloadCaffeeQuantityBagPallet").val());
                $("#WeighingDownloadCaffeeBags").val(pallet + 1);
                $("#WeighingDownloadCaffeeQuantityBagPallet").val(pallet + 1);
            }

            function removeBag() {
                var pallet = parseInt($("#WeighingDownloadCaffeeQuantityBagPallet").val());
                $("#WeighingDownloadCaffeeBags").val(pallet - 1);
                $("#WeighingDownloadCaffeeQuantityBagPallet").val(pallet - 1);
            }
            
            $("#WeighingDownloadCaffeeMaterial").click(function () {
                var idMaterial = $(this).val();
                findMaterialCode(idMaterial);
            });
            
            function findMaterialCode(idMaterial){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "../../MaterialCoffees/getMaterialCode/"+idMaterial,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         console.log(json);
                         if(JSON.stringify(json) !== "[]"){
                             $("#WeighingDownloadCaffeeDescMaterial").val(json['MaterialCoffee']['cod_material']+"-"+json['MaterialCoffee']['description']);
                         }
                         else{
                             $("#WeighingDownloadCaffeeDescMaterial").val("NO ARROJO RESULTADOS");
                         }
                  }});
            }
            
            function updateWeighPallet(idPallet){
                var reweigh = $("#WeighingDownloadCaffeeWeight").val();
                var data = idPallet.split("-");
                if(reweigh !== 0){
                    var rebags = $("#WeighingDownloadCaffeeQuantityBagPallet").val();
                    var idBagsWeigh = (data[0]+"-"+rebags+"-"+reweigh);
                    $.ajax({
                        type: "GET",
                        datatype: "json",
                        url: "/RemittancesCaffees/reweighPallet/" + idBagsWeigh,
                        error: function (msg) {
                            alert("Error networking");
                        },
                        success: function (response) {
                            if (data !== "") {
                                var result = JSON.parse(response);
                                if(result === 1){
                                    alert("Se actualizo el peso del pallet correctamente!");
                                    window.location.href = "/remittances_caffees/view/"+data[1];
                                }
                                else{
                                    alert("No se actualizo el peso");
                                }
                            }
                        }
                    });
                }
                else{
                    alert("Peso en 0. No se puede actualizar el peso");
                }
            }
            
            $("a#reweigh").click(function(e){
                var idPallet = $(this).attr('value');    
                updateWeighPallet(idPallet);
            });
            
            $("#WeighingDownloadCaffeeBascule").change(function () {
                if($(this).val() !== "MANUAL"){
                    $("#WeighingDownloadCaffeeWeight").attr("disabled", true);
                    $("#WeighingDownloadCaffeeBags").attr("disabled", true);
                    setInterval(function () {
                        $.ajax({
                            url: $("#WeighingDownloadCaffeeBascule").val(),
                            crossDomain: true,
                            success: function (response) {
                                $("#WeighingDownloadCaffeeWeight").val(parseInt(response));
                                $("#WeighingDownloadCaffeeWeightPallet").val(parseInt(response));
                                setValueQuantityBags(parseInt(response));
                            }
                        });
                    }, 500); //medio segundo
                }
                else{
                    $("#WeighingDownloadCaffeeWeight").removeAttr('disabled');
                    $("#WeighingDownloadCaffeeBags").removeAttr('disabled');
                }
            });
            
            $("#WeighingDownloadCaffeeWeight").keyup(function () {
                var pesoManual = $(this).val();
                $("#WeighingDownloadCaffeeWeightPallet").val(pesoManual);
                $("#WeighingDownloadCaffeeWeight").val(pesoManual)
            });
            
            $("#WeighingDownloadCaffeeBags").keyup(function () {
                var unidades = $(this).val();
                $("#WeighingDownloadCaffeeQuantityBagPallet").val(unidades);
            });
                        
            function setValueQuantityBags(weigthBascule){
                if(weigthBascule !== weigthPallet){
                    weigthPallet = weigthBascule;
                    $("#WeighingDownloadCaffeeQuantityBagPallet").val(Math.floor((weigthBascule-70)/bagWeigth));
                    $("#WeighingDownloadCaffeeBags").val(Math.floor((weigthBascule-70)/bagWeigth));  
                }     
            }

        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php
        if($this->Session->read('User.opera') != 1){
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'index')); 
            echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));
        }
        else
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'download_caffee')); 
        ?></li>
    </ul>
</div>