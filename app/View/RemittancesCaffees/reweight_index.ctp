<div class="remittancesCaffees index">
    <h2><?php echo __('Listado de Remesas a Repesar'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','CO-Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('stores_caffee_id','Pos Bod'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_in_pallet_caffee','Pallet In'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_real','Peso Real'); ?></th>
                <th><?php echo $this->Paginator->sort('tare_download','Tara Descargue'); ?></th>
                <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha Radicado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //debug($remittancesCaffee);exit;
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h('3-'.$remittancesCaffee['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['SlotStore']['name_space']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_real']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['tare_download']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>

                <td class="actions">
			<?php echo $this->Html->link(__('Paletizar'), array('action' => 'return_pallet', $remittancesCaffee['RemittancesCaffee']['id'])); ?>
			<?php echo $this->Html->link(__('Tara'), array('action' => 'tare', $remittancesCaffee['RemittancesCaffee']['id'])); ?>
			<?php echo $this->Html->link(__('Finalizar'), array('action' => 'return_index', $remittancesCaffee['RemittancesCaffee']['id']),array('confirm'=>('Desea finalizar proceso de descargue de la remesa '.$remittancesCaffee['RemittancesCaffee']['id'].'?'))); ?>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'basculero')); ?></li>
        <li><?php //echo $this->Html->link(__('Buscar Remesa'), array('controller'=>'pages','action' => 'motorista')); ?></li>
    </ul>
</div>
