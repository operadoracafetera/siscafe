<div class="remittancesCaffees view">
<?php 
echo $this->Html->script('jquery.min');
echo $this->Form->create('RemittancesCaffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Radicar remesa café'); ?></legend>
	
                <div class="row action">
                <?php 
                    echo $this->Form->input('codigo_exp',array('label' => 'Exportador codigo FNC','disabled' => 'enable','required' => true));
                    //echo $this->Form->input('id_cliente',array('label' => 'Id Cliente','disabled' => 'disabled'));
                    echo $this->Form->input('client_id',array('label' => 'Id Cliente','type' => 'hidden'));
                    echo $this->Form->input('client',array('label' => 'Cliente','disabled' => 'disabled'));
                 ?>
                </div>
        <?php
		echo $this->Form->input('lot_caffee',array('label' => 'Lote café','disabled' => 'enable','maxlength'=>'4','required' => true));
		echo $this->Form->input('quantity_radicated_bag_in',array('label' => 'Sacos radicados In ','disabled' => 'enable','required' => true));
		echo $this->Form->input('vehicle_plate',array('label' => 'Placa vehiculo','disabled' => 'enable','maxlength'=>'6','required' => true));
		echo $this->Form->input('guide',array('label' => 'Guia transito','disabled' => 'enable'));
                echo $this->Form->label('guide_document','Copia Guia transito');
                echo $this->Form->file('guide_document');
                if($userRegister['User']['departaments_id'] == 1) echo $this->Form->input('auto_otm',array('label' => 'Matricula','disabled' => 'enable','required' => true));
                echo $this->Form->input('nota_entrega');
                echo $this->Form->input('material');
                echo $this->Form->input('guide_id',array('label' => 'Guia','type' => 'hidden'));
                echo $this->Form->input('total_weight_net_nominal',array('label' => 'Estado','type' => 'hidden'));
                echo $this->Form->input('state_operation_id',array('label' => 'Estado','type' => 'hidden'));
                echo $this->Form->input('created_date',array('label' => 'Id Cliente','type' => 'hidden'));
                echo $this->Form->input('packing_cafee_id',array('type' => 'select','label' => 'Empaque Café - Gramaje','options' => $packingCaffee,'required' => true,'multiple' => false));
                echo $this->Form->input('type_units_id',array('label' => 'Tipo de unidad','disabled' => 'enable','options' => $typesUnits,'required' => true,'multiple' => false));
		echo $this->Form->input('units_cafee_id',array('type' => 'select','label' => 'Peso Unidad','options' => $units,'multiple' => false,'required' => true));
		echo $this->Form->input('mark_cafee_id',array('type' => 'select','label' => 'Marca Café','options' => $marks_caffee,'multiple' => false,'required' => true));
		echo $this->Form->input('city_source_id',array('type' => 'select','label' => 'Procedencia','options' => $citys,'multiple' => false,'required' => true));
		echo $this->Form->input('shippers_id',array('type' => 'select','label' => 'Empresa Transporte','options' => $shippers,'multiple' => false,'required' => true));
		echo $this->Form->input('slot_store_id',array('type' => 'select','label' => 'Ubicación Libre','options' => $slotsEmpty,'multiple' => false,'required' => true));
		echo $this->Form->input('staff_sample_id',array('type' => 'select','label' => 'Muestreador','options' => $usersSamplers,'multiple' => false,'required' => true));
		echo $this->Form->input('staff_driver_id',array('type' => 'select','label' => 'Motorista','options' => $usersDriver,'multiple' => false,'required' => true));
                echo $this->Form->input('custom_id',array('type' => 'select','label' => 'Agencia Aduanas','options' => $customns,'multiple' => false,'required' => true));
		echo $this->Form->input('observation',array('label' => 'Observación'));
                if($userRegister['User']['departaments_id'] != 1){
                    echo $this->Form->input('quantity_bag_in_store',array('label'=>'Cant. Sacos (Ingreso)','required' => true));
                    echo $this->Form->input('quantity_in_pallet_caffee',array('label'=>'Cant. Pallets (Ingreso)','required' => true));
                    echo $this->Form->input('total_weight_net_real',array('value' => 0, 'type' => 'hidden'));
                    echo $this->Form->input('tare_download',array('value' => 0, 'type' => 'hidden'));
                }
		echo $this->Form->input('details_weight',array('label' => 'Detalles de pesos'));
                if($userRegister['User']['departaments_id'] == 1) echo $this->Form->input('seals',array('label' => 'Sellos Vehiculo'));
		
	?>
        
        </fieldset>
    <script type="text/javascript">
        $("#RemittancesCaffeeCodigoExp").keyup(function () {
                var codExpFNC = $(this).val();
                if(codExpFNC === '001'){
                    $("#RemittancesCaffeeNotaEntrega").attr('type', 'text');
                    $("#RemittancesCaffeeMaterial").attr('type', 'hidden');
                    console.log("22211");
                    console.log(codExpFNC);
                }
                else{
                    $("#RemittancesCaffeeNotaEntrega").attr('type', 'hidden');
                    $("#RemittancesCaffeeMaterial").attr('type', 'text');
                    console.log("5623234");
                    console.log(codExpFNC);
                }
                findExportador(codExpFNC);
            });
        
        function findExportador(codExpo){
            $.ajax({
               type: "GET",
               datatype: "json",
               url: "/Clients/findByExpo/"+codExpo,
               error: function(msg){alert("Error networking");},
               success: function(data){
                    var json = JSON.parse(data);
                    if(JSON.stringify(json) !== "[]"){
                        $("#RemittancesCaffeeIdCliente").val(json.Client.id);
                        $("#RemittancesCaffeeClientId").val(json.Client.id);
                        $("#RemittancesCaffeeClient").val(json.Client.exporter_code+"-"+json.Client.business_name);
                    }
                    else{
                        $("#RemittancesCaffeeClient").val("NO ARROJO RESULTADOS");
                    }
             }});
        }
        
        function clearField(){
            $("#RemittancesCaffeeCodigo").val("");
            $("#RemittancesCaffeeIdCliente").val("");
            $("#RemittancesCaffeeClient").val("");
        }
    </script>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado remesas'), array('action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1)); ?></li>
                <li><?php echo $this->Html->link(__('Mis descargues'), array('action' => 'index',$this->Session->read('User.id'))); ?></li>
	</ul>
</div>
