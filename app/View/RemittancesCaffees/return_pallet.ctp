<div class="remittancesCaffees view">
<?php 
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('WeighingReturnCoffee', array('url' => 'returnPallet')); ?>
    <fieldset>
        <legend><?php 
        if($this->Session->read('User.centerId') == 1)
            echo __('Paletizar Remesa Devolución'); 
        else
            echo __('Información Remesa (Número de servicio) - Descargue'); 
        ?></legend>
	<?php
                //debug($remittancesCaffeeReturnsCaffee);exit;
		echo $this->Form->input('remittances_caffee',array('value'=>$this->request->data,'type' => 'hidden'));
		echo $this->Form->input('return_type',array('label' => 'Material a devolver','value'=>$returnCaffee['ReturnsCaffee']['return_type'],'disabled' => 'disabled'));
		echo $this->Form->input('remesa',array('label' => 'CO - Remesa','value'=>$this->Session->read('User.centerId')."-".$this->request->data, 'disabled' => 'disabled'));
		echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
                /*if($this->Session->read('User.centerId') != 1){
                   $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
                   echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
                   echo $this->Form->input('sacos',array('label' => 'Cant. Sacos','value'=>$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'], 'disabled' => 'disabled'));
                   echo $this->Form->input('driver',array( 'label' => 'Motorista', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['Users']['first_name']." ".  $remittancesCaffee['Users']['last_name']) );
                   echo $this->Form->input('fecha_descargue',array( 'label' => 'Fecha descargue ', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['download_caffee_date']) );
                   echo $this->Form->input('details_weight',array( 'label' => 'Detalle de peso', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['details_weight']) );
                   echo $this->Form->input('observation',array( 'label' => 'Observación', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['RemittancesCaffee']['observation']) );
                   echo $this->Form->input('empaque',array( 'label' => 'Empaque Café ingreso', 'disabled' => 'disabled', 'value' => $remittancesCaffee['PackingCaffee']['name']." - (".$remittancesCaffee['PackingCaffee']['weight'].") Kg x Saco") );
                   echo $this->Form->input('type_unit',array( 'label' => 'Tipo de unidad', 'disabled' => 'disabled', 'value' => $remittancesCaffee['TypeUnit']['name']) );
                   echo $this->Form->input('unit_caffe',array( 'label' => 'Unidad Cafe', 'disabled' => 'disabled', 'value' => $remittancesCaffee['UnitsCaffee']['name']) );

                }*/
                //if($this->Session->read('User.centerId') == 1){
                    echo $this->Form->input('act_posicion',array( 'label' => 'Act ubicación', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['SlotStore']['name_space']) );
                    echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."","MANUAL"=>"MANUAL")));
                    echo $this->Form->input('weight',array( 'label' => 'Peso','type' => 'number', 'required' => true, 'disabled' => 'disabled') );
                    echo $this->Form->input('weight_pallet',array( 'type' => 'hidden') );
                    echo $this->Form->input('qta_bag_return',array( 'type' => 'hidden', 'value'=>$remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['qta_bags']) );
                    echo $this->Form->input('return_id',array( 'type' => 'hidden', 'value'=>$remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id']) );
                    echo $this->Form->input('quantity_bag_pallet',array('type' => 'hidden','value'=>25) );
                    echo $this->Form->input('bags',array( 'label' => 'Cantidad de sacos','type' => 'number','min' => 1 ,'required' => true) );
                //}
                //echo $this->Form->input('weighing_date',array( 'label' => 'Fecha','type' => 'datetime' ) );
	?> 
        <?php
            //if($this->Session->read('User.centerId') == 1):?>
                <div class="row actions">
                    <?php echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addBag();' ) );?>
                    <?php echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removeBag();') );?>
                </div>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                        <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                        <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                        <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $quantityPallet = 0;
                    $quantityBags = 0;
                    foreach ($weighingReturnCoffees as $weighingReturnCoffee): 
                            $quantityPallet = $quantityPallet +1;
                    ?>
                    <tr>
                        <td><?php echo h($weighingReturnCoffee['WeighingReturnCoffee']['seq_weight_pallet']); ?>&nbsp;</td>
                        <td><?php echo h($weighingReturnCoffee['WeighingReturnCoffee']['weight_pallet']); ?>&nbsp;</td>
                        <td><?php 
                            $quantityBags = $quantityBags + $weighingReturnCoffee['WeighingReturnCoffee']['quantity_bag_pallet'];
                            echo h($weighingReturnCoffee['WeighingReturnCoffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                        <td><?php echo h($weighingReturnCoffee['WeighingReturnCoffee']['weighing_date']); ?>&nbsp;</td>
                    </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        
	<?php
            echo $this->Form->input('quantity_pallet',array('label' => 'Pallets devueltos', 'disabled'=>'disabled', 'value' => $quantityPallet));
            echo $this->Form->input('quantity_bags',array( 'label' => 'Sacos devueltos','type' => 'number', 'disabled'=>'disabled', 'value' => $quantityBags ) );
            echo $this->Form->input('total_bags',array('type' => 'hidden','value' => $quantityBags ) );
            echo $this->Form->input('tare_download',array( 'label' => 'Tara ingreso','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['tare_download']) );
            $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
            echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );?>
	<?php //endif;?>
        <?php //if($this->Session->read('User.centerId') == 1) echo $this->Form->end(__('Submit')); 
				 echo $this->Form->end(__('Guardar'));?>
        <script type="text/javascript">
            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            var typeReturn = <?php echo "'".$returnCaffee['ReturnsCaffee']['return_type']."'";?>;
            var weigthPallet = 0;

            function addBag() {
                var pallet = parseInt($("#WeighingReturnCoffeeQuantityBagPallet").val());
                $("#WeighingReturnCoffeeBags").val(pallet + 1);
                $("#WeighingReturnCoffeeQuantityBagPallet").val(pallet + 1);
            }

            function removeBag() {
                var pallet = parseInt($("#WeighingReturnCoffeeQuantityBagPallet").val());
                $("#WeighingReturnCoffeeBags").val(pallet - 1);
                $("#WeighingReturnCoffeeQuantityBagPallet").val(pallet - 1);
            }
            
            $("#WeighingReturnCoffeeBascule").change(function () {
                if($(this).val() !== "MANUAL"){
                    $("#WeighingReturnCoffeeWeight").attr("disabled", true);
                    //$("#WeighingReturnCoffeeBags").attr("disabled", true);
                    setInterval(function () {
                        $.ajax({
                            url: $("#WeighingReturnCoffeeBascule").val(),
                            crossDomain: true,
                            success: function (response) {
                                $("#WeighingReturnCoffeeWeight").val(parseInt(response));
                                $("#WeighingReturnCoffeeWeightPallet").val(parseInt(response));
                                setValueQuantityBags(parseInt(response));
                            }
                        });
                    }, 500); 
                }//medio segundo
                else{
                    $("#WeighingReturnCoffeeWeight").removeAttr('disabled');
                    $("#WeighingReturnCoffeeBags").removeAttr('disabled');
                }
            });
            
            function setValueQuantityBags(weigthBascule){
                console.log(typeReturn);
                if(weigthBascule !== weigthPallet && typeReturn !== 'FIQUE'){
                    weigthPallet = weigthBascule;
                    $("#WeighingReturnCoffeeQuantityBagPallet").val(Math.floor((weigthBascule-70)/bagWeigth));
                    $("#WeighingReturnCoffeeBags").val(Math.floor((weigthBascule-70)/bagWeigth));  
                }
                else{
                    $("#WeighingReturnCoffeeQuantityBagPallet").val($("#WeighingReturnCoffeeBags").val());
                }
            }

        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php
        /*if($this->Session->read('User.centerId') != 1){
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'index')); 
            echo $this->Html->link(__('Lotes descargados'), array('controller'=>'ViewLotCoffees','action' => 'index',1));
        }
        else*/
            echo $this->Html->link(__('Listado Remesas'), array('action' => 'return_index')); 
        ?></li>
    </ul>
</div>