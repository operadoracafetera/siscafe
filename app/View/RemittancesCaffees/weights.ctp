<div class="remittancesCaffees view">
<?php 
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('WeighingDownloadCaffee', array('url' => 'savePallet')); ?>
    <fieldset>
        <legend><?php echo __('Vista Pesajes Descargue'); ?></legend>
	<?php
		echo $this->Form->input('remittances_caffee',array('value'=>$this->request->data,'type' => 'hidden'));
		echo $this->Form->input('remesa',array('value'=>$this->request->data, 'disabled' => 'disabled'));
		echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
                echo $this->Form->input('act_posicion',array( 'label' => 'Ubicación-Bodega', 'disabled' => 'disabled' , 'value' => $remittancesCaffee['SlotStore']['name_space']) );
                echo $this->Form->input('state_operation',array( 'label' => 'Estado operación','type' => 'text', 'disabled' => 'disabled', 'value' => $remittancesCaffee['StateOperation']['name']) );
	?>   
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                    <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                    <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                </tr>
            </thead>
            <tbody>
		<?php 
		$quantityPallet = 0;
		$quantityBags = 0;
		foreach ($weighingDownloadCaffees as $weighingDownloadCaffee): 
			$quantityPallet = $quantityPallet +1;?>
                <tr>
                    <td><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['seq_weight_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['weight_pallet']); ?>&nbsp;</td>
                    <td><?php 
			$quantityBags = $quantityBags + $weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet'];
			echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingDownloadCaffee['WeighingDownloadCaffee']['weighing_date']); ?>&nbsp;</td>
                </tr>

		<?php endforeach; ?>
            </tbody>
        </table>

	<?php
            echo $this->Form->input('quantity_pallet',array('label' => 'Pallets ingresados', 'disabled' => 'disabled', 'value' => $quantityPallet));
            echo $this->Form->input('quantity_radicated_bag_in',array( 'label' => 'Sacos Rad In','type' => 'number', 'disabled' => 'disabled', 'value' => $quantityBags ) );
            echo $this->Form->input('total_bags',array('type' => 'hidden','value' => $quantityBags ) );
            echo $this->Form->input('units_cafee_id',array('type' => 'hidden','value' => $bagWeigth['UnitsCaffee']['quantity'] ) );
            echo $this->Form->input('empaque',array( 'label' => 'Empaque Café ingreso', 'disabled' => 'disabled', 'value' => $remittancesCaffee['PackingCaffee']['name']." - (".$remittancesCaffee['PackingCaffee']['weight'].") Kg x Saco") );
            echo $this->Form->input('total_weight_net_real',array('label' => 'Total peso Net (Kg)', 'value' => $remittancesCaffee['RemittancesCaffee']['total_weight_net_real'],'disabled' => 'disabled') );
            echo $this->Form->input('tare_download',array( 'label' => 'Tara ingreso','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['tare_download']) );
            echo $this->Form->input('quantity_bag_in_store',array( 'label' => 'Sacos en Bod','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']) );
            echo $this->Form->input('quantity_bag_out_store',array( 'label' => 'Sacos X Fuera','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['quantity_bag_out_store']) );
            $companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
            echo $this->Form->input('sampler',array('label'=>'Muestreador','disabled' => 'disabled','value' => $remittancesCaffee['Users1']['first_name']." ".$remittancesCaffee['Users1']['last_name'] ) );
            echo $this->Form->input('driver',array('label'=>'Motorista IN','disabled' => 'disabled','value' => $remittancesCaffee['Users2']['first_name']." ".$remittancesCaffee['Users2']['last_name'] ) );
            echo $this->Form->input('wt_in',array('label'=>'Bascula IN','disabled' => 'disabled','value' => $remittancesCaffee['Users3']['first_name']." ".$remittancesCaffee['Users3']['last_name'] ) );
            echo $this->Form->input('observation',array('disabled' => 'disabled','value' => $remittancesCaffee['RemittancesCaffee']['observation'] ) );
            echo $this->Form->input('details_weight',array('disabled' => 'disabled','value' => $remittancesCaffee['RemittancesCaffee']['details_weight'] ) );
	    echo $this->Form->input('cargolot_id',array( 'label' => 'CargoLot Id', 'disabled' => 'disabled' ,'type'=>'text', 'value' => $remittancesCaffee['RemittancesCaffee']['cargolot_id']) );
            echo $this->Form->input('vehicle_plate',array( 'label' => 'Vehiculo', 'disabled' => 'disabled' ,'type'=>'text', 'value' => $remittancesCaffee['RemittancesCaffee']['vehicle_plate']) );
            echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
	?>
        
        <?php //echo $this->Form->end(__('Submit')); ?>
        <script type="text/javascript">
            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            var weigthPallet = 0;
            
            function addBag() {
                var pallet = parseInt($("#WeighingDownloadCaffeeQuantityBagPallet").val());
                $("#WeighingDownloadCaffeeBags").val(pallet + 1);
                $("#WeighingDownloadCaffeeQuantityBagPallet").val(pallet + 1);
            }

            function removeBag() {
                var pallet = parseInt($("#WeighingDownloadCaffeeQuantityBagPallet").val());
                $("#WeighingDownloadCaffeeBags").val(pallet - 1);
                $("#WeighingDownloadCaffeeQuantityBagPallet").val(pallet - 1);
            }
            
            $("#WeighingDownloadCaffeeBascule").change(function () {
                setInterval(function () {
                    $.ajax({
                        url: $("#WeighingDownloadCaffeeBascule").val(),
                        crossDomain: true,
                        success: function (response) {
                            $("#WeighingDownloadCaffeeWeight").val(parseInt(response));
                            $("#WeighingDownloadCaffeeWeightPallet").val(parseInt(response));
                            setValueQuantityBags(parseInt(response));
                        }
                    });
                }, 500); //medio segundo
            });
            
            function setValueQuantityBags(weigthBascule){
                if(weigthBascule !== weigthPallet){
                    weigthPallet = weigthBascule;
                    $("#WeighingDownloadCaffeeQuantityBagPallet").val(Math.floor((weigthBascule-70)/bagWeigth));
                    $("#WeighingDownloadCaffeeBags").val(Math.floor((weigthBascule-70)/bagWeigth));  
                }     
            }

        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo ($this->Session->read('User.profiles_id')==1) ? $this->Html->link(__('Listado Remesas'), array('action' => 'index')):""; ?></li>
        <li><?php echo $this->Html->link(__('Papeleta'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=1&toPdf=true&idPrinter=1");?></li>
        <li><?php echo ($this->Session->read('User.profiles_id')==2) ? $this->Html->link(__('Regresar'), array('controller'=>'RemittancesCaffees','action' => 'search')):""; ?></li>
        <li><?php echo ($this->Session->read('User.profiles_id')==1) ? $this->Html->link(__('Gestionar novedades'), array('controller'=>'RemittancesCaffeeHasNoveltysCaffees','action' => 'add',$this->request->data)):""; ?></li>
    </ul>
</div>