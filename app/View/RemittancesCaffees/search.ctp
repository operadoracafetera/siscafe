<div class="remittancesCaffees form">
<?php echo $this->Html->script('jquery.min');
echo $this->Html->script('jquery-ui.min');
echo $this->Html->css('style');
    echo $this->Form->create('RemittancesCaffee'); ?>
    <fieldset>
        <legend><?php echo __('Buscar información de Café - (Id - Lote - Exportador - CargoLot)'); ?></legend>
        <table>
            <table>
                <tr>
                    <td><?php echo ($this->Session->read('User.profiles_id') == 1 || ($this->Session->read('User.profiles_id') == 2)) ? $this->Form->input('remittance_id',array('label' => 'Remesa', 'type'=>'text')):""; ?></td>
                    <td><?php echo ($this->Session->read('User.profiles_id') == 1 || ($this->Session->read('User.profiles_id') == 2)) ? $this->Form->input('lot',array('label' => 'Lote', 'type'=>'text')):""; ?></td>
                    <td><?php echo ($this->Session->read('User.profiles_id') == 1 || ($this->Session->read('User.profiles_id') == 2)) ? $this->Form->input('cargolot_id',array('label' => 'Cargo Lot Id', 'type'=>'text')):""; ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><?php echo $this->Form->input('lot_coffee_exporter_id',array('label' => 'Exportador-Lote (E.j.: 001-22112)','', 'type'=>'text')); ?></td>
                    <td style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="findLotAndClient();">Buscar...</a></h3></td>
                    <td><?php echo $this->Form->input('user_profile',array('type'=>'hidden','value'=>$this->Session->read('User.profiles_id'))); ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><?php //echo $this->Form->input('exporter_code',array('label' => 'Exportador Codigo', 'type'=>'text')); ?></td>
                    <td><?php //echo $this->Form->input('exporter_name',array('label' => 'Exportador', 'disabled' => 'disabled')); ?></td>
                </tr>
            </table>
        <h2><?php echo __('Resultado de la busqueda'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Rad'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('client','Cliente'); ?></th>
                    <th><?php echo $this->Paginator->sort('name','Emp'); ?></th>
                    <th><?php echo $this->Paginator->sort('type_coffee','Tipo'); ?></th>
		    <th><?php echo $this->Paginator->sort('name_unit','Unidad Café'); ?></th>
                    <th><?php echo $this->Paginator->sort('name_unit','Ubi'); ?></th>
		    <th><?php echo $this->Paginator->sort('microlot','Microlote?'); ?></th>
                    <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
		    <th><?php echo ($this->Session->read('User.profiles_id') != 9) ? $this->Paginator->sort('locked','Bloq. Emb?'):""; ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos Rad'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos Disp'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th class="actions"><?php echo ($this->Session->read('User.profiles_id') != 9) ? __('Acciones'):""; ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
    </fieldset>
</div>
<script type="text/javascript">
    
    var pathPrintWS = 'http://siscafe.copcsa.com:8080/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&';
    
    $('#RemittancesCaffeeDate1').attr('type', 'date');
    $('#RemittancesCaffeeDate2').attr('type', 'date');
    $('#RemittancesCaffeeDate3').attr('type', 'date');
    $('#RemittancesCaffeeDate4').attr('type', 'date');

    $("#RemittancesCaffeeRemittanceId").keyup(function () {
        findInfoRemittancesByRemittanceId($(this).val());
    });

    $("#RemittancesCaffeeLot").keyup(function () {
        findInfoRemittancesByLot($(this).val());
    });

    $("#RemittancesCaffeeExporterCode").keyup(function () {
        findByClient($(this).val());
    });
    
    
    $("#RemittancesCaffeeCargolotId").keyup(function () {
        findInfoRemittancesByCargoLot($(this).val());
    });
    
    function findInfoRemittancesByCargoLot(cagolotid){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByCargoLotId/" + cagolotid,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
		else{
                    $('#tableRemittances').children().remove();
                    $('#tableRemittances').append('');
                }
            }
        });
    }

    function findInfoRemittancesByRemittanceId(remittanceId) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByRemittanceId/" + remittanceId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
		else{
                    $('#tableRemittances').children().remove();
                    $('#tableRemittances').append('');
                }
            }
        });
    }

    function findInfoRemittancesByLot(lot) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByLot/" + lot,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
		else{
                    $('#tableRemittances').children().remove();
                    $('#tableRemittances').append('');
                }
            }
        });
    }

    function findByClient(clientCode){
 	$.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByExportCode/" + clientCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var clientData = JSON.parse(data)['clientData'];
                    var remittancesData = JSON.parse(data)['remittancesData'];
		    $("#RemittancesCaffeeExporterName").val(clientData['Client']['business_name']);
                    addRemittanceDataToTable(remittancesData);
                }
		else{
                    $('#tableRemittances').children().remove();
                    $('#tableRemittances').append('');
                }
            }
        });
    }

    function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        var user_profile = $("#RemittancesCaffeeUserProfile").val();
        $.each(remittancesData, function (i, value) {
	    console.log(value);
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + '3-'+value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee'] + '</td>';
            trHTML += '<td>' + value['Client']['business_name'] + '</td>';
            trHTML += '<td>' + value['PackingCaffee']['name'] + '</td>';
	    trHTML += '<td>' + value['TypeUnit']['name'] + '</td>';
            trHTML += '<td>' + value['UnitsCaffee']['name_unit'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['ref_driver'] + " " + value['SlotStore']['name_space']+'</td>';
	    trHTML += '<td>' + ((value['RemittancesCaffee']['microlot'] === "0") ? "NO":"SI") + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
	    var conBloqueos=false;
            for (var i = 0; i < value['RemittancesCaffeeHasNoveltysCaffee'].length; i++) {
                if(value['RemittancesCaffeeHasNoveltysCaffee'][i]['active'] === true)
                {
                    conBloqueos = true;
                }
            }
            if(user_profile !== 9){
                trHTML += (conBloqueos !== false) ? '<td style="background-color:yellow" class="actions"><a href="javascript:void(0)" onclick="seeData(\'' + value['RemittancesCaffee']['id'] + '\');">Si, Ver</a></td>':"<td>NO";
            }
            else{
               trHTML += '<td></td>'; 
            }
            trHTML += '<td>' + value['RemittancesCaffee']['total_rad_bag_out'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            if(user_profile != 9){
	       if(value['StateOperation']['id'] == 1){
	       	trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="editRemittance(\'' + remittanceId.toString() + '\');">Editar</a></td>'; 
	       }
		trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="addRemittance(\'' + remittanceId.toString() + '\');">Ver</a></td>';
            }
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }
    
    function findLotAndClient(){
        var lotClient = $("#RemittancesCaffeeLotCoffeeExporterId").val();
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByLotClient/" + lotClient,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
		else{
                    $('#tableRemittances').children().remove();
                    $('#tableRemittances').append('');
                }
            }
        });
    }

    function addRemittance(remittanceId) {
        window.open("http://siscafe.copcsa.com/RemittancesCaffees/weights/"+remittanceId,'_blank');
    }

    function editRemittance(remittanceId) {
        window.open("http://siscafe.copcsa.com/RemittancesCaffees/edit/"+remittanceId,'_blank');
    }

    function seeData(remittanceId) {
        window.open("http://siscafe.copcsa.com/RemittancesCaffeeHasNoveltysCaffees/add/"+remittanceId,'_blank');
    }
	
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php 
               if(($this->Session->read('User.profiles_id') == 1)){
                  echo $this->Html->link(__('Regresar'), array('action' => 'index')); 
               }
               else if(($this->Session->read('User.profiles_id') == 2)){
                   echo $this->Html->link(__('Regresar'), array('controller' => 'Pages','action' => 'Basculero'));
               }
               else if($this->Session->read('User.profiles_id') == 9){
                  echo $this->Html->link(__('Regresar'), array('controller' => 'Pages','action' => 'invitado'));
               }
             ?></li>
        <li><?php 
              if(($this->Session->read('User.profiles_id') == 1) || ($this->Session->read('User.profiles_id') == 2)){
                  echo $this->Html->link(__('Reportes'), array('action' => 'report'));
               }
             ?></li>
    </ul>
</div>
