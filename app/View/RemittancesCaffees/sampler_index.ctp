<div class="remittancesCaffees index">
    <h2><?php echo __('Listado de trazabilidad ingreso'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id','CO-Remesa ID'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                <th><?php if($this->Session->read('User.centerId') == 1) echo $this->Paginator->sort('total_weight_net_real','Peso Real'); ?></th>
                <th><?php if($this->Session->read('User.centerId') == 1) echo $this->Paginator->sort('tare_download','Tara Descargue'); ?></th>
                <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date', 'Fecha Radicado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
            <tr>
                <td><?php 
                //debug($remittancesCaffee);exit;
                echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h("3-".$remittancesCaffee['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                <td><?php if($this->Session->read('User.centerId') == 1) echo h($remittancesCaffee['RemittancesCaffee']['total_weight_net_real']); ?>&nbsp;</td>
                <td><?php if($this->Session->read('User.centerId') == 1) echo h($remittancesCaffee['RemittancesCaffee']['tare_download']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>

                <td class="actions">
			<?php 
                        //debug($remittancesCaffees);exit;
                        if($remittancesCaffee['TraceabilityDownload']['id'] != null){
                            if($departaments_id ==1)
                                echo $this->Html->link(__('Ver Trazabilidad'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=2&toPdf=true&idPrinter=1"); 
                            else{
                                echo $this->Html->link(__('Ver Trazabilidad'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=".$remittancesCaffee['RemittancesCaffee']['id']."&idCertificado=0&idReport=6&toPdf=true&idPrinter=1"); 
                                echo $this->Html->link(__('Fotos'), array('controller'=>'TraceabilityDownloads','action' => 'zip', $remittancesCaffee['RemittancesCaffee']['id']));
				}
                        }
                        else{
                            echo $this->Html->link(__('Trazabilidad'), array('controller'=>'TraceabilityDownloads','action' => 'add', $remittancesCaffee['RemittancesCaffee']['id']));
                        }?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages','action' => 'colaborador')); ?></li>
    </ul>
</div>
