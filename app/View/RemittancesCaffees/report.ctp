<div class="remittancesCaffees form">
<?php echo $this->Html->script('jquery.min');
echo $this->Html->script('jquery-ui.min');
echo $this->Html->css('style');

echo $this->Html->script('jquery.simple-dtpicker');
echo $this->Html->css('jquery.simple-dtpicker');


    echo $this->Form->create('RemittancesCaffee'); ?>  
    <fieldset>
        <legend><?php echo __('Reportes información de Café '); ?></legend>                  
        <table>
            <h3>1. Genera reporte descargue Café: (Rango fechas) Todas las bodegas y  Exportadores</h3>
            <tr>
                <td><?php  echo $this->Form->input('date1',array('type' => 'text','label' => 'Fecha inicial')); ?></td>
                <td><?php  echo $this->Form->input('date2',array('type' => 'text','label' => 'Fecha final')); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListDownloadDates();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>2. Genera reporte descargue Café: (Rango fechas) Por Bodegas y Exportadores</h3>
            <tr>
                <td><?php  echo $this->Form->input('date5',array('type' => 'text','label' => 'Fecha inicial')); ?></td>
                <td><?php  echo $this->Form->input('date6',array('type' => 'text','label' => 'Fecha final')); ?></td>
                <td><?php  echo $this->Form->input('stored',array('type' => 'select', 'style'=>'height:30px;color:#E32;','label' => 'Bodega','empty'=>'Seleccione una bodega...  ',
                    'options' => array('2' => 'Bodega 2', '3' => 'Bodega 3','10' => 'CIAMSA SPB'))); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListDownloadDatesStored();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>3. Genera reporte Permanencia de Café: Café Almacenado Por Bodegas</h3>
            <tr>
                <td><?php  echo $this->Form->input('stored1',array('type' => 'select', 'style'=>'height:33px;width:330px;color:#E32;','label' => 'Bodega','empty'=>'Seleccione una bodega...  ',
                    'options' => array('2' => 'Bodega 2', '3' => 'Bodega 3'))); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListPermanencia();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>4. Genera reporte descargue Café: (Rango fechas y Bodegas) Por FNC o Particulares</h3>
            <tr>
                <td style="width:330px;"><?php  echo $this->Form->input('date7',array('type' => 'text','label' => 'Fecha inicial')); ?></td>
                <td><?php  echo $this->Form->input('date8',array('type' => 'text','label' => 'Fecha final')); ?></td>
            </tr>
            <tr>
                <td><?php  echo $this->Form->input('stored2',array('type' => 'select', 'style'=>'height:33px;width:330px;color:#E32;','label' => 'Bodega','empty'=>'Seleccione una bodega...  ',
                    'options' => array('2' => 'Bodega 2', '3' => 'Bodega 3'))); ?></td>
                <td>
                    <?php 
                        echo $this->Form->input('cod_expo', array('class'=>'select','type'=>'checkbox','label'=>'FNC', 'value'=>'')); 
                        echo $this->Form->input('cod_expo1', array('class'=>'select','type'=>'checkbox','label'=>'Particulares', 'value'=>''));        
                    ?>                    
                </td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListDownloadDatesStoredClient();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>5. Genera reporte descargue Café: (Por exportador y Rango fechas)</h3>
            <tr>
                <td><?php  echo $this->Form->input('date3',array('type' => 'text','label' => 'Fecha inicial')); ?></td>
                <td><?php  echo $this->Form->input('date4',array('type' => 'text','label' => 'Fecha final')); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('exporter_code2',array('label' => 'Exportador Codigo', 'type'=>'text')); ?></td>
                <td><?php echo $this->Form->input('exporter_name2',array('label' => 'Exportador', 'disabled' => 'disabled')); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListDownloadDatesClient();">Generar</a></h3></td>
            </tr>
        </table>

    </fieldset>
</div>
<style>
    .timelist_item{
        color: black;
    }
    .datepicker_table{
	color:	#000000;
	font-size: small;
	text-align:center;
	
	user-select: none;
		-webkit-user-select: none;
		-moz-user-select: none;
	cursor: pointer;
    }
    .datepicker_table .active {
	color: #ffffff;
	background-color:	#808080;
    }
    .datepicker_table .hover {
	color: #000000;
	background-color:	#c8c8c8;
    }
    .wday_sun {
	color:	#e13b00;
    }
    .wday_sat {
	color:	#0044aa;
    }
</style>
<script type="text/javascript">
    
    $('input.select').click(function (event) {
        var check = $(event.target).prop('checked');
            if(check){
                $(event.target).attr('checked', true);
                $(event.target).attr('value', '001');
            }
            else{
                $(event.target).attr('checked', false);
                $(event.target).attr('value', '');
            }
        });
    
    var pathPrintWS = 'http://siscafe.copcsa.com:8080/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&';
    
    var d = new Date('Y/m/d');
    $('#RemittancesCaffeeDate1').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 6:00 am");
    $('#RemittancesCaffeeDate1').appendDtpicker();
    $('#RemittancesCaffeeDate2').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 20:00 ");
    $('#RemittancesCaffeeDate2').appendDtpicker();
    $('#RemittancesCaffeeDate3').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 6:00 am");
    $('#RemittancesCaffeeDate3').appendDtpicker();
    $('#RemittancesCaffeeDate4').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 20:00 ");
    $('#RemittancesCaffeeDate4').appendDtpicker();
    $('#RemittancesCaffeeDate5').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 6:00 am");
    $('#RemittancesCaffeeDate5').appendDtpicker();
    $('#RemittancesCaffeeDate6').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 20:00 ");
    $('#RemittancesCaffeeDate6').appendDtpicker();
    $('#RemittancesCaffeeDate7').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 6:00 am");
    $('#RemittancesCaffeeDate7').appendDtpicker();
    $('#RemittancesCaffeeDate8').val(d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " 20:00 ");
    $('#RemittancesCaffeeDate8').appendDtpicker();

    $("#RemittancesCaffeeExporterCode2").keyup(function () {
        findByClient($(this).val());
    });
    
    function reportListDownloadDates(){
        window.location.href = pathPrintWS + 'idReport=15&date1='+$('#RemittancesCaffeeDate1').val()+'&date2='+$('#RemittancesCaffeeDate2').val();
    }
    
    function reportListPermanencia(){
        //console.log($('select#RemittancesCaffeeStored1').val());
        window.location.href = pathPrintWS + 'idReport=19&stored='+$('#RemittancesCaffeeStored1').val();
    }
    
    function reportListDownloadDatesStored(){
        window.location.href = pathPrintWS + 'idReport=17&date1='+$('#RemittancesCaffeeDate5').val()+'&date2='+$('#RemittancesCaffeeDate6').val()+'&stored='+$('#RemittancesCaffeeStored').val();
    }

    function reportListDownloadDatesClient(){
        window.location.href = pathPrintWS + 'idReport=16&date1='+$('#RemittancesCaffeeDate3').val()+'&date2='+$('#RemittancesCaffeeDate4').val()+'&code_exporter='+$('#RemittancesCaffeeExporterCode2').val();
    }
    
    function reportListDownloadDatesStoredClient(){
        $('input.select').each(function(i, el) {
            if(el.checked){
                if($("#RemittancesCaffeeCodExpo").val()=='001'){
//                    alert("FCN: " + $("#RemittancesCaffeeCodExpo").val());
                    window.location.href = pathPrintWS + 'idReport=18&date1='+$('#RemittancesCaffeeDate7').val()+'&date2='+$('#RemittancesCaffeeDate8').val()+'&stored='+$('#RemittancesCaffeeStored2').val()+'&cod_expo='+$('#RemittancesCaffeeCodExpo').val();
                }
                else if($("#RemittancesCaffeeCodExpo1").val()=='001'){
//                    alert("PARTICULAR: " + $("#RemittancesCaffeeCodExpo1").val());
                    window.location.href = pathPrintWS + 'idReport=18&date1='+$('#RemittancesCaffeeDate7').val()+'&date2='+$('#RemittancesCaffeeDate8').val()+'&stored='+$('#RemittancesCaffeeStored2').val()+'&cod_expo='+$('#RemittancesCaffeeCodExpo').val()+'&cod_expo1='+$('#RemittancesCaffeeCodExpo1').val();
                }
                else if ($("#RemittancesCaffeeCodExpo").val()=='001' && $("#RemittancesCaffeeCodExpo1").val()=='001'){
                    window.location.href = pathPrintWS + 'idReport=18&date1='+$('#RemittancesCaffeeDate7').val()+'&date2='+$('#RemittancesCaffeeDate8').val()+'&stored='+$('#RemittancesCaffeeStored2').val()+'&cod_expo='+$('#RemittancesCaffeeCodExpo').val()+'&cod_expo1='+$('#RemittancesCaffeeCodExpo1').val();
                }                
            }
        });         
    }

    function findByClient(clientCode){
 	$.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByExportCode/" + clientCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var clientData = JSON.parse(data)['clientData'];
                    var remittancesData = JSON.parse(data)['remittancesData'];
		    $("#RemittancesCaffeeExporterName2").val(clientData['Client']['business_name']);
                }
            }
        });
    }

</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>        
         <?php 
            $userprofile = $this->Session->read('User.profile');
            if($this->Session->read('User.profiles_id') == 1){ ?>
            <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('Buscar'), array('action' => 'search')); ?></li>
        <?php } ?>
        <?php 
            $userprofile = $this->Session->read('User.profile');
            if($this->Session->read('User.profiles_id') == 2){ ?>
            <li><?php echo $this->Html->link(__('Regresar'), array('controller' => 'Pages', 'action' => 'basculero')); ?></li>
            <li><?php echo $this->Html->link(__('Buscar'), array('action' => 'search')); ?></li>
        <?php } ?>
    </ul>
</div>
