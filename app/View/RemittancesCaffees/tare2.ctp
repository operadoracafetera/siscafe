<div class="remittancesCaffees form">
<?php echo $this->Html->script('jquery.min');
	  echo $this->Form->create('RemittancesCaffee', array('url' => 'updateTare2')); ?>
    <fieldset> 
        <legend><?php echo __('Ingresar Tara Descargue'); ?></legend>
	<?php
            echo $this->Form->input('id',array('value'=>$this->request->data));
            echo $this->Form->input('remesa',array('value'=>$this->request->data, 'disabled' => 'disabled'));
            echo $this->Form->input('oie',array('value'=>$oie, 'type' => 'hidden'));
            echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."","MANUAL"=>"MANUAL")));
            echo $this->Form->input('weigth',array( 'label' => 'Peso','type' => 'number' , 'required' => true,'disabled' => 'disabled') );
            echo $this->Form->input('tare_download',array( 'type' => 'hidden') );
            echo $this->Form->input('pallet_tare_download',array( 'label' => 'Cantidad de estibas','type' => 'number','disabled' => 'disabled', 'max' => 15 , 'required' => true) );
            echo $this->Form->input('pallets',array( 'type' => 'hidden',) );
        ?>
        <div class="row actions">
            <?php 
                echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addPallet();' ) );
                echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removePallet();') );
             ?>
        </div>
    </fieldset>
    <script type="text/javascript">
        var weigthPallet = 0;
        
        function addPallet() {
            var pallet = parseInt($("#RemittancesCaffeePallets").val());
            $("#RemittancesCaffeePalletTareDownload").val(pallet + 1);
            $("#RemittancesCaffeePallets").val(pallet + 1);
        }

        function removePallet() {
            var pallet = parseInt($("#RemittancesCaffeePallets").val());
            $("#RemittancesCaffeePalletTareDownload").val(pallet - 1);
            $("#RemittancesCaffeePallets").val(pallet - 1);
        }

        $("#RemittancesCaffeeBascule").change(function () {
            if($(this).val() !== "MANUAL"){
                $("#RemittancesCaffeeWeigth").attr('disabled',true);
                $("#RemittancesCaffeePalletTareDownload").attr('disabled',true);
                setInterval(function () {
                    $.ajax({
                        url: $("#RemittancesCaffeeBascule").val(),
                        crossDomain: true,
                        success: function (response) {
                            $("#RemittancesCaffeeWeigth").val(parseInt(response));
                            $("#RemittancesCaffeeTareDownload").val(parseInt(response));
                            setValueQuantityPallet(parseInt(response));
                        }
                    });
                }, 500); //medio segundo
            }
            else{
                $("#RemittancesCaffeeWeigth").removeAttr('disabled');
                $("#RemittancesCaffeePalletTareDownload").removeAttr('disabled');
            }
        });
        
        $("#RemittancesCaffeeWeigth").keyup(function () {
                var pesoManual = $(this).val();
                $("#RemittancesCaffeeWeigth").val(pesoManual);
                $("#RemittancesCaffeeTareDownload").val(pesoManual);
                
            });
            
            $("#RemittancesCaffeePalletTareDownload").keyup(function () {
                var unidades = $(this).val();
                $("#RemittancesCaffeePallets").val(unidades);
            });
        
        function setValueQuantityPallet(weigthBascule){
            if(weigthBascule !== weigthPallet){
                weigthPallet = weigthBascule;
                $("#RemittancesCaffeePalletTareDownload").val(weigthPallet / 70);
                $("#RemittancesCaffeePallets").val(weigthPallet / 70);
            }
        }

    </script>    
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listado Remesas'), array('controller'=>'DetailPackagingCaffees','action' => 'packaging_empty')); ?></li>
    </ul>
</div>
