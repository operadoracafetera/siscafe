<div class="servicePackageHasItemsServices index">
	<h2><?php echo __('Service Package Has Items Services'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('service_package_id'); ?></th>
			<th><?php echo $this->Paginator->sort('items_services_id'); ?></th>
			<th><?php echo $this->Paginator->sort('qta'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($servicePackageHasItemsServices as $servicePackageHasItemsService): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($servicePackageHasItemsService['ServicePackage']['id'], array('controller' => 'service_packages', 'action' => 'view', $servicePackageHasItemsService['ServicePackage']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($servicePackageHasItemsService['ItemsService']['id'], array('controller' => 'items_services', 'action' => 'view', $servicePackageHasItemsService['ItemsService']['id'])); ?>
		</td>
		<td><?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['qta']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Service Package Has Items Service'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Service Packages'), array('controller' => 'service_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package'), array('controller' => 'service_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Items Services'), array('controller' => 'items_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Items Service'), array('controller' => 'items_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
