<div class="servicePackageHasItemsServices form">
<?php echo $this->Form->create('ServicePackageHasItemsService'); ?>
	<fieldset>
		<legend><?php echo __('Edit Service Package Has Items Service'); ?></legend>
	<?php
		echo $this->Form->input('service_package_id');
		echo $this->Form->input('items_services_id');
		echo $this->Form->input('qta');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ServicePackageHasItemsService.service_package_id,items_services_id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ServicePackageHasItemsService.service_package_id,items_services_id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Service Package Has Items Services'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Service Packages'), array('controller' => 'service_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package'), array('controller' => 'service_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Items Services'), array('controller' => 'items_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Items Service'), array('controller' => 'items_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
