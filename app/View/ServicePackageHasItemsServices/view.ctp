<div class="servicePackageHasItemsServices view">
<h2><?php echo __('Service Package Has Items Service'); ?></h2>
	<dl>
		<dt><?php echo __('Service Package'); ?></dt>
		<dd>
			<?php echo $this->Html->link($servicePackageHasItemsService['ServicePackage']['id'], array('controller' => 'service_packages', 'action' => 'view', $servicePackageHasItemsService['ServicePackage']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Items Service'); ?></dt>
		<dd>
			<?php echo $this->Html->link($servicePackageHasItemsService['ItemsService']['id'], array('controller' => 'items_services', 'action' => 'view', $servicePackageHasItemsService['ItemsService']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qta'); ?></dt>
		<dd>
			<?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['qta']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Service Package Has Items Service'), array('action' => 'edit', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Service Package Has Items Service'), array('action' => 'delete', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicePackageHasItemsService['ServicePackageHasItemsService']['service_package_id,items_services_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Package Has Items Services'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package Has Items Service'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Packages'), array('controller' => 'service_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Package'), array('controller' => 'service_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Items Services'), array('controller' => 'items_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Items Service'), array('controller' => 'items_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
