<div class="servicePackageHasItemsServices form">
<?php 
        echo $this->Html->script('jquery.min');
        echo $this->Form->create('ServicePackageHasItemsService'); ?>
	<fieldset>
		<legend><?php echo __('Información Paquete de Servicios'); ?></legend>
	<?php
                echo $this->Form->input('nombre_paquete',array('label'=>'Nombre Paquete de Servicios','value' => $servicePackage['ServicePackage']['name_package'],'disabled' => 'disabled'));
		echo $this->Form->input('id_paquete',array('label'=>'Id Paquete de Servicios','value' => $servicePackage['ServicePackage']['id'],'disabled' => 'disabled'));
                echo $this->Form->input('active_paquete',array('label'=>'Estado','value' => $servicePackage['ServicePackage']['active'] == true ? "ACTIVO" : "DESACTIVO",'disabled' => 'disabled'));
	?>
                <legend><?php echo __('Agregar Item de servicios'); ?></legend>
                <br>
                <?php 
                echo $this->Form->input('id_item',array('label' => 'Id Item Servicio'));
                echo $this->Form->input('items_services_id',array('label' => 'Id Item Servicio','type'=>'hidden','value' => $servicePackage['ServicePackage']['id']));
                echo $this->Form->input('name_item',array('label' => 'Nombre Item Servicio','disabled' => 'disabled'));
		echo $this->Form->input('qta',array('label' => 'Cantidad (Horas - Unidades)'));
                echo $this->Form->input('flag_suma',array('label' => 'Sumar al total de Remisión?'));
                echo $this->Form->input('flag_multiplica',array('label' => 'Multiplica el Item por unidad de Remisión?'));
                echo $this->Form->input('valor',array('label' => 'Valor'));
                ?>
                <table cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                                <th><?php echo $this->Paginator->sort('id','ID Item'); ?></th>
                                <th><?php echo $this->Paginator->sort('unoee_ref','ID Ref UnoEE'); ?></th>
                                <th><?php echo $this->Paginator->sort('name_item','Nombre Item Servicio'); ?></th>
                                <th><?php echo $this->Paginator->sort('qta','Cantidad (Horas - Unidad)'); ?></th>
                                <th><?php echo $this->Paginator->sort('flag_suma','Suma a la Remisión'); ?></th>
                                <th><?php echo $this->Paginator->sort('flag_multiplica','Multiplica por unidad a la Remisión'); ?></th>
                                <th><?php echo $this->Paginator->sort('price','Precio'); ?></th>
                                <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
                </thead>
                <tbody>
	<?php foreach ($servicePackageHasItemsServices as $servicePackageHasItemsService): ?>
	<tr>
		<td><?php 
                //debug($servicePackageHasItemsService);exit;
                echo h($servicePackageHasItemsService["ItemsService"]['id']); ?>&nbsp;</td>
		<td><?php echo h($servicePackageHasItemsService['ItemsService']['unoee_ref']); ?>&nbsp;</td>
		<td><?php echo h($servicePackageHasItemsService['ItemsService']['services_name']); ?>&nbsp;</td>
		<td><?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['qta']); ?>&nbsp;</td>
                <td><?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['flag_suma'] == true ? "SI":"NO"); ?>&nbsp;</td>
                <td><?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['flag_multiplica'] == true ? "SI":"NO"); ?>&nbsp;</td>
                <td><?php echo h($servicePackageHasItemsService['ServicePackageHasItemsService']['valor']); ?>&nbsp;</td>
		<td class="actions">
			<li><?php  echo $this->Html->link(__('Borrar'), array('action' => 'delete','?' => ['service_package_id' => $servicePackageHasItemsService['ServicePackage']['id'], 'items_services_id' => $servicePackageHasItemsService['ItemsService']['id']])); ?></li>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
        <script type="text/javascript">
            $("#ServicePackageHasItemsServiceIdItem").keyup(function () {
                var idItemService = $(this).val();
                findItemService(idItemService);
            });
            
            function findItemService(itemService){
            $.ajax({
               type: "GET",
               datatype: "json",
               url: "/ItemsServices/findByIdItem/"+itemService,
               error: function(msg){alert("error networking");},
               success: function(data){
                    var json = JSON.parse(data);
                    if(JSON.stringify(json) !== "[]"){
                        $("#ServicePackageHasItemsServiceNameItem").val(json.ItemsService.unoee_ref+" - "+json.ItemsService.services_name);
                        $("#ServicePackageHasItemsServiceItemsServicesId").val(json.ItemsService.id);
                    }
                    else{
                        $("#ServicePackageHasItemsServiceNameItem").val("NO ARROJO RESULTADOS");
                    }
             }});
        }
            function clearField(){
                $("#ServicePackageHasItemsServiceIdItem").val("");
                $("#ServicePackageHasItemsServiceNameItem").val("");
                $("#ServicePackageHasItemsServiceItemsServicesId").val("");
            }
        </script>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Menú'), array('controller'=>'pages' ,'action' => 'colaborador')); ?></li>
		<li><?php echo $this->Html->link(__('List Paquetes de Servicio'), array('controller' => 'service_packages', 'action' => 'index')); ?> </li>
		
	</ul>
</div>
