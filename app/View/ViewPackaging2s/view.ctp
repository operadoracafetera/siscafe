<div class="viewPackaging2s view">
<h2><?php echo __('Tarja del contenedor ' . $viewPackaging2['ViewPackaging2']['BIC_CTN']); ?></h2>
	<dl>
		<dt><?php echo __('OIE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['OIE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AUTORIZACIÓN'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['AUTORIZACIÓN']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LOTES'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['LOTES']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PROFORMA ID'); ?></dt>
		<dd>
		<?php echo $this->Html->link(__($viewPackaging2['ViewPackaging2']['ID_PROFORMA']), array('controller' => 'InfoNavies',
		'action' => 'elementAdicional',$viewPackaging2['ViewPackaging2']['ID_PROFORMA']),['target'=>'_blank','escape' => false]); ?>
		</dd>
		<dt><?php echo __('FECHA EMBALAJE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['FECHA_REGISTROS']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BOOKING'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['BOOKING']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MOTONAVA'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['MOTONAVA']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NAVIERO'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['NAVIERO']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ADUANAS'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['ADUANAS']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('VIAJE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['VIAJE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('EXPORTADOR'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['EXPORTADOR']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LINEA'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['LINEA']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CTA CAFE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['CTA_CAFE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PESO CTN'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['PESO_CTN']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SELLO 1'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['SELLO_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SELLO 2'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['SELLO_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PUERTO'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['PUERTO']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ESTADO'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['ESTADO']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ISO CTN'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['ISO_CTN']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BIC CTN'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['BIC_CTN']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MODALIDAD'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['MODALIDAD']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('TIPO EMBALAJE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['TIPO_EMBALAJE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DESTINO'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['DESTINO']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ELEMENTOS EMBALAJE'); ?></dt>
		<dd>
			<?php echo h($viewPackaging2['ViewPackaging2']['ELEMENTOS_EMBALAJE']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado CTNS'), array('controller'=>'PackagingCaffees','action' => 'index')); ?> </li>
		<li><?php if($this->Session->read('User.profiles_id') != 6) {
                    echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); 
                  }
                  else{
                      echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'terminal')); 
                  } ?> </li>
	</ul>
</div>
