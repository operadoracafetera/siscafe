<div class="viewPackaging2s index">
	<h2><?php echo __('View Packaging2s'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('OIE'); ?></th>
			<th><?php echo $this->Paginator->sort('AUTORIZACIÓN'); ?></th>
			<th><?php echo $this->Paginator->sort('LOTES'); ?></th>
			<th><?php echo $this->Paginator->sort('FECHA_REGISTROS'); ?></th>
			<th><?php echo $this->Paginator->sort('BOOKING'); ?></th>
			<th><?php echo $this->Paginator->sort('MOTONAVA'); ?></th>
			<th><?php echo $this->Paginator->sort('NAVIERO'); ?></th>
			<th><?php echo $this->Paginator->sort('ADUANAS'); ?></th>
			<th><?php echo $this->Paginator->sort('VIAJE'); ?></th>
			<th><?php echo $this->Paginator->sort('EXPORTADOR'); ?></th>
			<th><?php echo $this->Paginator->sort('LINEA'); ?></th>
			<th><?php echo $this->Paginator->sort('CTA_CAFE'); ?></th>
			<th><?php echo $this->Paginator->sort('PESO_CTN'); ?></th>
			<th><?php echo $this->Paginator->sort('SELLO_1'); ?></th>
			<th><?php echo $this->Paginator->sort('SELLO_2'); ?></th>
			<th><?php echo $this->Paginator->sort('PUERTO'); ?></th>
			<th><?php echo $this->Paginator->sort('ESTADO'); ?></th>
			<th><?php echo $this->Paginator->sort('ISO_CTN'); ?></th>
			<th><?php echo $this->Paginator->sort('BIC_CTN'); ?></th>
			<th><?php echo $this->Paginator->sort('MODALIDAD'); ?></th>
			<th><?php echo $this->Paginator->sort('TIPO_EMBALAJE'); ?></th>
			<th><?php echo $this->Paginator->sort('DESTINO'); ?></th>
			<th><?php echo $this->Paginator->sort('ELEMENTOS_EMBALAJE'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($viewPackaging2s as $viewPackaging2): ?>
	<tr>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['OIE']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['AUTORIZACIÓN']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['LOTES']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['FECHA_REGISTROS']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['BOOKING']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['MOTONAVA']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['NAVIERO']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['ADUANAS']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['VIAJE']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['EXPORTADOR']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['LINEA']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['CTA_CAFE']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['PESO_CTN']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['SELLO_1']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['SELLO_2']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['PUERTO']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['ESTADO']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['ISO_CTN']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['BIC_CTN']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['MODALIDAD']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['TIPO_EMBALAJE']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['DESTINO']); ?>&nbsp;</td>
		<td><?php echo h($viewPackaging2['ViewPackaging2']['ELEMENTOS_EMBALAJE']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $viewPackaging2['ViewPackaging2']['n'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $viewPackaging2['ViewPackaging2']['n'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $viewPackaging2['ViewPackaging2']['n']), array('confirm' => __('Are you sure you want to delete # %s?', $viewPackaging2['ViewPackaging2']['n']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New View Packaging2'), array('action' => 'add')); ?></li>
	</ul>
</div>
