<div class="returnsCaffees form">
<?php echo $this->Form->create('ReturnsCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Añadir una Devolución de Café'); ?></legend>
	<?php
		echo $this->Form->input('return_date',array('type'=>'hidden'));
                echo $this->Form->input('exporter_id', array('label' => 'Código Cliente', 'type' => 'text'));
                echo $this->Form->input('client_name', array('label' => 'Nombre Cliente', 'disabled' => 'disabled'));
                echo $this->Form->input('client_id', array('label' => 'Client Id', 'type' => 'hidden'));
                echo $this->Form->input('return_date',array('type'=>'hidden'));
		echo $this->Form->input('observation',array('label' => 'Observación'));
		echo $this->Form->input('vehicle_plate',array('label' => 'Placa del vehiculo'));
                echo $this->Form->input('return_type',array('label' => 'Tipo operación','options'=>array('CAFE'=>'CAFE','FIQUE'=>'FIQUE')));
		echo $this->Form->input('jetty',array('label' => 'Terminal','options'=>$deparaments));	
		echo $this->Form->input('user_register',array('type'=>'hidden'));
		echo $this->Form->input('order_service',array('type'=>'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<script>
    $("#ReturnsCaffeeExporterId").keyup(function () {
        findInfoRemittancesByClientCode($(this).val());
    });

    function findInfoRemittancesByClientCode(exportCode) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByExportCode/" + exportCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var clientData = JSON.parse(data)['clientData'];
                    $("#ReturnsCaffeeClientName").val(clientData['Client']['business_name']);
                    $("#ReturnsCaffeeClientId").val(clientData['Client']['id']);
                }
            }
        });
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Mis devoluciones'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Listado de remesas'), array('controller' => 'remittances_caffees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nueva remesa'), array('controller' => 'remittances_caffees', 'action' => 'add')); ?> </li>
	</ul>
</div>
