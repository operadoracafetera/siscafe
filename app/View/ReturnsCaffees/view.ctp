<div class="returnsCaffees view">
<h2><?php echo __('Returns Caffee'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Return Date'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['return_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle Plate'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['vehicle_plate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Register'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['user_register']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Weight'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['user_weight']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Service'); ?></dt>
		<dd>
			<?php echo h($returnsCaffee['ReturnsCaffee']['order_service']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Returns Caffee'), array('action' => 'edit', $returnsCaffee['ReturnsCaffee']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Returns Caffee'), array('action' => 'delete', $returnsCaffee['ReturnsCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $returnsCaffee['ReturnsCaffee']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Returns Caffees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Returns Caffee'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Remittances Caffees'), array('controller' => 'remittances_caffees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Remittances Caffee'), array('controller' => 'remittances_caffees', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Remittances Caffees'); ?></h3>
	<?php if (!empty($returnsCaffee['RemittancesCaffee'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Lot Caffee'); ?></th>
		<th><?php echo __('Quantity Bag In Store'); ?></th>
		<th><?php echo __('Quantity Bag Out Store'); ?></th>
		<th><?php echo __('Quantity In Pallet Caffee'); ?></th>
		<th><?php echo __('Quantity Out Pallet Caffee'); ?></th>
		<th><?php echo __('Quantity Radicated Bag In'); ?></th>
		<th><?php echo __('Quantity Radicated Bag Out'); ?></th>
		<th><?php echo __('Total Weight Net Nominal'); ?></th>
		<th><?php echo __('Total Weight Net Real'); ?></th>
		<th><?php echo __('Tare Download'); ?></th>
		<th><?php echo __('Tare Packaging'); ?></th>
		<th><?php echo __('Source Location'); ?></th>
		<th><?php echo __('Auto Otm'); ?></th>
		<th><?php echo __('Vehicle Plate'); ?></th>
		<th><?php echo __('Download Caffee Date'); ?></th>
		<th><?php echo __('Packaging Caffee Date'); ?></th>
		<th><?php echo __('Created Date'); ?></th>
		<th><?php echo __('Updated Dated'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Guide Id'); ?></th>
		<th><?php echo __('Client Id'); ?></th>
		<th><?php echo __('Units Cafee Id'); ?></th>
		<th><?php echo __('Packing Cafee Id'); ?></th>
		<th><?php echo __('Mark Cafee Id'); ?></th>
		<th><?php echo __('Custom Id'); ?></th>
		<th><?php echo __('City Source Id'); ?></th>
		<th><?php echo __('Shippers Id'); ?></th>
		<th><?php echo __('Slot Store Id'); ?></th>
		<th><?php echo __('Staff Sample Id'); ?></th>
		<th><?php echo __('Staff Wt In Id'); ?></th>
		<th><?php echo __('Staff Wt Out Id'); ?></th>
		<th><?php echo __('Staff Driver Id'); ?></th>
		<th><?php echo __('Observation'); ?></th>
		<th><?php echo __('Details Weight'); ?></th>
		<th><?php echo __('Total Tare'); ?></th>
		<th><?php echo __('Weight Pallet Packaging Total'); ?></th>
		<th><?php echo __('State Operation Id'); ?></th>
		<th><?php echo __('Novelty In Caffee Id'); ?></th>
		<th><?php echo __('Traceability Download Id'); ?></th>
		<th><?php echo __('Type Units Id'); ?></th>
		<th><?php echo __('Total Rad Bag Out'); ?></th>
		<th><?php echo __('Ref Packaging'); ?></th>
		<th><?php echo __('Seals'); ?></th>
		<th><?php echo __('User Register'); ?></th>
		<th><?php echo __('Jetty'); ?></th>
		<th><?php echo __('Cargolot Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($returnsCaffee['RemittancesCaffee'] as $remittancesCaffee): ?>
		<tr>
			<td><?php echo $remittancesCaffee['id']; ?></td>
			<td><?php echo $remittancesCaffee['lot_caffee']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_bag_in_store']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_bag_out_store']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_in_pallet_caffee']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_out_pallet_caffee']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_radicated_bag_in']; ?></td>
			<td><?php echo $remittancesCaffee['quantity_radicated_bag_out']; ?></td>
			<td><?php echo $remittancesCaffee['total_weight_net_nominal']; ?></td>
			<td><?php echo $remittancesCaffee['total_weight_net_real']; ?></td>
			<td><?php echo $remittancesCaffee['tare_download']; ?></td>
			<td><?php echo $remittancesCaffee['tare_packaging']; ?></td>
			<td><?php echo $remittancesCaffee['source_location']; ?></td>
			<td><?php echo $remittancesCaffee['auto_otm']; ?></td>
			<td><?php echo $remittancesCaffee['vehicle_plate']; ?></td>
			<td><?php echo $remittancesCaffee['download_caffee_date']; ?></td>
			<td><?php echo $remittancesCaffee['packaging_caffee_date']; ?></td>
			<td><?php echo $remittancesCaffee['created_date']; ?></td>
			<td><?php echo $remittancesCaffee['updated_dated']; ?></td>
			<td><?php echo $remittancesCaffee['is_active']; ?></td>
			<td><?php echo $remittancesCaffee['guide_id']; ?></td>
			<td><?php echo $remittancesCaffee['client_id']; ?></td>
			<td><?php echo $remittancesCaffee['units_cafee_id']; ?></td>
			<td><?php echo $remittancesCaffee['packing_cafee_id']; ?></td>
			<td><?php echo $remittancesCaffee['mark_cafee_id']; ?></td>
			<td><?php echo $remittancesCaffee['custom_id']; ?></td>
			<td><?php echo $remittancesCaffee['city_source_id']; ?></td>
			<td><?php echo $remittancesCaffee['shippers_id']; ?></td>
			<td><?php echo $remittancesCaffee['slot_store_id']; ?></td>
			<td><?php echo $remittancesCaffee['staff_sample_id']; ?></td>
			<td><?php echo $remittancesCaffee['staff_wt_in_id']; ?></td>
			<td><?php echo $remittancesCaffee['staff_wt_out_id']; ?></td>
			<td><?php echo $remittancesCaffee['staff_driver_id']; ?></td>
			<td><?php echo $remittancesCaffee['observation']; ?></td>
			<td><?php echo $remittancesCaffee['details_weight']; ?></td>
			<td><?php echo $remittancesCaffee['total_tare']; ?></td>
			<td><?php echo $remittancesCaffee['weight_pallet_packaging_total']; ?></td>
			<td><?php echo $remittancesCaffee['state_operation_id']; ?></td>
			<td><?php echo $remittancesCaffee['novelty_in_caffee_id']; ?></td>
			<td><?php echo $remittancesCaffee['traceability_download_id']; ?></td>
			<td><?php echo $remittancesCaffee['type_units_id']; ?></td>
			<td><?php echo $remittancesCaffee['total_rad_bag_out']; ?></td>
			<td><?php echo $remittancesCaffee['ref_packaging']; ?></td>
			<td><?php echo $remittancesCaffee['seals']; ?></td>
			<td><?php echo $remittancesCaffee['user_register']; ?></td>
			<td><?php echo $remittancesCaffee['jetty']; ?></td>
			<td><?php echo $remittancesCaffee['cargolot_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'remittances_caffees', 'action' => 'view', $remittancesCaffee['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'remittances_caffees', 'action' => 'edit', $remittancesCaffee['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'remittances_caffees', 'action' => 'delete', $remittancesCaffee['id']), array('confirm' => __('Are you sure you want to delete # %s?', $remittancesCaffee['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Remittances Caffee'), array('controller' => 'remittances_caffees', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
