<div class="returnsCaffees view">
<?php echo $this->Html->script('jquery.min');
echo $this->Html->script('jquery-ui.min');
echo $this->Html->css('jquery-ui.min');
    echo $this->Form->create('ReturnsCaffee'); ?>
    <fieldset>
        <legend><?php echo __('Información de la devolución Id '.$returnCoffee['ReturnsCaffee']['id']); ?></legend>       
        <table>
            <tr>
                <td colspan="2"><?php 
                echo $this->Form->input('clientCode',array('label' => 'Código Cliente', 'disabled' => 'disabled','value'=>$returnCoffee['Client']['exporter_code']." - ".$returnCoffee['Client']['business_name'])); ?></td>
                <td><?php echo $this->Form->input('code_expo',array('type'=>'hidden','value'=>$returnCoffee['Client']['exporter_code'])); ?></td>
            </tr>
            <tr>
                <tr>
                    <td><?php echo $this->Form->input('vehicle_plate',array('label' => 'Placa ','disabled' => 'disabled','value'=>$returnCoffee['ReturnsCaffee']['vehicle_plate'])); ?></td>
                    <td><?php echo $this->Form->input('return_type',array('label' => 'Tipo operación ','disabled' => 'disabled','value'=>$returnCoffee['ReturnsCaffee']['return_type'])); ?></td>
                </tr>
                <td><?php echo $this->Form->input('observation',array('label' => 'Observación','disabled' => 'disabled','value'=>$returnCoffee['ReturnsCaffee']['observation'])); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('datetime_register',array('label' => 'Fecha registro','disabled' => 'disabled','value'=>$returnCoffee['ReturnsCaffee']['return_date'])); ?></td>
                <td><?php echo $this->Form->input('service_order',array('label' => 'Orden servicio ','disabled' => 'disabled','value'=>$returnCoffee['ReturnsCaffee']['order_service'])); ?></td>
            </tr>
        </table>
        <br><br>
        <h2><?php echo __('Relación remesas de café para devolución'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('download_caffee_date', 'Fecha Descargue'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id', 'Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee', 'Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store_after', 'Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store', 'Sacos In Despues'); ?></th>
                    <th><?php echo $this->Paginator->sort('qta_bags', 'Sacos x devolver'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <?php foreach ($remittancesCaffeeReturnsCaffees as $remittancesCaffeeReturnsCaffee): ?>
                <tr>
                    <td><?php 
                    //debug($remittancesCaffeeReturnsCaffee);exit;
                    echo h($remittancesCaffeeReturnsCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
                    <td><?php echo h($this->Session->read('User.centerId').'-'.$remittancesCaffeeReturnsCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                    <td><?php echo h('3-'.$returnCoffee['Client']['exporter_code'].'-'.$remittancesCaffeeReturnsCaffee["RemittancesCaffee"]['lot_caffee']); ?>&nbsp;</td>
                    <td><?php echo h(($remittancesCaffeeReturnsCaffee['RemittancesCaffee']['quantity_bag_in_store'])); ?>&nbsp;</td>
                    <td><?php echo h(($remittancesCaffeeReturnsCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] - $remittancesCaffeeReturnsCaffee['RemittancesCaffee']['total_rad_bag_out'])); ?>&nbsp;</td>
                    <td><?php echo h($remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['qta_bags']); ?>&nbsp</td>
                    <td class="actions">
                        <?php
                        echo ($returnCoffee['ReturnsCaffee']['state_return'] == "REGISTRADA") ? '<a href="javascript:void(0)" onclick="delReturnRemmitancesCofffe('.$remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['id'].')">Quitar</a>':"";
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
            ));
            ?>	</p>
        <div class="paging">
            <?php
            echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
        </div>
        <br><br>
                    <legend><?php echo __('Buscar información para devolución de Café. '); ?></legend>
    <table>
            <tr>
                <td ><?php echo $this->Form->input('remittance_id',array('label' => 'Remesa', 'type'=>'text')); ?></td>
                <td ><?php echo $this->Form->input('lot',array('label' => 'Lote', 'type'=>'text')); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('qta_bag',array('label' => 'Cant sacos ó unidades', 'value'=>0 ,'type'=>'text')); ?></td>
                <td><?php echo $this->Form->input('remittance_coffee_id',array('label' => 'Remesa seleccionada','disabled' => 'disabled', 'type'=>'text')); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="returnRemmitancesCofffe();">Devolver</a></h3></td>
            </tr>
        </table>
        <h2><?php echo __('Resultado de la busqueda'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('client','Cliente'); ?></th>
                    <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos disponibles'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
    </fieldset>
<?php echo ($returnCoffee['ReturnsCaffee']['state_return'] == "REGISTRADA") ? $this->Form->end(__('Confirmar devolución')):""; ?>
</div>
<script type="text/javascript">
    /*$("RemittancesCaffeeClientCode").keyup(function () {
        //findInfoRemittancesByClientCode($(this).val());
        alert("ssdads");
    });*/

    $("#ReturnsCaffeeRemittanceId").keyup(function () {
        var client = $("#ReturnsCaffeeCodeExpo").val();
        var type = $("#ReturnsCaffeeReturnType").val();
        findInfoByClientRemittances($(this).val()+"-"+client);
    });

    $("#ReturnsCaffeeLot").keyup(function () {
        var client = $("#ReturnsCaffeeCodeExpo").val();
        var type = $("#ReturnsCaffeeReturnType").val();
        findLotAndClient(client+"-"+$(this).val());
    });

    function returnRemmitancesCofffe(){
        var remesa = $("#ReturnsCaffeeRemittanceCoffeeId").val();
        var qta_bags = $("#ReturnsCaffeeQtaBag").val();
        var return_id = <?php echo $returnCoffee['ReturnsCaffee']['id'];?>;
        var type = $("#ReturnsCaffeeReturnType").val();
        if(remesa === '' || qta_bags === '0'){
            alert("Remesa ó cantidad de café no valido. Por favor verifique");
            return;
        }
        else{
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "http://siscafe.copcsa.com/RemittancesCaffees/returnCaffeeRemittances?remesa="+remesa+"&return_id="+return_id+"&qta_bags="+qta_bags+"&type="+type,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data != "") {
                        alert('Se realizo correctamente la adición de la devolución para la remesa ' + remesa);
                        window.location.href = "http://siscafe.copcsa.com/ReturnsCaffees/return_lot/"+return_id;
                    }
                    else{
                        alert("No se pudo realizar esta operación de devolución; la remesa almacenada "+remesa+" tiene saldos insuficientes.");
                    }
                }
            });
        }
        
    }
    
    function delReturnRemmitancesCofffe(detalle_id){
        var return_id = <?php echo $returnCoffee['ReturnsCaffee']['id'];?>;
        var type = $("#ReturnsCaffeeReturnType").val();
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "http://siscafe.copcsa.com/RemittancesCaffees/deleteReturnCaffeeRemittances?detalle_id="+detalle_id+"&type="+type,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    alert('Se realizo correctamente la eliminación del registros');
                    window.location.href = "http://siscafe.copcsa.com/ReturnsCaffees/return_lot/"+return_id;
                }
                else{
                    alert("No se puede realizar la operación. Por favor intentelo nuevamente");
                }
            }
        });
    }
    
    function findInfoByClientRemittances(remittanceId) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "http://siscafe.copcsa.com/RemittancesCaffees/findByRemittancesClient/" + remittanceId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    console.log("hay data");
                    console.log(remittanceId);
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function findLotAndClient(clientLot){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "http://siscafe.copcsa.com/RemittancesCaffees/findRemittancesByLotClient/" + clientLot,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    
    function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        $.each(remittancesData, function (i, value) {
		console.log(value['RemittancesCaffee']);
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + '3-'+value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee'] + '</td>';
            trHTML += '<td>' + value['Client']['business_name'] + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['quantity_radicated_bag_out'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            var estado=value['RemittancesCaffee']['state_operation_id'];
            var return_id = <?php echo $returnCoffee['ReturnsCaffee']['id'] ?>;
            trHTML += '<td class="actions" style="padding-top: 5px;"><h4><a href="javascript:void(0)" onclick="selectionRemmitancesCoffee('+remittanceId+');">Seleccionar</a></h4></td>';
            //trHTML += '<td class="actions"><a href="http://siscafe.copcsa.com/RemittancesCaffees/returnCaffeeRemittances?remesa='+value['RemittancesCaffee']['id']+'&return_id='+return_id+'">Devolver</a></td>';
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }
    
    

    function returnsRemittance(remittanceId) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "http://siscafe.copcsa.com/RemittancesCaffees/returnCaffeeRemittances/"+remittanceId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                        addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function selectionRemmitancesCoffee(id){
        $("#ReturnsCaffeeRemittanceCoffeeId").val(id);
    }
    
    
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>

