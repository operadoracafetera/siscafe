<div class="returnsCaffees form">
<?php echo $this->Form->create('ReturnsCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Returns Caffee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('return_date');
		echo $this->Form->input('observation');
		echo $this->Form->input('vehicle_plate');
		echo $this->Form->input('user_register');
		echo $this->Form->input('user_weight');
		echo $this->Form->input('order_service');
		echo $this->Form->input('RemittancesCaffee');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ReturnsCaffee.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ReturnsCaffee.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Returns Caffees'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Remittances Caffees'), array('controller' => 'remittances_caffees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Remittances Caffee'), array('controller' => 'remittances_caffees', 'action' => 'add')); ?> </li>
	</ul>
</div>
