<div class="returnsCaffees index">
	<h2><?php echo __('Listado de Devoluciones'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('#'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha devolución'); ?></th>
			<th><?php echo $this->Paginator->sort('Observación'); ?></th>
                        <th><?php echo $this->Paginator->sort('Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('Placa del vehiculo'); ?></th>
			<th><?php echo $this->Paginator->sort('Tipo'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($returnsCaffees as $returnsCaffee): ?>
	<tr>
		<td><?php echo h($returnsCaffee['ReturnsCaffee']['id']); ?>&nbsp;</td>
		<td><?php echo h($returnsCaffee['ReturnsCaffee']['return_date']); ?>&nbsp;</td>
		<td><?php echo h($returnsCaffee['ReturnsCaffee']['observation']); ?>&nbsp;</td>
                <td><?php echo h($returnsCaffee['ReturnsCaffee']['state_return']); ?>&nbsp;</td>
		<td><?php echo h($returnsCaffee['ReturnsCaffee']['vehicle_plate']); ?>&nbsp;</td>
		<td><?php echo h($returnsCaffee['ReturnsCaffee']['return_type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'return_lot', $returnsCaffee['ReturnsCaffee']['id']), array('target' => "_blank")); ?>
                        <?php 
                        if($returnsCaffee['ReturnsCaffee']['return_type'] != 'FIQUE'){
                            echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=14&returnId=".$returnsCaffee['ReturnsCaffee']['id']);
                        }
                        else{
                            echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=23&returnId=".$returnsCaffee['ReturnsCaffee']['id']);
                        }
			echo $this->Html->link(__('Fotografias'), array('controller'=>'OperationTrackings','action' => 'addreturn', $returnsCaffee['ReturnsCaffee']['id']),array('target' => "_blank"));
                        ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nueva devolución'), array('action' => 'add')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar devolución'), array('controller'=>'ReturnsCaffees', 'action' => 'searchReturns')); ?></li>
	</ul>
</div>
