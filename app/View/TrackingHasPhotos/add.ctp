<div class="trackingHasPhotos form">
<?php echo $this->Form->create('TrackingHasPhoto'); ?>
	<fieldset>
		<legend><?php echo __('Add Tracking Has Photo'); ?></legend>
	<?php
		echo $this->Form->input('tracking_id');
		echo $this->Form->input('name_file');
		echo $this->Form->input('created_date');
		echo $this->Form->input('updated_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tracking Has Photos'), array('action' => 'index')); ?></li>
	</ul>
</div>
