<div class="trackingHasPhotos view">
<h2><?php echo __('Tracking Has Photo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($trackingHasPhoto['TrackingHasPhoto']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tracking Id'); ?></dt>
		<dd>
			<?php echo h($trackingHasPhoto['TrackingHasPhoto']['tracking_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name File'); ?></dt>
		<dd>
			<?php echo h($trackingHasPhoto['TrackingHasPhoto']['name_file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($trackingHasPhoto['TrackingHasPhoto']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated Date'); ?></dt>
		<dd>
			<?php echo h($trackingHasPhoto['TrackingHasPhoto']['updated_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tracking Has Photo'), array('action' => 'edit', $trackingHasPhoto['TrackingHasPhoto']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tracking Has Photo'), array('action' => 'delete', $trackingHasPhoto['TrackingHasPhoto']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $trackingHasPhoto['TrackingHasPhoto']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Tracking Has Photos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tracking Has Photo'), array('action' => 'add')); ?> </li>
	</ul>
</div>
