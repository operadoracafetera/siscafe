<div class="noveltyInCaffees view">
<h2><?php echo __('Novelty In Caffee'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img1'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['img1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img2'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['img2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img3'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['img3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img4'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['img4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($noveltyInCaffee['NoveltyInCaffee']['created_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Novelty In Caffee'), array('action' => 'edit', $noveltyInCaffee['NoveltyInCaffee']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Novelty In Caffee'), array('action' => 'delete', $noveltyInCaffee['NoveltyInCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $noveltyInCaffee['NoveltyInCaffee']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Novelty In Caffees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Novelty In Caffee'), array('action' => 'add')); ?> </li>
	</ul>
</div>
