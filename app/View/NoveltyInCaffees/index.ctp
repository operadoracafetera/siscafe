<div class="noveltyInCaffees index">
	<h2><?php echo __('Novelty In Caffees'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('img1'); ?></th>
			<th><?php echo $this->Paginator->sort('img2'); ?></th>
			<th><?php echo $this->Paginator->sort('img3'); ?></th>
			<th><?php echo $this->Paginator->sort('img4'); ?></th>
			<th><?php echo $this->Paginator->sort('observation'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($noveltyInCaffees as $noveltyInCaffee): ?>
	<tr>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['id']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['img1']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['img2']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['img3']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['img4']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['observation']); ?>&nbsp;</td>
		<td><?php echo h($noveltyInCaffee['NoveltyInCaffee']['created_date']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $noveltyInCaffee['NoveltyInCaffee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $noveltyInCaffee['NoveltyInCaffee']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $noveltyInCaffee['NoveltyInCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $noveltyInCaffee['NoveltyInCaffee']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Novelty In Caffee'), array('action' => 'add')); ?></li>
	</ul>
</div>
