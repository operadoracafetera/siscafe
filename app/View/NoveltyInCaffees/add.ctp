<div class="noveltyInCaffees form">
<?php echo $this->Form->create('NoveltyInCaffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Agregar novedad a la Remesa Café'); ?></legend>
	<?php
                echo $this->Form->input('cliente',array('disabled' => 'disabled','label' => 'Exportador','value'=> $RemittancesCaffee['Client']['business_name']));
                echo $this->Form->input('remesa',array('disabled' => 'disabled','label' => 'Remesa','value'=> $RemittancesCaffee['RemittancesCaffee']['id']));
                echo $this->Form->input('lote',array('disabled' => 'disabled','label' => 'Lote café','value'=>'3-'.$RemittancesCaffee['Client']['exporter_code'] .'-'. $RemittancesCaffee['RemittancesCaffee']['lot_caffee']));
                echo $this->Form->label('img1','(Imagen 1)');
		echo $this->Form->file('img1');
                echo $this->Form->label('img2','(Imagen 2)');
		echo $this->Form->file('img2');
                echo $this->Form->label('img3','(Imagen 3)');
		echo $this->Form->file('img3');
                echo $this->Form->label('img4','(Imagen 4)');
		echo $this->Form->file('img4');
                //echo $this->Form->input('mojado',array('label'=>'Salvamento (Sacos Mojados?)', 'type'=>'checkbox'));
		echo $this->Form->input('observation',array('label'=>'(Observaciones - Novedades)'));
		//echo $this->Form->input('created_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado remesas'), array('controller'=>'RemittancesCaffees', 'action' => 'index')); ?></li>
	</ul>
</div>
