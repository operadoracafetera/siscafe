<div class="noveltyInCaffees form">
<?php echo $this->Form->create('NoveltyInCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Novelty In Caffee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('img1');
		echo $this->Form->input('img2');
		echo $this->Form->input('img3');
		echo $this->Form->input('img4');
		echo $this->Form->input('observation');
		echo $this->Form->input('created_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('NoveltyInCaffee.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('NoveltyInCaffee.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Novelty In Caffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
