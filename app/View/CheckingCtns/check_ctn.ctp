<div class="checkingCtns form">
<?php echo $this->Form->create('CheckingCtn',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Listado Chequeo Und Ctn '.$findPackagingCaffee['ReadyContainer']['bic_container']); ?></legend>
	<?php
                echo $this->Form->input('id',array('hiddenField' => true));
		echo $this->Form->input('question1', array('label' => '1. ¿Presenta filtración de luz?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'No'));
		echo $this->Form->input('question2',array('label' => '2. ¿Presenta oleres extraños?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'No'));
		echo $this->Form->input('question3',array('label' => '3. ¿Estado optimo de cierres, maniguetas y cauchos en puertas?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'Si'));
		echo $this->Form->input('question4',array('label' => '4. ¿Piso del contendor en buen estado?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'Si'));
		echo $this->Form->input('question5',array('label' => '5. ¿Limpieza adecuado del contenedor?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'Si'));
		echo $this->Form->input('question6',array('label' => '6. ¿Carga inspeccionada por antinarcóticos?','required' => true,'options' => array("Si"=>"Si", "No"=>"No"),'empty' => '(escoja una)','default'=>'Si'));
		echo $this->Form->input('status',array('label' => '¿Estado del general contenedor?','required' => true,'options' => array("ACEPTADO" => "ACEPTADO", "RECHAZADO" => "RECHAZADO"),'empty' => '(escoja una)'));
		echo $this->Form->input('observation',array('label' => 'Observaciones'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
