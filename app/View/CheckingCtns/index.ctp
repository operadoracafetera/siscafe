<div class="checkingCtns index">
	<h2><?php echo __('Checking Ctns'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('date_check'); ?></th>
			<th><?php echo $this->Paginator->sort('question1'); ?></th>
			<th><?php echo $this->Paginator->sort('question2'); ?></th>
			<th><?php echo $this->Paginator->sort('question3'); ?></th>
			<th><?php echo $this->Paginator->sort('question4'); ?></th>
			<th><?php echo $this->Paginator->sort('question5'); ?></th>
			<th><?php echo $this->Paginator->sort('question6'); ?></th>
			<th><?php echo $this->Paginator->sort('imagen1'); ?></th>
			<th><?php echo $this->Paginator->sort('imagen2'); ?></th>
			<th><?php echo $this->Paginator->sort('imagen3'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('observation'); ?></th>
			<th><?php echo $this->Paginator->sort('packaging_caffee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('users_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($checkingCtns as $checkingCtn): ?>
	<tr>
		<td><?php echo h($checkingCtn['CheckingCtn']['id']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['date_check']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question1']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question2']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question3']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question4']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question5']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['question6']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['imagen1']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['imagen2']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['imagen3']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['status']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['observation']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['packaging_caffee_id']); ?>&nbsp;</td>
		<td><?php echo h($checkingCtn['CheckingCtn']['users_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $checkingCtn['CheckingCtn']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $checkingCtn['CheckingCtn']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $checkingCtn['CheckingCtn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $checkingCtn['CheckingCtn']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Checking Ctn'), array('action' => 'add')); ?></li>
	</ul>
</div>
