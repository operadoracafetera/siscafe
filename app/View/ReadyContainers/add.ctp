<div class="readyContainers form">
<?php echo $this->Form->create('ReadyContainer'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Bic Ctnr '); ?></legend>
	<?php
		echo $this->Form->input('bic_container');
		//echo $this->Form->input('date_registre');
		echo $this->Form->input('shipping_lines_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Embalaje'), array('controller'=>'PackagingCaffees','action' => 'index')); ?></li>
	</ul>
</div>
