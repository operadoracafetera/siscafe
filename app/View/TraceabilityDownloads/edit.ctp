<div class="traceabilityDownloads form">
<?php echo $this->Form->create('TraceabilityDownload'); ?>
	<fieldset>
		<legend><?php echo __('Edit Traceability Download'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('img1');
		echo $this->Form->input('img2');
		echo $this->Form->input('img3');
		echo $this->Form->input('img4');
		echo $this->Form->input('img5');
		echo $this->Form->input('img6');
		echo $this->Form->input('observation');
		echo $this->Form->input('remittances_caffee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TraceabilityDownload.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('TraceabilityDownload.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Traceability Downloads'), array('action' => 'index')); ?></li>
	</ul>
</div>
