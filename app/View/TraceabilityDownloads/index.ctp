<div class="traceabilityDownloads index">
	<h2><?php echo __('Traceability Downloads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('img1'); ?></th>
			<th><?php echo $this->Paginator->sort('img2'); ?></th>
			<th><?php echo $this->Paginator->sort('img3'); ?></th>
			<th><?php echo $this->Paginator->sort('img4'); ?></th>
			<th><?php echo $this->Paginator->sort('img5'); ?></th>
			<th><?php echo $this->Paginator->sort('img6'); ?></th>
			<th><?php echo $this->Paginator->sort('observation'); ?></th>
			<th><?php echo $this->Paginator->sort('remittances_caffee_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($traceabilityDownloads as $traceabilityDownload): ?>
	<tr>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['id']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img1']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img2']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img3']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img4']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img5']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['img6']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['observation']); ?>&nbsp;</td>
		<td><?php echo h($traceabilityDownload['TraceabilityDownload']['remittances_caffee_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $traceabilityDownload['TraceabilityDownload']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $traceabilityDownload['TraceabilityDownload']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $traceabilityDownload['TraceabilityDownload']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $traceabilityDownload['TraceabilityDownload']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Traceability Download'), array('action' => 'add')); ?></li>
	</ul>
</div>
