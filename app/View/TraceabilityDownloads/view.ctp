<div class="traceabilityDownloads view">
<h2><?php echo __('Traceability Download'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img1'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img2'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img3'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img4'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img5'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img6'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['img6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remittances Caffee Id'); ?></dt>
		<dd>
			<?php echo h($traceabilityDownload['TraceabilityDownload']['remittances_caffee_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Traceability Download'), array('action' => 'edit', $traceabilityDownload['TraceabilityDownload']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Traceability Download'), array('action' => 'delete', $traceabilityDownload['TraceabilityDownload']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $traceabilityDownload['TraceabilityDownload']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Traceability Downloads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Traceability Download'), array('action' => 'add')); ?> </li>
	</ul>
</div>
