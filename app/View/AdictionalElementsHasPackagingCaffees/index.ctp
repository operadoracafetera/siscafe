<div class="adictionalElementsHasPackagingCaffees index">
	<h2><?php echo __('Adictional Elements Has Packaging Caffees'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('adictional_elements_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('type_unit'); ?></th>
			<th><?php echo $this->Paginator->sort('info_navy_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($adictionalElementsHasPackagingCaffees as $adictionalElementsHasPackagingCaffee): ?>
	<tr>
		<td><?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['adictional_elements_id']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['type_unit']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['info_navy_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n']), array('confirm' => __('Are you sure you want to delete # %s?', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Adictional Elements Has Packaging Caffee'), array('action' => 'add')); ?></li>
	</ul>
</div>
