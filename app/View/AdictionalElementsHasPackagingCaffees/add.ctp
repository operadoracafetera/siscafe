<div class="adictionalElementsHasPackagingCaffees form">
<?php echo $this->Form->create('AdictionalElementsHasPackagingCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Add Adictional Elements Has Packaging Caffee'); ?></legend>
	<?php
		echo $this->Form->input('adictional_elements_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('type_unit');
		echo $this->Form->input('info_navy_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Adictional Elements Has Packaging Caffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
