<div class="AdictionalElementsHasPackagingCaffee form">
<?php echo $this->Form->create('AdictionalElementsHasPackagingCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Elementos Adicionales a la Proforma '.$proforma); ?></legend>
	<?php
		echo $this->Form->input('adictional_elements_id',[
				    'type' => 'select',
				    'label'=>'Elementos adicionales',
				    'multiple' => false,
				    'options' => $allAdictionalElements, 
				    'empty' => true
				]);
		echo $this->Form->input('quantity', array('label'=>'Cantidad'));
		echo $this->Form->input('type_unit',array(
			      'label'=>'Unidad',
			      'options' => array('CAPA'=>'CAPA','UNIDAD'=>'UNIDAD','N/A'=>'N/A'),
			      'empty' => '(Escoja una unidad)'
			  ));
		echo $this->Form->input('observation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acción'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Regresar'), array('controller'=> 'InfoNavies','action' => 'index')); ?></li>
	</ul>
</div>
<div class="AdictionalElementsHasProformas form">
	<h2><?php echo __('Listado de elementos adicionales'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('adictional_elements_id','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('type_unit','Tipo'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity','Cantidad'); ?></th>
			<th><?php echo $this->Paginator->sort('observation','Observación'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($allElementsHasPackagingCaffee as $adictionalElement): ?>
	<tr>
		<td><?php 
                //debug($adictionalElement);exit;
                echo h($adictionalElement["AdictionalElement"]['name']); ?>&nbsp;</td>
		<td><?php $type_unit = $adictionalElement['AdictionalElementsHasPackagingCaffee']['type_unit']; 
		    if($type_unit == 0)
		      echo "CAPA";
		    else if($type_unit == 1)
		      echo "UNIDAD";
		    else if($type_unit == 2)
		      echo "N/A";
		?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElementsHasPackagingCaffee']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($adictionalElement['AdictionalElementsHasPackagingCaffee']['observation']); ?>&nbsp;</td>
		
		<td class="actions">
			<li><?php  echo $this->Html->link(__('Borrar'), array('action' => 'delete','?' => ['adictional_elements_id' => $adictionalElement['AdictionalElementsHasPackagingCaffee']['adictional_elements_id'], 'info_navy_id' => $adictionalElement['AdictionalElementsHasPackagingCaffee']['info_navy_id']])); ?></li>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>
