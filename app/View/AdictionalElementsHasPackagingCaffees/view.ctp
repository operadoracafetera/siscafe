<div class="adictionalElementsHasPackagingCaffees view">
<h2><?php echo __('Adictional Elements Has Packaging Caffee'); ?></h2>
	<dl>
		<dt><?php echo __('Adictional Elements Id'); ?></dt>
		<dd>
			<?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['adictional_elements_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type Unit'); ?></dt>
		<dd>
			<?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['type_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Info Navy Id'); ?></dt>
		<dd>
			<?php echo h($adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['info_navy_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Adictional Elements Has Packaging Caffee'), array('action' => 'edit', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Adictional Elements Has Packaging Caffee'), array('action' => 'delete', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n']), array('confirm' => __('Are you sure you want to delete # %s?', $adictionalElementsHasPackagingCaffee['AdictionalElementsHasPackagingCaffee']['n']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Adictional Elements Has Packaging Caffees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adictional Elements Has Packaging Caffee'), array('action' => 'add')); ?> </li>
	</ul>
</div>
