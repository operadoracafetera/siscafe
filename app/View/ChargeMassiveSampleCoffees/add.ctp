<div class="chargeMassiveSampleCoffees form">
<?php echo $this->Form->create('ChargeMassiveSampleCoffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar cargue Masivo'); ?></legend>
	<?php
                echo $this->Form->label('name_file','Cargar archivo (Solo un archivo XLS )');
		echo $this->Form->file('name_file');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Cargar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado Cargues'), array('action' => 'index',$this->Session->read('User.id'))); ?></li>
	</ul>
</div>
