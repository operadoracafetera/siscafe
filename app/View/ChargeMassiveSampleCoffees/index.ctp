<div class="chargeMassiveSampleCoffees index">
	<h2><?php echo __('Listado de cargues masivos de Solicitudes (Muestras, Trazabilidades)'); ?></h2>
        <?php echo $this->Html->link(__('Cargar Archivo...'), array('action' => 'add'));?>
        <p>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre del archivo'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha registro'); ?></th>
			<th><?php echo $this->Paginator->sort('Cant. Registros'); ?></th>
			<th><?php echo $this->Paginator->sort('Usuario registro'); ?></th>
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($chargeMassiveSampleCoffees as $chargeMassiveSampleCoffee): ?>
	<tr>
		<td><?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['id']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link(__($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['name_file']), array('action' => 'getTrackingRequest', $chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['name_file'])); ?></td>
		<td><?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['date_reg']); ?>&nbsp;</td>
		<td><?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['count_insert']); ?>&nbsp;</td>
		<td><?php echo h($chargeMassiveSampleCoffee['Users']['username']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver solicitudes'), array('action' => 'view', $chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Registrar solicitudes'), array('controller'=>'ChargeMassiveSampleCoffees','action' => 'index', $this->Session->read('User.id'))); ?></li>
                <li><?php echo $this->Html->link(__('Buscar solicitud'), array('controller'=>'CoffeeSamples','action' => 'search')); ?></li>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',$this->Session->read('User.id'))); ?> </li>
	</ul>
</div>
