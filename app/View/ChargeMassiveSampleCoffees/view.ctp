<div class="chargeMassiveSampleCoffees view">
<h2><?php echo __('Ver solicitudes del cargue masivo #'.$chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['id']); ?></h2>
	<dl>
		<dt><?php echo __('Nombre del archivo cargado'); ?></dt>
		<dd>
			<?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['name_file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Registro'); ?></dt>
		<dd>
			<?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['date_reg']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cant. registros exitosos'); ?></dt>
		<dd>
			<?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['count_insert']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php 
                        echo h($chargeMassiveSampleCoffee['Users']['username']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Errores'); ?></dt>
                <dd>
			<?php echo h($chargeMassiveSampleCoffee['ChargeMassiveSampleCoffee']['msg_error']); ?>
			&nbsp;
		</dd>
                </dl>
                <h2><?php echo ('Listado de solicitudes (Muestras, Trazabilidades)') ?></h2>
                <table id="tableData" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id','Id Muestra'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_coffee_ref','Ref lote'); ?></th>
                            <th><?php echo $this->Paginator->sort('qta_bag','Cant. Sacos'); ?></th>
                            <th><?php echo $this->Paginator->sort('client_id','Exportador'); ?></th>

                            <th><?php echo $this->Paginator->sort('type_sample_coffe_id','Tipo muestra'); ?></th>

                            <th><?php echo $this->Paginator->sort('status','Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
                            <th><?php echo $this->Paginator->sort('operation_center','Centro de operaciones'); ?></th>
                            <th><?php echo $this->Paginator->sort('terminal','Terminal'); ?></th>
                            <th><?php //echo $this->Paginator->sort('register_users_id','Registro usuario'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($coffeeSamples as $coffeeSample): ?>
                        <tr>
                            <td><?php 
                            //debug($coffeeSample);exit;
                            echo h($coffeeSample['CoffeeSample']['id']); ?>&nbsp;</td>
                            <td><?php echo h('3-'.$coffeeSample['CoffeeSample']['lot_coffee_ref']); ?>&nbsp;</td>
                            <td><?php echo h($coffeeSample['CoffeeSample']['qta_bag']); ?>&nbsp;</td>
                            <td><?php echo h($coffeeSample['Client']['business_name']); ?>&nbsp;</td>

                            <td>
                                    <?php 
                                    $dataTypeCoffeeSample="";
                                    foreach($coffeeSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                                        $key = $typeSampleCoffe['type_sample_coffee_id'];
                                        $dataTypeCoffeeSample .= $typeSampleCoffes[$key]." ";
                                    }
                                    echo $dataTypeCoffeeSample; ?>
                            </td>

                            <td id="tdStatus"><?php echo h($coffeeSample['CoffeeSample']['status']); ?>&nbsp;</td>
                            <td><?php echo h($coffeeSample['CoffeeSample']['created_date']); ?>&nbsp;</td>
                            <td id="tdStatus"><?php echo h($coffeeSample['CoffeeSample']['operation_center']); ?>&nbsp;</td>
                            <td id="tdStatus"><?php echo h($coffeeSample['CoffeeSample']['terminal']); ?>&nbsp;</td>
                            <td><?php //echo h($coffeeSample['CoffeeSample']['register_users_id']); ?>&nbsp;</td>

                            <td class="actions">
                                    <?php echo $this->Html->link(__('Ver'), array('controller'=>'CoffeeSamples', 'action' => 'view', $coffeeSample['CoffeeSample']['id'])); ?>
                                    <?php echo ($coffeeSample['CoffeeSample']['status'] == "REGISTRADA") ? $this->Html->link(__('Revisar'), array('controller'=>'CoffeeSamples','action' => 'edit', $coffeeSample['CoffeeSample']['id'])): ""; ?>
                                    <?php echo ($this->Session->read('User.profiles_id') == 1) ? $this->Html->link(__('Procesar'), array('controller'=>'CoffeeSamples', 'action' => 'make', $coffeeSample['CoffeeSample']['id'])): ""; ?>
                                    <?php echo ($coffeeSample['CoffeeSample']['status'] == "COMPLETADA" && $this->Session->read('User.profiles_id') == 1) ? $this->Html->link(__('Cerrar'), array('controller'=>'CoffeeSamples', 'action' => 'complete', $coffeeSample['CoffeeSample']['id'])):""; ?>
                                    <?php echo ($coffeeSample['CoffeeSample']['status'] == "REGISTRADA") ? $this->Html->link(__('Borrar'), array('controller'=>'CoffeeSamples', 'action' => 'delete', $coffeeSample['CoffeeSample']['id'])):""; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                        </table>
                
</div>
<script type="text/javascript">
    
    tdChangeColor();
    
    function tdChangeColor(){
        var col;
        $("tr td:nth-child(6)").each(function(i) {
            col = $(this);
            if(col.html() == "COMPLETADA&nbsp;"){
                col.css('background', 'yellow');
            }
            else if(col.html() == "REGISTRADA&nbsp;"){
                col.css('background', 'red');
            }
            else if(col.html() == "CERRADA&nbsp;"){
                col.css('background', 'green');
            }
        })
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Lista de cargues'), array('action' => 'index')); ?> </li>
		<li><?php 
                if($this->Session->read('User.profiles_id')==9){
                    echo $this->Html->link(__('Menú'), array('controller'=>'Pages','action' => 'invitado')); 
                }
                else{
                    echo $this->Html->link(__('Menú'), array('controller'=>'Pages','action' => 'colaborador')); 
                }
                
                
                ?> </li>
	</ul>
</div>
