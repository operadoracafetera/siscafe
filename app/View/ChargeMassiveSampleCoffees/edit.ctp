<div class="chargeMassiveSampleCoffees form">
<?php echo $this->Form->create('ChargeMassiveSampleCoffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Charge Massive Sample Coffee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name_file');
		echo $this->Form->input('date_reg');
		echo $this->Form->input('count_insert');
		echo $this->Form->input('user_reg');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ChargeMassiveSampleCoffee.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ChargeMassiveSampleCoffee.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Charge Massive Sample Coffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
