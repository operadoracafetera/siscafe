<div class="supplies form">
<?php echo $this->Form->create('Supply'); ?>
	<fieldset>
		<legend><?php echo __('Agregar nuevo suministro'); ?></legend>
	<?php
		echo $this->Form->input('name',array('type' => 'text','label' => 'Nombre del suministro','required'=>true));
		echo $this->Form->input('description',array('type' => 'text','label' => 'Descripción','required'=>true));
		echo $this->Form->input('saldo',array('type' => 'text','label' => 'Saldo de unidades','required'=>true));
		//echo $this->Form->input('created_date');
		//echo $this->Form->input('updated_date');
		echo $this->Form->input('observation',array('type' => 'text','label' => 'Observaciones'));
		echo $this->Form->input('jetty',array('type' => 'select','label' => 'Terminal Portuario','options' => array('COMPAS'=>'COMPAS','SPRC'=>'SPRC','SPB'=>'SPB','BLOC PORT'=>'BLOC PORT'),'multiple' => false,'required'=>true,'empty' => true));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado Suministros'), array('action' => 'index')); ?></li>
	</ul>
</div>
