<div class="supplies index">
	<h2><?php echo __('Listado maestro suministros '); ?></h2>
        <h1><?php echo $this->Html->link(__('Nuevo suministro'), array('action' => 'add')); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('saldo','SALDO'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date'); ?></th>
			<th><?php echo $this->Paginator->sort('updated_date'); ?></th>
			<th><?php echo $this->Paginator->sort('observation'); ?></th>
			<th><?php echo $this->Paginator->sort('jetty','Puerto Maritimo'); ?></th>
			<th class="actions"><?php echo __('Acción'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($supplies as $supply): ?>
	<tr>
		<td><?php echo h($supply['Supply']['id']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['name']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['description']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['saldo']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['created_date']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['updated_date']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['observation']); ?>&nbsp;</td>
		<td><?php echo h($supply['Supply']['jetty']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $supply['Supply']['id'])); ?>
                        <?php echo $this->Html->link(__('Movimientos'), array('controller'=>'SuppliesOperations','action' => 'index', $supply['Supply']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('antes'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		
                <li><?php echo $this->Html->link(__('Menu'), array('controller'=>'Pages','action' => 'colaborador')); ?></li>
	</ul>
</div>
