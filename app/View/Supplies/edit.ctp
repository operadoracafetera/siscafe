<div class="supplies form">
<?php echo $this->Form->create('Supply'); ?>
	<fieldset>
		<legend><?php echo __('Modificar registro Suministro'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('label' => 'Nombre del suministro'));
		echo $this->Form->input('description',array('label' => 'Descripción'));
		//echo $this->Form->input('saldo');
		//echo $this->Form->input('created_date');
		//echo $this->Form->input('updated_date');
		echo $this->Form->input('observation',array('label' => 'Observación'));
		echo $this->Form->input('jetty',array('label' => 'Terminal portuario'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Actualizar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado Suministros'), array('action' => 'index')); ?></li>
	</ul>
</div>
