<div class="supplies view">
<h2><?php echo __('Supply'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Saldo'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['saldo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated Date'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['updated_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jetty'); ?></dt>
		<dd>
			<?php echo h($supply['Supply']['jetty']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Supply'), array('action' => 'edit', $supply['Supply']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Supply'), array('action' => 'delete', $supply['Supply']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $supply['Supply']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supply'), array('action' => 'add')); ?> </li>
	</ul>
</div>
