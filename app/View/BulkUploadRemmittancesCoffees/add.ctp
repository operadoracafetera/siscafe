<div class="bulkUploadRemmittancesCoffees form">
<?php echo $this->Form->create('BulkUploadRemmittancesCoffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Subir Plano descargue (Cargue masivo Lotes Café)'); ?></legend>
	<?php
		echo $this->Form->label('name_file','Cargar archivo (Solo un archivo XLS )');
		echo $this->Form->file('name_file');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Cargar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Lista cargues'), array('action' => 'index')); ?></li>
	</ul>
</div>
