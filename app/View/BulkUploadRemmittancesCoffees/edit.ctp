<div class="bulkUploadRemmittancesCoffees form">
<?php echo $this->Form->create('BulkUploadRemmittancesCoffee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Bulk Upload Remmittances Coffee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name_file');
		echo $this->Form->input('date_reg');
		echo $this->Form->input('count_insert');
		echo $this->Form->input('user_reg');
		echo $this->Form->input('msg_error');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BulkUploadRemmittancesCoffee.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('BulkUploadRemmittancesCoffee.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Bulk Upload Remmittances Coffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
