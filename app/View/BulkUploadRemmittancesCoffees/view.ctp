<div class="chargeMassiveSampleCoffees view">
<h2><?php echo __('Ver Descargue lotes de Café (Cargue masivo #'.$bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['id'].' )'); ?></h2>
	<dl>
		<dt><?php echo __('Nombre del archivo cargado'); ?></dt>
		<dd>
			<?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['name_file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Registro'); ?></dt>
		<dd>
			<?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['date_reg']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cant. registros exitosos'); ?></dt>
		<dd>
			<?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['count_insert']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php 
                        echo h($bulkUploadRemmittancesCoffee['Users']['username']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Errores'); ?></dt>
                <dd>
			<?php echo h($bulkUploadRemmittancesCoffee['BulkUploadRemmittancesCoffee']['msg_error']); ?>
			&nbsp;
		</dd>
                </dl>
                <h2><?php echo ('Listado de lotes de café (Remesas de café)') ?></h2>
                <table id="tableData" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id','C.O-Remesa ID'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_caffee','Numero Lote'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fecha Rad'); ?></th>
                            <th><?php echo $this->Paginator->sort('state_operation_id','Estado Operativo'); ?></th>
                            <th><?php echo $this->Paginator->sort('slotStore','Posición'); ?></th>
                            <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad In'); ?></th>
                            <th><?php echo $this->Paginator->sort('total_weight_net_nominal','Peso Nominal'); ?></th>
                            <th><?php echo $this->Paginator->sort('vehicle_plate','Placa Vehiculo'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($remmittancesCoffees as $remmittancesCoffee): ?>
                        <tr>
                            <td><?php 
                            //debug($remmittancesCoffee);exit;
                            echo h($remmittancesCoffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                            <td><?php echo h('3-'.$remmittancesCoffee['Client']['exporter_code'].'-'.$remmittancesCoffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                            <td><?php echo h($remmittancesCoffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
                            <td id="tdStatus"><?php echo h($remmittancesCoffee['StateOperation']['name']); ?>&nbsp;</td>
                            <td><?php echo h($remmittancesCoffee['SlotStore']['name_space']); ?>&nbsp;</td>
                            <td><?php echo h($remmittancesCoffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</td>
                            <td><?php echo h($remmittancesCoffee['RemittancesCaffee']['total_weight_net_nominal']); ?>&nbsp;</td>
                            <td><?php echo h($remmittancesCoffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>

                            <td class="actions">
                               <?php echo $this->Html->link(__('Ver Pesos'), array('controller'=>'RemittancesCaffees','action' => 'weights', $remmittancesCoffee['RemittancesCaffee']['id']),array('target' => "_blank"));?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                        </table>
                
</div>
<script type="text/javascript">
    
    tdChangeColor();
    
    function tdChangeColor(){
        var col;
        $("tr td:nth-child(6)").each(function(i) {
            col = $(this);
            if(col.html() == "COMPLETADA&nbsp;"){
                col.css('background', 'yellow');
            }
            else if(col.html() == "REGISTRADA&nbsp;"){
                col.css('background', 'red');
            }
            else if(col.html() == "CERRADA&nbsp;"){
                col.css('background', 'green');
            }
        })
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Lista de cargues'), array('action' => 'index')); ?> </li>
		<li><?php 
                    echo $this->Html->link(__('Menú'), array('controller'=>'Pages','action' => 'colaborador'));
                ?> </li>
	</ul>
</div>
