<div class="viewTrackgingPackagings index">
	<h2><?php echo __('Trazabilidad Embalajes (Registro Fotograficos)'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('oie'); ?></th>
			<th><?php echo $this->Paginator->sort('state_packaging'); ?></th>
			<th><?php echo $this->Paginator->sort('date_oie'); ?></th>
			<th><?php echo $this->Paginator->sort('iso'); ?></th>
			<th><?php echo $this->Paginator->sort('cta_coffee'); ?></th>
			<th><?php echo $this->Paginator->sort('lotes'); ?></th>
			<th><?php echo $this->Paginator->sort('cargolot'); ?></th>
			<th><?php echo $this->Paginator->sort('remesas'); ?></th>
			<th><?php echo $this->Paginator->sort('seal_1'); ?></th>
			<th><?php echo $this->Paginator->sort('seal_2'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($viewTrackgingPackagings as $viewTrackgingPackaging): ?>
	<tr>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['oie']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['state_packaging']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['date_oie']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['iso']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['cta_coffee']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['lotes']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['cargolot']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['remesas']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['seal_1']); ?>&nbsp;</td>
		<td><?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['seal_2']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Fotografiar'), array('controller'=>'OperationTrackings','action' => 'addctns', $viewTrackgingPackaging['ViewTrackgingPackaging']['oie']),array('target'=>'_blank')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listar Conts'), array('action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Cont'), array('action' => 'search')); ?></li>
	</ul>
</div>
