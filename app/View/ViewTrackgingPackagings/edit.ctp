<div class="viewTrackgingPackagings form">
<?php echo $this->Form->create('ViewTrackgingPackaging'); ?>
	<fieldset>
		<legend><?php echo __('Edit View Trackging Packaging'); ?></legend>
	<?php
		echo $this->Form->input('oie');
		echo $this->Form->input('state_packaging');
		echo $this->Form->input('date_oie');
		echo $this->Form->input('iso');
		echo $this->Form->input('cta_coffee');
		echo $this->Form->input('lotes');
		echo $this->Form->input('cargolot');
		echo $this->Form->input('remesas');
		echo $this->Form->input('seal_1');
		echo $this->Form->input('seal_2');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ViewTrackgingPackaging.oie')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ViewTrackgingPackaging.oie')))); ?></li>
		<li><?php echo $this->Html->link(__('List View Trackging Packagings'), array('action' => 'index')); ?></li>
	</ul>
</div>
