<div class="viewTrackgingPackagings view">
<h2><?php echo __('View Trackging Packaging'); ?></h2>
	<dl>
		<dt><?php echo __('Oie'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['oie']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State Packaging'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['state_packaging']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Oie'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['date_oie']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Iso'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['iso']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cta Coffee'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['cta_coffee']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lotes'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['lotes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cargolot'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['cargolot']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remesas'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['remesas']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal 1'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['seal_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal 2'); ?></dt>
		<dd>
			<?php echo h($viewTrackgingPackaging['ViewTrackgingPackaging']['seal_2']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit View Trackging Packaging'), array('action' => 'edit', $viewTrackgingPackaging['ViewTrackgingPackaging']['oie'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete View Trackging Packaging'), array('action' => 'delete', $viewTrackgingPackaging['ViewTrackgingPackaging']['oie']), array('confirm' => __('Are you sure you want to delete # %s?', $viewTrackgingPackaging['ViewTrackgingPackaging']['oie']))); ?> </li>
		<li><?php echo $this->Html->link(__('List View Trackging Packagings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New View Trackging Packaging'), array('action' => 'add')); ?> </li>
	</ul>
</div>
