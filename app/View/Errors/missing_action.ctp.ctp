<div class="tallyPackagings form">
	<fieldset>
		<legend><?php echo __('Error en la pagina solicitada'); ?></legend>
		<?php echo __('Por favor dar clic para salir del sistema,'); ?>
		<?php echo $this->Html->link(__('Salir'), array('controller' => 'pages', 'action' => 'logout', $this->Session->read('User.id'))); ?>
	</fieldset>
</div>
