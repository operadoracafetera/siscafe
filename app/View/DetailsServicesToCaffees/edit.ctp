<div class="detailsServicesToCaffees form">
    <?php
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('jquery-ui.min');
    echo $this->Html->css('jquery-ui.min');
    echo $this->Html->css('style');
    echo $this->Form->create('DetailsServicesToCaffee',array('type' => 'file'));
    ?>
    <fieldset>
        <legend><?php echo __('Agregar servicios a la orden ' . $this->request->data['DetailsServicesToCaffee']['service_package_id']); ?></legend>
        <h2><?php echo __('Listado de remesas'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date', 'Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id', 'Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee', 'Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out', 'Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags', 'Sacos disponibles'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store', 'Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store', 'Sacos Out'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
                <tr>
                    <td><?php echo h($remittancesCaffee['RemittancesCaffee']['created_date']); ?>&nbsp;</td>
                    <td><?php echo h($remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                    <td><?php echo h($remittancesCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                    <td><?php echo h($remittancesCaffee['RemittancesCaffee']['total_rad_bag_out']); ?>&nbsp;</td>
                    <td><?php echo h(($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] - $remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'])); ?>&nbsp;</td>
                    <?php if ($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']) { ?>
                        <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?></td>
                    <?php } else { ?>
                        <td>0</td>
                    <?php } ?>
                    <?php if ($remittancesCaffee['RemittancesCaffee']['quantity_bag_out_store']) { ?>
                        <td><?php echo h($remittancesCaffee['RemittancesCaffee']['quantity_bag_out_store']); ?></td>
                    <?php } else { ?>
                        <td>0</td>
                    <?php } ?>
                    <td class="actions">
                        <a href="javascript:void(0)" onclick="addRemittance('<?= $remittancesCaffee['RemittancesCaffee']['id'] ?>');">Agregar</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
            ?>	</p>
        <div class="paging">
            <?php
            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
        </div>
        <table>
            <tr>
                <td><?php
                    echo $this->Form->input('id',array('type' => 'hidden'));
                    echo $this->Form->input('services_orders_id', array('type' => 'hidden', 'value' => $this->request->data['DetailsServicesToCaffee']['services_orders_id']));
                    echo $this->Form->input('remittances_caffee_id', array('type' => 'hidden', 'required' => true, 'value'=>$this->request->data['DetailsServicesToCaffee']['remittances_caffee_id']));
                    echo $this->Form->input('remittances_caffee', array('label' => 'Remesa', 'disabled' => 'disabled', 'type' => 'text', 'value'=>$this->request->data['DetailsServicesToCaffee']['remittances_caffee_id']));
                    ?></td>
                <td><?php echo $this->Form->input('qta_bag_to_work', array('label' => 'Cantidad de sacos', 'required' => true)); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('service_package', array('label' => 'Servicio', 'required' => true, 'type' => 'text','value'=> $listServicePackages[$this->request->data['DetailsServicesToCaffee']['service_package_id']])); ?></td>
                <td><?php echo $this->Form->input('completed', array('label' => 'Completado')); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('observation', array('label' => 'Observaciones', 'type' => 'textarea')); ?></td>
                <td><?php
                    echo $this->Form->label('document', 'Adjuntar documento');
                    echo $this->Html->link(__('Ver Documento'), '/'.$this->request->data['DetailsServicesToCaffee']['document']);
                    echo $this->Form->file('document');
                    ?></td>
            </tr>
        </table>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript">
    var servicePackages = [<?php echo "'" . implode("','", $servicePackages) . "'"; ?>];
    $("#DetailsServicesToCaffeeServicePackage").autocomplete({
        source: servicePackages
    });

    function addRemittance(remittanceId) {
        $('#DetailsServicesToCaffeeRemittancesCaffee').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val("");
        $('#DetailsServicesToCaffeeQuantityRadicatedBagOut').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffee').val(remittanceId);
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val(remittanceId);
    }

</script>

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Ordenes de Servicio'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
    </ul>
</div>

