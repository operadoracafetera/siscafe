<div class="detailsServicesToCaffees form">
    <?php
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('jquery-ui.min');
    echo $this->Html->css('jquery-ui.min');
    echo $this->Html->css('style');
    echo $this->Form->create('DetailsServicesToCaffee',array('type' => 'file'));
    ?>
    <fieldset>
        <legend><?php echo __('Agregar servicios a la orden ' . $serviceOrderId); ?></legend>
        <h3 style="text-decoration: underline;"><?php echo ($serviceOrder['ServicesOrder']['instructions'] != null)?'Instrucciones del Cliente: (Autogestión Cliente)':''; ?></h3>
        <h3 style="color: green; background-color: #ffff42"><?php echo __($serviceOrder['ServicesOrder']['instructions']); ?></h3>
        <h3><?php echo __('Buscar información de Café - (Id - Lote - Exportador - CargoLot)'); ?></h3>
        <h2><?php echo __($client['Client']['business_name']); ?></h2>
            <table>
                <tr>
                    <td><?php echo $this->Form->input('remittance_id',array('label' => 'Remesa', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('lot',array('label' => 'Lote', 'type'=>'text')); ?></td>
                    <td><?php echo $this->Form->input('cargolot_id',array('label' => 'Cargo Lot Id', 'type'=>'text')); ?></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><?php //echo $this->Form->input('exporter_code',array('label' => 'Exportador Codigo', 'type'=>'text')); ?></td>
                    <td><?php //echo $this->Form->input('exporter_name',array('label' => 'Exportador', 'disabled' => 'disabled')); ?></td>
                </tr>
            </table>
        <h2><?php echo __('Resultado de la busqueda'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('cargolot_id','Cargolot ID'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('client','Cliente'); ?></th>
                    <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos Rad In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
    </fieldset>
        <table>
            <tr>
                <td><?php
                    echo $this->Form->input('services_orders_id', array('type' => 'hidden', 'value' => $serviceOrderId));
                    echo $this->Form->input('remittances_caffee_id', array('type' => 'hidden'));
                    echo $this->Form->input('expo_code', array('type' => 'hidden', 'required' => true, 'value' => $serviceOrder['ServicesOrder']['exporter_code']));
                    echo $this->Form->input('remittances_caffee', array('label' => 'Remesa asociada', 'disabled' => 'disabled', 'type' => 'text'));
                    ?></td>
                <td><?php echo $this->Form->input('qta_bag_to_work', array('label' => 'Cantidad de unidades', 'required' => true)); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('service_package', array('label' => 'Servicio', 'required' => true, 'type' => 'text')); ?></td>
                <td><?php echo $this->Form->input('lock_remesa', array('label' => 'Bloquear CargoLotId (Remesa)','type' => 'checkbox','checked'=>'checked')); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('observation', array('label' => 'Nota aclaratoria - Observaciones', 'required' => true, 'type' => 'textarea')); ?></td>
                <td><?php
                    echo $this->Form->label('document', 'Adjuntar documento');
                    echo $this->Form->file('document');
                    ?></td>
            </tr>
        </table>
    </fieldset>
    <?php echo $this->Form->end(__('Registrar')); ?>
</div>
<script type="text/javascript">
    var servicePackages = [<?php echo "'" . implode("','", $servicePackages) . "'"; ?>];
    var temp = $("#DetailsServicesToCaffeeServicePackage").autocomplete({
        source: servicePackages,
         select: function( event , ui ) {
             var packageName = ui.item.label;
             if(packageName.includes("DEVOLUCIÓN CAFE")){
                 $("#DetailsServicesToCaffeeDesactive").removeAttr("disabled");
             }
             else{
                 $("#DetailsServicesToCaffeeDesactive").attr("disabled", true);
             }
         }
    });
    
    $("#DetailsServicesToCaffeeRemittanceId").keyup(function () {
        findRemittancesByRemittanceIdByClient($(this).val());
    });
    
    $("#DetailsServicesToCaffeeLot").keyup(function () {
        findRemittancesByLotByClient($(this).val());
    });
    
    $("#DetailsServicesToCaffeeCargolotId").keyup(function () {
        findRemittancesByCargoLotByClient($(this).val());
    });
        
    function findRemittancesByCargoLotByClient(cargolot){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var clientCargoLot = client+"_"+cargolot;
        console.log(clientCargoLot);
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByCargoLotIdClient/" + clientCargoLot,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function findRemittancesByLotByClient(lotCoffee){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var clientLotCoffee = client+"-"+lotCoffee;
        console.log(clientLotCoffee);
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByLotClient/" + clientLotCoffee,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function findRemittancesByRemittanceIdByClient(remittance){
        var client = $("#DetailsServicesToCaffeeExpoCode").val();
        var remittanceClient = remittance+"-"+client;
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByRemittancesClient/" + remittanceClient,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }
    
    function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        $.each(remittancesData, function (i, value) {
		console.log(value['RemittancesCaffee']);
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['cargolot_id'] + '</td>';
            trHTML += '<td>' + '3-'+value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee'] + '</td>';
            trHTML += '<td>' + value['Client']['business_name'] + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['total_rad_bag_out'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="addRemittance(\'' + remittanceId.toString() + '\');">Asociar</a></td>';
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }
    
    function addRemittance(remittanceId) {
        $('#DetailsServicesToCaffeeRemittancesCaffee').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val("");
        $('#DetailsServicesToCaffeeQuantityRadicatedBagOut').val("");
        $('#DetailsServicesToCaffeeRemittancesCaffee').val(remittanceId);
        $('#DetailsServicesToCaffeeRemittancesCaffeeId').val(remittanceId);
    }

</script>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Ordenes de Servicio'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
    </ul>
</div>

