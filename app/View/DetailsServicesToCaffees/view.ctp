<div class="detailsServicesToCaffes view">
<h2><?php echo __('Services Order'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($servicesOrder['DetailsServicesToCaffe']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($servicesOrder['DetailsServicesToCaffe']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Closed Date'); ?></dt>
		<dd>
			<?php echo h($servicesOrder['DetailsServicesToCaffe']['closed_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img Letter'); ?></dt>
		<dd>
			<?php echo h($servicesOrder['DetailsServicesToCaffe']['img_letter']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Services Order'), array('action' => 'edit', $servicesOrder['DetailsServicesToCaffe']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Services Order'), array('action' => 'delete', $servicesOrder['DetailsServicesToCaffe']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $servicesOrder['DetailsServicesToCaffe']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Services Orders'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Services Order'), array('action' => 'add')); ?> </li>
	</ul>
</div>
