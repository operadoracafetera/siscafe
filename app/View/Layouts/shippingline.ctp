<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo "C.I.E" ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header">
                    <div id="logo">
                        <h1><?php echo "Proceso de Embalaje de Contenedores de Café - C.O.P.C.S.A" ?></h1>
                    </div>
                    <div id="user-settings">
                        <h1><?php echo "Bienvenido: ".$this->Session->read('User.first_name')." ".$this->Session->read('User.last_name')?></h1>
                    </div>
		</div>
                
		<div id="content">
                    
                    <div id="breadcrumbs">
			<h1><?php echo $this->Html->getCrumbs(' > ', array(
                            'text' => 'Home',
                            'url' => array('controller' => 'pages', 'action' => 'shippingline'),
                            'escape' => false
                        ));?></h1>
                    </div>

			<?php echo $this->Flash->render(); ?>
                        
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<?php /*echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
				);*/
			?>
                    <?php //echo $this->element('sql_dump');  ?>
		</div>
	</div>
	
</body>
</html>
