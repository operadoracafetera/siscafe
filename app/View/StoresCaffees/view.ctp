<div class="storesCaffees view">
<h2><?php echo __('Stores Caffee'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($storesCaffee['StoresCaffee']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store Name'); ?></dt>
		<dd>
			<?php echo h($storesCaffee['StoresCaffee']['store_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City Location'); ?></dt>
		<dd>
			<?php echo h($storesCaffee['StoresCaffee']['city_location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($storesCaffee['StoresCaffee']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated Date'); ?></dt>
		<dd>
			<?php echo h($storesCaffee['StoresCaffee']['updated_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Stores Caffee'), array('action' => 'edit', $storesCaffee['StoresCaffee']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Stores Caffee'), array('action' => 'delete', $storesCaffee['StoresCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $storesCaffee['StoresCaffee']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Stores Caffees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stores Caffee'), array('action' => 'add')); ?> </li>
	</ul>
</div>
