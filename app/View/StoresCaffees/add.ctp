<div class="storesCaffees form">
<?php echo $this->Form->create('StoresCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Add Stores Caffee'); ?></legend>
	<?php
		echo $this->Form->input('store_name');
		echo $this->Form->input('city_location');
		echo $this->Form->input('created_date');
		echo $this->Form->input('updated_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Stores Caffees'), array('action' => 'index')); ?></li>
	</ul>
</div>
