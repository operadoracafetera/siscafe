<div class="RemittancesCaffeeHasNoveltysCaffees form">
<?php echo $this->Form->create('RemittancesCaffeeHasNoveltysCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Gestionar novedades a la remesa '.$remittancesCaffee['RemittancesCaffee']['id']); ?></legend>
                <h3 style="color: green; background-color: #ffff42"><?php echo (($flagBlock) == true) ? 'Nota: Actualmente la remesa se encuentra bloqueada para Embalaje':""; ?></h3>
	<?php
		echo $this->Form->input('noveltys_caffee_id',[
				    'type' => 'select',
				    'label'=>'Novedades de café',
				    'multiple' => false,
				    'options' => $noveltysCoffee, 
				    'empty' => true
				]);
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acción'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Regresar'), array('controller'=> 'RemittancesCaffees','action' => 'weights',$remittancesCaffee['RemittancesCaffee']['id'])); ?></li>
	</ul>
</div>
<div class="AdictionalElementsHasProformas form">
	<h2><?php echo __('Listado de bloqueos a la remesa: (Novedades al Café)'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('noveltys_caffee_id','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
			<th><?php echo $this->Paginator->sort('active','Estado'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($remittancesCaffeeHasNoveltysCaffee as $coffeeHasNovelty): ?>
	<tr>
		<td><?php 
                //debug($adictionalElement);exit;
                echo h($coffeeHasNovelty["NoveltysCoffee"]['name']); ?>&nbsp;</td>
		<td><?php echo h($coffeeHasNovelty["RemittancesCaffeeHasNoveltysCaffee"]['created_date']); ?>&nbsp;</td>
		<td><?php echo ($coffeeHasNovelty['RemittancesCaffeeHasNoveltysCaffee']['active'] == 1) ? "ACTIVO":"DESACTIVADO"; ?>&nbsp;</td>
		
		<td class="actions">
			<li><?php echo $this->Html->link(__('Desbloquear'), array('action' => 'delete','?' => 
                                ['remittances_caffee_id' => $coffeeHasNovelty['RemittancesCaffeeHasNoveltysCaffee']['remittances_caffee_id'], 
                                    'noveltys_caffee_id' => $coffeeHasNovelty['RemittancesCaffeeHasNoveltysCaffee']['noveltys_caffee_id']]));
                        
                             ?></li>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>
