<div class="detailPackagingCaffees view">
<?php 
	  echo $this->Html->script('jquery.min');
	  echo $this->Form->create('WeighingPackagingCaffee', array('url' => 'savePackagingPallet')); ?>
    <fieldset>
        <legend><?php echo __('Paletizar Remesa - Salida'); ?></legend>
	<?php
            //debug($packagingCaffee);exit;
		echo $this->Form->input('remittances_cafee_id',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'],'type' => 'hidden'));
                echo $this->Form->input('remesa',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'], 'disabled' => 'disabled'));
		echo $this->Form->input('ref_driver',array( 'label' => 'Ref Motorista' , 'value' => $remittancesCaffee['RemittancesCaffee']['ref_driver']) );
                echo $this->Form->input('id_oie',array('type' => 'hidden','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']));
                echo $this->Form->input('oie',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']." - ".$packagingCaffee['ReadyContainer']['bic_container'], 'disabled' => 'disabled'));
                echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
		//echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."", "MANUAL"=>"MANUAL")));
		echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => $bascules));
		echo $this->Form->input('weight',array( 'label' => 'Peso','type' => 'number', 'disabled' => 'disabled','required' => true) );
                echo $this->Form->input('weight_pallet',array( 'type' => 'hidden') );
                echo $this->Form->input('bags',array( 'label' => 'Cantidad de sacos','type' => 'number','min' => 1 ,'required' => true,'disabled' => 'disabled') );
                echo $this->Form->input('quantity_bag_pallet',array( 'type' => 'hidden') );
		//echo $this->Form->input('weighing_date',array( 'label' => 'Fecha','type' => 'datetime' ) );
	?>  
        <div class="row actions">
            <?php 
                echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addBag();' ) );
                echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removeBag();') );
             ?>
            <?php echo $this->Form->end(__('Guardar peso')); ?>
        </div>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                    <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                    <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody>
		<?php 
		$quantityPallet = 0;
		$quantityBags = 0;
		foreach ($weighingPackagingCaffee as $weighingCaffee): 
			$quantityPallet = $quantityPallet +1;?>
                <tr>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['seq_weight_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['weight_pallet']); ?>&nbsp;</td>
                    <td><?php 
			$quantityBags = $quantityBags + $weighingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet'];
			echo h($weighingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingPackagingCaffee']['weighing_date']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('Repesar'), 'javascript:void(0)',array('id'=>'reweigh','value'=>$weighingCaffee['WeighingPackagingCaffee']['id'])); ?>
                    </td>
                </tr>

		<?php endforeach; ?>
            </tbody>
        </table>

	<?php
		echo $this->Form->input('bags_out_rad',array('label' => 'Sacos X Embalar','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'], 'disabled' => 'disabled'));
                echo $this->Form->input('quantity_pallet',array('label' => 'Pallets embalados', 'disabled' => 'disabled', 'value' => $quantityPallet));
		echo $this->Form->input('quantity_bags',array( 'type' => 'hidden', 'value' => $quantityBags ) );
                echo $this->Form->input('total_bags',array( 'label' => 'Sacos embalados','type' => 'number', 'disabled' => 'disabled','value' => $quantityBags ) );
		$companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
		echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
	?>
        
        <script type="text/javascript">
            
            $("a#reweigh").click(function(e){
                var idPallet = $(this).attr('value');    
                updateWeighPallet(idPallet);
                //console.log(idPallet);
            });
            
            function updateWeighPallet(idPallet){
                var reweigh = $("#WeighingPackagingCaffeeWeight").val();
                var remesa = $("#WeighingPackagingCaffeeRemesa").val();
                var oie = $("#WeighingPackagingCaffeeIdOie").val();
                if(reweigh !== 0){
                    var rebags = $("#WeighingPackagingCaffeeQuantityBagPallet").val();
                    var idBagsWeigh = (idPallet+"-"+rebags+"-"+reweigh);
                    $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/DetailPackagingCaffees/reweighPallet/" + idBagsWeigh,
                    error: function (msg) {
                        alert("Error networking");
                    },
                    success: function (data) {
                        if (data !== "") {
                            var result = JSON.parse(data);
                            alert("Se actualizo el peso del pallet correctamente!");
                            window.location.href = "../DetailPackagingCaffees/view?OIE="+oie+"&remesa="+remesa
                        }
                        else{
                            alert("No se actualizo el peso");
                        }
                    }
                    });
                }
                else{
                    alert("Peso en 0. No se puede actualizar el peso");
                }
            }

            var weigthPallet = 0;

            function addBag() {
                var pallet = parseInt($("#WeighingPackagingCaffeeBags").val());
                $("#WeighingPackagingCaffeeQuantityBagPallet").val(pallet + 1);
                $("#WeighingPackagingCaffeeBags").val(pallet + 1);
            }

            function removeBag() {
                var pallet = parseInt($("#WeighingPackagingCaffeeBags").val());
                $("#WeighingPackagingCaffeeQuantityBagPallet").val(pallet - 1);
                $("#WeighingPackagingCaffeeBags").val(pallet - 1);
            }

            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            $("#WeighingPackagingCaffeeBascule").change(function () {
                if($(this).val() !== "MANUAL"){
                    $("#WeighingPackagingCaffeeWeight").attr("disabled", true);
                    $("#WeighingPackagingCaffeeBags").attr("disabled", true);
                    setInterval(function () {
                        $.ajax({
                            url: $("#WeighingPackagingCaffeeBascule").val(),
                            crossDomain: true,
                            success: function (response) {
                                $("#WeighingPackagingCaffeeWeight").val(parseInt(response));
                                $("#WeighingPackagingCaffeeWeightPallet").val(parseInt(response));
                                setValueQuantityBags(parseInt(response));
                            }
                        });
                    }, 500); //medio segundo
                }else
                {
                    $("#WeighingPackagingCaffeeWeight").removeAttr('disabled');
                    $("#WeighingPackagingCaffeeBags").removeAttr('disabled');
                }
            });
            
            $("#WeighingPackagingCaffeeWeight").keyup(function () {
                var pesoManual = $(this).val();
                $("#WeighingPackagingCaffeeWeightPallet").val(pesoManual);
                $("#WeighingPackagingCaffeeWeight").val(pesoManual)
            });
            
            $("#WeighingPackagingCaffeeBags").keyup(function () {
                var unidades = $(this).val();
                $("#WeighingPackagingCaffeeQuantityBagPallet").val(unidades);
            });

            function setValueQuantityBags(weigthBascule) {
                if (weigthBascule !== weigthPallet) {
                    weigthPallet = weigthBascule;
                    $("#WeighingPackagingCaffeeQuantityBagPallet").val(Math.floor((weigthPallet - 70) / bagWeigth));
                    $("#WeighingPackagingCaffeeBags").val(Math.floor((weigthPallet - 70) / bagWeigth));
                }
            }

        </script> 
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listado Remesas'), array('action' => 'packaging_processing')); ?></li>
    </ul>
</div>