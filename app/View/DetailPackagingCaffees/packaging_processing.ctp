<div class="detailPackagingCaffees index">
    <h2><?php echo __('Listado salida café en proceso '); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('Contenedor'); ?></th>
                <th><?php echo $this->Paginator->sort('P.S'); ?></th>
                <th><?php echo $this->Paginator->sort('Ubic Café'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Rad Out'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Out'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($detailPackagingCaffees as $detailPackagingCaffee): ?>
            <tr>
                <td><?php 
                //debug($detailPackagingCaffee);exit;
                echo h($detailPackagingCaffee['RemittancesCaffee']['id']);?></td>
                <td><?php echo h('3-'.$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code'].'-'.$detailPackagingCaffee['RemittancesCaffee']['lot_caffee']);?></td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['id']); ?></td>
                <td><?php echo (empty($detailPackagingCaffee['PackagingCaffee']['ReadyContainer']['bic_container']) == false) ? $detailPackagingCaffee['PackagingCaffee']['ReadyContainer']['bic_container']:"SIN CTN"; ?></td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['weight_to_out'] == 1 ? "SI" : "NO"); ?></td>
                <td><?php echo h($detailPackagingCaffee['RemittancesCaffee']['SlotStore']['name_space']); ?></td>
                <td style="background-color: #eef126;"><span style="font-size: 15px ;color: black;"><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</span></td>
                <td style="background-color: #f16526;"><span style="font-size: 15px ;color: black;"><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']); ?>&nbsp;</span></td>
                <td style="background-color: #9dd4f5;"><span style="font-size: 15px; color: black;"><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_out_store']); ?>&nbsp;</span></td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td class="actions">
			<?php 
                            if($detailPackagingCaffee['PackagingCaffee']['weight_to_out'] == true){
                                echo $this->Html->link(__('Pesar'), array('action' => 'view', '?' => array('OIE' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'remesa' => $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'])));
                                echo $this->Html->link(__('Finalizar'), 'javascript:void(0)',array('class'=>'end','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']."-".$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'],'onclick'=> 'endWeight();'));
                            }
                            else{
                                echo $this->Html->link(__('COMPLETAR'), 'javascript:void(0)',array('class'=>'end','style'=>'font-size: 20px','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']."-".$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']."-".$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$detailPackagingCaffee['RemittancesCaffee']['lot_caffee'],'onclick'=> 'endWeightToOut();'));
                            }                            
                            ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
    <script type="text/javascript">
    
    function endWeight(e){
        var target = $( event.target );
        if ( target.is( "a.end" ) ) {
          var cadena = target.attr('value');
          var posicion = cadena.split('-');
          var remesaId = posicion[1];
          var oie = posicion[0];
          completeWeightingCoffeeSISCAFE(remesaId,oie);
          //console.log(remesaId+"-"+oie);
        }
    }

    function endWeightToOut(e){
        var target = $( event.target );
        if ( target.is( "a.end" ) ) {
          var cadena = target.attr('value');
          var posicion = cadena.split('-');
          var remesaId = posicion[1];
          var oie = posicion[0];
          var exportador = posicion[2];
          var lote = posicion[3];
          var confirmEmbalaje = confirm("Esta seguro que desea completar el embalaje de la Lote ( "+exportador+" - "+lote+" ). "  );
            if (confirmEmbalaje === true) {
                  completeWeightingCoffeeToOutSISCAFE(remesaId,oie);
            } 
        }
    }
    
    function weightContainerToN4(container, weight){
        console.log(container+" - "+weight);
        $.ajax({
            url: "http://siscafe.copcsa.com:8181/SISCAFE-IntegrationWS/WS/integration/getWeightByContainer?container="+container+"&weight="+weight,
            crossDomain: true,
            error: function(xhr, status, error){
                console.log(xhr.responseText);
                console.log(container+" "+weight);
                window.location.href = "http://siscafe.copcsa.com/DetailPackagingCaffees/packaging_processing";
                alert("Se finalizó el proceso de embalaje del contenedor "+container);
            },
            success: function (data) {
                console.log(data);
            }
        });
    }
    
    function completeWeightingCoffeeSISCAFE(remesaId,oie){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/DetailPackagingCaffees/completeRemmitancesCoffee?OIE="+oie+"&remesa="+remesaId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {//El embalaje se finaliza si y solo si tiene una unidad asignada.
                if (data !== "") {
                    var result = JSON.parse(data);
                    var container = result.ViewPackaging.bic_ctn;
                    console.log(result.ViewPackaging);
                    console.log("container: "+container);
                    var total_pallet_embalados = result.ViewPackaging.total_pallets_embalaje;
                    console.log("total_pallet_embalados: "+total_pallet_embalados);
                    var total_pallets_remesa = result.ViewPackaging.total_pallets_remesa;
                    console.log("total_pallet_remesas: "+total_pallets_remesa);
                    var pctjPallets = (total_pallet_embalados/total_pallets_remesa);
                    console.log("pctjPallets: "+pctjPallets);
                    var tare_estibas = result.ViewPackaging.tare_estibas_descargue;
                    console.log("tare_estibas: "+tare_estibas);
                    var tare_embalaje = (pctjPallets*tare_estibas);
                    console.log("tare_embalaje: "+tare_embalaje);
                    var total_peso_cafe = result.ViewPackaging.total_cafe_empaque;
                    console.log("total_peso_cafe: "+total_peso_cafe);
                    var modalidad = result.ViewPackaging.packaging_mode;
                    console.log("modalidad: "+modalidad);
                    var tare_empaque = result.ViewPackaging.tare_empaque;
                    console.log("tare_empaque: "+tare_empaque);
                    var weightContainerNet = (total_peso_cafe - (tare_embalaje));
                    console.log("weightContainerNet: "+weightContainerNet);
                    if(modalidad === 'Granel'){
                        weightContainerNet = weightContainerNet - tare_empaque;
                    }
                    //weightContainerToN4(oie,weightContainerNet);
		    window.location.href = "http://siscafe.copcsa.com/DetailPackagingCaffees/packaging_processing";
                    alert("Se finalizó el proceso de embalaje del contenedor "+container);
                }
                else{
                    alert("Contenedor no ha sido finalizado, tiene pendiente sacos por embalar � NO tiene contenedor registrado");
		    
                }
            }
        });
    }

    function completeWeightingCoffeeToOutSISCAFE(remesaId,oie){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/DetailPackagingCaffees/completeRemmitancesCoffeeToOut?OIE="+oie+"&remesa="+remesaId+"&weightIn=0",
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {//El embalaje se finaliza si y solo si tiene una unidad asignada.
                if (data !== "") {
                    var result = JSON.parse(data);
                    var container = result.ViewPackaging.bic_ctn;
                    console.log(result.ViewPackaging);
                    console.log("container: "+container);
                    var total_pallet_embalados = result.ViewPackaging.total_pallets_embalaje;
                    console.log("total_pallet_embalados: "+total_pallet_embalados);
                    var total_pallets_remesa = result.ViewPackaging.total_pallets_remesa;
                    console.log("total_pallet_remesas: "+total_pallets_remesa);
                    var pctjPallets = (total_pallet_embalados/total_pallets_remesa);
                    console.log("pctjPallets: "+pctjPallets);
                    var tare_estibas = result.ViewPackaging.tare_estibas_descargue;
                    console.log("tare_estibas: "+tare_estibas);
                    var tare_embalaje = (pctjPallets*tare_estibas);
                    console.log("tare_embalaje: "+tare_embalaje);
                    var total_peso_cafe = result.ViewPackaging.total_cafe_empaque;
                    console.log("total_peso_cafe: "+total_peso_cafe);
                    var modalidad = result.ViewPackaging.packaging_mode;
                    console.log("modalidad: "+modalidad);
                    var tare_empaque = result.ViewPackaging.tare_empaque;
                    console.log("tare_empaque: "+tare_empaque);
                    var weightContainerNet = (total_peso_cafe - (tare_embalaje));
                    console.log("weightContainerNet: "+weightContainerNet);
                    if(modalidad === 'Granel'){
                        weightContainerNet = weightContainerNet - tare_empaque;
                    }
                        //weightContainerToN4(oie,weightContainerNet);
//                        alert("Se finaliz� el proceso de embalaje del contenedor "+container);
                        window.location.href = "/DetailPackagingCaffees/packaging_processing";
               }
                else{                    
                    alert("Contenedor no ha sido finalizado, NO tiene el BIC contenedor registrado");
                    window.location.href = "/DetailPackagingCaffees/packaging_processing";
                }
            }
        });
    }
    
    </script>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=> 'Pages','action' => 'basculero')); ?></li>
        <li><?php echo $this->Html->link(__('Salir'), array('controller' => 'users', 'action' => 'logout',$this->Session->read('User.id'))); ?> </li>
    </ul>
</div>
