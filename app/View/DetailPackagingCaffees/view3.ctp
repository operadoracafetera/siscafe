<div class="detailPackagingCaffees view">
<?php 
        echo $this->Html->script('jquery.min');
        echo $this->Html->script('jquery-ui.min');
        echo $this->Html->css('jquery-ui.min');
	  
	  echo $this->Form->create('WeighingEmptyCoffee', array('url' => 'savePackagingPallet3')); 
          ?>
    <style>      
          
    </style>
    <fieldset>
        <legend><?php 
        echo __('Paletizar Remesa'); ?></legend>
	<?php
		echo $this->Form->input('remittances_caffee_id',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'],'type' => 'hidden'));
		echo $this->Form->input('remesa',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'], 'disabled' => 'disabled'));
		echo $this->Form->input('oie_id',array('type' => 'hidden','value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']));
                echo $this->Form->input('oie',array('value'=>$detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'disabled' => 'disabled'));
                echo $this->Form->input('lote',array( 'label' => 'Lote', 'disabled' => 'disabled' , 'value' => '3-'.$client['Client']['exporter_code'].'-'.$remittancesCaffee['RemittancesCaffee']['lot_caffee']) );
		echo $this->Form->input('bascule',array('label' => 'Seleccione Bascula','empty'=>'(Seleccione...)', 'required' => true,'options' => array("".$user['Bascule']['bascule'].""=>"".$user['Bascule']['name']."","MANUAL"=>"MANUAL")));
		echo $this->Form->input('weight',array( 'label' => 'Peso','type' => 'number','disabled' => 'disabled','required' => true) );
                echo $this->Form->input('weight_pallet',array( 'type' => 'hidden') );
                echo $this->Form->input('bags',array( 'label' => 'Cantidad de sacos','type' => 'number', 'max' => 100 ,'min' => 1 ,'required' => true,'disabled' => 'disabled','default'=>'0') );
                echo $this->Form->input('quantity_bag_pallet',array( 'type' => 'hidden',) );
                //echo $this->Form->input('posicion_final',array('label' => 'posición','type' => 'text'));
                //echo $this->Form->input('posicion_final2',array('label' => 'posición','type' => 'hidden'));
                //echo $this->Form->input('slot_store_id',array('label' => 'id slot','type' => 'hidden'));
	?>  
        <div class="row actions">
            <?php 
                echo $this->Html->link(__('Agregar'),'javascript:void(0)',array('onclick'=> 'addBag();' ) );
                echo $this->Html->link(__('Quitar'),'javascript:void(0)',array('onclick'=> 'removeBag();') );
                echo $this->Form->end(__('Guarda Peso'));
             ?>
        </div>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('sequence','Secuencia'); ?></th>
                    <th><?php echo $this->Paginator->sort('weigth','Peso'); ?></th>
                    <th><?php echo $this->Paginator->sort('sack','Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('date','Fecha'); ?></th>
                </tr>
            </thead>
            <tbody>
		<?php 
		$quantityPallet = 0;
		$quantityBags = 0;
		foreach ($WeighingEmptyCoffee as $weighingCaffee): 
			$quantityPallet = $quantityPallet +1;?>
                <tr>
                    <td><?php echo h($weighingCaffee['WeighingEmptyCoffee']['seq_weight_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingEmptyCoffee']['weight_pallet']); ?>&nbsp;</td>
                    <td><?php 
			$quantityBags = $quantityBags + $weighingCaffee['WeighingEmptyCoffee']['quantity_bag_pallet'];
			echo h($weighingCaffee['WeighingEmptyCoffee']['quantity_bag_pallet']); ?>&nbsp;</td>
                    <td><?php echo h($weighingCaffee['WeighingEmptyCoffee']['weighing_date']); ?>&nbsp;</td>
                </tr>

		<?php endforeach; ?>
            </tbody>
        </table>

	<?php
		echo $this->Form->input('quantity_pallet',array('label' => 'Pallets ingresados', 'disabled' => 'disabled', 'value' => $quantityPallet));
		echo $this->Form->input('quantity_bags',array( 'type' => 'hidden', 'value' => $quantityBags ) );
                echo $this->Form->input('total_bags',array( 'label' => 'Sacos ingresados','type' => 'number', 'disabled' => 'disabled','value' => $quantityBags ) );
                echo $this->Form->input('tare_download',array( 'label' => 'Tara ingreso','type' => 'number', 'disabled' => 'disabled', 'value' => $remittancesCaffee['RemittancesCaffee']['tare_download']) );
		$companyName = $client['Client']['exporter_code']."-".$client['Client']['business_name'];
		echo $this->Form->input('exporter',array( 'label' => 'Exportador', 'disabled' => 'disabled' , 'value' => $companyName) );
	?>
        <script type="text/javascript">

            var weigthPallet = 0;

            function addBag() {
                var pallet = parseInt($("#WeighingEmptyCoffeeBags").val());
                $("#WeighingEmptyCoffeeQuantityPallet").val(pallet + 1);
                $("#WeighingEmptyCoffeeBags").val(pallet + 1);
            }

            function removeBag() {
                var pallet = parseInt($("#WeighingEmptyCoffeeBags").val());
                $("#WeighingEmptyCoffeeQuantityPallet").val(pallet - 1);
                $("#WeighingEmptyCoffeeBags").val(pallet - 1);
            }

            var bagWeigth = <?php echo $bagWeigth['UnitsCaffee']['quantity'];?>;
            $("#WeighingEmptyCoffeeBascule").change(function () {
            if($(this).val() !== "MANUAL"){
                $("#WeighingEmptyCoffeeWeight").attr("disabled", true);
                $("#WeighingEmptyCoffeeBags").attr("disabled", true);
                
                    setInterval(function () {
                        $.ajax({
                            url: $("#WeighingEmptyCoffeeBascule").val(),
                            crossDomain: true,
                            success: function (response) {
                                $("#WeighingEmptyCoffeeWeight").val(parseInt(response));
                                $("#WeighingEmptyCoffeeWeightPallet").val(parseInt(response));
                                setValueQuantityBags(parseInt(response));
                            }
                        });
                    }, 500); //medio segundo
            }
            else{
                $("#WeighingEmptyCoffeeWeight").removeAttr('disabled');
                $("#WeighingEmptyCoffeeBags").removeAttr('disabled');
            }
            });
            
            $("#WeighingEmptyCoffeeWeight").keyup(function () {
                var pesoManual = $(this).val();
                $("#WeighingEmptyCoffeeWeightPallet").val(pesoManual);
                $("#WeighingEmptyCoffeeWeight").val(pesoManual)
            });
            
            $("#WeighingEmptyCoffeeBags").keyup(function () {
                var unidades = $(this).val();
                $("#WeighingEmptyCoffeeQuantityBagPallet").val(unidades);
            });

            function setValueQuantityBags(weigthBascule) {
                if (weigthBascule !== weigthPallet) {
                    weigthPallet = weigthBascule;
                    $("#WeighingEmptyCoffeeQuantityBagPallet").val(Math.floor((weigthPallet - 70) / bagWeigth));
                    $("#WeighingEmptyCoffeeBags").val(Math.floor((weigthPallet - 70) / bagWeigth));
                }
            }

        </script> 
    </fieldset>
    
    <div id="dialogmodalbic" style="width: 200px; height: 100px; overflow-y: scroll;" title="Posiciones de las Bodegas.">
            <div class="banner" style="display: none"></div>
            <fieldset>
               
                <table style="border: 0px solid #C0C0C0; ">
                    <tr>
                        <td style="border: 0px solid #C0C0C0;width: 20%;">
                            <br>
                            <input type="button" id="B2" value="Bodega 2">
                        </td>   
                        <td style="border: 0px solid #C0C0C0;width: 20%;">
                            <br>
                            <input type="button" id="B3" value="Bodega 3">
                        </td>
                        <td style="border: 0px solid #C0C0C0; ">
                            <div>
                                <label for="info_lot">Información Lote Café:</label>
                                <textarea disabled id="info_lot" style="font-size: 20px;margin: 0px; height: 129px; width: 643px;"></textarea>
                            </div>
                        </td>
                    </tr>
                </table>
                
            <div id="panel" >   
                <table class="tb2" style="width:100%">
                   <tr>
                       <td id="67"   class="posicion inactiva">F1-2</td>
                       <td id="83"  class="posicion inactiva">G3-2</td>
                       <td id="82"  class="posicion inactiva">G2-2</td>
                       <td id="81"  class="posicion inactiva">G1-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td  class="posicion inactiva" colspan="2">Basc 2 OUT</td>
                       <td  class="posicion inactiva" colspan="3"></td>
                       <td id="1"  class="posicion inactiva">A1-2</td>
                   </tr>
                   <tr>
                       <td  class="posicion inactiva" colspan="11"></td>
                       <td id="2"  class="posicion inactiva">A2-2</td>
                   </tr>
                   <tr>
                       <td id="68"  class="posicion inactiva">F2-2</td>
                       <td  class="posicion inactiva" colspan="7"></td>
                       <td id="30"  class="posicion inactiva">C1-2</td>
                       <td id="17"  class="posicion inactiva">B1-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="3"  class="posicion inactiva">A3-2</td>
                   </tr>
                   <tr>
                       <td id="69"  class="posicion inactiva">F3-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="55"  class="posicion inactiva" colspan="2">E1-2</td>
                       <td id="43"  class="posicion inactiva" colspan="2">D1-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="31"  class="posicion inactiva">C2-2</td>
                       <td id="18"  class="posicion inactiva">B2-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="4"  class="posicion inactiva">A4-2</td>
                   </tr>
                   <tr>
                       <td id="70"  class="posicion inactiva">F4-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="56"  class="posicion inactiva" colspan="2">E2-2</td>
                       <td id="44"  class="posicion inactiva" colspan="2">D2-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="32"  class="posicion inactiva">C3-2</td>
                       <td id="19"  class="posicion inactiva">B3-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="5"  class="posicion inactiva">A5-2</td>
                   </tr>
                   <tr>
                       <td id="71"  class="posicion inactiva">F5-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="57"  class="posicion inactiva" colspan="2">E3-2</td>
                       <td id="45"  class="posicion inactiva" colspan="2">D3-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="33"  class="posicion inactiva">C4-2</td>
                       <td id="20"  class="posicion inactiva">B4-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="6"  class="posicion inactiva">A6-2</td>
                   </tr>
                   <tr>
                       <td id="72"  class="posicion inactiva">F6-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="58"  class="posicion inactiva" colspan="2">E4-2</td>
                       <td id="46"  class="posicion inactiva" colspan="2">D4-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="34"  class="posicion inactiva">C5-2</td>
                       <td id="21"  class="posicion inactiva">B5-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="7"  class="posicion inactiva">A7-2</td>
                   </tr>
                   <tr>
                       <td id="73"  class="posicion inactiva">F7-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="59"  class="posicion inactiva" colspan="2">E5-2</td>
                       <td id="47"  class="posicion inactiva" colspan="2">D5-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="35"  class="posicion inactiva">C6-2</td>
                       <td id="22"  class="posicion inactiva">B6-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="8"  class="posicion inactiva">A8-2</td>
                   </tr>
                   <tr>
                       <td id="74"  class="posicion inactiva">F8-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="60"  class="posicion inactiva" colspan="2">E6-2</td>
                       <td id="48"  class="posicion inactiva" colspan="2">D6-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="36"  class="posicion inactiva">C7-2</td>
                       <td id="23"  class="posicion inactiva">B7-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="9"  class="posicion inactiva">A9-2</td>
                   </tr>
                   <tr>
                       <td id="75"  class="posicion inactiva">F9-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="61"  class="posicion inactiva" colspan="2">E7-2</td>
                       <td id="49"  class="posicion inactiva" colspan="2">D7-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="37"  class="posicion inactiva">C8-2</td>
                       <td id="24"  class="posicion inactiva">B8-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="10"  class="posicion inactiva">A10-2</td>
                   </tr>
                   <tr>
                       <td id="76"  class="posicion inactiva">F10-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="62"  class="posicion inactiva" colspan="2">E8-2</td>
                       <td id="50"  class="posicion inactiva" colspan="2">D8-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="38"  class="posicion inactiva">C9-2</td>
                       <td id="25"  class="posicion inactiva">B9-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="11"  class="posicion inactiva">A11-2</td>
                   </tr>
                   <tr>
                       <td id="77"  class="posicion inactiva">F11-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="63"  class="posicion inactiva" colspan="2">E9-2</td>
                       <td id="51"  class="posicion inactiva" colspan="2">D9-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="39"  class="posicion inactiva">C10-2</td>
                       <td id="26"  class="posicion inactiva">B10-2<br></td>
                       <td  class="posicion inactiva" width="9%"></td>
                       <td id="12"  class="posicion inactiva">A12-2</td>
                   </tr>
                   <tr>
                       <td id="78"  class="posicion inactiva">F12-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="64"  class="posicion inactiva" colspan="2">E10-2</td>
                       <td id="52"  class="posicion inactiva" colspan="2">D10-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="40"  class="posicion inactiva">C11-2</td>
                       <td id="27"  class="posicion inactiva">B11-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="13"  class="posicion inactiva">A13-2</td>
                   </tr>
                   <tr>
                       <td id="79"  class="posicion inactiva">F13-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="65"  class="posicion inactiva" colspan="2">E11-2</td>
                       <td id="53"  class="posicion inactiva" colspan="2">D11-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="41"  class="posicion inactiva">C12-2</td>
                       <td id="28"  class="posicion inactiva">B12-2<br></td>
                       <td  class="posicion inactiva"></td>
                       <td id="14"  class="posicion inactiva">A14-2</td>
                   </tr>
                   <tr>
                       <td id="80"  class="posicion inactiva">F14-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td id="66"  class="posicion inactiva" colspan="2">E12-2</td>
                       <td id="54"  class="posicion inactiva" colspan="2">D12-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="42"  class="posicion inactiva">C13-2</td>
                       <td id="29"  class="posicion inactiva">B13-2</td>
                       <td  class="posicion inactiva"></td>
                       <td id="15"  class="posicion inactiva">A15-2</td>
                   </tr>
                   <tr>
                       <td  class="posicion inactiva" colspan="11"></td>
                       <td id="16"  class="posicion inactiva">A16-2</td>
                   </tr>
                   <tr>
                       <td  class="posicion inactiva" colspan="8"></td>
                       <td id="323"  class="posicion inactiva">B14-2</td>
                       <td  class="posicion inactiva" colspan="3"></td>
                   </tr>
                   <tr>
                       <td  class="posicion inactiva"></td>
                       <td id="84"  class="posicion inactiva">G4-2</td>
                       <td id="85"  class="posicion inactiva">G5-2</td>
                       <td id="86"  class="posicion inactiva" >G6-2</td>
                       <td id="87"  class="posicion inactiva">G7-2</td>
                       <td id="88"  class="posicion inactiva">G8-2</td>
                       <td id="89"  class="posicion inactiva">G9-2</td>
                       <td id="90"  class="posicion inactiva">G10-2</td>
                       <td id="324"  class="posicion inactiva">B15-2</td>
                       <td  class="posicion inactiva" colspan="2"></td>
                       <td  class="posicion inactiva">Basc 2 IN</td>
                   </tr>
               </table>
            </div>

            <div id="panel2">
                <table class="tb3" style="width:100%">
                    <tr>
                        <td  class="posicion inactiva" colspan="4"></td>
                        <td id="91"  class="posicion inactiva">A1-3</td>
                        <td id="92"  class="posicion inactiva">A2-3</td>
                        <td id="93"  class="posicion inactiva">A3-3</td>
                        <td id="94"  class="posicion inactiva">A4-3</td>
                        <td id="95"  class="posicion inactiva">A5-3</td>
                        <td id="96"  class="posicion inactiva">A6-3</td>
                        <td id="97"  class="posicion inactiva">A7-3</td>
                        <td id="98"  class="posicion inactiva">A8-3</td>
                        <td id="99"  class="posicion inactiva">A9-3</td>
                        <td id="100"  class="posicion inactiva">A10-3</td>
                        <td id="101"  class="posicion inactiva">A11-3</td>
                        <td id="102"  class="posicion inactiva">A12-3</td>
                        <td id="103"  class="posicion inactiva">A13-3</td>
                        <td id="104"  class="posicion inactiva">A14-3</td>
                        <td id="105"  class="posicion inactiva">A15-3</td>
                        <td id="106"  class="posicion inactiva">A16-3</td>
                        <td id="107"  class="posicion inactiva">A17-3</td>
                        <td id="108"  class="posicion inactiva">A18-3</td>
                        <td id="109"  class="posicion inactiva">A19-3</td>
                        <td id="110"  class="posicion inactiva">A20-3</td>
                        <td id="111"  class="posicion inactiva">A21-3</td>
                        <td  class="posicion inactiva" colspan="2">BASC 3 OUT</td>
                        <td  class="posicion inactiva" colspan="2"></td>
                        <td id="112"  class="posicion inactiva">A22-3</td>
                        <td id="113"  class="posicion inactiva">A23-3</td>
                        <td id="114"  class="posicion inactiva">A24-3</td>
                        <td id="115"  class="posicion inactiva">A25-3</td>
                        <td id="116"  class="posicion inactiva">A26-3</td>
                        <td id="117"  class="posicion inactiva">A27-3</td>
                        <td id="118"  class="posicion inactiva">A28-3</td>
                        <td id="119"  class="posicion inactiva">A29-3</td>
                        <td id="120"  class="posicion inactiva">A30-3</td>
                        <td id="121"  class="posicion inactiva">A31-3</td>
                        <td id="122"  class="posicion inactiva">A32-3</td>
                        <td  class="posicion inactiva"></td>
                        <td id="309"  class="posicion inactiva">G1-3</td>
                        <td id="310"  class="posicion inactiva">G2-3</td>
                        <td id="311"  class="posicion inactiva">G3-3</td>
                    </tr>
                    <tr>
                        <td  class="posicion inactiva" colspan="44"></td>
                    </tr>
                    <tr>
                        <td id="123"  class="posicion inactiva">B1-3</td>
                        <td id="124"  class="posicion inactiva">B2-3</td>
                        <td id="125"  class="posicion inactiva">B3-3</td>
                        <td id="126"  class="posicion inactiva">B4-3</td>
                        <td id="127"  class="posicion inactiva">B5-3</td>
                        <td id="128"  class="posicion inactiva">B6-3</td>
                        <td id="129"  class="posicion inactiva">B7-3</td>
                        <td id="130"  class="posicion inactiva">B8-3</td>
                        <td id="131"  class="posicion inactiva">B9-3</td>
                        <td id="132"  class="posicion inactiva">B10-3</td>
                        <td id="133"  class="posicion inactiva">B11-3</td>
                        <td id="134"  class="posicion inactiva">B12-3</td>
                        <td id="135"  class="posicion inactiva">B13-3</td>
                        <td id="136"  class="posicion inactiva">B14-3</td>
                        <td id="137"  class="posicion inactiva">B15-3</td>
                        <td id="138"  class="posicion inactiva">B16-3</td>
                        <td id="139"  class="posicion inactiva">B17-3</td>
                        <td id="140"  class="posicion inactiva">B18-3</td>
                        <td id="141"  class="posicion inactiva">B19-3</td>
                        <td id="142"  class="posicion inactiva">B20-3</td>
                        <td id="143"  class="posicion inactiva">B21-3</td>
                        <td id="144"  class="posicion inactiva">B22-3</td>
                        <td id="145"  class="posicion inactiva">B23-3</td>
                        <td id="146"  class="posicion inactiva">B24-3</td>
                        <td id="147"  class="posicion inactiva">B25-3</td>
                        <td id="148"  class="posicion inactiva">B26-3</td>
                        <td id="325"  class="posicion inactiva">P1-3</td>
                        <td id="149"  class="posicion inactiva">B27-3</td>
                        <td id="150"  class="posicion inactiva">B28-3</td>
                        <td id="151"  class="posicion inactiva">B29-3</td>
                        <td id="152"  class="posicion inactiva">B30-3</td>
                        <td id="153"  class="posicion inactiva">B31-3</td>
                        <td id="154"  class="posicion inactiva">B32-3</td>
                        <td id="155"  class="posicion inactiva">B33-3</td>
                        <td id="156"  class="posicion inactiva">B34-3</td>
                        <td id="157"  class="posicion inactiva">B35-3</td>
                        <td id="158"  class="posicion inactiva">B36-3</td>
                        <td id="159"  class="posicion inactiva">B37-3</td>
                        <td id="160"  class="posicion inactiva">B38-3</td>
                        <td id="161"  class="posicion inactiva">B39-3</td>
                        <td  class="posicion inactiva"></td>
                        <td id="312"  class="posicion inactiva">G4-3</td>
                        <td id="313"  class="posicion inactiva">G5-3</td>
                        <td id="314"  class="posicion inactiva">G6-3</td>
                    </tr>
                    <tr>
                        <td id="162"  class="posicion inactiva">C1-3</td>
                        <td id="163"  class="posicion inactiva">C2-3</td>
                        <td id="164"  class="posicion inactiva">C3-3</td>
                        <td id="165"  class="posicion inactiva">C4-3</td>
                        <td id="166"  class="posicion inactiva">C5-3</td>
                        <td id="167"  class="posicion inactiva">C6-3</td>
                        <td id="168"  class="posicion inactiva">C7-3</td>
                        <td id="169"  class="posicion inactiva">C8-3</td>
                        <td id="170"  class="posicion inactiva">C9-3</td>
                        <td id="171"  class="posicion inactiva">C10-3</td>
                        <td id="172"  class="posicion inactiva">C11-3</td>
                        <td id="173"  class="posicion inactiva">C12-3</td>
                        <td id="174"  class="posicion inactiva">C13-3</td>
                        <td id="175"  class="posicion inactiva">C14-3</td>
                        <td id="176"  class="posicion inactiva">C15-3</td>
                        <td id="177"  class="posicion inactiva">C16-3</td>
                        <td id="178"  class="posicion inactiva">C17-3</td>
                        <td id="179"  class="posicion inactiva">C18-3</td>
                        <td id="180"  class="posicion inactiva">C19-3</td>
                        <td id="181"  class="posicion inactiva">C20-3</td>
                        <td id="182"  class="posicion inactiva">C21-3</td>
                        <td id="183"  class="posicion inactiva">C22-3</td>
                        <td id="184"  class="posicion inactiva">C23-3</td>
                        <td id="185"  class="posicion inactiva">C24-3</td>
                        <td id="186"  class="posicion inactiva">C25-3</td>
                        <td id="187"  class="posicion inactiva">C26-3</td>
                        <td id="326"  class="posicion inactiva">P2-3</td>
                        <td id="188"  class="posicion inactiva">C27-3</td>
                        <td id="189"  class="posicion inactiva">C28-3</td>
                        <td id="190"  class="posicion inactiva">C29-3</td>
                        <td id="191"  class="posicion inactiva">C30-3</td>
                        <td id="192"  class="posicion inactiva">C31-3</td>
                        <td id="193"  class="posicion inactiva">C32-3</td>
                        <td id="194"  class="posicion inactiva">C33-3</td>
                        <td id="195"  class="posicion inactiva">C34-3</td>
                        <td id="196"  class="posicion inactiva">C35-3</td>
                        <td id="197"  class="posicion inactiva">C36-3</td>
                        <td id="198"  class="posicion inactiva">C37-3</td>
                        <td id="199"  class="posicion inactiva">C38-3</td>
                        <td id="200"  class="posicion inactiva">C39-3</td>
                        <td  class="posicion inactiva"></td>
                        <td  class="posicion inactiva" colspan="3"></td>
                    </tr>
                    <tr>
                        <td  class="posicion inactiva" colspan="44"></td>
                    </tr>
                    <tr>
                        <td id="201"  class="posicion inactiva">D1-3</td>
                        <td id="202"  class="posicion inactiva">D2-3</td>
                        <td id="203"  class="posicion inactiva">D3-3</td>
                        <td id="204"  class="posicion inactiva">D4-3</td>
                        <td id="205"  class="posicion inactiva">D5-3</td>
                        <td id="206"  class="posicion inactiva">D6-3</td>
                        <td id="207"  class="posicion inactiva">D7-3</td>
                        <td id="208"  class="posicion inactiva">D8-3</td>
                        <td id="209"  class="posicion inactiva">D9-3</td>
                        <td id="210"  class="posicion inactiva">D10-3</td>
                        <td id="211"  class="posicion inactiva">D11-3</td>
                        <td id="212"  class="posicion inactiva">D12-3</td>
                        <td id="213"  class="posicion inactiva">D13-3</td>
                        <td id="214"  class="posicion inactiva">D14-3</td>
                        <td id="215"  class="posicion inactiva">D15-3</td>
                        <td id="216"  class="posicion inactiva">D16-3</td>
                        <td id="217"  class="posicion inactiva">D17-3</td>
                        <td id="218"  class="posicion inactiva">D18-3</td>
                        <td id="219"  class="posicion inactiva">D19-3</td>
                        <td id="220"  class="posicion inactiva">D20-3</td>
                        <td id="221"  class="posicion inactiva">D21-3</td>
                        <td id="222"  class="posicion inactiva">D22-3</td>
                        <td id="223"  class="posicion inactiva">D23-3</td>
                        <td id="224"  class="posicion inactiva">D24-3</td>
                        <td id="225"  class="posicion inactiva">D25-3</td>
                        <td id="226"  class="posicion inactiva">D26-3</td>
                        <td id="327"  class="posicion inactiva">P3-3</td>
                        <td id="227"  class="posicion inactiva">D27-3</td>
                        <td id="228"  class="posicion inactiva">D28-3</td>
                        <td id="229"  class="posicion inactiva">D29-3</td>
                        <td id="230"  class="posicion inactiva">D30-3</td>
                        <td id="231"  class="posicion inactiva">D31-3</td>
                        <td id="232"  class="posicion inactiva">D32-3</td>
                        <td id="233"  class="posicion inactiva">D33-3</td>
                        <td id="234"  class="posicion inactiva">D34-3</td>
                        <td id="235"  class="posicion inactiva">D35-3</td>
                        <td id="236"  class="posicion inactiva">D36-3</td>
                        <td id="237"  class="posicion inactiva">D37-3</td>
                        <td id="238"  class="posicion inactiva">D38-3</td>
                        <td id="239"  class="posicion inactiva">D39-3</td>
                        <td  class="posicion inactiva"></td>
                        <td  class="posicion inactiva" colspan="3"></td>
                    </tr>
                    <tr>
                        <td id="240"  class="posicion inactiva">E1-3</td>
                        <td id="241"  class="posicion inactiva">E2-3</td>
                        <td id="242"  class="posicion inactiva">E3-3</td>
                        <td id="243"  class="posicion inactiva">E4-3</td>
                        <td id="244"  class="posicion inactiva">E5-3</td>
                        <td id="245"  class="posicion inactiva">E6-3<br></td>
                        <td id="246"  class="posicion inactiva">E7-3</td>
                        <td id="247"  class="posicion inactiva">E8-3</td>
                        <td id="248"  class="posicion inactiva">E9-3</td>
                        <td id="249"  class="posicion inactiva">E10-3</td>
                        <td id="250"  class="posicion inactiva">E11-3</td>
                        <td id="251"  class="posicion inactiva">E12-3</td>
                        <td id="252"  class="posicion inactiva">E13-3</td>
                        <td id="253"  class="posicion inactiva">E14-3</td>
                        <td id="254"  class="posicion inactiva">E15-3</td>
                        <td id="255"  class="posicion inactiva">E16-3</td>
                        <td id="256"  class="posicion inactiva">E17-3</td>
                        <td id="257"  class="posicion inactiva">E18-3</td>
                        <td id="258"  class="posicion inactiva">E19-3</td>
                        <td id="259"  class="posicion inactiva">E20-3</td>
                        <td id="260"  class="posicion inactiva">E21-3</td>
                        <td id="261"  class="posicion inactiva">E22-3<br></td>
                        <td id="262"  class="posicion inactiva">E23-3</td>
                        <td id="263"  class="posicion inactiva">E24-3</td>
                        <td id="264"  class="posicion inactiva">E25-3</td>
                        <td id="265"  class="posicion inactiva">E26-3</td>
                        <td id="328"  class="posicion inactiva">P4-3</td>
                        <td id="266"  class="posicion inactiva">E27-3</td>
                        <td id="267"  class="posicion inactiva">E28-3</td>
                        <td id="268"  class="posicion inactiva">E29-3</td>
                        <td id="269"  class="posicion inactiva">E30-3</td>
                        <td id="270"  class="posicion inactiva">E31-3</td>
                        <td id="271"  class="posicion inactiva">E32-3</td>
                        <td id="272"  class="posicion inactiva">E33-3</td>
                        <td id="273"  class="posicion inactiva">E34-3</td>
                        <td id="274"  class="posicion inactiva">E35-3</td>
                        <td id="275"  class="posicion inactiva">E36-3</td>
                        <td id="276"  class="posicion inactiva">E37-3</td>
                        <td id="277"  class="posicion inactiva">E38-3</td>
                        <td id="278"  class="posicion inactiva">E39-3</td>
                        <td  class="posicion inactiva"></td>
                        <td  class="posicion inactiva" colspan="3"></td>
                    </tr>
                    <tr>
                        <td  class="posicion inactiva" colspan="44"></td>
                    </tr>
                    <tr>
                        <td id="279"  class="posicion inactiva">F1-3</td>
                        <td id="280"  class="posicion inactiva">F2-3</td>
                        <td id="281"  class="posicion inactiva">F3-3</td>
                        <td id="282"  class="posicion inactiva">F4-3</td>
                        <td  class="posicion inactiva" colspan="2"></td>
                        <td  class="posicion inactiva" colspan="2">BASC 3 AUX</td>
                        <td  class="posicion inactiva" colspan="4"></td>
                        <td id="283"  class="posicion inactiva">F5-3</td>
                        <td id="284"  class="posicion inactiva">F6-3</td>
                        <td id="285"  class="posicion inactiva">F7-3</td>
                        <td id="286"  class="posicion inactiva">F8-3</td>
                        <td id="287"  class="posicion inactiva">F9-3</td>
                        <td id="288"  class="posicion inactiva">F10-3</td>
                        <td id="289"  class="posicion inactiva">F11-3</td>
                        <td id="290"  class="posicion inactiva">F12-3</td>
                        <td id="291"  class="posicion inactiva">F13-3</td>
                        <td id="292"  class="posicion inactiva">F14-3</td>
                        <td id="293"  class="posicion inactiva">F15-3</td>
                        <td id="294"  class="posicion inactiva">F16-3</td>
                        <td id="295"  class="posicion inactiva">F17-3</td>
                        <td id="296"  class="posicion inactiva">F18-3</td>
                        <td  class="posicion inactiva" colspan="2">BASC 3 IN</td>
                        <td id="297"  class="posicion inactiva">F19-3</td>
                        <td id="298"  class="posicion inactiva">F20-3</td>
                        <td id="299"  class="posicion inactiva">F21-3</td>
                        <td id="300"  class="posicion inactiva">F22-3</td>
                        <td id="301"  class="posicion inactiva">F23-3</td>
                        <td id="302"  class="posicion inactiva">F24-3</td>
                        <td id="303"  class="posicion inactiva">F25-3</td>
                        <td id="304"  class="posicion inactiva">F26-3</td>
                        <td id="305"  class="posicion inactiva">F27-3</td>
                        <td id="306"  class="posicion inactiva">F28-3</td>
                        <td id="307"  class="posicion inactiva">F29-3</td>
                        <td id="308"  class="posicion inactiva">F30-3</td>
                        <td  class="posicion inactiva"></td>
                        <td  class="posicion inactiva" colspan="3"></td>
                    </tr>
                 </table>
            </div>

            </fieldset>
        </div>
    
    <div id="dialogPosition" title="Mensaje">
        <div class="position" style="display: none"></div>
    </div>
    
    <script type="text/javascript">
        $(function() {
            $('#dialogmodalbic').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: 1200,
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                }
              });
              
            $('#dialogPosition').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: 300,
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                },
                buttons: {
                    "Aceptar": function() {
                      $( this ).dialog( "destroy" );
                    },
                    "Si":function() {
                        $("#WeighingDownloadCaffeePosicionFinal").val($("#WeighingDownloadCaffeePosicionFinal2").val());
                        $( this ).dialog( "destroy" );
                    },
                    "No":function() {
                        $( this ).dialog( "destroy" );
                        $( "#dialogmodalbic" ).dialog( "open" );
                    }
                  }
              });
	});
        
        $("#WeighingDownloadCaffeePosicionFinal").click(function() {
            var lotCoffeeClient = $("#WeighingDownloadCaffeeLote").val();
            lotCoffeeClient = lotCoffeeClient.substring(2, 10);
            if(lotCoffeeClient != ""){
                findRemittancesCoffeeByLotClient(lotCoffeeClient);
            }
            else{
              $("div.position").css("style:display: line");
              var htmlResponse = "<div class='position'><span><br>Codigo Exportador y Lote Cafe VACIOS. Por favor validar.<br><br></div>";
              $("div.position").replaceWith(htmlResponse);
              $(":button:contains('Si')").hide();
              $(":button:contains('No')").hide();
              $("#dialogPosition").dialog( "open" );
            }
        });
        
        function findRemittancesCoffeeByLotClient(lotClient){
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findRemittancesByLotClient/" + lotClient,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    if (data != "") {
                        var remittancesData = JSON.parse(data);
                        var fractionCoffee=0;
                        var idPosi;
                        var posName;
                        $.each(remittancesData, function (i, value) {
                            posName = value['SlotStore']['name_space'];
                            idPosi = value['SlotStore']['id'];
                            fractionCoffee++;
                        });
                        $("div.position").css("style:display: line");
                            var htmlResponse = "<div class='position'><span><br>Existe este número "+fractionCoffee+" de fracciones ubicado en la Posición \n\
                            "+posName+" del mismo Lote "+3+"-"+lotClient+". Por favor revisar el mapa de Bodegas.<br><br> ¿Quiere usar esta ubicación para almacenar este nuevo descargue de Café que esta registrando?<br><br></div>";
                            $("div.position").replaceWith(htmlResponse);
                            $("#WeighingDownloadCaffeePosicionFinal2").val(posName);
                            $("#WeighingDownloadCaffeeSlotStoreId").val(idPosi);
                            $(":button:contains('Aceptar')").hide();
                            $("#dialogPosition").dialog( "open" );
                    }
                    else{
                        $( "#dialogmodalbic" ).dialog( "open" );
                    }
                }
            });
        }
        
        $("#B2").click(function() {
              inactive_slots(2);
              empty_slots(2);
              busy_slots(2);
              getSlotStoreId(); 
              $("#info_lot").val("");

              $("#panel").slideDown("slow");
              $("#panel2").slideUp();     
          });
        $("#B3").click(function() {
                inactive_slots(3);
                empty_slots(3);
                busy_slots(3);
                $("#info_lot").val("");
              getSlotStoreId();

              $("#panel2").slideDown("slow");
              $("#panel").slideUp();     
          });

        $(".posicion").click(function() {      
              $(".activa").click(function() {      
                var slot_store_id = $(this).attr("id");
                $("#WeighingDownloadCaffeeSlotStoreId").val(slot_store_id);   
                var slot = $(this).text();
                $("#WeighingDownloadCaffeePosicionFinal").val(slot);

                $("#panel").slideUp();
                $("#panel2").slideUp();
                $( "#dialogmodalbic" ).dialog( "close" );
                $("#info_lot").val("");               
              });       
           });
            
            function empty_slots(storeId){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/SlotStores/findAvailableSlotStoreByStoreId/"+storeId,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         if(JSON.stringify(json) !== "[]"){
                             var data = json;
                             console.log(data);
                            var i;
                            for (i = 1; i<=392; i++){
                                if(data[i] != null){
                                    $("td#"+i).css('background-color','green');
                                    $("td#"+i).removeClass('inactiva');
                                    $("td#"+i).addClass('activa');
                                }
                            }  
                         }
                         else{
                             $("#WeighingDownloadCaffeePosicionFinal").val("NO ARROJO RESULTADOS");
                         }
                }});
            }

            function busy_slots(storeId){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/SlotStores/findBusySlotStoreByStoreId/"+storeId,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         if(JSON.stringify(json) !== "[]"){
                             var data = json;
                             console.log(data);
                            var i;
                            for (i = 1; i<=392; i++){
                                if(data[i] != null){
                                    $("td#"+i).css('background-color','red');
                                    $("td#"+i).addClass('lote');
                                }
                            }  
                         }
                         else{
                             $("#WeighingDownloadCaffeePosicionFinal").val("NO ARROJO RESULTADOS");
                         }
                }});
            }

          function inactive_slots(storeId){   
            $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/SlotStores/findDisableSlotStoreByStoreId/"+storeId,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         if(JSON.stringify(json) !== "[]"){
                             var data = json;
                             console.log(data);
                            var i;
                            for (i = 1; i<=392; i++){
                                if(data[i] != null){
                                    $("td#"+i).css('background-color','yellow');
                                    $("td#"+i).removeClass('inactiva');
                                    $("td#"+i).addClass('bloqueada');
                                }
                            }  
                         }
                         else{
                             $("#WeighingDownloadCaffeePosicionFinal").val("NO ARROJO RESULTADOS");
                         }
                }});
            }
            
            function getSlotStoreId(){
                $(".posicion").mouseover(function() {
                    var slotStore = $(this).attr("id");
                    var flagClass = $(this).attr("class");
                    if(flagClass === "posicion inactiva lote"){
                        //console.log(slotStore); 
                        $.ajax({
                                type: "GET",
                                datatype: "json",
                                url: "/RemittancesCaffees/findRemittancesBySlotStore/"+slotStore,
                                error: function(msg){alert("Error networking");},
                                success: function(data){
                                     var json = JSON.parse(data);
                                     if(JSON.stringify(json) !== "[]"){
                                        var data = json;
                                        var result="";
                                        var i;
                                        for(i=0; i<data.length;i++){
                                            result += data[i].RemittancesCaffee.id+": "+3+"-"+data[i].Client.exporter_code+"-"+data[i].RemittancesCaffee.lot_caffee+" X "+
                                                    data[i].RemittancesCaffee.quantity_bag_in_store+"\n";
                                        }
                                        $("#info_lot").val("");
                                        $("#info_lot").val(result);
                                     }
                                     else{
                                         $("#info_lot").val("NO ARROJO RESULTADOS");
                                     }
                            }});
                    }
                });
            }
           
        function findLotCoffeeBySlotStore(SlotStoreId){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/SlotStores/findRemittancesBySlotStore/"+SlotStoreId,
                    error: function(msg){alert("Error networking");},
                    success: function(data){
                         var json = JSON.parse(data);
                         if(JSON.stringify(json) !== "[]"){
                             var data = json;
                             console.log(data);
                            
                         }
                         else{
                             $("#WeighingDownloadCaffeePosicionFinal").val("NO ARROJO RESULTADOS");
                         }
                }});
        }
        
        
        function clearAll(){
            $("#modalFindBIC").val("");
            $("#modalFindOIE").val("");
            $("#modalFindAutorizacion").val("");
            $("div.banner").replaceWith('<div class="banner" style="display: none"></div>');
            $("div.banner").css("style:display: line");
        }
        
        function clearField(){
            $("#RemittancesCaffeeCodigo").val("");
            $("#RemittancesCaffeeIdCliente").val("");
            $("#RemittancesCaffeeClient").val("");
        }
    </script>
    
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listado Remesas'), array('action' => 'packaging_empty')); ?></li>
    </ul>
</div>