<div class="detailPackagingCaffees form">
<?php echo $this->Html->script('jquery.min');
    echo $this->Form->create('DetailPackagingCaffee'); ?>
    <fieldset>
        <legend><?php echo __('Agregar Remesas Amparadas a la OIE '.$oie_id); ?></legend>
        <table>
            <tr>
                <td><?php  echo $this->Form->input('packaging_caffee_id',array('type'=>'hidden','value'=>$oie_id));
                //echo $this->Form->input('client_code',array('label' => 'Código Cliente', 'type'=>'text')); ?></td>
                <td><?php //echo $this->Form->input('client_name',array('label' => 'Nombre Cliente','disabled' => 'disabled')); ?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('cargolot_id',array('label' => 'Carlo Lot Id (Auto)', 'type'=>'text')); ?></td>
                <td><?php echo $this->Form->input('remittance_id',array('label' => 'Remesa (Auto)', 'type'=>'text')); ?></td>
                <td><?php echo $this->Form->input('lot',array('label' => 'Expo-Lote Ej.: 001-2235', 'type'=>'text')); ?></td>
            </tr>
        </table>
        <h2><?php echo __('Listado de remesas'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                    <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                    <th><?php echo $this->Paginator->sort('cargolot_id','Cargo Lot Id'); ?></th>
                    <th><?php echo $this->Paginator->sort('state_operation_id','Estado'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                    <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('available_bags','Sacos disponibles'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                    <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                    <th><?php echo $this->Paginator->sort('nolvetys','Bloqueos (Novedades)'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
        </table>
        <table>
            <tr>
                <td><?php echo $this->Form->input('remittances_caffee_id',array('type'=>'hidden', 'required' => true));
                          echo $this->Form->input('remittances_caffee',array('label' => 'Remesa','disabled' => 'disabled' )); ?></td>
                <td><?php echo $this->Form->input('quantity_radicated_bag_out',array('label' => 'Cantidad de sacos','required' => true)); ?></td>
            </tr>
        </table>
    </fieldset>
    <?php echo $this->Form->end(__('Registrar')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('lot_coffee','Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('packaging_caffee_id','OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('quantity_radicated_bag_out','Sacos'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($detailPackagingCaffees as $detailPackagingCaffee): ?>
            <tr>
                <td><?php
                echo h($detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']); ?>&nbsp;</td>
                <td><?php echo h("3-".$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$detailPackagingCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']); ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', '?' => ['oie_id' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'],'remittance_id'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']] )); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">

    $("#DetailPackagingCaffeeRemittanceId").keyup(function () {
        findInfoRemittancesByRemittanceId($(this).val());
    });
    
    $("#DetailPackagingCaffeeCargolotId").keyup(function () {
        findInfoRemittancesByCarlogLotId($(this).val());
    });

    $("#DetailPackagingCaffeeLot").keyup(function () {
        findInfoRemittancesByLot($(this).val());
    });

    function findInfoRemittancesByCarlogLotId(cargolotid) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByCargoLotId/" + cargolotid,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    var remittancesData = JSON.parse(data);
                    console.log(remittancesData[0].RemittancesCaffeeHasNoveltysCaffee);
                    //$("#DetailPackagingCaffeeClientName").val(clientData['Client']['business_name']);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }

    function findInfoRemittancesByRemittanceId(remittanceId) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByRemittanceId/" + remittanceId,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }

    function findInfoRemittancesByLot(lot) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findByLotClient/" + lot,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    var remittancesData = JSON.parse(data);
                    addRemittanceDataToTable(remittancesData);
                }
            }
        });
    }

    function addRemittanceDataToTable(remittancesData) {
        $('#tableRemittances').children().remove();
        $.each(remittancesData, function (i, value) {
            trHTML = '<tr>';
            trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['cargolot_id'] + '</td>';
            trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
            trHTML += '<td>' +'3-'+ value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee']+'</td>';
            trHTML += '<td>' + value['RemittancesCaffee']['total_rad_bag_out'] + '</td>';
            trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_in_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            if (value['RemittancesCaffee']['quantity_bag_out_store'])
                trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
            else
                trHTML += '<td>' + 0 + '</td>';
            var remittanceId = value['RemittancesCaffee']['id'];
            var noveltysCoffee="";
            $.each(value['RemittancesCaffeeHasNoveltysCaffee'], function(n,val){
                noveltysCoffee += val['NoveltysCoffee']['name'] + " - activo: "+val['active']+" | ";
            });
            if(noveltysCoffee === "")
                trHTML += '<td>' + "Sin novedades" + '</td>';
            else
                trHTML += '<td style="color: green; background-color: #ffff42">' + noveltysCoffee + '</td>';
            trHTML += '<td class="actions"><a href="javascript:void(0)" onclick="addRemittance(\'' + remittanceId.toString() + '\');">Agregar</a></td>';
            trHTML += '</tr>';
            $('#tableRemittances').append(trHTML);
        });
    }

    function addRemittance(remittanceId) {
        $('#DetailPackagingCaffeeRemittancesCaffee').val("");
        $('#DetailPackagingCaffeeRemittancesCaffeeId').val("");
        $('#DetailPackagingCaffeeQuantityRadicatedBagOut').val("");
        $('#DetailPackagingCaffeeRemittancesCaffee').val(remittanceId);
        $('#DetailPackagingCaffeeRemittancesCaffeeId').val(remittanceId);
    }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('controller'=> 'PackagingCaffees','action' => 'allOie')); ?></li>
    </ul>
</div>
