<div class="detailPackagingCaffees index">
    <h2><?php echo __('Listado salida café pendiente '); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Remesa'); ?></th>
                <th><?php echo $this->Paginator->sort('Lote'); ?></th>
                <th><?php echo $this->Paginator->sort('OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos In'); ?></th>
                <th><?php echo $this->Paginator->sort('Estado detalle'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Rad Out'); ?></th>
                <th><?php echo $this->Paginator->sort('Sacos Out'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($detailPackagingCaffees as $detailPackagingCaffee): ?>
            <tr>
                <td>
		<?php echo h($detailPackagingCaffee['RemittancesCaffee']['id']);?>
                </td>
                <td><?php echo h('3-'.$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code'].'-'.$detailPackagingCaffee['RemittancesCaffee']['lot_caffee']);?></td>
                <td>
                    <?php echo h($detailPackagingCaffee['PackagingCaffee']['id']); ?>
                </td>
                <td><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_in_store']); ?>&nbsp;</td>
                <td><?php 
                    if($detailPackagingCaffee['DetailPackagingCaffee']['state'] == 1){
                        echo h("EN EMBALAJE");
                    }
                    else if($detailPackagingCaffee['DetailPackagingCaffee']['state'] == 7){
                        echo h("PENDIENTE");
                    }
                 ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']); ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['RemittancesCaffee']['quantity_bag_out_store']); ?>&nbsp;</td>
                <td><?php echo h($detailPackagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td class="actions">
			<?php 
                            echo $this->Html->link(__('Procesar'), array('action' => 'process', '?' => array('OIE' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'], 'remesa' => $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'])));
                         ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Menú'), array('controller'=> 'Pages','action' => 'basculero')); ?></li>
        <li><?php echo $this->Html->link(__('Salir'), array('controller' => 'users', 'action' => 'logout',$this->Session->read('User.id'))); ?>
    </ul>
</div>
