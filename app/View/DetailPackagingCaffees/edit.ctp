<div class="detailPackagingCaffees form">
<?php echo $this->Form->create('DetailPackagingCaffee'); ?>
    <fieldset>
        <legend><?php echo __('Editar remesa amparada a la OIE '.$oie_id); ?></legend>
	<table>
            <tr>
                <td><?php echo $this->Form->input('packaging_caffee_id',array('type'=>'hidden', 'required' => true, 'value'=>$oie_id));
                          echo $this->Form->input('remittances_caffee_id',array('type'=>'hidden', 'required' => true, 'value'=>$remittance_id));
                          echo $this->Form->input('remittances_caffee',array('label' => 'Remesa','disabled' => 'disabled', 'value'=>$remittance_id )); ?></td>
                <td><?php echo $this->Form->input('quantity_radicated_bag_out',array('label' => 'Cantidad de sacos','required' => true)); ?></td>
            </tr>
        </table>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'add', '?' => ['oie_id' => $oie_id] )); ?></li>
    </ul>
</div>
