<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging'); ?>
	<fieldset>
		<legend><?php echo __('Edit Traceability Packaging'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('img1');
		echo $this->Form->input('img2');
		echo $this->Form->input('img3');
		echo $this->Form->input('img4');
		echo $this->Form->input('img5');
		echo $this->Form->input('img6');
		echo $this->Form->input('img7');
		echo $this->Form->input('img8');
		echo $this->Form->input('img9');
		echo $this->Form->input('img10');
		echo $this->Form->input('date_created');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TraceabilityPackaging.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('TraceabilityPackaging.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Traceability Packagings'), array('action' => 'index')); ?></li>
	</ul>
</div>
