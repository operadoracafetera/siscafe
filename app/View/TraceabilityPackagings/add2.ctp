<div class="traceabilityPackagings form">
<?php echo $this->Form->create('TraceabilityPackaging',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Realizar trazabilidad Embalaje '.$packagingCoffee['ReadyContainer']['bic_container']); ?></legend>
	<h3><?php echo __('Sequencia fotografica del embalaje 2'); ?></h3>
	<?php
		echo $this->Form->label('img4','(Ingreso mercancia - Número de lotes - Imagen 4 )');
                echo $this->Form->file('img4');
		echo $this->Form->label('img5','(Llenado del contenedor - Imagen 5 )');
		echo $this->Form->file('img5');
		echo $this->Form->label('img6','(Lllenado del contenedor - Imagen 6 )');
		echo $this->Form->file('img6');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'),$this->request->referer() ); ?></li>
	</ul>
</div>
