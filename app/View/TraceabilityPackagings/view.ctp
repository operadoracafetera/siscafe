<div class="traceabilityPackagings view">
<h2><?php echo __('Traceability Packaging'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img1'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img2'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img3'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img4'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img5'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img6'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img7'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img7']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img8'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img8']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img9'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img9']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Img10'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['img10']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Created'); ?></dt>
		<dd>
			<?php echo h($traceabilityPackaging['TraceabilityPackaging']['date_created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Traceability Packaging'), array('action' => 'edit', $traceabilityPackaging['TraceabilityPackaging']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Traceability Packaging'), array('action' => 'delete', $traceabilityPackaging['TraceabilityPackaging']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $traceabilityPackaging['TraceabilityPackaging']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Traceability Packagings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Traceability Packaging'), array('action' => 'add')); ?> </li>
	</ul>
</div>
