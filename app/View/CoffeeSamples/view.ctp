<div class="coffeeSamples view">
    <h2><?php echo __('Información Solicitud (Muestreo, Trazabilidad)'); ?></h2>
	<dl>
		<dt><?php echo __('Id Muestra'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cant. Sacos'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['qta_bag']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Exportador'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['Client']['exporter_code']."-".$coffeeSample['Client']['business_name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Ref. Lote'); ?></dt>
		<dd>
                    <?php 
                            echo h('3-'.$coffeeSample['CoffeeSample']['lot_coffee_ref']);
                        ?>
		</dd>
		<dt><?php echo __('Tipo de muestra'); ?></dt>
		<dd>
			<?php 
                            $dataTypeCoffeeSample="";
                            foreach($coffeeSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                                $key = $typeSampleCoffe['type_sample_coffee_id'];
                                $dataTypeCoffeeSample .= $typeSampleCoffes[$key]." ";
                            }
                            echo $dataTypeCoffeeSample;  
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Carta solicitud muestreo'); ?></dt>
		<dd>
			<?php  echo ($coffeeSample['CoffeeSample']['img_request_sample']) ? $this->Html->link(__('Ver Documento'), array('controller' => 'CoffeeSamples', 'action' => 'getTrackingRequest',$coffeeSample['CoffeeSample']['id']),array('target' => '_blank')):"NA"; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Archivo de respuesta'); ?></dt>
		<dd>
			<?php echo ($coffeeSample['CoffeeSample']['img_finish_sample']) ? $this->Html->link(__('Descargar'), array('controller' => 'CoffeeSamples', 'action' => 'getTracking',$coffeeSample['CoffeeSample']['id']),array('target' => '_blank')):"NA"; ?>
		</dd>
                <dt><?php echo __('Archivo evidencia Muestra'); ?></dt>
		<dd>
			<?php echo (!empty($coffeeSample['CoffeeSample']['img_response_sample'])) ? $this->Html->link(__('Descargar'), array('controller' => 'CoffeeSamples', 'action' => 'getTrackingEvidence',$coffeeSample['CoffeeSample']['id']),array('target' => '_blank')):"NA"; ?>
		</dd>
		<dt><?php echo __('Estado'); ?></dt>
		<dd>
			<?php 
                        echo h($coffeeSample['CoffeeSample']['status']);
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha registro'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha envio al Exportador'); ?></dt>
		<dd>
                        <?php 
                        if($coffeeSample['CoffeeSample']['send_date'] != null)
                            echo h($coffeeSample['CoffeeSample']['send_date']); 
                        else
                            echo h("NO ENVIADA");
                        ?>
		</dd>
		<dt><?php echo __('Fecha finalización'); ?></dt>
		<dd>
                    <?php 
                        if($coffeeSample['CoffeeSample']['end_date'] != null)
                            echo h($coffeeSample['CoffeeSample']['end_date']); 
                        else
                            echo h("NO FINALIZADA");
                        ?>
		</dd>
                <dt><?php echo __('Puerto Maritimo'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['operation_center']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observación'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('N° Guia Envio'); ?></dt>
		<dd>
                    <?php 
                        /*if($coffeeSample['CoffeeSample']['status'] == "COMPLETADA")
                            echo $this->Html->link(__('Enviar'), array('controller' => 'CoffeeSamples', 'action' => 'send_sample',$coffeeSample['CoffeeSample']['id']));
                        else
                            echo $this->Html->link(__($coffeeSample['CoffeeSample']['tracking_supply_id']), array('controller' => 'CoffeeSamples', 'action' => 'download_shipping',$coffeeSample['CoffeeSample']['id']));
                        */
                        echo h($coffeeSample['CoffeeSample']['tracking_supply']); 
                        ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Empresa de Transporte'); ?></dt>
		<dd>
			<?php echo h($coffeeSample['CoffeeSample']['supply_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Muestreador'); ?></dt>
		<dd>
                    <b><?php echo $coffeeSample['User']['first_name']." ".$coffeeSample['User']['last_name'];?></b>
			&nbsp;
		</dd>
	</dl>
    <h2><?php echo __('Café relacionado a la Solicitud'); ?></h2>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                        <th><?php echo $this->Paginator->sort('lot_coffee','Lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('state_operation','Estado'); ?></th>
                        <th><?php echo $this->Paginator->sort('packing_coffee','Tipo Empaque'); ?></th>
                        <th><?php echo $this->Paginator->sort('quantity_radicated_bag_in','Sacos Rad'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($dataSampleRemmitances as $dataSampleRemmitance): ?>
                    <tr>
                        <td><?php
                        //debug($dataSampleRemmitance);exit;
                        echo h($dataSampleRemmitance['RemittancesCaffee']['id']); ?>&nbsp;</td>
                        <td><?php echo h("3-".$dataSampleRemmitance['RemittancesCaffee']['Client']['exporter_code']."-".$dataSampleRemmitance['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['StateOperation']['name']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['PackingCaffee']['name']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php //echo $this->Html->link(__('Seleccionar'), array('action' => 'edit', '?' => ['oie_id' => $dataSampleRemmitance['DetailPackagingCaffee']['packaging_caffee_id'],'remittance_id'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']] )); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
</div>


<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',$this->Session->read('User.id'))); ?> </li>
	</ul>
</div>
