<div class="coffeeSamples index">
	<h2><?php echo ('Busqueda de solicitudes (Muestras, Trazabilidades)') ?></h2>
        <table>
            <tr>
                <tr>
                    <td><?php echo $this->Form->input('lotSampleCoffee',array('label' => 'Expo-Lote Ej.: 001-02235', 'type'=>'text')); ?></td>
                </tr>
        </table>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('id','Id'); ?></th>
                    <th><?php echo $this->Paginator->sort('lot_coffee_ref','Lot Café'); ?></th>
                    <th><?php echo $this->Paginator->sort('qta_bag','Cant. Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('client_id','Exportador'); ?></th>
                    <th><?php echo $this->Paginator->sort('type_sample_coffe_id','Tipo muestra'); ?></th>
		    <th><?php echo $this->Paginator->sort('status','Estado?'); ?></th>
		    <th><?php echo $this->Paginator->sort('get_sample','Se toma?'); ?></th>
                    <th><?php echo $this->Paginator->sort('terminal','Terminal'); ?></th>
                    <th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody id="tableRemittances">
            </tbody>
       </table>
        
	<p>
</div>
<script type="text/javascript">
    
    
    $("#lotSampleCoffee").keyup(function () {
            findInfoRemittancesByLot($(this).val());
        });
        
    function findInfoRemittancesByLot(lot) {
            var dataLot = lot.split("-");
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/CoffeeSamples/findCoffeeSample/" + lot,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var coffeeSamples = JSON.parse(data);
                        //console.log(coffeeSamples);
                        addRemittanceDataToTable(coffeeSamples);
                    }
                    else{
                        alert("No se encontro NINGUN registro");
                        $('#tableRemittances').children().remove();
                    }
                }
            });
    }
    
    function addRemittanceDataToTable(remittancesData) {
            $('#tableRemittances').children().remove();
            const formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
              });
            $.each(remittancesData, function (i, value) {
                trHTML = '<tr>';
                trHTML += '<td>' + value['CoffeeSample']['id'] + '</td>';
                trHTML += '<td>' + value['CoffeeSample']['lot_coffee_ref'] + '</td>';
                trHTML += '<td>' + value['CoffeeSample']['qta_bag'] + '</td>';
                trHTML += '<td>' + value['Client']['business_name'] + '</td>';
                trHTML += '<td>' + value[0] + '</td>';
                trHTML += '<td>' + value['CoffeeSample']['status'] + '</td>';
		trHTML += '<td>' + (value['CoffeeSample']['get_sample'] ? "SI":"NO") + '</td>';
		trHTML += '<td>' + value['CoffeeSample']['terminal'] + '</td>';
                trHTML += '<td>' + value['CoffeeSample']['created_date'] + '</td>';
                var profile = <?php echo $this->Session->read('User.profiles_id');?>;
                if(profile === 1){
			trHTML += '<td class="actions">'+'<a target="_blank" href="/CoffeeSamples/make/'+value['CoffeeSample']['id']+'">Editar</a></td>';
                }
		else if(value['CoffeeSample']['status'] === 'REGISTRADA' && profile === 9){
			trHTML += '<td class="actions">'+'<a target="_blank" href="/CoffeeSamples/edit/'+value['CoffeeSample']['id']+'">Modificar</a></td>';
			
		}
                trHTML += '</tr>';
                $('#tableRemittances').append(trHTML);
            });
        }
    </script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Registrar solicitudes'), array('controller'=>'ChargeMassiveSampleCoffees','action' => 'index',$this->Session->read('User.id'))); ?></li>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',$this->Session->read('User.id'))); ?> </li>
	</ul>
</div>
