
<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>
<div class="coffeeSamples index">
	<h2><?php echo ('Listado Maestro de solicitudes (Muestras, Trazabilidades)') ?></h2>
   
        <table id="tableData" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id','Id Muestra'); ?></th>
                        <th><?php echo $this->Paginator->sort('lot_coffee_ref','Ref lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('qta_bag','Cant. Sacos'); ?></th>
                        <th><?php echo $this->Paginator->sort('client_id','Exportador'); ?></th>
			
			<th><?php echo $this->Paginator->sort('type_sample_coffe_id','Tipo muestra'); ?></th>
			
                        <th><?php echo $this->Paginator->sort('status','Estado'); ?></th>
			<th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
                        <th><?php echo $this->Paginator->sort('operation_center','Centro de operaciones'); ?></th>
                        <th><?php echo $this->Paginator->sort('terminal','Terminal'); ?></th>
			<th><?php echo $this->Paginator->sort('get_sample','Se Toma?'); ?></th>
                        <th><?php echo $this->Paginator->sort('remittancesCaffee','Lotes Descargados'); ?></th>
                        <th><?php //echo $this->Paginator->sort('charger','Id Cargue Masivo'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($coffeeSamples as $coffeeSample): ?>
	<tr>
		<td><?php 
                echo h($coffeeSample['CoffeeSample']['id']); ?>&nbsp;</td>
                <td><?php echo h('3-'.$coffeeSample['CoffeeSample']['lot_coffee_ref']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['CoffeeSample']['qta_bag']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['Client']['business_name']); ?>&nbsp;</td>
		
		<td>
			<?php 
                        $dataTypeCoffeeSample="";
                        foreach($coffeeSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                            $key = $typeSampleCoffe['type_sample_coffee_id'];
                            $dataTypeCoffeeSample .= $typeSampleCoffes[$key]." ";
                        }
                        echo $dataTypeCoffeeSample; ?>
		</td>
                
                <td id="tdStatus"><?php echo h($coffeeSample['CoffeeSample']['status']); ?>&nbsp;</td>
		<td><?php echo h($coffeeSample['CoffeeSample']['created_date']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['CoffeeSample']['operation_center']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['CoffeeSample']['terminal']); ?>&nbsp;</td>
		<?php echo ($coffeeSample['CoffeeSample']['get_sample']) ? "<td style=background: green><span>SI</span></td>":"<td style=background: red><span>NO</span></td>" ?>
                <td><?php 
                    if($coffeeSample[0]['remittancesCaffee']){
                        foreach($coffeeSample[0]['remittancesCaffee'] as $remittancesCaffees){
                            echo h($remittancesCaffees['RemittancesCaffee']['created_date'] . " x " . 
                                    $remittancesCaffees['RemittancesCaffee']['quantity_radicated_bag_in']). " -> ".
                                    $remittancesCaffees['StateOperation']['name'] . " | ".
				    $remittancesCaffees['SlotStore']['name_space'] . " | ";
                        }
                    }
                    else{
                        echo h("Sin descargar Café");
                    }
                ?>&nbsp;</td>
                <td><?php //echo $this->Html->link(__("V-".$coffeeSample['CoffeeSample']['id_charger']), array('controller'=>'ChargeMassiveSampleCoffees','action' => 'view', $coffeeSample['CoffeeSample']['id_charger'])); ?>&nbsp;</td>
		
		<td class="actions">
		<div class="dropdown">
            <button class="dropbtn"><strong>+</strong></button>
            <div class="dropdown-content">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $coffeeSample['CoffeeSample']['id'])); ?>
                        <?php //echo ($coffeeSample['CoffeeSample']['status'] == "REGISTRADA") ? $this->Html->link(__('Revisar'), array('action' => 'edit', $coffeeSample['CoffeeSample']['id'])): ""; ?>
                        <?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('Procesar'), array('action' => 'make', $coffeeSample['CoffeeSample']['id'])): ""; ?>
						<?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('Eliminar'), array('action' => 'delete', $coffeeSample['CoffeeSample']['id'])): ""; ?>
			</div>
			</div>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
        
        
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<script type="text/javascript">
    
    tdChangeColor();
    refreshPage();
    
    function tdChangeColor(){
        var col;
        $("tr td:nth-child(6)").each(function(i) {
            col = $(this);
            if(col.html() == "COMPLETADA&nbsp;"){
                col.css('background', 'yellow');
            }
            else if(col.html() == "REGISTRADA&nbsp;"){
                col.css('background', 'red');
            }
            else if(col.html() == "CERRADA&nbsp;"){
                col.css('background', 'green');
            }
        })
    }
    
    function refreshPage(){
        var interval = setInterval(function() {
            location.reload();
        }, 30000);
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Registrar solicitudes'), array('controller'=>'ChargeMassiveSampleCoffees','action' => 'index', $this->Session->read('User.id'))); ?></li>
                <li><?php echo $this->Html->link(__('Buscar solicitud'), array('controller'=>'CoffeeSamples','action' => 'search')); ?></li>
                <li><?php echo $this->Html->link(__('REG MITSUI'), array('controller' => 'CoffeeSamples', 'action' => 'index',1)); ?> </li>
		<li><?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('COM MITSUI'), array('controller' => 'CoffeeSamples', 'action' => 'index',2)):"";?> </li>
		<li><?php echo $this->Html->link(__('REG CONT.MUESTRAS'), array('controller' => 'CoffeeSamples', 'action' => 'index',3)); ?> </li>
		<li><?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('COM CONT.MUESTRAS'), array('controller' => 'CoffeeSamples', 'action' => 'index',4)):"";?> </li>
		<li><?php echo $this->Html->link(__('REG WOLTHERS'), array('controller' => 'CoffeeSamples', 'action' => 'index',5)); ?> </li>
		<li><?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('COM WOLTHERS'), array('controller' => 'CoffeeSamples', 'action' => 'index',6)):"";?> </li>
		<li><?php echo $this->Html->link(__('REG NESTLE'), array('controller' => 'CoffeeSamples', 'action' => 'index',7)); ?> </li>
		<li><?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('COM NESTLE'), array('controller' => 'CoffeeSamples', 'action' => 'index',8)):"";?> </li>
		<li><?php echo $this->Html->link(__('REG SUCAFINA'), array('controller' => 'CoffeeSamples', 'action' => 'index',9)); ?> </li>
		<li><?php echo ($userCurrent['User']['profiles_id'] == 1) ? $this->Html->link(__('COM SUCAFINA'), array('controller' => 'CoffeeSamples', 'action' => 'index',10)):"";?> </li>

	</ul>
</div>
