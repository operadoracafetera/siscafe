<div class="coffeeSamples form">
<?php echo $this->Form->create('CoffeeSample',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo (($sample_coffee_id)) != 0 ? 
                __('Finalizar Toma de Muestra #'.$this->Html->link(__($sample_coffee_id), 
                        array('controller' => 'CoffeeSamples', 'action' => 'view',$sample_coffee_id."-V")))." a descargue de Café":""; ?></legend>
                <legend><?php echo (($sample_coffee_id)) == 0 ? __('Asignación de nueva toma de muestra'):""; ?></legend>
	<?php
                $var_total_bag=0;
                foreach ($remittancesCaffees as $remittancesCaffee){
                    $var_total_bag+=$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'];
                }
                echo $this->Form->input('lot_coffe',array('label'=>'Lote Café','value' => '3-'.$remittancesCaffees[0]['Client']['exporter_code'].'-'.$remittancesCaffees[0]['RemittancesCaffee']['lot_caffee'], 'disabled' => 'disabled'));
		echo $this->Form->input('exporter',array('label'=>'Exportador','value' => $remittancesCaffees[0]['Client']['exporter_code'].' - '.$remittancesCaffees[0]['Client']['business_name'],'disabled' => 'disabled'));
		echo $this->Form->input('qta_bags',array('label'=>'Total Sacos Lote','value' =>$var_total_bag,'disabled' => 'disabled'));
                ?>
                <h2><?php echo __('Listado de remesas que consolidan el lote de café'); ?></h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('create_date', 'Fecha Descargue'); ?></th>
                            <th><?php echo $this->Paginator->sort('remittance_id', 'Remesa'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_caffee', 'Lote'); ?></th>
                            <th><?php echo $this->Paginator->sort('remittance_id', 'Placa Vehiculo'); ?></th>
                            <th><?php echo $this->Paginator->sort('available_bags', 'Sacos disponibles'); ?></th>
                            <th><?php echo $this->Paginator->sort('actions', 'Acciones'); ?></th>
                        </tr>
                    </thead>
                    <?php foreach ($remittancesCaffees as $remittancesCaffee): ?>
                        <tr>
                            <td><?php 
                            //debug($remittancesCaffee);exit;
                            echo h($remittancesCaffee['RemittancesCaffee']['download_caffee_date']); ?>&nbsp;</td>
                            <td><?php echo h($this->Session->read('User.centerId').'-'.$remittancesCaffee['RemittancesCaffee']['id']); ?>&nbsp;</td>
                            <td><?php echo h('3-'.$remittancesCaffee['Client']['exporter_code'].'-'.$remittancesCaffee["RemittancesCaffee"]['lot_caffee']); ?>&nbsp;</td>
                            <td><?php echo h($remittancesCaffee['RemittancesCaffee']['vehicle_plate']); ?>&nbsp;</td>
                            <td><?php echo h(($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] - $remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'])); ?>&nbsp;</td>
                            <td><?php echo $this->Html->link(__('Ver'), array('controller'=>'RemittancesCaffees','action' => 'weights', $remittancesCaffee['RemittancesCaffee']['id'])); ?>&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <br>
                <br>
                <?php if($coffeeSamples != null):?>
                <h2><?php echo __('Tomas de muestras asignadas al lote de café'); ?></h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('coffee_sample_id', 'Id Toma'); ?></th>
                            <th><?php echo $this->Paginator->sort('action', 'Acciones'); ?></th>
                        </tr>
                    </thead>
                    <?php $var_tmp="";?>
                    <?php foreach ($coffeeSamples as $coffeeSample): ?>
                    <?php if($var_tmp != $coffeeSample['SampleRemmitances']['coffee_sample_id']):?>
                       <tr>
                            <td><?php 
                            //debug($coffeeSample);exit;
                            echo $coffeeSample['SampleRemmitances']['coffee_sample_id']; ?>&nbsp;</td>
                            <td><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $coffeeSample['SampleRemmitances']['coffee_sample_id']."-V"),array('target' => '_blank')); ?>&nbsp;</td>
                        </tr>
                        <?php endif; ?>
                        <?php $var_tmp = $coffeeSample['SampleRemmitances']['coffee_sample_id'];?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else:?>
                    <h3><?php echo __('No hay Tomas de muestras asignadas al lote de café'); ?></h3>
                <?php endif;?>
                <br>
                <br>
                <h2><?php echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "": __('Registrar datos para nueva toma de muestra'); ?></h2>
                <?php
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->label('type_sample_coffe_id','Tipo de muestra');
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('type_sample_coffe_id',array('label'=>'','empty'=>'Seleccione...','required' => true));
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->label('img_request_sample','Carta Solicitud Muestra (Aduana, Exportador) doc,..pdf');
		echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->file('img_request_sample');
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->label('operation_center','Lugar toma muestra');
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('operation_center',array('label' => '','empty'=>'Seleccione...', 'required' => true,
                'options' => array('SPIA' => 'SPIA','TCBUN' => 'TCBUN','SPBUN' => 'SPBUN','SPRC' => 'SPRC', 'COMPAS' => 'COMPAS','CONTECAR' => 'CONTECAR')));
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->label('observation','Observaciones');
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('observation',array('label'=>''));
		//echo $this->Form->input('tracking_supply_id');
		echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('supply_company',array('label' => 'Empresa transportadora','empty'=>'Seleccione...', 'required' => true,
                'options' => array('SERVIENTREGA' => 'SERVIENTREGA', 'DEPRISA' => 'DEPRISA')));
		//echo $this->Form->input('remittances_caffee_id');
		echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('register_users_id',array('type' => 'hidden'));
		echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('sampler_users_id',array('type' => 'hidden'));
		echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('services_orders_id',array('type' => 'hidden'));
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->label('services_packages','Paquete de servicio *Información para Orden de servicio');
                echo ($sample_coffee_id > 0 || $sample_coffee_id  == "V" ) ? "":$this->Form->input('services_packages',array('label' => '','empty'=>'Seleccione...', 'required' => true,
                'options' => $servicePackages));
	?>
	</fieldset>
<?php 
        if($sample_coffee_id > 0 && $flag == "F"){
            echo $this->Form->end(__('Registrar'));
        }
        else if($sample_coffee_id > 0 && !isset($flag)){
            echo $this->Form->end(__('Registrar'));
        }
        else if($sample_coffee_id == 0 && $sample_coffee_id != 'V'){
            echo $this->Form->end(__('Registrar'));
        }
        ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Descargue Lote Café'), array('controller' => 'ViewLotCoffees','action' => 'index',2)); ?></li>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
