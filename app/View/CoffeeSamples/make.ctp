<div class="coffeeSamples form">
<?php echo $this->Form->create('CoffeeSample',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Atender solicitud ID-'.$dataCoffeeSample['CoffeeSample']['id'].' (Muestra, Trazabilidad)'); ?></legend>
                <table>
                    <tr>
                        <tr>
                            <td><?php echo $this->Form->input('lot',array('label' => 'Expo-Lote Ej.: 001-02235', 'disabled' => 'disabled', 'type'=>'text','value'=>($dataCoffeeSample) ? $dataCoffeeSample['CoffeeSample']['lot_coffee_ref']:'')); ?></td>
                            <td><?php echo $this->Form->input('qta_bag',array('label'=>'Total Sacos Descargados','disabled' => 'disabled','placeholder'=>'(E.j.: 300)','value'=>($dataCoffeeSample) ? $dataCoffeeSample['CoffeeSample']['qta_bag']:'','required' => true));?></td>
                        </tr>
                        <tr>
                            <td><?php echo $this->Form->input('client_id',array('label' => 'Id Cliente','disabled' => 'disabled','type' => 'hidden','value'=>$dataCoffeeSample['Client']['id']));?></td>
                        </tr>
                </table>
                <table>
                        <tr>
                            <td>
                                <?php echo $this->Form->input('client',array('label' => 'Cliente','disabled' => 'disabled','value'=>$dataCoffeeSample['Client']['business_name']));;?>
                            </td>
                        </tr>
                    </tr>
                </table>
                <fieldset>
                <table>
                <tr>
                    <td><?php
                        echo $this->Form->label('type_sample_coffe_id','Tipo de muestra');
                        //echo $this->Form->input('type_sample_coffe_id',array('label'=>'','disabled' => 'disabled','options'=>$typeSampleCoffes,'value'=>$dataCoffeeSample['TypeSampleCoffe']['id']));
                        $dataTypeCoffeeSample = array();
                        
                        foreach($dataCoffeeSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                            array_push($dataTypeCoffeeSample, $typeSampleCoffe['type_sample_coffee_id']);
                        }
                        
                        echo $this->Form->input('type_sample_coffe_id', array('label'  => false,'disabled' => 'disabled','type' => 'select',
                                                'multiple'=>'checkbox',
                                                'options' => $typeSampleCoffes,
                                                'selected' => $dataTypeCoffeeSample)); 
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('img_request_sample','Carta Solicitud (Agencia Aduana)');
                        echo ($dataCoffeeSample['CoffeeSample']['img_request_sample']) ? $this->Html->link(__('Ver Documento'), array('controller' => 'CoffeeSamples', 'action' => 'getTrackingRequest',$dataCoffeeSample['CoffeeSample']['id'])):"NA"; ?>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->label('operation_center','Ciudad');
                            echo $this->Form->input('operation_center',array('label' => '','empty'=>'Seleccione...', 'required' => true,'value'=>$dataCoffeeSample['CoffeeSample']['operation_center'],
                            'options' => array('BUN' => 'BUN','CTG' => 'CTG','STM' => 'STM')));?>
                    </td>
		    <td>
                        <?php
                            echo $this->Form->label('terminal','Terminal');
                            echo $this->Form->input('terminal',array('label' => '','empty'=>'Seleccione...', 'required' => true,'value'=>$dataCoffeeSample['CoffeeSample']['terminal'],
                            'options' => array('SPIA' => 'SPIA','TCBUEN' => 'TCBUEN','SPB' => 'SPB','SPRC' => 'SPRC', 'SMITCO' => 'SMITCO','COMPAS' => 'COMPAS','CONTECAR' => 'CONTECAR')));?>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <?php
                        echo $this->Form->input('record_pictures',array('label'=>'Registros Fotograficos?','disabled' => 'disabled','type'=>'checkbox','checked'=>$dataCoffeeSample['CoffeeSample']['record_pictures']));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->input('tracking_packaging',array('label'=>'Realizar Trazabilidad?','required' => true, 'disabled' => 'disabled','type'=>'checkbox','checked'=>$dataCoffeeSample['CoffeeSample']['tracking_packaging']));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('observation','Observaciones');
                        echo $this->Form->input('observation',array('label'=>'','type'=>'textarea','required' => true,'disabled' => 'disabled','value'=>$dataCoffeeSample['CoffeeSample']['observation']));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo $this->Form->label('user_sampler','Muestreadores');
                        echo $this->Form->input('user_sampler',array('label'=>'','options'=>$usersSamplers, 'value'=>$dataCoffeeSample['CoffeeSample']['sampler_users_id']));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('request_reponse','Anotación - Aclaración de Respuesta');
                        echo $this->Form->input('request_reponse',array('label'=>'','value'=>$dataCoffeeSample['CoffeeSample']['request_reponse']));
                        ?>
                    </td>
                    
                    <td>
                        <?php
                        echo $this->Form->label('value_bill','Valor Muestra');
                        echo $this->Form->input('value_bill',array('label'=>'','required' => true,'value'=>$dataCoffeeSample['CoffeeSample']['qta_bag']*188));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        echo $this->Form->label('supply_company','Transportadora');
                        echo $this->Form->input('supply_company',array('label'=>'','options'=>array('DEPRISA'=>'DEPRISA','SERVIENTREGA'=>'SERVIENTREGA'),'value'=>$dataCoffeeSample['CoffeeSample']['supply_company']));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('sample_packing','Empaque Muestra');
                        echo $this->Form->input('sample_packing',array('label'=>'','options'=>array('CAJA'=>'CAJA','BOLSA'=>'BOLSA'),'value'=>$dataCoffeeSample['CoffeeSample']['sample_packing']));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('tracking_supply','#Guia de Envio');
                        echo $this->Form->input('tracking_supply',array('label'=>'','type'=>'text','required' => true,'value'=>$dataCoffeeSample['CoffeeSample']['tracking_supply']));
                        ?>
                    </td>
                </tr>
            </table>
                    <table>
                        <tr>
                            <td>
                                <?php
                                echo $this->Form->label('status','Estado solicitud');
                                echo $this->Form->input('status',array('label'=>'','disabled' => 'disabled','value'=>$dataCoffeeSample['CoffeeSample']['status']));
                                echo $this->Form->input('status',array('label'=>'','type' => 'hidden','value'=>$dataCoffeeSample['CoffeeSample']['status']));?>
                            </td>  
                            <td>
                                <?php
                                echo $this->Form->label('doc_sample','1. Archivo respuesta Trazabilidad (doc,zip,pdf,...etc');
                                echo $this->Form->file('doc_sample');?>
                            </td> 
                            <td>
                                <?php
                                echo $this->Form->label('img_response_sample','2. Archivo evidencia Muestra (Imagen JPG)');
                                echo $this->Form->file('img_response_sample');?>
                            </td>  
                        </tr>
                    </table>
                    <?php
                    echo $this->Form->label('email_alt','Email destino');
                    echo $this->Form->input('email_alt',array('label'=>'','required' => true,'type'=>'text','value'=>$dataCoffeeSample['CoffeeSample']['email_alt']));
                    ?>
            </fieldset>
            <br><p>
            <h2><?php echo __('Café relacionado a la Solicitud'); ?></h2>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                        <th><?php echo $this->Paginator->sort('lot_coffee','Lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('state_operation','Estado Operativo'); ?></th>
                        <th><?php echo $this->Paginator->sort('packing_coffee','Tipo Empaque'); ?></th>
                        <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos Rad'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($dataSampleRemmitances as $dataSampleRemmitance): ?>
                    <tr>
                        <td><?php
                       //debug($dataSampleRemmitance);exit;
                        echo h($dataSampleRemmitance['RemittancesCaffee']['id']); ?>&nbsp;</td>
                        <td><?php echo h("3-".$dataSampleRemmitance['RemittancesCaffee']['Client']['exporter_code']."-".$dataSampleRemmitance['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['StateOperation']['name']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['PackingCaffee']['name']); ?>&nbsp;</td>
                        <td><?php echo h($dataSampleRemmitance['RemittancesCaffee']['quantity_radicated_bag_in']); ?>&nbsp;</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <br>
            <?php echo $this->Form->end(__('Guardar')); ?>
            
	</fieldset>
    <script type="text/javascript">
    </script>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',1)); ?> </li>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
