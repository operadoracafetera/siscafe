<div class="coffeeSamples view">
    <h2><?php echo __('Envio de Muestra de Café'); ?></h2>
    <?php echo $this->Form->create('CoffeeSample',array('type' => 'file')); ?>
	<dl>
                <?php echo $this->Form->input('supply_company',array('label' => 'Empresa transportadora','empty'=>'Seleccione...', 'required' => true,
                'options' => array('SERVIENTREGA' => 'SERVIENTREGA', 'DEPRISA' => 'DEPRISA')));?>
                <?php echo $this->Form->label('address_des','Dirección de destinatario');?>
                <?php echo $this->Form->input('address_des',array('label'=>'','placeholder'=>'E.j: Carrera 4 #78-54')); ?>
                <?php echo $this->Form->label('city_des','Ciudad de destinatario');?>
                <?php echo $this->Form->input('city_des',array('label'=>'','placeholder'=>'E.j: Pereira')); ?>
                <?php echo $this->Form->label('phone_des','Telefono destinatario');?>
                <?php echo $this->Form->input('phone_des',array('label'=>'','placeholder'=>'E.j: 092-1231232')); ?>
                <?php echo $this->Form->label('name_contact','Contacto destinatario');?>
                <?php echo $this->Form->input('name_contact',array('label'=>'','placeholder'=>'E.j: Juan Carlos Perez')); ?>
                <?php echo $this->Form->label('email_contact','Email destinatario');?>
                <?php echo $this->Form->input('email_contact',array('label'=>'','placeholder'=>'E.j: jcarlos@algocorreo.com')); ?>
                <?php echo $this->Form->label('departament_des','Departamento de destinatario');?>
                <?php echo $this->Form->input('departament_des',array('label'=>'','placeholder'=>'E.j: Risaralda',)); ?>
                <?php echo $this->Form->label('sample_packing','Empaque de muestra');?>
                <?php echo $this->Form->input('sample_packing',array('label' => '','empty'=>'Seleccione...', 'required' => true,
                'options' => array('SOBRE' => 'SOBRE', 'CAJA' => 'CAJA')));?>
                <?php echo $this->Form->input('des_code_postal',array('label' => 'Codigo Postal Destinatario','empty'=>'Seleccione...', 'required' => true,
                                'options' => array('660001' => 'PEREIRA - 660001','763041' => 'BUGA - 763041', '110911' => 'BOGOTA - 110911')));?>
                <?php echo $this->Form->label('img_finish_sample','Evidencia de muestra');?>
		<?php echo $this->Form->file('img_finish_sample',array('label'=>''));?>
	</dl>
            <br>
            <br>
            <h2><?php echo __('Listado de muestras de café SIN ENVIAR'); ?></h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('id','Id Muestra'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_coffee_ref','Lote Café'); ?></th>
                            <th><?php echo $this->Paginator->sort('qta_bag','Cant. Sacos'); ?></th>
                            <th><?php echo $this->Paginator->sort('client_id','Exportador'); ?></th>
                            <th><?php echo $this->Paginator->sort('type_sample_coffe_id','Tipo muestra'); ?></th>
                            <th><?php echo $this->Paginator->sort('status','Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fecha registro'); ?></th>
                            <th><?php echo $this->Paginator->sort('actions', 'Acciones'); ?></th>
                        </tr>
                    </thead>
                <?php foreach ($coffeeSamples as $coffeeSample): ?>
                <tr>
		<td><?php 
                //debug($coffeeSample);exit;
                echo h($coffeeSample['CoffeeSample']['id']); ?>&nbsp;</td>
                <td><?php echo h('3-'.$coffeeSample['Client']['exporter_code']."-".substr($coffeeSample['CoffeeSample']['lot_coffee_ref'], 0, 4)); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['CoffeeSample']['qta_bag']); ?>&nbsp;</td>
                <td><?php echo h($coffeeSample['Client']['business_name']); ?>&nbsp;</td>
		<td>
			<?php echo $coffeeSample['TypeSampleCoffe']['name']; ?>
		</td>
		<td><?php echo h($coffeeSample['CoffeeSample']['status']); ?>&nbsp;</td>
		<td><?php echo h($coffeeSample['CoffeeSample']['created_date']); ?>&nbsp;</td>
		<td class="actions">
                    <input type="checkbox" name="id_sample_coffe" value="<?php echo $coffeeSample['CoffeeSample']['id'];?>">
		</td>
                </tr>
                <?php endforeach; ?>
                    </tbody>
                </table>
                <p>
                <?php
                echo $this->Paginator->counter(array(
                        'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
                ));
                ?>	</p>
                <div class="paging">
                <?php
                        echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
                </div>
        <br>
            <br>
            <script type="text/javascript">
                $("#CoffeeSampleExporter").keyup(function () {
                        var codExpFNC = $(this).val();
                        findExportador(codExpFNC);
                    });

                function findExportador(codExpo){
                    $.ajax({
                       type: "GET",
                       datatype: "json",
                       url: "/siscafe-web-dev/Clients/findByExpo/"+codExpo,
                       error: function(msg){alert("Error networking");},
                       success: function(data){
                            var json = JSON.parse(data);
                            if(JSON.stringify(json) !== "[]"){
                                $("#CoffeeSampleClientId").val(json.Client.id);
                                $("#CoffeeSampleClient").val(json.Client.exporter_code+"-"+json.Client.business_name);
                            }
                            else{
                                $("#CoffeeSampleClient").val("NO ARROJO RESULTADOS");
                            }
                     }});
                }

                function clearField(){
                    $("#RemittancesCaffeeCodigo").val("");
                    $("#RemittancesCaffeeIdCliente").val("");
                    $("#RemittancesCaffeeClient").val("");
                }
            </script>   
    <?php echo $this->Form->end(__('Despachar')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Descargue Lote Café'), array('controller' => 'ViewLotCoffees', 'action' => 'index',2)); ?> </li>
                <li><?php echo $this->Html->link(__('Muestras pendientes'), array('controller' => 'CoffeeSamples', 'action' => 'index',2)); ?> </li>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',1)); ?> </li>
		<li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
