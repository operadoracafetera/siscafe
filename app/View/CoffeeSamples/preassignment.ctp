<div class="coffeeSamples form">
<?php echo $this->Form->create('CoffeeSample',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Solicitud de Toma de Muestra, Trazabilidad'); ?></legend>
                <table>
                    <tr>
                        <tr>
                            <td><?php echo $this->Form->input('lot',array('label' => 'Expo-Lote Ej.: 001-02235', 'type'=>'text','value'=>($dataCoffeeSample) ? $dataCoffeeSample['CoffeeSample']['lot_coffee_ref']:'')); ?></td>
                            <td><?php echo $this->Form->input('qta_bag',array('label'=>'Total Sacos Descargados','placeholder'=>'(E.j.: 300)','value'=>($dataCoffeeSample) ? $dataCoffeeSample['CoffeeSample']['qta_bag']:'','required' => true));?></td>
                        </tr>
                        <tr>
                            <td><?php echo $this->Form->input('client_id',array('label' => 'Id Cliente','type' => 'hidden'));?></td>
                            <td><?php echo $this->Form->input('user_profile',array('type' => 'hidden','value'=>$userCurrent['User']['profiles_id']));?></td>
                        </tr>
                </table>
                <table>
                        <tr>
                            <td>
                                <?php echo $this->Form->input('client',array('label' => 'Cliente','disabled' => 'disabled'));;?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $this->Form->input('msg_coffee_find',array('type'=>'textarea','label' => 'Resultado busqueda Café','disabled' => 'disabled'));;?>
                            </td>
                        </tr>
                    </tr>
                </table>
                <fieldset>
                <table>
                <tr>
                    <td><?php
                        echo $this->Form->label('type_sample_coffe_id','Tipo de muestra');

                        echo $this->Form->input('type_sample_coffe_id', array('label'  => false,'type' => 'select',
                                                'multiple'=>'checkbox',
                                                'options' => $typeSampleCoffes)); 
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('img_request_sample','Carta Solicitud (Solamente formato PDF)');
                        echo $this->Form->file('img_request_sample',array('required' => false));?>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->label('operation_center','Lugar toma muestra');
                            echo $this->Form->input('operation_center',array('label' => '','empty'=>'Seleccione...', 'required' => true,
                            'options' => array('SPIA' => 'SPIA','TCBUN' => 'TCBUN','BUN' => 'BUN','SPRC' => 'SPRC', 'COMPAS' => 'COMPAS','CONTECAR' => 'CONTECAR')));?>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <?php
                        echo $this->Form->input('record_pictures',array('label'=>'Registros Fotograficos?','type'=>'checkbox'));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->input('tracking_packaging',array('label'=>'Realizar Trazabilidad?','type'=>'checkbox'));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->label('observation','Observaciones');
                        echo $this->Form->input('observation',array('label'=>'','type'=>'textarea'));
                        ?>
                    </td>
                    
                </tr>
            
            </table>
            <div id="tablaCoffeeFind1">
                <h2><?php echo __('Resultado detalle café descargado'); ?></h2>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('selection','Selección'); ?></th>
                            <th><?php echo $this->Paginator->sort('remittance_id','Remesa'); ?></th>
                            <th><?php echo $this->Paginator->sort('create_date','Fecha Creación'); ?></th>
                            <th><?php echo $this->Paginator->sort('cargolot_id','Cargo Lot Id'); ?></th>
                            <th><?php echo $this->Paginator->sort('state_operation_id','Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('lot_caffee','Lote'); ?></th>
                            <th><?php echo $this->Paginator->sort('total_rad_bag_out','Sacos Rad Out'); ?></th>
                            <th><?php echo $this->Paginator->sort('available_bags','Sacos disponibles'); ?></th>
                            <th><?php echo $this->Paginator->sort('quantity_bag_in_store','Sacos In'); ?></th>
                            <th><?php echo $this->Paginator->sort('quantity_bag_out_store','Sacos Out'); ?></th>
                            <th><?php echo $this->Paginator->sort('nolvetys','Bloqueos (Novedades)'); ?></th>
                        </tr>
                    </thead>
                    <tbody id="tableRemittances">
                    </tbody>
                </table>
            </div>
            </fieldset>
            
            <div id="tablaCoffeeFind2">   
            <h2><?php echo __('Café relacionado a la Solicitud'); ?></h2>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                        <th><?php echo $this->Paginator->sort('lot_coffee','Lote'); ?></th>
                        <th><?php echo $this->Paginator->sort('packaging_caffee_id','OIE'); ?></th>
                        <th><?php echo $this->Paginator->sort('quantity_radicated_bag_out','Sacos'); ?></th>
                        <th class="actions"><?php echo __('Acciones'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($dataSampleRemmitances as $dataSampleRemmitance): ?>
                    <tr>
                        <td><?php
                        echo h($detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']); ?>&nbsp;</td>
                        <td><?php echo h("3-".$detailPackagingCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$detailPackagingCaffee['RemittancesCaffee']['lot_caffee']); ?>&nbsp;</td>
                        <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id']); ?>&nbsp;</td>
                        <td><?php echo h($detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('Seleccionar'), array('action' => 'edit', '?' => ['oie_id' => $detailPackagingCaffee['DetailPackagingCaffee']['packaging_caffee_id'],'remittance_id'=>$detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id']] )); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            </div>
            <br>
            <?php echo $this->Form->end(__('Registrar')); ?>
	</fieldset>
    <script type="text/javascript">
        
        checkUserUILotCoffee();
        
        $("#CoffeeSampleExporter").keyup(function () {
                var codExpFNC = $(this).val();
                findExportador(codExpFNC);
            });
            
        $("#CoffeeSampleLot").keyup(function () {
            findInfoRemittancesByLot($(this).val());
        });
        
        function findExportador(codExpo){
            $.ajax({
               type: "GET",
               datatype: "json",
               url: "/siscafe-test/Clients/findByExpo/"+codExpo,
               error: function(msg){alert("Error networking");},
               success: function(data){
                    var json = JSON.parse(data);
                    if(JSON.stringify(json) !== "[]"){
                        $("#CoffeeSampleClientId").val(json.Client.id);
                        $("#CoffeeSampleClient").val(json.Client.exporter_code+"-"+json.Client.business_name);
                    }
                    else{
                        $("#CoffeeSampleClient").val("NO ARROJO RESULTADOS");
                    }
             }});
        }
        
        function findInfoRemittancesByLot(lot) {
            var dataLot = lot.split("-");
            findExportador(dataLot[0]);
            var msgResponseLotCoffee="";
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/siscafe-test/RemittancesCaffees/findByLotClient/" + lot,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {
                    console.log(data);
                    if (data !== "") {
                        var remittancesData = JSON.parse(data);
                        addRemittanceDataToTable(remittancesData);
                    }
                    else{
                        msgResponseLotCoffee+="Lote de Café SIN ser descargado en Bodega";
                        msgResponseLotCoffee+="\n\nNOTA: Los parciales de cafe EMBALADOS NO pueden ser muestreados,\n"+
                                 "los ALMACENADO EN BODEGA y NO DESCARGADOS se muestrean sujeto a una previa verificación.\n";
                        $("#CoffeeSampleMsgCoffeeFind").val(msgResponseLotCoffee);
                        $('#tableRemittances').children().remove();
                         
                    }
                }
            });
        }
        
        function checkUserUILotCoffee(){
            var userProfile = $("#CoffeeSampleUserProfile").val();
            if(userProfile == 1){
              $("#tablaCoffeeFind1").show();
              $("#tablaCoffeeFind2").show();
            }
            else{
                $("#tablaCoffeeFind1").hide();
                $("#tablaCoffeeFind2").hide();
            }
        }
        
        function addRemittanceDataToTable(remittancesData) {
            $('#tableRemittances').children().remove();
            var msgResponseLotCoffee="";
            $.each(remittancesData, function (i, value) {
                trHTML = '<tr>';
                if(value['StateOperation']['id'] == 1 || value['StateOperation']['id'] == 2){
                    trHTML += '<td><input type="checkbox" id="dataRemittancesCaffee" name="data[dataRemittancesCaffee]['+value['RemittancesCaffee']['id']+']" value="'+value['RemittancesCaffee']['id']+'"></td>';
                }
                else{
                    trHTML += '<td>NA</td>';
                }
                trHTML += '<td>' + value['RemittancesCaffee']['id'] + '</td>';
                trHTML += '<td>' + value['RemittancesCaffee']['created_date'] + '</td>';
                trHTML += '<td>' + value['RemittancesCaffee']['cargolot_id'] + '</td>';
                trHTML += '<td>' + value['StateOperation']['name'] + '</td>';
                if(value['StateOperation']['id'] == 4 || value['StateOperation']['id'] == 5){
                    msgResponseLotCoffee +="Parcial de Café "+value['RemittancesCaffee']['id']+" Embalado\n";
                }
                else if(value['StateOperation']['id'] == 1 || value['StateOperation']['id'] == 2 || value['StateOperation']['id'] == 3){
                    msgResponseLotCoffee +="Parcial de Café "+value['RemittancesCaffee']['id']+" Almacenado en Bodega, \n";
                }
                trHTML += '<td>' +'3-'+ value['Client']['exporter_code']+'-'+value['RemittancesCaffee']['lot_caffee']+'</td>';
                trHTML += '<td>' + value['RemittancesCaffee']['total_rad_bag_out'] + '</td>';
                trHTML += '<td>' + (value['RemittancesCaffee']['quantity_radicated_bag_in'] - value['RemittancesCaffee']['total_rad_bag_out']) + '</td>';
                if (value['RemittancesCaffee']['quantity_bag_in_store'])
                    trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_in_store'] + '</td>';
                else
                    trHTML += '<td>' + 0 + '</td>';
                if (value['RemittancesCaffee']['quantity_bag_out_store'])
                    trHTML += '<td>' + value['RemittancesCaffee']['quantity_bag_out_store'] + '</td>';
                else
                    trHTML += '<td>' + 0 + '</td>';
                
                var noveltysCoffee="";
                $.each(value['RemittancesCaffeeHasNoveltysCaffee'], function(n,val){
                    noveltysCoffee += val['NoveltysCoffee']['name'] + " - activo: "+val['active']+" | ";
                });
                if(noveltysCoffee === "")
                    trHTML += '<td>' + "Sin novedades" + '</td>';
                else
                    trHTML += '<td style="color: green; background-color: #ffff42">' + noveltysCoffee + '</td>';
                
                trHTML += '</tr>';
                $('#tableRemittances').append(trHTML);
            });
            msgResponseLotCoffee+="\nNOTA: Los parciales de cafe EMBALADOS NO pueden ser muestreados,\n"+
                                 "los ALMACENADO EN BODEGA y NO DESCARGADOS se muestrean sujeto a una previa verificación\n";
            $("#CoffeeSampleMsgCoffeeFind").val(msgResponseLotCoffee);
        }

    </script>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',$this->Session->read('User.id'))); ?> </li>
		<li><?php 
                    if(($this->Session->read('User.profiles_id') == 1) || ($this->Session->read('User.profiles_id') == 3)){
                       echo $this->Html->link(__('Regresar'), array('action' => 'index')); 
                    }
                    else if($this->Session->read('User.profiles_id') == 9){
                       echo $this->Html->link(__('Regresar'), array('controller' => 'Pages','action' => 'invitado'));
                    }
                ?></li>
	</ul>
</div>
