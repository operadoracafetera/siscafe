<div class="fumigationServices view">
<h2><?php echo __('Fumigation Service'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Fumigation'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['product_fumigation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity Poison Used'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['quantity_poison_used']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Date'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['finished_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remittances Caffee Id'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['remittances_caffee_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volumen Fumigated'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['volumen_fumigated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resquest Service Date'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['resquest_service_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resquest Certificate Date'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['resquest_certificate_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($fumigationService['FumigationService']['observation']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fumigation Service'), array('action' => 'edit', $fumigationService['FumigationService']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fumigation Service'), array('action' => 'delete', $fumigationService['FumigationService']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $fumigationService['FumigationService']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Fumigation Services'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fumigation Service'), array('action' => 'add')); ?> </li>
	</ul>
</div>
