<div class="fumigationServices form">
<?php echo $this->Form->create('FumigationService'); ?>
	<fieldset>
		<legend><?php echo __('Agregar servicio de fumigación - Paso #1'); ?></legend>
            <?php
                echo $this->Form->input('exporter_id', array('label' => 'Código Cliente', 'type' => 'text'));
                echo $this->Form->input('client_name', array('label' => 'Nombre Cliente', 'disabled' => 'disabled'));
                echo $this->Form->input('client_id', array('label' => 'Client Id', 'type' => 'hidden'));
		echo $this->Form->input('destiny',array('label' => 'Destino café - (Sitio geográfico importador)'));
		echo $this->Form->input('motonavy',array('label' => 'Motonave'));
		echo $this->Form->input('start_date',array('label' => 'Fecha Registro'));
		echo $this->Form->input('client_name',array('label' => 'Nombre de importador Café'));
                echo $this->Form->input('ica',array('label' => 'ICA'));
                echo $this->Form->input('observation',array('label' => 'Observaciones (Solicitud exportador - Certificado fumigación)'));
                echo $this->Form->input('instructions',array('label' => 'Instrucciones de fumigación (E.j.: fumigar lotes 3-001-2233 300 sacos)'));?>
            </fieldset>
            <?php echo $this->Form->end(__('Registrar')); ?>

</div>
<script>
    $("#FumigationServiceExporterId").keyup(function () {
        findInfoRemittancesByClientCode($(this).val());
    });

    function findInfoRemittancesByClientCode(exportCode) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffees/findRemittancesByExportCode/" + exportCode,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data != "") {
                    var clientData = JSON.parse(data)['clientData'];
                    $("#FumigationServiceClientName").val(clientData['Client']['business_name']);
                    $("#FumigationServiceClientId").val(clientData['Client']['id']);
                }
            }
        });
    }
</script>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado fumigaciones'), array('action' => 'index')); ?></li>
	</ul>
</div>
