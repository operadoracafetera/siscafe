<div class="fumigationServices index">
	<h2><?php echo __('Listado de servicios de fumigación'); ?></h2>
	<?php echo $this->Html->link(__('Nueva fumigación'), array('action' => 'add')); ?>
        <table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('Id Certificado'); ?></th>
                        <th><?php //echo $this->Paginator->sort('Remision'); ?></th>
                        <th><?php echo $this->Paginator->sort('Cta Cafe Solicitado'); ?></th>
			<th><?php echo $this->Paginator->sort('Cta Cafe Descargado'); ?></th>
			<th><?php echo $this->Paginator->sort('Lotes Programados'); ?></th>
			
                        <th><?php echo $this->Paginator->sort('CO - Remesas'); ?></th>
                        <th><?php echo $this->Paginator->sort('Lotes descargados'); ?></th>
			<th><?php echo $this->Paginator->sort('Estado'); ?></th>
			<th><?php //echo $this->Paginator->sort('Dosis (gramos PH3/m3)'); ?></th>
                        <th><?php //echo $this->Paginator->sort('Cta Veneno'); ?></th>
			<th><?php echo $this->Paginator->sort('Fecha inicio'); ?></th>
			<th><?php echo $this->Paginator->sort('Finalizado'); ?></th>
			<th><?php //echo $this->Paginator->sort('Volumen fumigado (M3)'); ?></th>
                        <th><?php echo $this->Paginator->sort('Pesaje fumigado (Kg)'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($fumigationServices as $fumigationService): ?>
	<tr>
		<td><?php 
                //debug($fumigationService);exit;
                //debug($fumigationService['RemittancesCaffeeHasFumigationService']);exit;
                echo h($fumigationService['FumigationService']['id']); ?>&nbsp;</td>
                <td><?php //echo $this->Html->link(__('R-'.$fumigationService['ServicesOrder']['id']), array('controller' => 'ServicesOrders','action' => 'view', $fumigationService['ServicesOrder']['id']),array('target' => '_blank')); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['qta_coffee_request']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['qta_bags']); ?>&nbsp;</td>
		
                <td>
                <?php 
                    $tmp;
                    $current="";
                    foreach ($fumigationService['ScheduleCoffeeFumigation'] as $scheduleCoffee):
                        //debug($remittancesCaffee);exit;
                        $tmp = ($scheduleCoffee['lot_coffee']);
                        if($current != $tmp){
                            echo h($scheduleCoffee['lot_coffee']);
                            $current = $tmp;
                        }
                    ?>
                <?php endforeach; ?>
                </td>
                <td>
                <?php 
                    $tmp;
                    $current="";
                    foreach ($fumigationService['RemittancesCaffeeHasFumigationService'] as $remittancesCaffee):
                        //debug($remittancesCaffee);exit;
                        $tmp = ($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']);
                        if($current != $tmp){
                            echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']." ; ");
                            $current = $tmp;
                        }
                    ?>
                <?php endforeach; ?>
                </td>
                <td>
                <?php 
                    $tmp;
                    $current="";
                    foreach ($fumigationService['RemittancesCaffeeHasFumigationService'] as $remittancesCaffee):
                        //debug($remittancesCaffee);exit;
                        $tmp = ($remittancesCaffee['RemittancesCaffee']['id']);
                        if($current != $tmp){
                            echo h(("3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee'])." x " .$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']."  ");
							$current = $tmp;
                        }
                    ?>
                <?php endforeach; ?>
                </td>
				<?php  if($fumigationService['HookupStatus']['id'] == 2) {
				echo "<td style=\"background: red\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 1) {
				echo "<td style=\"background: blue\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 3) {
				echo "<td style=\"background: yellow\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			}
			else if($fumigationService['HookupStatus']['id'] == 4) {
				echo "<td style=\"background: green\"><span>".$fumigationService['HookupStatus']['status_name']."</span></td>";
			} ?>
		<td><?php //echo h($fumigationService['FumigationService']['dosis']); ?>&nbsp;</td>
                <td><?php //echo h($fumigationService['FumigationService']['quantity_poison_used']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($fumigationService['FumigationService']['finished_date'] != NULL ? $fumigationService['FumigationService']['finished_date'] : "SIN COMPLETAR"); ?>&nbsp;</td>
		<td><?php //echo h(number_format($fumigationService['FumigationService']['volumen_fumigated'],2,'.', '')); ?>&nbsp;</td>
                <td><?php echo h($fumigationService['FumigationService']['weight_caffee']); ?>&nbsp;</td>
		<td class="actions">
			<?php
                        echo $this->Html->link(__('Fotografias'), array('controller' => 'OperationTrackings','action' => 'addfumigation', $fumigationService['FumigationService']['id'])); 
                        if($fumigationService['FumigationService']['hookup_status_id'] == 2){
                          echo $this->Html->link(__('Procesar'), array('controller' => 'FumigationServices','action' => 'process', $fumigationService['FumigationService']['id'])); 
                        }
                        else if($fumigationService['FumigationService']['hookup_status_id'] == 3){
                                echo $this->Html->link(__('Completar'), array('controller' => 'FumigationServices','action' => 'complete', $fumigationService['FumigationService']['id'])); 
                        }
                        else if($fumigationService['FumigationService']['hookup_status_id'] == 4){
                            echo $this->Html->link(__('Certificado'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=0&idCertificado=".$fumigationService['FumigationService']['id']."&idReport=3&toPdf=true&idPrinter=1");
                        }
                        ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Ordenes Servicios'), array('controller' => 'ServicesOrders','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Buscar Servicios'), array('controller'=>'FumigationServices', 'action' => 'searchServices')); ?></li>
	</ul>
</div>
