<div class="fumigationServices index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('FumigationService');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda de Servicios de Fumigación. '); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('codigo',array('label' => 'Código de certificado'));?></td>
                <td><?php echo $this->Form->input('remision',array('label' => 'Remisión'));?></td>
                <td><?php echo $this->Form->input('lote',array('label' => 'Exportador-Lote'));?></td>
                <td><?php echo $this->Form->input('remesa',array('label' => 'Remesa'));?></td>
            </tr>  
        </table>
        <?php echo $this->Form->end(__('Buscar')); ?>
                    <br>

    </fieldset>
    
        <table id="tbDataOie">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('Código Certificado'); ?></th>
                    <th><?php echo $this->Paginator->sort('Remision'); ?></th>
                    <th><?php echo $this->Paginator->sort('Qta Sacos'); ?></th>
                    <th><?php echo $this->Paginator->sort('CO - Remesas'); ?></th>
                    <th><?php echo $this->Paginator->sort('Lotes de café'); ?></th>
                    <th><?php echo $this->Paginator->sort('Dosis (gramos PH3/m3)'); ?></th>
                    <th><?php echo $this->Paginator->sort('Cta Veneno'); ?></th>
                    <th><?php echo $this->Paginator->sort('Fecha inicio'); ?></th>
                    <th><?php echo $this->Paginator->sort('Finalizado'); ?></th>
                    <th><?php echo $this->Paginator->sort('Volumen fumigado (M3)'); ?></th>
                    <th><?php echo $this->Paginator->sort('Pesaje fumigado (Kg)'); ?></th>
                    <th><?php echo $this->Paginator->sort('Fecha certificado Exp'); ?></th>
                    <th class="actions"><?php echo __('Acciones'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($fumigationServices as $fumigationService): ?>
                    <tr>
                        <td><?php echo h($fumigationService['FumigationService']['id']); ?>&nbsp;</td>
                        <td><?php echo $this->Html->link(__('R-'.$fumigationService['ServicesOrder']['id']), array('controller' => 'ServicesOrders','action' => 'view', $fumigationService['ServicesOrder']['id']),array('target' => '_blank')); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['qta_bags']); ?>&nbsp;</td>
                        <td>
                        <?php 
                            $tmp;
                            $current="";
                            foreach ($fumigationService['RemittancesCaffeeHasFumigationService'] as $remittancesCaffee):
                                //debug($remittancesCaffee);exit;
                                $tmp = ($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']);
                                if($current != $tmp){
                                    echo h($this->Session->read('User.centerId')."-".$remittancesCaffee['RemittancesCaffee']['id']." ; ");
                                    $current = $tmp;
                                }
                            ?>
                        <?php endforeach; ?>
                        </td>
                        <td>
                        <?php 
                            $tmp;
                            $current="";
                            foreach ($fumigationService['RemittancesCaffeeHasFumigationService'] as $remittancesCaffee):
                                //debug($remittancesCaffee);exit;
                                $tmp = ("3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee']);
                                if($current != $tmp){
                                    echo h(("3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".$remittancesCaffee['RemittancesCaffee']['lot_caffee'])." : " .$remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] );
                                    $current = $tmp;
                                }
                            ?>
                        <?php endforeach; ?>
                        </td>
                        <td><?php echo h($fumigationService['FumigationService']['dosis']); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['quantity_poison_used']); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['start_date']); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['finished_date'] != NULL ? $fumigationService['FumigationService']['finished_date'] : "SIN COMPLETAR"); ?>&nbsp;</td>
                        <td><?php echo h(number_format($fumigationService['FumigationService']['volumen_fumigated'],2,'.', '')); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['weight_caffee']); ?>&nbsp;</td>
                        <td><?php echo h($fumigationService['FumigationService']['resquest_certificate_date']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php 
                            if($fumigationService['FumigationService']['completed'] == false){
                                echo $this->Html->link(__('Modificar'), array('controller' => 'RemittancesCaffeeHasFumigationServices','action' => 'add', '?' => ['services_orders_id' => $fumigationService['ServicesOrder']['id'],'fumigation_services_id' => $fumigationService['FumigationService']['id']])); 
                                echo $this->Html->link(__('Completar'), array('controller' => 'FumigationServices','action' => 'complete', $fumigationService['FumigationService']['id'])); 
                            }
                            else{
                                echo $this->Html->link(__('Certificado'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=0&idCertificado=".$fumigationService['FumigationService']['id']."&idReport=3&toPdf=true&idPrinter=1");
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
                   

</div>

<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
