<div class="packagingCaffees form">
<?php echo $this->Form->create('PackagingCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Vaciar la OIE  '.$findPackagingCaffee['PackagingCaffee']['id']); ?></legend>
	<?php
                echo __('Estado:  '.$findPackagingCaffee['StatePackaging']['name']);?><p><?php
		//echo $this->Form->input('bic_container',array('label'=>'Numero de contenedor'));
                echo $this->Form->input('cause_emptied_ctn_id',array('label'=>'Causas de vaciado', 'empty'=>'Seleccione...', 'required' => true,'options'=>$causeEmptiedCtn));
                //echo $this->Form->input('services_package_id',array('label'=>'Paquete de servicio', 'empty'=>'Seleccione...', 'required' => true,'options'=>$servicePackages));
		if($findPackagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                    echo $this->Form->input('state_packaging_id',array('label'=>'Estado del contenedor', 'empty'=>'Seleccione...', 'required' => true,'options'=>array('4'=>'EMBALAJE VACIADO','7'=>'EMBALAJE VACIADO-LLENADO')));
                }
                else if($findPackagingCaffee['PackagingCaffee']['state_packaging_id'] == 5){
                    echo $this->Form->input('state_packaging_id',array('label'=>'Estado del contenedor', 'empty'=>'Seleccione...', 'required' => true,'options'=>array('8'=>'EMBALAJE VACIADO-CANCELADO')));
                }
		//echo $this->Form->input('shipping_lines_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Embalaje'), array('controller'=>'PackagingCaffees','action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
