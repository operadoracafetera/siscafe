<div class="packagingCaffees index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('PackagingCaffee');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda OIE'); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('oie',array('label' => 'OIE'));?></td>
                <td><?php echo $this->Form->input('bic_container',array('label' => 'Contenedor'));?></td>
            </tr>  
        </table>
        <?php echo $this->Form->end(__('Buscar')); ?>
                    <br>
            <table style="width: 100%; "> 
            <tr>
            <td><?php echo $this->Form->input('date',array('label' => 'Fecha de embalaje','type' => 'text')); ?></td>
            <td><?php echo $this->Form->input('jornally',array('label' => 'Jornada', 'style' => 'width:100%', 'empty'=>'Seleccione...',
                    'options' => array('M' => 'Mañana', 'T' => 'Tarde')));?></td>
            <td><?php echo $this->Form->input('ready_cantainer_new',array('label' => 'Nuevo Contenedor','type' => 'text')); ?></td>
            </tr>
            <tr>
                <td style="padding-top: 20px; background-color: white;"></td>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="VaciarllenarOieSelected();">Vaciado Llenado</a></h3></td>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="UpdateContenedorOieSelected();">Modificar Contenedor</a></h3></td>
            </tr>
        </table>
    </fieldset>
    <table>
        <tr>
            <td>
                <table id="tbDataOie">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('', 'OIE'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fecha'); ?></th>
                            <th><?php echo $this->Paginator->sort('bic_ctn','Ctn'); ?></th>
                            <th><?php echo $this->Paginator->sort('lotes','Lotes a Embalar'); ?></th>
                            <th><?php echo $this->Paginator->sort('proforma','Proforma'); ?></th>
                            <th><?php echo $this->Paginator->sort('booking','Booking'); ?></th>
                            <th><?php echo $this->Paginator->sort('shipping_lines_id','Línea Marítima'); ?></th>
                            <th><?php echo $this->Paginator->sort('motorship_name','Motonave'); ?></th>
                            <th><?php echo $this->Paginator->sort('iso_ctn','LONG'); ?></th>
                            <th><?php echo $this->Paginator->sort('jornally','Jornada Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('novedades',' Bloqueos - (Novedades)'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($searchInfoList){
                        foreach ($searchInfoList as $searchInfo): ?>
                        <tr>
                            <td><?php 
                            //debug($searchInfoList);exit;
                            echo $this->Form->input($searchInfo['PackagingCaffee']['id'],array( 'class'=>'select','type'=>'checkbox'));;?></td>
                            <td><?php echo h($searchInfo['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ReadyContainer']['bic_container']); ?>&nbsp;</td>
                            <td><?php
                            $lotes="";
                            foreach ($searchInfo['DetailPackagingCaffee'] as $remittancesCaffee){
                                $lotes .= "| 3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".
                                        $remittancesCaffee['RemittancesCaffee']['lot_caffee']." x ".$remittancesCaffee['quantity_radicated_bag_out']."| ";
                            }
                            echo h($lotes); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['proforma']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['booking']); ?>&nbsp;</td>
                            <td><?php echo h($shippingLines[$searchInfo['InfoNavy']['shipping_lines_id']]); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['motorship_name']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['PackagingCaffee']['iso_ctn']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['PackagingCaffee']['jornally']); ?>&nbsp;</td>
                            <td><?php echo h($statePackaging[$searchInfo['PackagingCaffee']['state_packaging_id']]); ?>&nbsp;</td>
                            <td><?php $lockCoffee = false;
                            foreach ($searchInfo['DetailPackagingCaffee'] as $remittancesCaffee){
                                foreach($remittancesCaffee['RemittancesCaffee']['RemittancesCaffeeHasNoveltysCaffee'] as $noveltys){
                                    if($noveltys['active'])
                                    {
                                        $lockCoffee = true;
                                        break;
                                    }
                                }
                                if($lockCoffee)
                                {
                                    break;
                                }
                            }
                            echo ($lockCoffee == true) ? "REMESAS BLOQUEADAS":"No tiene café bloqueado" ?>&nbsp;</td>
                            <td class="actions">
                                <a href="javascript:void(0)" onclick="searchNoveltysByOIE(<?php echo h($searchInfo['PackagingCaffee']['id']); ?>);">Ver</a>
                            </td>
                        </tr>
                    <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                            <th><?php echo $this->Paginator->sort('name','Novedad'); ?></th>
                            <th><?php echo $this->Paginator->sort('active','Estado'); ?></th>
                        </tr>
                    </thead>  
                    <tbody id="tableNovelty">
                    </tbody>
                </table>    
            </td>
        </tr>
    </table>

</div>
<script type="text/javascript">
    $('#date').attr('type', 'date');
    
    $('input.select').click(function (event) {
        var check = $(event.target).prop('checked');
            if(check){
                $(event.target).attr('checked', true);
            }
            else{
                $(event.target).attr('checked', false);
            }
        });
    
    function VaciarllenarOieSelected(){
        if($('#jornally').val() !== "" && $('#date').val() !== "" && $('#ready_cantainer_new').val() !== "" ){
            $('input.select').each(function(i, el) {
                var oie = el.id;
                var date = $('#date').val();
                var jornada = $('#jornally').val();
                var contenedor = $('#ready_cantainer_new').val();
                if(el.checked){
                    //console.log(oie+" "+date+" "+jornada);
                    //debugger;
                    vaciarByOieDate(oie,date,jornada,contenedor);
                }
            });
        }
        else{
            alert("El Campo Fecha, Jornada y Contenedor se encuentran vacio. Por favor vefique");
        }
        //window.location.reload();
    }
    
    function vaciarByOieDate(oie,date,jornada,contenedor){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/vaciarOIE?oie="+oie+"&date="+date+"&jornally="+jornada+"&ready_cantainer_new="+contenedor,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "http://siscafe.copcsa.com/PackagingCaffees/index";
                }
            }
        });
    }
    
     function UpdateContenedorOieSelected(){
        if($('#ready_cantainer_new').val() !== "" ){
            $('input.select').each(function(i, el) {
                var oie = el.id;
                var contenedor = $('#ready_cantainer_new').val();
                if(el.checked){
                    UpdateContenedorByOieDate(oie,contenedor);
                }
            });
        }
        else{
            alert("El Campo Contenedor se encuentra vacio. Por favor vefique");
        }
    }
    
    function UpdateContenedorByOieDate(oie,contenedor){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/updateContenedor?oie="+oie+"&ready_cantainer_new="+contenedor,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "http://siscafe.copcsa.com/PackagingCaffees/index";
                }
            }
        });
    }
    
    function searchNoveltysByOIE(oie) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffeeHasNoveltysCaffees/ajaxCoffeeNoveltysByOIE/" + oie,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    $('#tableNovelty').children().remove();
                    $.each(data, function (i, value) {
                        var trHTML = '<tr>';
                        trHTML += '<td>' + value['RemittancesCaffeeHasNoveltysCaffee']['remittances_caffee_id'] + '</td>';
                        trHTML += '<td>' + value['NoveltysCaffee']['name'] + '</td>';
                        if (value['RemittancesCaffeeHasNoveltysCaffee']['active'] === true) {
                            trHTML += '<td>Activo</td><tr>';
                        } else {
                            trHTML += '<td>Inactivo</td><tr>';
                        }
                        $('#tableNovelty').append(trHTML);
                    });
                }
            }
        });
    }

</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
