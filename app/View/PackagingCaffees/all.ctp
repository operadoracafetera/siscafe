<div class="packagingCaffees index">
    <?php echo $this->Html->script('jquery.min');?>
    <?php echo $this->Html->script('jquery-ui.min');?>
    <?php echo $this->Html->css('jquery-ui.min');?>
    <h2><?php 
        if($this->Session->read('User.centerId') == 1)
            echo __('Listado de Embalaje Completados'); 
        ?></h2>
        <?php if($this->Session->read('User.profiles_id') == 6) echo $this->Html->link(__('Buscar por Fechas'), 'javascript:void(0)',array('onclick'=> 'showBIC();')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'OIE'); ?></th>
                <th><?php if($this->Session->read('User.centerId') == 1) echo $this->Paginator->sort('created_date','Fecha Programación'); ?></th>
                <th><?php echo $this->Paginator->sort('state_packaging_id','Estado'); ?></th>
                <th><?php echo $this->Paginator->sort('ready_container_id','BIC'); ?></th>
                <th><?php echo $this->Paginator->sort('iso','ISO'); ?></th>
                <th><?php echo $this->Paginator->sort('lotes'); ?></th>
                <th><?php echo $this->Paginator->sort('packaging_date','Fecha Emb'); ?></th>
                <th><?php //echo $this->Paginator->sort('iso_ctn'); ?></th>
                <th><?php //echo $this->Paginator->sort('rem_ref','Orden servicio'); ?></th>
                <th><?php //echo $this->Paginator->sort('seal_date','Fecha Sellado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($packagingCaffees as $packagingCaffee): ?>
            <tr>
                <td><?php
                //debug($packagingCaffee);exit;
                echo h($packagingCaffee['PackagingCaffee']['id']); ?>&nbsp;</td>
                <td><?php if($this->Session->read('User.centerId') == 1) echo date("Y-m-d", strtotime($packagingCaffee['PackagingCaffee']['created_date']) ); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['StatePackaging']['name']); ?></td>
		<td><?php
                if($packagingCaffee['ReadyContainer']['bic_container'] != null){
		  echo h($packagingCaffee['ReadyContainer']['bic_container']);
		}
		else{
		  echo h("SIN CONTENEDOR");
		}
		 ?>&nbsp;</td>
                <td><?php echo $packagingCaffee['PackagingCaffee']['iso_ctn'];?></td>
                <td><?php 
                if($this->Session->read('User.centerId') == 1){
		  $detailsCaffee = $packagingCaffee['DetailPackagingCaffee'];
		  $tmp;
		  $current="";
		  foreach($detailsCaffee as $detail){
		    $tmp = h('3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee']);
		    if($current != $tmp){
		      echo '3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee'] . " | \n";
		      $current = $tmp;
		    }
		  }
                }
                else{
                    echo $packagingCaffee['PackagingCaffee']['observation'];
                }
		?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['packaging_date']); ?>&nbsp;</td>

                <td><?php //echo h($packagingCaffee['PackagingCaffee']['iso_ctn']); ?>&nbsp;</td>
                <td><?php //echo $this->Html->link(__('P-'.$packagingCaffee['PackagingCaffee']['rem_ref']),array('controller'=>'ServicesOrders','action' => 'view', $packagingCaffee['PackagingCaffee']['rem_ref']), array('target' => '_blank')); ?>&nbsp;</td>
                <td><?php /*if($packagingCaffee['PackagingCaffee']['seal_date'] != null)
			    echo h($packagingCaffee['PackagingCaffee']['seal_date']);
			  else
			    echo h("SIN SELLAR");*/?>&nbsp;</td>
                <td class="actions">
			<?php
                          if($packagingCaffee['ReadyContainer']['bic_container'] != null){
                            if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                                echo $this->Html->link(__('Ver Tarja'), array('controller'=>'ViewPackagings','action' => 'view', $packagingCaffee['PackagingCaffee']['id']),array('target'=>'_blank'));
                            }
                            else if($this->Session->read('User.profiles_id') != 6){
                                echo $this->Html->link(__('Ver Check'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=8&oie=".$packagingCaffee['PackagingCaffee']['id']);
                            }

                            else if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3 || $packagingCaffee['PackagingCaffee']['state_packaging_id'] == 7){
			      echo $this->Html->link(__('Ver Tarja'), array('controller'=>'ViewPackagings','action' => 'view', $packagingCaffee['PackagingCaffee']['id']));
                            }
                          }
			 ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div id="dialogmodalbic" style="width: 200px; height: 100px; overflow-y: scroll;" title="Buscar información embalaje - Criterio">
    <div class="banner" style="display: none"></div>
    <fieldset>
    <span> <p>Buscar información de embalaje por rango de fechas<p></span>
    <ul>
        <li>
            <label for="modalFindFechas">Periodos de embalajes:</label>
            <p>Fecha inicial: <input type="text" id="datepickerIni"></p>
            <p>Fecha final: <input type="text" id="datepickerFin"></p>
        </li>
    </ul>
    <br>
    <button id="modalFindSubmit">Buscar</button>
    <button id="modalFindClean">Limpiar</button>
    </fieldset>
</div>

<script type="text/javascript">
	$(function() {
            $('#dialogmodalbic').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: 600,
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                }
              });
	});
       
       $( "#datepickerIni" ).datepicker({dateFormat: "yy-mm-dd"});
       $( "#datepickerFin" ).datepicker({dateFormat: "yy-mm-dd"});
       
       $("#modalFindClean").click(function () {
           clearAll();
       });
       
       $( "#modalFindSubmit" ).click(function () {

            var datepickerIni = $("#datepickerIni").val();
            var datepickerFin = $("#datepickerFin").val();
            
            if(datepickerIni !== "" && datepickerFin !== ""){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/ViewPackagings/findByDates?dateIni=" + datepickerIni+"&dateFin="+datepickerFin,
                    error: function (msg) {

                    },
                    success: function (data) {
                        if (data !== "") {
                            $("div.banner").css("style:display: line");
                            var htmlResponse = "<div style='background-color: #a7b5b9' class='banner'><span>Información encontrada. </span><a href=http://siscafe.copcsa.com/ViewPackagings/result?dateIni="+datepickerIni+"&dateFin="+datepickerFin+" target='_blank'>Ver resultados de busqueda</a><br><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                        else{
                            $("div.banner").css("style:display: line;");
                            var htmlResponse = "<div style='background-color: #f16526' class='banner' ><span>No arrojo resultado la consulta. Intentelo nuevamente</span><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                    }
                    });
            }
       });
       
        function showBIC(){
            clearAll();
            $( "#dialogmodalbic" ).dialog( "open" );
        }
        
        function clearAll(){
            $("#modalFindBIC").val("");
            $("#modalFindOIE").val("");
            $("#modalFindLotClient").val("");
            $("#datepickerIni").val("");
            $("#datepickerFin").val("");
            $("div.banner").replaceWith('<div class="banner" style="display: none"></div>');
            $("div.banner").css("style:display: line");
        }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php //if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Vaciados'), array('controller' => 'PackagingCaffees', 'action' => 'empty_oie')); ?></li>
        <li><?php //if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Cancelados'), array('controller' => 'PackagingCaffees', 'action' => 'cancelled_oie')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes en Proceso'), array('controller' => 'PackagingCaffees', 'action' => 'process_oie')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Confirmado'), array('controller' => 'PackagingCaffees', 'action' => 'confirm_oie')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Todos Embalajes'), array('controller' => 'PackagingCaffees', 'action' => 'allctns')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Buscar CTNS'), array('controller' => 'PackagingCaffees', 'action' => 'empty_loaded_oie')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Buscar CTN'), array('controller' => 'PackagingCaffees', 'action' => 'empty_loaded_oie')); ?></li>
        <li><?php if($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') == 6) echo $this->Html->link(__('Buscar CTN'), array('controller' => 'PackagingCaffees', 'action' => 'search_packaging')); ?></li>
        <li><?php if($this->Session->read('User.profiles_id') != 6) {
                    echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); 
                  }
                  else{
                      echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'terminal')); 
                  }
        ?></li>
    </ul>
</div>
