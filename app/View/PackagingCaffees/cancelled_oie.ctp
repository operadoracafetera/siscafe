<div class="packagingCaffees index">
    <h2><?php echo __('Listado de Embalaje Cancelados'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'OIE'); ?></th>
                <th><?php echo $this->Paginator->sort('created_date','Fecha Ordenado'); ?></th>
                <th><?php echo $this->Paginator->sort('state_packaging_id','Estado Embalaje'); ?></th>
                <th><?php echo $this->Paginator->sort('ready_container_id','Contenedor'); ?></th>
                <th><?php echo $this->Paginator->sort('lotes'); ?></th>
                <th><?php echo $this->Paginator->sort('cancelled_date','Fecha Cancelado'); ?></th>
                <th><?php echo $this->Paginator->sort('iso_ctn'); ?></th>
                <th><?php echo $this->Paginator->sort('seal_date','Fecha Sellado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($packagingCaffees as $packagingCaffee): ?>
            <tr>
                <td><?php
                //debug($packagingCaffee);exit;
                echo h($packagingCaffee['PackagingCaffee']['id']); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php 
		  if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 6){
		      echo h("EMBALAJE CANCELADO");
		  }
		   ?>&nbsp;</td>
                <td><?php 
		if($packagingCaffee['ReadyContainer']['bic_container'] != null){
		  echo h($packagingCaffee['ReadyContainer']['bic_container']);
		}
		else{
		  echo h("CONTENEDOR NO ASIGNADO");
		}
		 ?>&nbsp;</td>
                <td><?php 
		  $detailsCaffee = $packagingCaffee['DetailPackagingCaffee'];
		  $tmp;
		  $current="";
                  if($detailsCaffee == NULL)
                  {
                      echo "SIN LOTE ASIGNADO";
                  }
		  foreach($detailsCaffee as $detail){
		    $tmp = h('3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee']);
		    if($current != $tmp){
		      echo '3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee'] . " | \n";
		      $current = $tmp;
		    }
		  }
		?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['cancelled_date']); ?>&nbsp;</td>
                <td><?php if($packagingCaffee['PackagingCaffee']['iso_ctn'] == null){
                            echo "N/A";
                }
                    ?>&nbsp;</td>
  
                <td><?php if($packagingCaffee['PackagingCaffee']['seal_date'] != null)
			    echo h($packagingCaffee['PackagingCaffee']['seal_date']);
			  else
			    echo h("SIN SELLAR");?>&nbsp;</td>
                <td class="actions">
                          <?php echo $this->Html->link(__('Imprimir'), "http://siscafe.copcsa.com:8080/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=1&oie=".$packagingCaffee['PackagingCaffee']['id']); ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Embalajes Vaciados'), array('controller' => 'PackagingCaffees', 'action' => 'empty_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes Completados'), array('controller' => 'PackagingCaffees', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes en Proceso'), array('controller' => 'PackagingCaffees', 'action' => 'process_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Embalajes Confirmado'), array('controller' => 'PackagingCaffees', 'action' => 'confirm_oie')); ?></li>
        <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?></li>
    </ul>
</div>
