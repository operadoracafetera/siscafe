<div class="packagingCaffees index">
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('PackagingCaffee');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda OIE'); ?></legend>
                    <style>td.readycantainer label{margin-left: 13%;}</style>
        <table>
            <tr>
                <td><?php echo $this->Form->input('oie',array('label' => 'OIE'));?></td>
                <td><?php echo $this->Form->input('lote',array('label' => 'Exportador-Lote'));?></td>
                <td><?php echo $this->Form->input('bic_container',array('label' => 'Contenedor'));?></td>
                <td><?php echo $this->Form->end(__('Buscar')); ?></td>
            </tr>  
        </table>
                    <br>
            <table style="width: 100%;"> 
            <tr>
                <td style="width:360px;">
                    <?php echo $this->Form->input('date',array('label' => 'Fecha de embalaje','type' => 'text')); ?>
                    <p class="date" style="color: red;" hidden="true"><strong>* Campo requerido</strong></p>
                </td>
                <td style="width:260px;">
                    <?php echo $this->Form->input('jornally',array('label' => 'Jornada', 'style' => 'width:125%', 'empty'=>'Seleccione...',
                        'options' => array('M' => 'Mañana', 'T' => 'Tarde')));?>
                    <p class="jornally" style="color: red;" hidden="true"><strong>* Campo requerido</strong></p>
                </td>
                <td  class="readycantainer" colspan="2">
                    <?php echo $this->Form->input('ready_cantainer_new',array('style' => 'width:56%;margin-left: 13%;', 'label' => 'Nuevo Contenedor','type' => 'text')); ?>
                    <p class="ready_cantainer_new" style="color: red;" hidden="true"><strong>* Campo requerido</strong></p>
                </td>                
            </tr>
            <tr></tr>
            <tr>
                <td colspan="4">
                    <?php echo $this->Form->input('observation',array('label' => 'Observaciones','type' => 'textarea')); ?>
                    <p class="observation" style="color: red;" hidden="true"><strong>* Campo requerido</strong></p>
                </td>
            </tr>
            <tr>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="EmptyOIESelected();">Proceso Vaciado</a></h3></td>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="EmptyLoadedOIESelected();">Vaciado Llenado</a></h3></td>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="UpdateContainerOieSelected();">Modificar Contenedor</a></h3></td>
                <td class="actions" style="padding-top: 20px;"><h3><a href="javascript:void(0)" onclick="BreakfreeLoteSelected();">Liberar Contenedor</a></h3></td>
            </tr>
            <tr><td colspan="4"><p class="breakfreeLote" style="color: red; text-align: right;" hidden="true"><strong>Solo se pueden LIBERAR los CONTENEDORES que se encuentren en estado de confirmacion</strong></p></td></tr>
        </table>
    </fieldset>
    <table>
        <tr>
            <td>
                <table id="tbDataOie">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('', 'OIE'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_date','Fecha llenado'); ?></th>
                            <th><?php echo $this->Paginator->sort('empty_date','Fecha vaciado'); ?></th>
                            <th><?php echo $this->Paginator->sort('packaging_date','Estado de Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('bic_ctn','Ctn'); ?></th>
                            <th><?php echo $this->Paginator->sort('lotes','Lotes'); ?></th>
                            <th><?php echo $this->Paginator->sort('proforma','Proforma'); ?></th>
                            <th><?php echo $this->Paginator->sort('booking','Booking'); ?></th>
                            <th><?php echo $this->Paginator->sort('motorship_name','Motonave'); ?></th>
                            <th><?php echo $this->Paginator->sort('iso_ctn','LONG'); ?></th>
                            <th><?php echo $this->Paginator->sort('jetty','Puerto Terminal'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($searchInfoList){
                        foreach ($searchInfoList as $searchInfo): ?>
                        <tr>
                            <td><?php 
//                            debug($searchInfoList);exit;
                            echo $this->Form->input($searchInfo['ViewPackaging']['oie'],array( 'class'=>'select','type'=>'checkbox'));;?></td>
                            <td><?php echo h($searchInfo['ViewPackaging']['packaging_date']); ?>&nbsp;</td>
			    <td><?php echo h($searchInfo['ViewPackaging']['empty_date']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['estado_embalaje']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['bic_ctn']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['lotes']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['proforma']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['booking']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['motorship_name']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['iso_ctn']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ViewPackaging']['jetty']); ?>&nbsp;</td>
                            <td class="actions">
                                <?php
                                    if($searchInfo['ViewPackaging']['estado_embalaje'] == 'EMBALAJE COMPLETADO'){
                                        if(date($searchInfo['ViewPackaging']['packaging_date']) <= date('2019-07-24')){
                  			  if($searchInfo['ViewPackaging']['upload_tracking_photos_date']){
                   			    echo $this->Html->link(__('Fot1 Ctn'), array('controller'=>'PackagingCaffees','action' => 'getTrackingOie', $searchInfo['ViewPackaging']['oie']),array('target' => "_blank"));
		    			    echo $this->Html->link(__('Fot2 Ctn'), array('controller'=>'OperationTrackings','action' => 'addctns', $searchInfo['ViewPackaging']['oie']),array('target' => "_blank"));
                  			  }
                  			  else{
                    			    echo $this->Html->link(__('Fotografias Ctn'), array('controller'=>'PackagingCaffees','action' => 'trackingOie', $searchInfo['ViewPackaging']['oie']),array('target' => "_blank"));
                  			  }
                			}
                			else{
                 		          echo $this->Html->link(__('Fotografias Ctn'), array('controller'=>'OperationTrackings','action' => 'addctns', $searchInfo['ViewPackaging']['oie']),array('target' => "_blank"));
                			}
                                        if($searchInfo['ViewPackaging']['jetty'] == 'BUN'){
                                                echo $this->Html->link(__('V. Ch'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=8&oie=".$searchInfo['ViewPackaging']['oie']);
                                                echo $this->Html->link(__('V.T'), array('controller'=>'ViewPackagings','action' => 'view', $searchInfo['ViewPackaging']['oie']));
                                        }
                                        else{
                                                echo $this->Html->link(__('V.T'), array('controller'=>'ViewPackaging2s','action' => 'view', $searchInfo['ViewPackaging']['oie']));
                                        }
                                    }
                                 ?>
                            </td>
                        </tr>
                    <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            
        </tr>
    </table>

</div>
<script type="text/javascript">
    $('#date').attr('type', 'date');
    
    $('input.select').click(function (event) {
        var check = $(event.target).prop('checked');
            if(check){
                $(event.target).attr('checked', true);
            }
            else{
                $(event.target).attr('checked', false);
            }
        });

    function BreakfreeLoteSelected(){
        $('input.select').each(function(i, el) {
        var oie = el.id;
            if(el.checked){
                $('.breakfreeLote').show();
                breakfreeLoteByOieDate(oie);
            }else{
                alert("Seleccione la OIE a liberar. ");
            }
        }); 
            
    }   
    
    function breakfreeLoteByOieDate(oie){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/breakfreeLoteOIE?oie="+oie,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "/PackagingCaffees/empty_loaded_oie";
                }
            }
        });
    }

    function EmptyOIESelected(){
       
        if( $('#observation').val() !== "" ){
            $('input.select').each(function(i, el) {
            var oie = el.id;
            var observacion = $('#observation').val();                
                if(el.checked){
                    processEmptyByOieDate(oie,observacion);
                }
            });            
        }else{
            $('#observation').css('border-color','red'); 
            $('.observation').show(); 
            alert("El Campo Observaciones se encuentran vacio. Por favor vefique");
        }
    }
    
    function processEmptyByOieDate(oie,observacion){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/processEmptyOIE?oie="+oie+"&observation="+observacion,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "/PackagingCaffees/index";
                }
            }
        });
    }

    function EmptyLoadedOIESelected(){
        if($('#jornally').val() !== "" && $('#date').val() !== "" && $('#ready_cantainer_new').val() !== "" && $('#observation').val() !== "" ){
            $('input.select').each(function(i, el) {
                var oie = el.id;
                var date = $('#date').val();
                var jornada = $('#jornally').val();
                var contenedor = $('#ready_cantainer_new').val();                
                var observacion = $('#observation').val();                
                if(el.checked){
                    //console.log(oie+" "+date+" "+jornada);
                    //debugger;
                    emptyByOieDate(oie,date,jornada,contenedor,observacion);
                }
            });
        }
        else{                
                $('#date').css('border-color','#f16526');
                $('#jornally').css('border-color','#f16526');
                $('#ready_cantainer_new').css('border-color','#f16526');               
                $('#observation').css('border-color','#f16526'); 
                $('.date').show();
                $('.jornally').show();
                $('.ready_cantainer_new').show();              
                $('.observation').show(); 
            alert("El Campo Fecha, Jornada, Contenedor y Observaciones se encuentran vacio. Por favor vefique");
        }
        //window.location.reload();
    }
    
    function emptyByOieDate(oie,date,jornada,contenedor,observacion){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/emptyOIE?oie="+oie+"&date="+date+"&jornally="+jornada+"&ready_cantainer_new="+contenedor+"&observation="+observacion,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "/PackagingCaffees/index";
                }
            }
        });
    }

     function UpdateContainerOieSelected(){
        if($('#ready_cantainer_new').val() !== "" ){
            $('input.select').each(function(i, el) {
                var oie = el.id;
                var contenedor = $('#ready_cantainer_new').val();
                if(el.checked){
                    UpdateContainerByOieDate(oie,contenedor);
                }
            });
        }
        else{
            $('#ready_cantainer_new').css('border-color','red'); 
            $('.ready_cantainer_new').show(); 
            alert("El Campo Contenedor se encuentra vacio. Por favor vefique");
        }
    }
    
    function UpdateContainerByOieDate(oie,contenedor){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/updateContainer?oie="+oie+"&ready_cantainer_new="+contenedor,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                    window.location.href = "../PackagingCaffees/index";
                }
            }
        });
    }
    
    function searchNoveltysByOIE(oie) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffeeHasNoveltysCaffees/ajaxCoffeeNoveltysByOIE/" + oie,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    $('#tableNovelty').children().remove();
                    $.each(data, function (i, value) {
                        var trHTML = '<tr>';
                        trHTML += '<td>' + value['RemittancesCaffeeHasNoveltysCaffee']['remittances_caffee_id'] + '</td>';
                        trHTML += '<td>' + value['NoveltysCaffee']['name'] + '</td>';
                        if (value['RemittancesCaffeeHasNoveltysCaffee']['active'] === true) {
                            trHTML += '<td>Activo</td><tr>';
                        } else {
                            trHTML += '<td>Inactivo</td><tr>';
                        }
                        $('#tableNovelty').append(trHTML);
                    });
                }
            }
        });
    }

</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'index')); ?></li>
    </ul>
</div>
