<div class="packagingCaffees index">
<?php echo $this->Form->create('PackagingCaffee');  ?>
   <fieldset>
        <legend><?php echo __('Vaciado de Contenedor '.$infoOIE[0]['ReadyContainer']['bic_container'] .' de la OIE '.$OIE); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('cause_emptied_ctn_id',array('label' => 'Causa del vaciado','options' => $noveltysCoffee,'multiple' => false));?></td>
                <td><?php echo $this->Form->input('created_date',array('label' => 'Fecha de creación', 'type'=>'text', 'disabled'=>true, 'value'=>$infoOIE[0]['PackagingCaffee']['created_date']));?></td>
            </tr>  
            <tr>
                <td><?php echo $this->Form->input('observation',array('label' => 'Causa del vaciado'));?></td>
            </tr>
        </table>
        <?php echo $this->Form->end(__('Submit')); ?>
    </fieldset>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'allOie')); ?></li>
    </ul>
</div>
