<div class="packagingCaffees form">
<?php echo $this->Form->create('PackagingCaffee'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Contenedor '); ?></legend>
	<?php
		echo $this->Form->input('bic_container',array('label'=>'Numero de contenedor'));
                echo $this->Form->label('img_ctn','Fotografia contenedor (Frente Puerta contenedor)');
		echo $this->Form->file('img_ctn');
		//echo $this->Form->input('date_registre');
		//echo $this->Form->input('shipping_lines_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Embalaje'), array('controller'=>'PackagingCaffees','action' => 'index')); ?></li>
	</ul>
</div>
