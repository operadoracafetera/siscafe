<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -150px;
            right: 110px;
            margin-top: -100px;
        background-color: #003d4c;
        padding-top: 10px;
        min-width: 150px;
        height: 110px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>
<div class="packagingCaffees index">
    <?php echo $this->Html->script('jquery.min');?>
    <?php echo $this->Html->script('jquery-ui.min');?>
    <?php echo $this->Html->css('jquery-ui.min');?>
    <h2><?php 
        if($this->Session->read('User.centerId') == 1)
            echo __('Listado de Embalaje Completados'); 
        else
            echo __('Listado de Llenados de Café'); 
        ?></h2>
    <?php if($this->Session->read('User.opera') != 1) echo $this->Html->link(__('Registrar Tarja Embalaje'), array('action' => 'add')); ?>
    <?php if($this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Buscar por embalaje'), 'javascript:void(0)',array('onclick'=> 'showBIC();')); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'OIE'); ?></th>
                <th><?php if($this->Session->read('User.centerId') == 1) echo $this->Paginator->sort('created_date','Fecha Ordenado'); ?></th>
                <th><?php echo $this->Paginator->sort('state_packaging_id','Estado'); ?></th>
                <th><?php echo $this->Paginator->sort('ready_container_id','BIC'); ?></th>
                <th><?php if($this->Session->read('User.centerId') != 1) echo $this->Paginator->sort('autorization','Num Autori.'); ?></th>
                <th><?php echo $this->Paginator->sort('lotes'); ?></th>
                <th><?php echo $this->Paginator->sort('packaging_date','Fecha Emb'); ?></th>
                <th><?php echo $this->Paginator->sort('iso_ctn'); ?></th>
                <th><?php echo $this->Paginator->sort('rx_status_spb_navis','VGM N4 Ok?'); ?></th>
                <th><?php echo $this->Paginator->sort('jetty','Puerto Maritimo'); ?></th>
                <th><?php echo $this->Paginator->sort('packaging_date','Trazabilidad?'); ?></th>
                <th><?php //echo $this->Paginator->sort('rem_ref','Orden servicio'); ?></th>
                <th><?php //echo $this->Paginator->sort('seal_date','Fecha Sellado'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
	<?php foreach ($packagingCaffees as $packagingCaffee): ?>
            <tr>
                <td><?php
                //debug($packagingCaffee);exit;
                echo h($packagingCaffee['PackagingCaffee']['id']); ?>&nbsp;</td>
                <td><?php if($this->Session->read('User.opera') == 1) echo h($packagingCaffee['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['StatePackaging']['name']); ?></td>
		<td><?php
                if($packagingCaffee['ReadyContainer']['bic_container'] != null){
		  echo h($packagingCaffee['ReadyContainer']['bic_container']);
		}
		else{
		  echo h("SIN CONTENEDOR");
		}
		 ?>&nbsp;</td>
                <td><?php if($this->Session->read('User.centerId') != 1) echo $packagingCaffee['PackagingCaffee']['autorization'];?></td>
                <td><?php 
                if($this->Session->read('User.opera') == 1){
		  $detailsCaffee = $packagingCaffee['DetailPackagingCaffee'];
		  $tmp;
		  $current="";
		  foreach($detailsCaffee as $detail){
		    $tmp = h('3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee']);
		    if($current != $tmp){
		      echo '3-'.$detail['RemittancesCaffee']['Client']['exporter_code'].'-'.$detail['RemittancesCaffee']['lot_caffee'] . " | \n";
		      $current = $tmp;
		    }
		  }
                }
                else{
                    echo $packagingCaffee['PackagingCaffee']['observation'];
                }
		?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['packaging_date']); ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['iso_ctn']); ?>&nbsp;</td>
                <td><?php if($packagingCaffee['PackagingCaffee']['rx_status_spb_navis'] == 1) {echo  "SI";}
                          else if($packagingCaffee['PackagingCaffee']['seal_date'] && $packagingCaffee['PackagingCaffee']['rx_status_spb_navis'] ==0){
                              echo $this->Html->link(__('Enviar VGM'),'javascript:void(0)',array('id'=>'oie','value'=>$packagingCaffee['PackagingCaffee']['id'],'onclick'=> 'resendN4VGM();' ) );
                          }
                          else{echo  "Sin Sellar";}
                ?>&nbsp;</td>
                <td><?php echo h($packagingCaffee['PackagingCaffee']['jetty']); ?>&nbsp;</td>
                <td class="actions"><?php 
                if(date($packagingCaffee['PackagingCaffee']['created_date']) <= date('2019-07-24')){
                  if($packagingCaffee['PackagingCaffee']['upload_tracking_photos_date']){
                    echo $this->Html->link(__('Fot1 Ctn'), array('controller'=>'PackagingCaffees','action' => 'getTrackingOie', $packagingCaffee['PackagingCaffee']['id']),array('target' => "_blank"));
		    echo $this->Html->link(__('Fot2 Ctn'), array('controller'=>'OperationTrackings','action' => 'addctns', $packagingCaffee['PackagingCaffee']['id']),array('target' => "_blank"));
                  }
                  else{
                    echo $this->Html->link(__('Fotografias Ctn'), array('controller'=>'PackagingCaffees','action' => 'trackingOie', $packagingCaffee['PackagingCaffee']['id']),array('target' => "_blank"));
                  }
                }
                else{
                 echo $this->Html->link(__('Fotografias Ctn'), array('controller'=>'OperationTrackings','action' => 'addctns', $packagingCaffee['PackagingCaffee']['id']),array('target' => "_blank"));
                }
                ?></td>
                <td><?php //echo $this->Html->link(__('P-'.$packagingCaffee['PackagingCaffee']['rem_ref']),array('controller'=>'ServicesOrders','action' => 'view', $packagingCaffee['PackagingCaffee']['rem_ref']), array('target' => '_blank')); ?>&nbsp;</td>
                <td><?php /*if($packagingCaffee['PackagingCaffee']['seal_date'] != null)
			    echo h($packagingCaffee['PackagingCaffee']['seal_date']);
			  else
			    echo h("SIN SELLAR");*/?>&nbsp;</td>
                <td class="actions">
			<?php if($packagingCaffee['PackagingCaffee']['iso_ctn'] == null && $this->Session->read('User.profiles_id') != 6){
                              echo $this->Html->link(__('Sellar Ctn'), array('controller'=>'PackagingCaffees', 'action' => 'seal', $packagingCaffee['PackagingCaffee']['id']));
                            }?>
					<div class="dropdown">
                    <button class="dropbtn"><strong>Menú</strong></button>
                    <div class="dropdown-content">
					<?php
                          if($packagingCaffee['ReadyContainer']['bic_container'] != null){
							if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                                if($this->Session->read('User.opera') == 1){
                                        echo $this->Html->link(__('Ver Tarja'), array('controller'=>'ViewPackagings','action' => 'view', $packagingCaffee['PackagingCaffee']['id']));
                                }
                                else{
			echo $this->Html->link(__('Eliminar'),array('controller'=>'PackagingCaffees', 'action' => 'delete', $packagingCaffee['PackagingCaffee']['id']),array('confirm'=>'¿Esta seguro de eliminar el registro?'));
                                        echo $this->Html->link(__('Ver Tarja'), array('controller'=>'ViewPackaging2s','action' => 'view', $packagingCaffee['PackagingCaffee']['id']));
                                        echo $this->Html->link(__('Editar'), array('controller'=>'PackagingCaffees','action' => 'edit', $packagingCaffee['PackagingCaffee']['id']));
                                }
                            }
                            if($packagingCaffee['PackagingCaffee']['iso_ctn'] == null && $this->Session->read('User.profiles_id') != 6){
                              echo $this->Html->link(__('Sellar Ctn'), array('controller'=>'PackagingCaffees', 'action' => 'seal', $packagingCaffee['PackagingCaffee']['id']));
                            }
                            if($packagingCaffee['PackagingCaffee']['checking_ctn_id'] == null){
                              echo $this->Html->link(__('Check Ctn'), array('controller'=>'CheckingCtns', 'action' => 'check_ctn', $packagingCaffee['PackagingCaffee']['id']));
                            }
			    if($this->Session->read('User.opera') == 1){
			      echo $this->Html->link(__('Ver Check'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=8&oie=".$packagingCaffee['PackagingCaffee']['id']);                              
			      
                            }
                            else if($this->Session->read('User.profiles_id') != 6){
                                echo $this->Html->link(__('Ver Check'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=8&oie=".$packagingCaffee['PackagingCaffee']['id']);
                            }
                            
                           
                            if($packagingCaffee['PackagingCaffee']['state_packaging_id'] == 3){
                                    //echo $this->Html->link(__('Sellar Ctn'), array('action' => 'seal', $packagingCaffee['PackagingCaffee']['id']));
                                }
                          }
                          else{
                              echo $this->Html->link(__('Contenedor'), array('action' => 'ctnr', $packagingCaffee['PackagingCaffee']['id']));
                          }
						?>
					</div>
                    </div>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div id="dialogmodalbic" style="width: 200px; height: 100px; overflow-y: scroll;" title="Buscar información embalaje - Criterio">
    <div class="banner" style="display: none"></div>
    <fieldset>
    <span> <p>En este cuadro de busqueda permite encontrar información por diferentes identificadores de la exportación<p></span>
    <ul>
        <li>
            <label for="modalFindOIE">OIE: </label>
            <input id="modalFindOIE" type="text" >
        </li>
        <li>
            <label for="modalFindBIC">Número de contenedor:</label>
            <input id="modalFindBIC" type="text" >
        </li>
        <li>
            <label for="modalFindLotClient">Número de lote p E.j: 450-3340</label>
            <input id="modalFindLotClient" type="text" >
        </li>
        <br><br>
        <li>
            <label for="modalFindFechas">Periodos de embalajes:</label>
            <p>Fecha inicial: <input type="text" id="datepickerIni"></p>
            <p>Fecha final: <input type="text" id="datepickerFin"></p>
        </li>
    </ul>
    <br>
    <button id="modalFindSubmit">Buscar</button>
    <button id="modalFindClean">Limpiar</button>
    </fieldset>
</div>

<script type="text/javascript">
    
    function resendN4VGM() {
        var idOie = $("a#oie").attr('value');
        if(!completeWeightingCoffeeSISCAFE(idOie)){
          alert("Se Envió VGM SISCAFE -> Navis del contenedor exitosamente!");
          window.location.href = "/PackagingCaffees/index";
        }
        else{
          alert("Contenedor no ha sido finalizado, tiene pendiente sacos por embalar � NO tiene contenedor registrado");   
        }
    }
    
    function weightContainerToN4(container, weight) {
            console.log(container + " - " + weight);
            $.ajax({
                url: "http://siscafe.copcsa.com:8181/SISCAFE-IntegrationWS/WS/integration/getWeightByContainer?container=" + container + "&weight=" + weight,
                crossDomain: true,
                async: false,
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    console.log(container + " " + weight);
                    return true;
                },
                success: function (data) {
                    console.log(data);
                    return false;
                }
            });
        }

        function completeWeightingCoffeeSISCAFE(oie) {
            var idOie = {'OIE':oie};
            $.ajax({
                type: "POST",
                datatype: "json",
                url: '<?= Router::url(['controller' => 'DetailPackagingCaffees', 'action' => 'findContainerByOie']) ?>',
                data: idOie,
                async: false,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {//El embalaje se finaliza si y solo si tiene una unidad asignada.
                    if (data !== "") {
                        var result = JSON.parse(data);
                        var container = result.ViewPackaging.bic_ctn;
                        console.log(result.ViewPackaging);
                        console.log("container: " + container);
                        var total_pallet_embalados = result.ViewPackaging.total_pallets_embalaje;
                        console.log("total_pallet_embalados: " + total_pallet_embalados);
                        var total_pallets_remesa = result.ViewPackaging.total_pallets_remesa;
                        console.log("total_pallet_remesas: " + total_pallets_remesa);
                        var pctjPallets = (total_pallet_embalados / total_pallets_remesa);
                        console.log("pctjPallets: " + pctjPallets);
                        var tare_estibas = result.ViewPackaging.tare_estibas_descargue;
                        console.log("tare_estibas: " + tare_estibas);
                        var tare_embalaje = (pctjPallets * tare_estibas);
                        console.log("tare_embalaje: " + tare_embalaje);
                        var total_peso_cafe = result.ViewPackaging.total_cafe_empaque;
                        console.log("total_peso_cafe: " + total_peso_cafe);
                        var modalidad = result.ViewPackaging.packaging_mode;
                        console.log("modalidad: " + modalidad);
                        var tare_empaque = result.ViewPackaging.tare_empaque;
                        console.log("tare_empaque: " + tare_empaque);
                        var weightContainerNet = (total_peso_cafe - (tare_embalaje));
                        console.log("weightContainerNet: " + weightContainerNet);
                        if (modalidad === 'Granel') {
                            weightContainerNet = weightContainerNet - tare_empaque;
                        }
                       return weightContainerToN4(oie, weightContainerNet);
                        //alert("Se finalizo el proceso de embalaje del contenedor "+container);
                    } else {
                        return false;
                    }
                }
            });
        }
    
	$(function() {
            $('#dialogmodalbic').dialog({
                autoOpen: false,
                modal: true,
                height: "auto",
                width: 600,
                resizable: false,
                show: {
                  effect: "blind",
                  duration: 100
                },
                hide: {
                  effect: "explode",
                  duration: 100
                }
              });
	});
       
       $( "#datepickerIni" ).datepicker({dateFormat: "yy-mm-dd"});
       $( "#datepickerFin" ).datepicker({dateFormat: "yy-mm-dd"});
       
       $("#modalFindClean").click(function () {
           clearAll();
       });
       
       $( "#modalFindSubmit" ).click(function () {
            var oie = $("#modalFindOIE").val();
            var bic = $("#modalFindBIC").val();
            var lotCoffee = $("#modalFindLotClient").val();
            var datepickerIni = $("#datepickerIni").val();
            var datepickerFin = $("#datepickerFin").val();
            if(oie !== ""){
                $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ViewPackagings/findByOIE/" + oie,
                error: function (msg) {
                    
                },
                success: function (data) {
                    if (data !== "") {
                        var viewPackagingData = JSON.parse(data);
                        $("div.banner").css("style:display: line");
                        var htmlResponse = "<div style='background-color: #a7b5b9' class='banner'><span>Contenedor encontrado "+viewPackagingData.ViewPackaging.bic_ctn+"!.</span><a href=http://siscafe.copcsa.com/ViewPackagings/view/"+oie+" target='_blank'>Ver Tarja</a><br><br></div>";
                        $("div.banner").replaceWith(htmlResponse);
                    }
                    else{
                        $("div.banner").css("style:display: line;");
                        var htmlResponse = "<div style='background-color: #f16526' class='banner' ><span>No arrojo resultado la consulta. Intentelo nuevamente</span><br></div>";
                        $("div.banner").replaceWith(htmlResponse);
                    }
                }
                });
            }
            else if(bic !== ""){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/ViewPackagings/findByBic/" + bic,
                    error: function (msg) {

                    },
                    success: function (data) {
                        if (data !== "") {
                            var viewPackagingData = JSON.parse(data);
                            console.log(viewPackagingData);
                            $("div.banner").css("style:display: line");
                            var htmlResponse = "<div style='background-color: #a7b5b9' class='banner'><span>Contenedor encontrado "+viewPackagingData.ViewPackaging.bic_ctn+".</span><a href=http://siscafe.copcsa.com/ViewPackagings/view/"+viewPackagingData.ViewPackaging.oie+" target='_blank'>Ver Tarja</a><br><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                        else{
                            $("div.banner").css("style:display: line;");
                            var htmlResponse = "<div style='background-color: #f16526' class='banner' ><span>No arrojo resultado la consulta. Intentelo nuevamente</span><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                    }
                    });
            }
            else if(lotCoffee !== ""){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/ViewPackagings/findByLotClient/" + lotCoffee,
                    error: function (msg) {

                    },
                    success: function (data) {
                        if (data !== "") {
                            var viewPackagingData = JSON.parse(data);
                            console.log(viewPackagingData[0]);
                            $("div.banner").css("style:display: line");
                            var htmlResponse = "<div style='background-color: #a7b5b9' class='banner'><span>Contenedor encontrado "+viewPackagingData[0].ViewPackaging.bic_ctn+".</span><a href=http://siscafe.copcsa.com/ViewPackagings/view/"+viewPackagingData[0].ViewPackaging.oie+" target='_blank'>Ver Tarja</a><br><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                        else{
                            $("div.banner").css("style:display: line;");
                            var htmlResponse = "<div style='background-color: #f16526' class='banner' ><span>No arrojo resultado la consulta. Intentelo nuevamente</span><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                    }
                    });
            }
            else if(datepickerIni !== "" && datepickerFin !== ""){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: "/ViewPackagings/findByDates?dateIni=" + datepickerIni+"&dateFin="+datepickerFin,
                    error: function (msg) {

                    },
                    success: function (data) {
                        if (data !== "") {
                            $("div.banner").css("style:display: line");
                            var htmlResponse = "<div style='background-color: #a7b5b9' class='banner'><span>Información encontrada. </span><a href=http://siscafe.copcsa.com/ViewPackagings/result?dateIni="+datepickerIni+"&dateFin="+datepickerFin+" target='_blank'>Ver resultados de busqueda</a><br><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                        else{
                            $("div.banner").css("style:display: line;");
                            var htmlResponse = "<div style='background-color: #f16526' class='banner' ><span>No arrojo resultado la consulta. Intentelo nuevamente</span><br></div>";
                            $("div.banner").replaceWith(htmlResponse);
                        }
                    }
                    });
            }
       });
       
        function showBIC(){
            clearAll();
            $( "#dialogmodalbic" ).dialog( "open" );
        }
        
        function clearAll(){
            $("#modalFindBIC").val("");
            $("#modalFindOIE").val("");
            $("#modalFindLotClient").val("");
            $("#datepickerIni").val("");
            $("#datepickerFin").val("");
            $("div.banner").replaceWith('<div class="banner" style="display: none"></div>');
            $("div.banner").css("style:display: line");
        }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php //if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Vaciados'), array('controller' => 'PackagingCaffees', 'action' => 'empty_oie')); ?></li>
        <li><?php //if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Cancelados'), array('controller' => 'PackagingCaffees', 'action' => 'cancelled_oie')); ?></li>
        <li><?php if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes en Proceso'), array('controller' => 'PackagingCaffees', 'action' => 'process_oie')); ?></li>
        <li><?php if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Embalajes Confirmado'), array('controller' => 'PackagingCaffees', 'action' => 'confirm_oie')); ?></li>
        <li><?php if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Todos Embalajes'), array('controller' => 'ViewTrackgingPackagings', 'action' => 'index')); ?></li>
        <li><?php if($this->Session->read('User.profiles_id') != 6) echo $this->Html->link(__('Buscar CTNS'), array('controller' => 'PackagingCaffees', 'action' => 'empty_loaded_oie')); ?></li>
        <li><?php if($this->Session->read('User.opera') == 1 && $this->Session->read('User.profiles_id') == 6) echo $this->Html->link(__('Buscar CTN'), array('controller' => 'PackagingCaffees', 'action' => 'search_packging')); ?></li>
        <li><?php if($this->Session->read('User.profiles_id') != 6) {
                    echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); 
                  }
                  else{
                      echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'terminal')); 
                  }
        ?></li>
    </ul>
</div>
