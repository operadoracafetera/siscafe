<div class="packagingCaffees index">
<style>
    .dropbtn {
        background-color: #62af56;
        color: white;
        padding: 7px;
        font-size: 14px;
        border: 2px solid #000;
        border-radius: 4px;
        cursor: pointer;
    }
    .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
            left: -160px;
            right: 120px;
            margin-top: -100px;
        background-color: #f9f9f9;
        min-width: 150px;
        height: 90px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        height: 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown-content a:hover {background-color: #dcdcdc}
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .dropdown:hover .dropbtn {
        background-color: #3e8e41;
    }
</style>
<?php echo $this->Html->script('jquery.min');
echo $this->Form->create('PackagingCaffee');  ?>
    <fieldset>
        <legend><?php echo __('Busqueda OIE'); ?></legend>
        <table>
            <tr>
                <td><?php echo $this->Form->input('oie',array('label' => 'OIE'));?></td>
                <td><?php echo $this->Form->input('proforma',array('label' => 'Proforma'));?></td>
                <td><?php echo $this->Form->input('booking',array('label' => 'Número de Booking'));?></td>
            </tr>
            <tr>
                <td><?php echo $this->Form->input('lote',array('label' => 'Exportador-Lote'));?></td>
                <td><?php echo $this->Form->input('remesa',array('label' => 'Remesa'));?></td>
                <td><?php echo $this->Form->end(__('Buscar')); ?></td>
            </tr>
        </table>
                    </br>
            <table style="width: 69%; b"> 
            <tr>
            <td><?php echo $this->Form->input('date',array('label' => 'Fecha de embalaje','type' => 'text')); ?></td>
            <td><?php echo $this->Form->input('jornally',array('label' => 'Jornada','empty'=>'Seleccione...',
                    'options' => array('M' => 'Mañana', 'T' => 'Tarde')));?></td>
            <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="embalarOieSelected();">Embalar</a></h3></td>
            </tr>
        </table>
    </fieldset>
    <table>
        <tr>
            <td>
                <table id="tbDataOie">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('', 'OIE'); ?></th>
                            <th><?php echo $this->Paginator->sort('created_date','Fecha'); ?></th>
                            <th><?php echo $this->Paginator->sort('bic_ctn','Ctn'); ?></th>
                            <th><?php echo $this->Paginator->sort('lotes','Lotes a Embalar'); ?></th>
                            <th><?php echo $this->Paginator->sort('proforma','Proforma'); ?></th>
                            <th><?php echo $this->Paginator->sort('weight_to_out','Pesar Salida?'); ?></th>
                            <th><?php echo $this->Paginator->sort('shipping_lines_id','Línea Marítima'); ?></th>
                            <th><?php echo $this->Paginator->sort('motorship_name','Motonave'); ?></th>
                            <th><?php echo $this->Paginator->sort('long_container','LONG'); ?></th>
                            <th><?php echo $this->Paginator->sort('jornally','Jornada Embalaje'); ?></th>
                            <th><?php echo $this->Paginator->sort('state','Estado'); ?></th>
                            <th><?php echo $this->Paginator->sort('novedades',' Bloqueos - (Novedades)'); ?></th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($searchInfoList){
                        foreach ($searchInfoList as $searchInfo): ?>
                        <tr>
                            <td><?php 
                            //debug($searchInfoList);exit;
                            echo $this->Form->input($searchInfo['PackagingCaffee']['id'],array( 'class'=>'select','type'=>'checkbox'));;?></td>
                            <td><?php echo h($searchInfo['PackagingCaffee']['created_date']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['ReadyContainer']['bic_container']); ?>&nbsp;</td>
                            <td><?php
                            $lotes="";
                            foreach ($searchInfo['DetailPackagingCaffee'] as $remittancesCaffee){
                                $lotes .= "| 3-".$remittancesCaffee['RemittancesCaffee']['Client']['exporter_code']."-".
                                        $remittancesCaffee['RemittancesCaffee']['lot_caffee']." x ".$remittancesCaffee['quantity_radicated_bag_out']."| ";
                            }
                            echo h($lotes); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['proforma']); ?>&nbsp;</td>
                            <?php echo ($searchInfo['PackagingCaffee']['weight_to_out'] !== false) ? "<td style='background-color:yellow'>SI</td>":"<td>NO</td>"; ?>&nbsp;
                            <td><?php echo h($shippingLines[$searchInfo['InfoNavy']['shipping_lines_id']]); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['motorship_name']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['InfoNavy']['long_container']); ?>&nbsp;</td>
                            <td><?php echo h($searchInfo['PackagingCaffee']['jornally']); ?>&nbsp;</td>
                            <td><?php echo h($statePackaging[$searchInfo['PackagingCaffee']['state_packaging_id']]); ?>&nbsp;</td>
                            <td><?php $lockCoffee = false;
                            foreach ($searchInfo['DetailPackagingCaffee'] as $remittancesCaffee){
                                foreach($remittancesCaffee['RemittancesCaffee']['RemittancesCaffeeHasNoveltysCaffee'] as $noveltys){
                                    if($noveltys['active'])
                                    {
                                        $lockCoffee = true;
                                        break;
                                    }
                                }
                                if($lockCoffee)
                                {
                                    break;
                                }
                            }
                            echo ($lockCoffee == true) ? "REMESAS BLOQUEADAS":"No tiene café bloqueado" ?>&nbsp;</td>
                            <td class="actions">
                                <div class="dropdown">
                                    <button class="dropbtn"><strong>Menú</strong></button>
                                    <div class="dropdown-content">
                                        <?php 
                                            if($searchInfo['PackagingCaffee']['state_packaging_id'] == 3 ){
                                               echo $this->Html->link(__('Ver'), array('controller'=> 'ViewPackagings', 'action' => 'view', $searchInfo['PackagingCaffee']['id'])); 
                                            }
                                        ?>
                                        <?php echo $this->Html->link(__('Imprimir'), Configure::read('siscafe.url_pro')."/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&idReport=1&oie=".$searchInfo['PackagingCaffee']['id']); ?>
                                        <?php 
                                            if($searchInfo['PackagingCaffee']['state_packaging_id'] == 1 || $searchInfo['PackagingCaffee']['state_packaging_id'] == 2){
                                               echo $this->Html->link(__('Cancelar'), array('action' => 'allOie', $searchInfo['PackagingCaffee']['id'],'cancel'),array('confirm'=>('Desea cancelar la OIE '.$searchInfo['PackagingCaffee']['id'].'?'))); 
                                            }
                                        ?>                                       
                                        <?php 
					      echo $this->Html->link(__('Modificar'), array('action' => 'edit', $searchInfo['PackagingCaffee']['id']));
                                            if($searchInfo['PackagingCaffee']['state_packaging_id'] == 2){
                                                echo $this->Html->link(__('Remesas amparadas'), array('controller'=> 'DetailPackagingCaffees','action' => 'add', '?' => ['oie_id' => $searchInfo['PackagingCaffee']['id']]));
                                            }
                                        ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach;} ?>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('remittances_caffee_id','Remesa'); ?></th>
                            <th><?php echo $this->Paginator->sort('name','Novedad'); ?></th>
                            <th><?php echo $this->Paginator->sort('active','Estado'); ?></th>
                        </tr>
                    </thead>  
                    <tbody id="tableNovelty">
                    </tbody>
                </table>    
            </td>
        </tr>
    </table>

</div>
<script type="text/javascript">
    $('#date').attr('type', 'date');
    
    $('input.select').click(function (event) {
        var check = $(event.target).prop('checked');
            if(check){
                $(event.target).attr('checked', true);
            }
            else{
                $(event.target).attr('checked', false);
            }
        });
    
    function embalarOieSelected(){
        if($('#jornally').val() !== "" && $('#date').val() !== ""){
            $('input.select').each(function(i, el) {
                var oie = el.id;
                var date = $('#date').val();
                var jornada = $('#jornally').val();
                if(el.checked){
                    //console.log(oie+" "+date+" "+jornada);
                    //debugger;
                    packByOieDate(oie,date,jornada);
                }
            });
        }
        else{
            alert("Fecha ó Jornada vacia. Por favor vefique");
        }
        //window.location.reload();
    }
    
    function packByOieDate(oie,date,jornada){
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/packOIE?oie=" + oie+"&date="+date+"&jornally="+jornada,
            error: function (msg) {
                //alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    alert(data.msj);
                }
            }
        });
    }
    
    function searchNoveltysByOIE(oie) {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/RemittancesCaffeeHasNoveltysCaffees/ajaxCoffeeNoveltysByOIE/" + oie,
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    $('#tableNovelty').children().remove();
                    $.each(data, function (i, value) {
                        var trHTML = '<tr>';
                        trHTML += '<td>' + value['RemittancesCaffeeHasNoveltysCaffee']['remittances_caffee_id'] + '</td>';
                        trHTML += '<td>' + value['NoveltysCaffee']['name'] + '</td>';
                        if (value['RemittancesCaffeeHasNoveltysCaffee']['active'] === true) {
                            trHTML += '<td>Activo</td><tr>';
                        } else {
                            trHTML += '<td>Inactivo</td><tr>';
                        }
                        $('#tableNovelty').append(trHTML);
                    });
                }
            }
        });
    }

</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'allOie')); ?></li>
    </ul>
</div>
