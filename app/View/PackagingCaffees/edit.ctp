<div class="packagingCaffees form">
    <?php

    echo $this->Html->css('style');
    echo $this->Form->create('PackagingCaffee'); ?>
    <fieldset>
        <legend><?php echo __('Editar Info Contenedor'); ?></legend>
        <?php
        if ($this->Session->read('User.centerId') == 1) {
            echo $this->Form->input('id');
            echo $this->Form->input('info_navy_id', array('type' => 'hidden'));
            echo $this->Form->input('info_navy_proforma', array('disabled' => true, 'type' => 'text', 'label' => 'Proforma'));
            echo $this->Form->input('weight_to_out', array('label' => 'Pesar a la salida'));
            echo $this->Form->input('consolidate', array('label' => 'Consolidado?'));
            echo $this->Form->input('long_container', array(
                'label' => 'Longitud Contenedor', 'empty' => 'Seleccione...', 'required' => true,
                'options' => array('20' => '20', '40' => '40', '20-RF' => '20 RF', '40-RF' => '40 RF')
            ));
            echo $this->Form->input('observation', array('label' => 'Observaciones'));
        } else {
            echo $this->Form->input('bic_container', array('label' => 'Numero de contenedor', 'value' => $dataPackagingCaffee['ReadyContainer']['bic_container']));
            echo $this->Form->input('bic_container_id', array('label' => 'Numero de contenedor', 'type' => 'hidden', 'value' => $dataPackagingCaffee['PackagingCaffee']['ready_container_id']));
            echo $this->Form->input('long_container', array(
                'label' => 'Longitud Contenedor', 'empty' => 'Seleccione...', 'value' => $dataPackagingCaffee['InfoNavy']['long_container'], 'required' => true,
                'options' => array('20' => '20', '40' => '40', '20-RF' => '20 RF', '40-RF' => '40 RF', 'value' => $dataPackagingCaffee['PackagingCaffee']['long_ctn'])
            ));
            echo $this->Form->input('iso_ctn', array('label' => 'ISO Contenedor', 'value' => $dataPackagingCaffee['PackagingCaffee']['iso_ctn'], 'options' => array('22G1' => '22G1', '44G1' => '44G1', '20-RF' => '20 RF', '40-RF' => '40 RF')));
            echo $this->Form->input('exporter_id', array('label' => 'Código Exportador', 'type' => 'text', 'value' => $dataClient['Clients']['exporter_code']));
            echo $this->Form->input('exporter_name', array('label' => 'Exportador', 'type' => 'text', 'disabled' => 'disabled', 'value' => $dataClient['Clients']['business_name']));
            echo $this->Form->input('exporter_cod', array('type' => 'hidden', 'value' => $dataClient['Clients']['id']));
            echo $this->Form->input('packaging_date', array('label' => 'Fecha Registro', 'required' => true, 'timeFormat' => 24, 'type' => 'datetime'));
            echo $this->Form->input('lots', array('label' => 'Lotes', 'type' => 'select', 'multiple' => 'multiple'));
            echo $this->Form->input('observation', array('label' => 'Lotes café - Mercancia dentro del Contenedor', 'required' => true, 'placeholder' => '(e.g: 3-001-1242:150 SACOS | 3-001-1578:100 SACOS)'));
            echo $this->Form->input('weight_container', array('label' => 'Peso Neto Café KG - (Peso sin unidad de Kg)', 'type' => 'number', 'placeholder' => 'E.g: 70 Kg x 300 Sacos = 21000', 'value' => $dataPackagingCaffee['PackagingCaffee']['weight_net_cont']));
            echo $this->Form->input('qta_unit', array('label' => 'Total Cant. Sacos ó Cajas', 'type' => 'number', 'value' => $dataPackagingCaffee['PackagingCaffee']['qta_bags_packaging']));
            echo $this->Form->input('autorizacion', array('required' => true, 'type' => 'number', 'label' => 'Número Autorización', 'value' => $dataPackagingCaffee['PackagingCaffee']['autorization']));
            echo $this->Form->input('seal_3', array('required' => true, 'type' => 'text', 'label' => 'Sello 1', 'value' => $dataPackagingCaffee['PackagingCaffee']['seal_3']));
            echo $this->Form->input('seal_4', array('type' => 'text', 'label' => 'Sello 2', 'value' => $dataPackagingCaffee['PackagingCaffee']['seal_4']));
            echo $this->Form->input('cooperativa_ctg', array('label' => 'Cooperativa (Cuadrilla)', 'value' => $dataPackagingCaffee['PackagingCaffee']['cooperativa_ctg'], 'options' => array('VINCULAR' => 'VINCULAR', 'OCUPAR' => 'OCUPAR', 'SILPORT' => 'SILPORT', 'COPC' => 'COPC', 'CUAD 1' => 'CUAD 1', 'CUAD 2' => 'CUAD 2')));
            echo $this->Form->input('info_navy_id', array('required' => true, 'type' => 'hidden', 'value' => $dataPackagingCaffee['InfoNavy']['id']));
            echo $this->Form->input('info_navy_booking', array('required' => true, 'type' => 'text', 'label' => 'Núm reserva - Booking', 'value' => $dataPackagingCaffee['InfoNavy']['booking']));
            echo $this->Form->input('info_navy_trip', array('required' => true, 'type' => 'text', 'label' => 'Viaje Motonave', 'value' => $dataPackagingCaffee['InfoNavy']['travel_num']));
            echo $this->Form->input('info_navy_motonave', array('required' => true, 'type' => 'text', 'label' => 'Nombre Motonave', 'value' => $dataPackagingCaffee['InfoNavy']['motorship_name']));
            echo $this->Form->input('info_navy_line', array('class' => 'chosen-select', 'value' => $dataPackagingCaffee['InfoNavy']['shipping_lines_id'], 'options' => $maritimeLines, 'label' => 'Linea Naviera'));
            echo $this->Form->input('info_navy_customid', array('required' => true, 'class' => 'chosen-select', 'label' => 'Cod Aduana', 'options' => $listCustom, 'value' => $dataPackagingCaffee['InfoNavy']['customs_id']));
            echo $this->Form->input('info_navy_navy', array('required' => true, 'class' => 'chosen-select', 'label' => 'Agente Naviero', 'value' => $dataPackagingCaffee['InfoNavy']['navy_agent_id'], 'options' => $listNavyAgent));
            echo $this->Form->input('info_navy_pod', array('required' => true, 'type' => 'text', 'label' => 'Puerto Destino - POD', 'options' => $listNavyAgent, 'value' => $dataPackagingCaffee['InfoNavy']['destiny']));
            echo $this->Form->input('mode_packaging', array('label' => 'Modalidad de embalaje', 'options' => array('LCL' => 'LCL', 'FCL' => 'FCL', 'value' => $dataPackagingCaffee['InfoNavy']['packaging_type'])));
            echo $this->Form->input('type_packaging', array('label' => 'Tipo de embalaje', 'options' => array('SACOS' => 'SACOS', 'GRANEL' => 'GRANEL', 'BIG BAG' => 'BIG BAG', 'TABACO' => 'TABACO', 'CARBON' => 'CARBON', 'CD_CACAO' => 'CD_CACAO', 'CACAO' => 'CACAO'), 'value' => $dataPackagingCaffee['InfoNavy']['packaging_mode']));
            echo $this->Form->input('info_navy_adicional_element', array('required' => true, 'placeholder' => '(e.g: KARFT 2 | DRY BAGS 2)', 'type' => 'textarea', 'label' => 'Elementos adicionales', 'value' => $dataPackagingCaffee['InfoNavy']['elements_adicional']));
            echo $this->Form->input('jetty', array('options' => array('STM' => 'STM', 'CONTECAR' => 'CONTECAR', 'SPRC' => 'SPRC', 'COMPAS' => 'COMPAS', 'SPIA' => 'SPIA'), 'label' => 'Puerto Maritimo', 'value' => $dataPackagingCaffee['PackagingCaffee']['jetty']));
            echo $this->Form->input('user_tarja', array('label' => 'Tarjador', 'options' => $operator, 'value' => $dataPackagingCaffee['PackagingCaffee']['user_tarja']));
            echo $this->Form->input('user_driver', array('label' => 'Operador', 'options' => $driver, 'value' => $dataPackagingCaffee['PackagingCaffee']['user_driver']));
            echo $this->Form->input('time_start', array('required' => true, 'type' => 'time', 'timeFormat' => 24, 'label' => 'Hora de Inicio', 'value' => $dataPackagingCaffee['PackagingCaffee']['time_start']));
            echo $this->Form->input('time_end', array('required' => true, 'type' => 'time', 'timeFormat' => 24, 'label' => 'Hora de Final', 'value' => $dataPackagingCaffee['PackagingCaffee']['time_end']));
        }
        ?>
    </fieldset>
    <script type="text/javascript">
        $(".chosen-select").chosen();

        var infoNavies = [<?php echo "'" . implode("','", $infoNavies) . "'";; ?>];
        $("#PackagingCaffeeInfoNavyId").autocomplete({
            source: infoNavies
        });


        var infoNavies = [<?php echo "'" . implode("','", $infoNavies) . "'"; ?>];
        $("#PackagingCaffeeInfoNavyId").autocomplete({
            source: infoNavies,
            select: function(event, ui) {
                var booking = ui.item.label;
                console.log(booking);
                //refillInfoNavyByBooking(booking);
            }
        });

        $('#PackagingCaffeeLots').select2({
            placeholder: 'Digite lote ...',
            minimumInputLength: 5,
            multiple: true,
            data: <?= $detailPackagingCaffees ?>,
            width: '100%',
            theme: "classic",
            tags: true,
            tokenSeparators: [',', ' '],
            ajax: {
                url: "<?= Router::url(['controller' => 'RemittancesCaffees', 'action' => 'findRemittancesByLotClientMultiple']); ?>",
                dataType: 'json',
                data: function(params) {
                    var codExpFNC = $("#PackagingCaffeeExporterId").val();
                    return {
                        q: codExpFNC + "-" + params.term,
                    };
                },
                processResults: function(data, params) {
                    var resultsData = [];
                    for (i = 0; i < data.length; i++) {
                        item = data[i];
                        var data = {
                            'id': item.RemittancesCaffee.id + "-" + item.RemittancesCaffee.quantity_radicated_bag_in,
                            'text': "Remesa = " + item.RemittancesCaffee.id + " , Lote = " + item.RemittancesCaffee.lot_caffee + " , Cantidad = " + item.RemittancesCaffee.quantity_radicated_bag_in,
                        };
                        resultsData.push(data);
                    }
                    return {
                        results: resultsData,
                    };
                }
            },
        })

        var selectedItems = $('#PackagingCaffeeLots option').map(function() {
            return this.value
        });
        $("#PackagingCaffeeLots").val(selectedItems).trigger("change");

        $("#PackagingCaffeeInfoNavyBooking").keyup(function() {
            if ($(this).val() === '') {
                $("#PackagingCaffeeBooking").val(false);
                $("#PackagingCaffeeInfoNavyMotonave").val("");
                $("#PackagingCaffeeInfoNavyTrip").val("");
                $("#PackagingCaffeeInfoNavyLinecod").val("");
                $("#PackagingCaffeeInfoNavyId").val("");
                $("#PackagingCaffeeInfoNavyPod").val("");
                $("#PackagingCaffeeInfoNavyCustomid").val("");
                $("#PackagingCaffeeModePackaging").val("");
                $("#PackagingCaffeeTypePackaging").val("");
                $("#PackagingCaffeeInfoNavyAdicionalElement").val("");
                $("#PackagingCaffeeInfoNavyCustomname").val("");
                $("#PackagingCaffeeInfoNavyLine").val("");
            }
        });

        $("#PackagingCaffeeExporterId").keyup(function() {
            findInfoRemittancesByClientCode($(this).val());
        });

        $("#PackagingCaffeeInfoNavyLinecod").keyup(function() {
            findLineById($(this).val());
        });

        $("#PackagingCaffeeInfoNavyCustomid").keyup(function() {
            findCustomsById($(this).val());
        });

        $("#PackagingCaffeeInfoNavyNavy").keyup(function() {
            findNayyAgentById($(this).val());
        });

        function refillInfoNavyByBooking(booking) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/InfoNavies/findInfoNavyByBooking/" + booking,
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    if (data !== "") {
                        var InfoNavyData = JSON.parse(data)[0]['InfoNavy'];
                        $("#PackagingCaffeeInfoNavyMotonave").val(InfoNavyData['motorship_name']);
                        $("#PackagingCaffeeInfoNavyTrip").val(InfoNavyData['travel_num']);
                        $("#PackagingCaffeeInfoNavyLinecod").val(InfoNavyData['shipping_lines_id']);
                        $("#PackagingCaffeeBooking").val(true);
                        findLineById(InfoNavyData['shipping_lines_id']);
                        $("#PackagingCaffeeInfoNavyId").val(InfoNavyData['proforma']);
                        $("#PackagingCaffeeInfoNavyPod").val(InfoNavyData['destiny']);
                        $("#PackagingCaffeeInfoNavyCustomid").val(InfoNavyData['customs_id']);
                        findCustomsById(InfoNavyData['customs_id']);
                        $("#PackagingCaffeeModePackaging").val(InfoNavyData['packaging_type']);
                        $("#PackagingCaffeeTypePackaging").val(InfoNavyData['packaging_mode']);
                        $("#PackagingCaffeeInfoNavyAdicionalElement").val(InfoNavyData['elements_adicional']);
                    }
                }
            });
        }

        function findInfoRemittancesByClientCode(exportCode) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/RemittancesCaffees/findRemittancesByExportCode/" + exportCode,
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    if (data !== "") {
                        var clientData = JSON.parse(data)['clientData'];
                        $("#PackagingCaffeeExporterName").val(clientData['Client']['business_name']);
                        $("#PackagingCaffeeExporterCod").val(clientData['Client']['id']);
                    }
                }
            });
        }

        function findNayyAgentById(idNavyAgent) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/NavyAgents/findById/" + idNavyAgent,
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    var json = JSON.parse(data);
                    if (JSON.stringify(json) !== "[]") {
                        $("#PackagingCaffeeInfoNavyNavyagent").val(json.NavyAgent.name);
                    } else {
                        $("#PackagingCaffeeInfoNavyNavyagent").val("NO ARROJO RESULTADOS");
                    }
                }
            });
        }


        function findLineById(idLine) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/ShippingLines/findById/" + idLine,
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    if (data !== "") {
                        var ShippingLineData = JSON.parse(data)['ShippingLine'];
                        console.log(ShippingLineData);
                        $("#PackagingCaffeeInfoNavyLine").val(ShippingLineData['business_name']);
                    }
                }
            });
        }

        function findCustomsById(idCustom) {
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "/Customs/findById/" + idCustom,
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    if (data !== "") {
                        var customsData = JSON.parse(data)['Custom'];
                        $("#PackagingCaffeeInfoNavyCustomname").val(customsData['cia_name']);
                    }
                }
            });
        }
    </script>
    <?php echo $this->Form->end(__('Actualizar')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
        <li><?php
            if ($this->Session->read('User.centerId') != 1)
                echo $this->Html->link(__('Regresar'), array('action' => 'index'));
            else
                echo $this->Html->link(__('Regresar'), array('action' => 'allOie'));
            ?></li>
    </ul>
</div>