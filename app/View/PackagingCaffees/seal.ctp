<div class="packagingCaffees form">
<?php echo $this->Html->script('jquery.min');
		echo $this->Html->script('jquery-ui.min');
		echo $this->Html->css('jquery-ui.min');
		echo $this->Form->create('PackagingCaffee',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Complemento Tarja Contenedor '.$findPackagingCaffee['ReadyContainer']['bic_container']); ?></legend>
	<?php
		echo $this->Form->input('iso_ctn',array('type' => 'select', 'empty'=>"Seleccionar",'required'=>true,'options' => $typectn, 'label'=>'ISO CTN:'));
		echo $this->Form->input('observation',array('label'=>'Observaciones','required'=>true,'value'=>$findPackagingCaffee['PackagingCaffee']['observation']));		
                echo $this->Form->input('cooperativa_ctg',array('label' => 'Cuadrilla Embalaje (Cuadrilla 1 (Jan Cabezas) | Cuadrilla 2 (Flower Sinisterra))?','type'=>'select','options' =>array('CUAD 1'=>'CUAD 1','CUAD 2'=>'CUAD 2'),'required' => true));
		echo $this->Form->label('img_ctn','Imagen Frontal Contenedor');
                echo $this->Form->file('img_ctn');
                echo $this->Form->input('seal1',array('label'=>'Número del Sello #1'));
                echo $this->Form->label('seal1_path','Registro fotografico Sello #1');
		echo $this->Form->file('seal1_path');
		echo $this->Form->input('seal2',array('label'=>'Número del Sello #2'));
		echo $this->Form->label('seal2_path','Registro fotografico Sello #2');
		echo $this->Form->file('seal2_path');
		
	?>
	</fieldset>   
<?php 
$saveSubmit = array(
    'label' => 'Update',
    'value' => 'Update!',
    'id' => 'submit1'
);
echo $this->Form->end($saveSubmit); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Tarja'), array('action' => 'index')); ?></li>
                <li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
<script type="text/javascript">
    
    $('#submit1').click(function(e) {
        var idOie=<?php echo $findPackagingCaffee['PackagingCaffee']['id'];?>;
        if(!completeWeightingCoffeeSISCAFE(idOie)){
          alert("Se Envió VGM SISCAFE -> Navis del contenedor exitosamente!");
        }
        else{
          alert("Contenedor no ha sido finalizado, tiene pendiente sacos por embalar � NO tiene contenedor registrado");   
        }
        $("#PackagingCaffeeSealForm").submit();
    });
    
    function weightContainerToN4(container, weight) {
            console.log(container + " - " + weight);
            $.ajax({
                url: "http://siscafe.copcsa.com:8181/SISCAFE-IntegrationWS/WS/integration/getWeightByContainer?container=" + container + "&weight=" + weight,
                crossDomain: true,
                async: false,
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    console.log(container + " " + weight);
                    return true;
                },
                success: function (data) {
                    console.log(data);
                    return false;
                }
            });
        }

        function completeWeightingCoffeeSISCAFE(oie) {
            var idOie = {'OIE':oie};
            $.ajax({
                type: "POST",
                datatype: "json",
                url: '<?= Router::url(['controller' => 'DetailPackagingCaffees', 'action' => 'findContainerByOie']) ?>',
                data: idOie,
                async: false,
                error: function (msg) {
                    alert("Error networking");
                },
                success: function (data) {//El embalaje se finaliza si y solo si tiene una unidad asignada.
                    if (data !== "") {
                        var result = JSON.parse(data);
                        var container = result.ViewPackaging.bic_ctn;
                        console.log(result.ViewPackaging);
                        console.log("container: " + container);
                        var total_pallet_embalados = result.ViewPackaging.total_pallets_embalaje;
                        console.log("total_pallet_embalados: " + total_pallet_embalados);
                        var total_pallets_remesa = result.ViewPackaging.total_pallets_remesa;
                        console.log("total_pallet_remesas: " + total_pallets_remesa);
                        var pctjPallets = (total_pallet_embalados / total_pallets_remesa);
                        console.log("pctjPallets: " + pctjPallets);
                        var tare_estibas = result.ViewPackaging.tare_estibas_descargue;
                        console.log("tare_estibas: " + tare_estibas);
                        var tare_embalaje = (pctjPallets * tare_estibas);
                        console.log("tare_embalaje: " + tare_embalaje);
                        var total_peso_cafe = result.ViewPackaging.total_cafe_empaque;
                        console.log("total_peso_cafe: " + total_peso_cafe);
                        var modalidad = result.ViewPackaging.packaging_mode;
                        console.log("modalidad: " + modalidad);
                        var tare_empaque = result.ViewPackaging.tare_empaque;
                        console.log("tare_empaque: " + tare_empaque);
                        var weightContainerNet = (total_peso_cafe - (tare_embalaje));
                        console.log("weightContainerNet: " + weightContainerNet);
                        if (modalidad === 'Granel') {
                            weightContainerNet = weightContainerNet - tare_empaque;
                        }
                       return weightContainerToN4(oie, weightContainerNet);
                        //alert("Se finalizo el proceso de embalaje del contenedor "+container);
                    } else {
                        return false;
                    }
                }
            });
        }
    
</script>
