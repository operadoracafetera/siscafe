<div class="packagingCaffees index">
    <fieldset>
        <legend><?php echo $this->Html->script('jquery.min');
echo $this->Html->script('jquery-ui.min');
echo $this->Html->css('jquery-ui.min');
echo $this->Html->css('style');
        echo __('Reportes'); ?></legend>
        <table>
            <h3>Listado de OIE por proforma</h3>
            <tr>
                <td><?php  echo $this->Form->input('proforma'); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportOIEByProforma();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>Relación de contenedor por Línea Naviera</h3>
            <tr>
                <td><?php  echo $this->Form->input('schedule',array('label' => 'Jornada','empty'=>'Seleccione...', 'required' => true,'options' => array('M' => 'Mañana', 'T' => 'Tarde'))); ?></td>
                <td><?php  echo $this->Form->input('date',array('type' => 'text')); ?></td>
                <td><?php  echo $this->Form->input('navyLine',array('label' => 'Línea Naviera','options' => array('' => 'Seleccione...'))); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportContainerByShippingLine();">Generar</a></h3></td>
            </tr>
            <tr>
                <td><?php  echo $this->Form->input('packaging_type',array('label' => 'Tipo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('FCL' => 'FCL', 'LCL' => 'LCL')));  ?></td>
                <td><?php  echo $this->Form->input('packaging_mode',array('label' => 'Modo Embalaje','empty'=>'Seleccione...', 'required' => true,'options' => array('Sacos' => 'Sacos', 'Granel' => 'Granel'))); ?></td>
            </tr>
        </table>
        <table>
            <h3>Programación de café por fecha y jornada</h3>
            <tr>
                <td><?php  echo $this->Form->input('schedule2',array('label' => 'Jornada','empty'=>'Seleccione...', 'required' => true,'options' => array('M' => 'Mañana', 'T' => 'Tarde'))); ?></td>
                <td><?php  echo $this->Form->input('date2',array('type' => 'text','label' => 'Fecha')); ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportCoffeeSchedule();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>Registro de entrega de mercancia</h3>
            <tr>
                <td><?php  echo $this->Form->input('proforma2',array('label' => 'Proforma')); ?></td>
                <td><?php  echo $this->Form->input('date5',array('type' => 'text','label' => 'Fecha')); ?></td>
                <td><?php  echo $this->Form->input('warehouse',array('label' => 'Bodega','options' => $warehouse, 'style'=>'height:28px;width:200px;'));  ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportCommodityDelivery();">Generar</a></h3></td>
            </tr>
        </table>
        <table>
            <h3>Listado de embalaje:  (Salidas por bodega SPB)</h3>
            <tr>
                <td><?php  echo $this->Form->input('date3',array('type' => 'text','label' => 'Fecha inicial')); ?></td>
                <td><?php  echo $this->Form->input('date4',array('type' => 'text','label' => 'Fecha final')); ?></td>
                <td><?php  echo $this->Form->input('warehouse3',array('label' => 'Bodega','options' => $warehouse));  ?></td>
                <td class="actions" style="padding-top: 25px;"><h3><a href="javascript:void(0)" onclick="reportListPackaging();">Generar</a></h3></td>
            </tr>
        </table>
    </fieldset>
</div>
<script type="text/javascript">
    var pathPrintWS = 'http://siscafe.copcsa.com:8080/SISCAFE-PrintWS/ws/print/getOIEReports?toPdf=true&idPrinter=1&';

    var infoNavies = [<?php echo "'".implode("','",$infoNavies)."'";?>];
    $("#proforma").autocomplete({
        source: infoNavies
    });
    
    $("#proforma2").autocomplete({
        source: infoNavies
    });
        
    $('#date').attr('type', 'date');
    $('#date2').attr('type', 'date');
    $('#date3').attr('type', 'date');
    $('#date4').attr('type', 'date');
    $('#date5').attr('type', 'date');

    $("#date").change(function () {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/PackagingCaffees/findInfoNaviesByCreateDateOIE/" + $('#date').val(),
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    data = JSON.parse(data);
                    console.log(data);
                    $('#navyLine').children().remove();
                    $.each(data, function (i, value) {
                        var html = '<option value="'+value['ShippingLines']['business_name']+'">'+value['ShippingLines']['business_name']+'</option>';
                        $('#navyLine').append(html);
                    });
                    
                }
            }
        });
    });

    function  reportOIEByProforma() {
        $.ajax({
            type: "GET",
            datatype: "json",
            url: "/InfoNavies/findInfoNavyByProforma/" + $('#proforma').val(),
            error: function (msg) {
                alert("Error networking");
            },
            success: function (data) {
                if (data !== "") {
                    var infoNavyData = JSON.parse(data)['InfoNavy'];
                    var infoNavyAgentData = JSON.parse(data)['InfoNavyAgent'];
                    //debugger;
                    window.location.href = pathPrintWS + 'idReport=2&proforma=' + infoNavyData['InfoNavy']['proforma'] +
                            '&motorShipName=' + infoNavyData['InfoNavy']['proforma'] + '&navyAgent=' +
                            infoNavyAgentData['NavyAgent']['name'] + '&to=' + infoNavyData['InfoNavy']['destiny'];
                }
            }
        });
    }

    function  reportContainerByShippingLine() {
        window.location.href = pathPrintWS + 'idReport=3&date='+$('#date').val()+'&schedule='+$('#schedule').val()+'&packingType='+$('#packaging_type').val()+'&packingMode='+$('#packaging_mode').val()+'&navyLine='+$('#navyLine').val();
    }

    function  reportCoffeeSchedule() {
        window.location.href = pathPrintWS + 'idReport=4&date='+$('#date2').val()+'&schedule='+$('#schedule2').val();
    }
    
    function reportCommodityDelivery(){
        window.location.href = pathPrintWS + 'idReport=5&proforma='+$('#proforma2').val()+'&warehouseId='+$('#warehouse').val()+'&date='+$('#date5').val();
    }
    
    function reportListPackaging(){
        window.location.href = pathPrintWS + 'idReport=13&date1='+$('#date3').val()+'&date2='+$('#date4').val()+'&warehouseId='+$('#warehouse3').val();
    }
</script>
<div class="actions">
    <h3><?php echo __('Acciones'); ?></h3>
    <ul>
         <?php 
            if($this->Session->read('User.profiles_id') == 1){ ?>
            <li><?php echo $this->Html->link(__('Regresar'), array('action' => 'allOie')); ?></li>
        <?php } ?>
        <?php 
            if($this->Session->read('User.profiles_id') == 2){ ?>
            <li><?php echo $this->Html->link(__('Regresar'), array('controller' => 'Pages', 'action' => 'basculero')); ?></li>
        <?php } ?>
    </ul>
</div>