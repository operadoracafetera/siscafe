<div class="packagingCaffees view">
<h2><?php echo __('Packaging Caffee'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight To Out'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['weight_to_out']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State Packaging Id'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['state_packaging_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Long Container'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['long_container']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Motonave'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['long_container']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ready Container'); ?></dt>
		<dd>
			<?php echo $this->Html->link($packagingCaffee['ReadyContainer']['id'], array('controller' => 'ready_containers', 'action' => 'view', $packagingCaffee['ReadyContainer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cause Emptied Ctn Id'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['cause_emptied_ctn_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Info Navy Id'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['info_navy_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Packaging Date'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['packaging_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cancelled Date'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['cancelled_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Iso Ctn'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['iso_ctn']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal 1'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['seal_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal 2'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['seal_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Users Id'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['users_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal Date'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['seal_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal1 Path'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['seal1_path']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seal2 Path'); ?></dt>
		<dd>
			<?php echo h($packagingCaffee['PackagingCaffee']['seal2_path']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Packaging Caffee'), array('action' => 'edit', $packagingCaffee['PackagingCaffee']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Packaging Caffee'), array('action' => 'delete', $packagingCaffee['PackagingCaffee']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $packagingCaffee['PackagingCaffee']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Packaging Caffees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Packaging Caffee'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ready Containers'), array('controller' => 'ready_containers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ready Container'), array('controller' => 'ready_containers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Detail Packaging Caffees'), array('controller' => 'detail_packaging_caffees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Detail Packaging Caffee'), array('controller' => 'detail_packaging_caffees', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Detail Packaging Caffees'); ?></h3>
	<?php if (!empty($packagingCaffee['DetailPackagingCaffee'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Remittances Caffee Id'); ?></th>
		<th><?php echo __('Packaging Caffee Id'); ?></th>
		<th><?php echo __('Quantity Radicated Bag Out'); ?></th>
		<th><?php echo __('Created Date'); ?></th>
		<th><?php echo __('Updated Date'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($packagingCaffee['DetailPackagingCaffee'] as $detailPackagingCaffee): ?>
		<tr>
			<td><?php echo $detailPackagingCaffee['remittances_caffee_id']; ?></td>
			<td><?php echo $detailPackagingCaffee['packaging_caffee_id']; ?></td>
			<td><?php echo $detailPackagingCaffee['quantity_radicated_bag_out']; ?></td>
			<td><?php echo $detailPackagingCaffee['created_date']; ?></td>
			<td><?php echo $detailPackagingCaffee['updated_date']; ?></td>
			<td><?php echo $detailPackagingCaffee['state']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'detail_packaging_caffees', 'action' => 'view', $detailPackagingCaffee['remittances_caffee_id,packaging_caffee_id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'detail_packaging_caffees', 'action' => 'edit', $detailPackagingCaffee['remittances_caffee_id,packaging_caffee_id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'detail_packaging_caffees', 'action' => 'delete', $detailPackagingCaffee['remittances_caffee_id,packaging_caffee_id']), array('confirm' => __('Are you sure you want to delete # %s?', $detailPackagingCaffee['remittances_caffee_id,packaging_caffee_id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Detail Packaging Caffee'), array('controller' => 'detail_packaging_caffees', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
