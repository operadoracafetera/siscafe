<div class="sampleCoffeeSends view">
<h2><?php echo __('Sample Coffee Send'); ?></h2>
	<dl>
		<dt><?php echo __('Coffee Sample Id'); ?></dt>
		<dd>
			<?php echo h($sampleCoffeeSend['SampleCoffeeSend']['coffee_sample_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Shipping Sample Id'); ?></dt>
		<dd>
			<?php echo h($sampleCoffeeSend['SampleCoffeeSend']['shipping_sample_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('N Shipping Id'); ?></dt>
		<dd>
			<?php echo h($sampleCoffeeSend['SampleCoffeeSend']['n_shipping_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Send'); ?></dt>
		<dd>
			<?php echo h($sampleCoffeeSend['SampleCoffeeSend']['date_send']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sample Coffee Send'), array('action' => 'edit', $sampleCoffeeSend['SampleCoffeeSend']['n'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sample Coffee Send'), array('action' => 'delete', $sampleCoffeeSend['SampleCoffeeSend']['n']), array('confirm' => __('Are you sure you want to delete # %s?', $sampleCoffeeSend['SampleCoffeeSend']['n']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Sample Coffee Sends'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sample Coffee Send'), array('action' => 'add')); ?> </li>
	</ul>
</div>
