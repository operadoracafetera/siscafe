<div class="sampleCoffeeSends form">
<?php echo $this->Form->create('SampleCoffeeSend'); ?>
	<fieldset>
		<legend><?php echo __('Generar Envió Muestras de Café'); ?></legend>
	<?php
		//echo $this->Form->input('coffee_sample_id');
		echo $this->Form->input('shipping_sample_id');
		echo $this->Form->input('n_shipping_id');
		echo $this->Form->input('date_send');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',1)); ?> </li>
                <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
