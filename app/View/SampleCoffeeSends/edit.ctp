<div class="sampleCoffeeSends form">
<?php echo $this->Form->create('SampleCoffeeSend'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sample Coffee Send'); ?></legend>
	<?php
		echo $this->Form->input('coffee_sample_id');
		echo $this->Form->input('shipping_sample_id');
		echo $this->Form->input('n_shipping_id');
		echo $this->Form->input('date_send');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SampleCoffeeSend.n')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('SampleCoffeeSend.n')))); ?></li>
		<li><?php echo $this->Html->link(__('List Sample Coffee Sends'), array('action' => 'index')); ?></li>
	</ul>
</div>
