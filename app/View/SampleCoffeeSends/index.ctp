<div class="sampleCoffeeSends index">
	<h2><?php echo __('Listado Envios Muestras Café'); ?></h2>
        <?php echo $this->Html->link(__('Registrar nuevo envio muestras'), array('action' => 'add')); ?>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('coffee_sample_id'); ?></th>
			<th><?php echo $this->Paginator->sort('shipping_sample_id'); ?></th>
			<th><?php echo $this->Paginator->sort('n_shipping_id'); ?></th>
			<th><?php echo $this->Paginator->sort('date_send'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sampleCoffeeSends as $sampleCoffeeSend): ?>
	<tr>
		<td><?php echo h($sampleCoffeeSend['SampleCoffeeSend']['coffee_sample_id']); ?>&nbsp;</td>
		<td><?php echo h($sampleCoffeeSend['SampleCoffeeSend']['shipping_sample_id']); ?>&nbsp;</td>
		<td><?php echo h($sampleCoffeeSend['SampleCoffeeSend']['n_shipping_id']); ?>&nbsp;</td>
		<td><?php echo h($sampleCoffeeSend['SampleCoffeeSend']['date_send']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $sampleCoffeeSend['SampleCoffeeSend']['n'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sampleCoffeeSend['SampleCoffeeSend']['n'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sampleCoffeeSend['SampleCoffeeSend']['n']), array('confirm' => __('Are you sure you want to delete # %s?', $sampleCoffeeSend['SampleCoffeeSend']['n']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, iniciando {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Listado Muestras'), array('controller' => 'CoffeeSamples', 'action' => 'index',1)); ?> </li>
                <li><?php echo $this->Html->link(__('Menú'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
