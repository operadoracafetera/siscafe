<div class="TrackingHasPhotos view">
<h2><?php echo __('Registro fotografico '.$operationTracking['OperationTracking']['ref_text']); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($operationTracking['OperationTracking']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contenedor - Placa vehiculo'); ?></dt>
		<dd>
			<?php echo ($operationTracking['OperationTracking']['type_tracking'] == 2) ? $tarja['ViewPackaging']['bic_ctn']:$operationTracking['OperationTracking']['ref_text']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lotes'); ?></dt>
		<dd>
			<?php echo ($operationTracking['OperationTracking']['type_tracking'] == 2) ? $tarja['ViewPackaging']['lotes']:$operationTracking['OperationTracking']['observation']; ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Conductor'); ?></dt>
		<dd>
			<?php echo h($operationTracking['OperationTracking']['driver_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cedula conductor'); ?></dt>
		<dd>
			<?php echo h($operationTracking['OperationTracking']['driver_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo de registro'); ?></dt>
		<dd>
			<?php 
			if($operationTracking['OperationTracking']['type_tracking'] == 1){echo "DESCARGUE";}
			else if($operationTracking['OperationTracking']['type_tracking'] == 2){echo "EMBALAJE";}
			else if($operationTracking['OperationTracking']['type_tracking'] == 3){echo "DEVOLUCIONES FIQUE / CAFE";}		
			?>
		</dd>
		<dt><?php echo __('Fecha creación'); ?></dt>
		<dd>
			<?php echo h($operationTracking['OperationTracking']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha actualización'); ?></dt>
		<dd>
			<?php echo h($operationTracking['OperationTracking']['updated_date']); ?>
			&nbsp;
		</dd>
                </dl >
        <br>
        <?php echo $this->Form->create('TrackingHasPhoto',array('type' => 'file','url' => 'savePhoto')); ?>
        
                <br>
        <div id="cargueFotos">
                <fieldset>
                    <legend>Capturar:</legend>
                    <?php echo $this->Form->label('name_file','Fotografia del operativo');
                    echo $this->Form->file('name_file');
                    echo $this->Form->input('trackging_id',array('value'=>$operationTracking['OperationTracking']['id'], 'type' => 'hidden'));
                        ?>
                </fieldset>
        <?php echo $this->Form->end('Guardar'); ?>
        </div>
<div class="actions">
<?php echo $this->Html->link(__("Descargar Fotos"), array('action' => 'downloadAllPhotos', $operationTracking['OperationTracking']['id']));?>
</div>
<br>
<br>
<br>
<br>
<br>
<h3> Imagenes trazabilidad descargue: </h3>
<br>
        <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                        <th><?php echo $this->Paginator->sort('name_file','Nombre Archivo'); ?></th>
                        <th><?php echo $this->Paginator->sort('name_file','Vista Foto'); ?></th>
                        <th><?php echo $this->Paginator->sort('created_date','Fecha Registro'); ?></th>
						<th><?php echo "Acciones";?></th>
                    </tr>
                    </thead>
                    <tbody>
            <?php foreach ($dataPhotos as $dataPhoto): ?>
            <tr>
                    <td><?php 
                    echo h($dataPhoto["TrackingHasPhoto"]['id']); ?>&nbsp;</td>
                    <td><?php echo $this->Html->link(__($dataPhoto['TrackingHasPhoto']['name_file']), array('action' => 'getTrackingRequest', $dataPhoto['TrackingHasPhoto']['id'])); ?>&nbsp;</td>
                    <td><?php echo $this->Html->image('https://trackingphotos.s3.amazonaws.com/'.$operationTracking['OperationTracking']['id'].'/'.$dataPhoto['TrackingHasPhoto']['name_file'],array('id'=>'photo'));?></td>
                    <td><?php echo h($dataPhoto['TrackingHasPhoto']['created_date']); ?>&nbsp;</td>
					<td class="actions">
					<?php 
					if($this->Session->read('User.profiles_id')==1){
						echo $this->Html->link(__('Quitar'), array('action' => 'deletePhoto', $dataPhoto['TrackingHasPhoto']['id'])); 
					}
					?>
					</td>
                    
            </tr>
            <?php endforeach; ?>
            </tbody>
            </table>

<div>
    
</div>
</div>
<script type="text/javascript">

    var profileUser = <?php echo $this->Session->read('User.profiles_id');?>;
    if(profileUser === 6){
        $("#cargueFotos").css("display", "none");
    }

    var idTracking = <?php echo $operationTracking['OperationTracking']['id'];?>;
    var idTypeTracking = <?php echo $operationTracking['OperationTracking']['type_tracking'];?>;
    if(idTracking === 248){
        $("#cargueFotos").css("display", "none");
    }
    
    if(idTypeTracking === 2){
        $("#divQuestions").css("display", "none");
    }
    
</script>


<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo ($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) ? $this->Html->link(__('Registros Descargues'), array('action' => 'index',1)):""; ?> </li>
                <li><?php echo ($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) ? $this->Html->link(__('Registro Embalajes'), array('action' => 'index',2)):""; ?> </li>
                <li><?php echo ($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) ? $this->Html->link(__('Todos Embalajes'), array('controller'=>'PackagingCaffees','action' => 'allctns')):""; ?> </li>
		<li><?php echo ($this->Session->read('User.centerId') == 1 && $this->Session->read('User.profiles_id') != 6) ? $this->Html->link(__('Nuevo'), array('action' => 'add')):""; ?> </li>
		<li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>

