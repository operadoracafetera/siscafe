<div class="operationTrackings form">
<?php echo $this->Form->create('OperationTracking'); ?>
	<fieldset>
		<legend><?php echo __('Actualizar información'); ?></legend>
	<?php
		echo $this->Form->input('expo_id',array('label'=>'Exportador','class' => 'chosen-select','options'=>$clients));
		echo $this->Form->input('ref_text',array('label'=>'Referencia operativa (Placa, Lote Caf�)','required' => true));
		echo $this->Form->input('observation',array('label'=>'Observaciones', 'required' => true,'type'=>'textarea'));
                echo $this->Form->input('jetty',array('label'=>'Puerto Maritimo','options'=>array('BUN'=>'BUN','STM'=>'STM','IMP'=>'IMPALA','COMPAS'=>'COMPAS','CONTECAR'=>'CONTECAR','SPIA'=>'SPIA','SPRC'=>'SPRC')));
                echo $this->Form->input('id',array('required' => true,'type'=>'textarea','type' => 'hidden'));
                echo $this->Form->input('driver_name',array('label'=>'Conductor'));
                echo $this->Form->input('driver_id',array('label'=>'Cedula','type'=>'text'));
		echo $this->Form->input('item1',array('label'=>'1- �Carpa en buen estado?','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('item2',array('label'=>'2- �Asegurada con sellos de seguridad?','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('seals',array('label'=>'3- Escriba los de Sellos de seguridad del vehiculo','required'=>'true'));
		echo $this->Form->input('item4',array('label'=>'4- Corresponde los registrados en los documentos de despacho','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('item5',array('label'=>'5- Llegaron todos en buen estado','required'=>'true','empty' => '(seleccione una...)','options'=>array('SI'=>'SI','NO'=>'NO','N/A'=>'N/A')));
		echo $this->Form->input('trackaing_fnc',array('label'=>'Trazabilidad FNC?','options'=>array('SI'=>'SI','NO'=>'NO')));
		echo $this->Form->input('break_seal',array('label'=>'Sellos vehiculo?','options'=>array('SI'=>'SI','NO'=>'NO')));
                echo $this->Form->input('ref_send',array('label'=>'Se envio notificacion?','empty'=>'Sin datos','options'=>array('SI'=>'SI','NO'=>'NO')));
		echo $this->Form->input('type_tracking',array('label'=>'Referencia operativa','options'=>array('1'=>'DESCARGUE TRAZABILIDAD','4'=>'DESCARGUE NOVEDAD','6'=>'DESCARGUE GRAIN PRO','7'=>'DESCARGUE SELLOS','8'=>'CONEXOS')));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Lista de registros'), array('action' => 'index',1)); ?></li>
                <li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<script>
	$(".chosen-select").chosen();
</script>