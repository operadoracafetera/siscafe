<div class="suppliesOperations form">
<?php echo $this->Form->create('SuppliesOperation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Supplies Operation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('operation');
		echo $this->Form->input('qta');
		echo $this->Form->input('observation');
		echo $this->Form->input('created_date');
		echo $this->Form->input('supplies_id');
		echo $this->Form->input('users_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SuppliesOperation.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('SuppliesOperation.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Supplies Operations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Supplies'), array('controller' => 'supplies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supply'), array('controller' => 'supplies', 'action' => 'add')); ?> </li>
	</ul>
</div>
