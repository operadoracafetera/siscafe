<div class="suppliesOperations form">
<?php echo $this->Form->create('SuppliesOperation'); ?>
	<fieldset>
		<legend><?php echo __('Registrar movimiento sobre el suministro '.$supply['Supply']['name']); ?></legend>
                <h3><?php echo __('Saldo actual: '.$supply['Supply']['saldo']); ?></h3>
	<?php
		echo $this->Form->input('operation',array('label'=>'Operación','options' =>array('INGRESO'=>'INGRESO','SALIDA'=>'SALIDA'),'multiple' => false,'required'=>true,'empty' => true));
		echo $this->Form->input('qta',array('label'=>'Cant. Unidades'));
		echo $this->Form->input('info_navy_id', array('required' => false, 'type' => 'text', 'label' => 'Proforma'));
		echo $this->Form->input('observation',array('label'=>'Observaciones sobre el movimiento'));
		echo $this->Form->input('created_date',array('label' => 'Fecha Reg'));
		echo $this->Form->input('supplies_id',array('type' => 'hidden'));
		echo $this->Form->input('users_id',array('type' => 'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Menu'), array('controller'=>'Pages','action' => 'colaborador')); ?></li>
                <li><?php echo $this->Html->link(__('Ver movimientos'), array('action' => 'index',$supply['Supply']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Listado Suministros'), array('controller'=>'supplies','action' => 'index')); ?></li>
	</ul>
</div>


<script type="text/javascript">
$(".chosen-select").chosen();
        var infoNavies = [<?php echo "'" . implode("','", $infoNavies) . "'"; ?>];
        $("#SuppliesOperationInfoNavyId").autocomplete({
            source: infoNavies,
            select: function(event, ui) {
                var booking = ui.item.label;
                //refillInfoNavyByBooking(booking);
            }
        });
</script>