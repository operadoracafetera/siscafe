<div class="suppliesOperations view">
<h2><?php echo __('Supplies Operation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Operation'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['operation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Qta'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['qta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observation'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['observation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created Date'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['created_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supply'); ?></dt>
		<dd>
			<?php echo $this->Html->link($suppliesOperation['Supply']['name'], array('controller' => 'supplies', 'action' => 'view', $suppliesOperation['Supply']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Users Id'); ?></dt>
		<dd>
			<?php echo h($suppliesOperation['SuppliesOperation']['users_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Supplies Operation'), array('action' => 'edit', $suppliesOperation['SuppliesOperation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Supplies Operation'), array('action' => 'delete', $suppliesOperation['SuppliesOperation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $suppliesOperation['SuppliesOperation']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplies Operations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supplies Operation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Supplies'), array('controller' => 'supplies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Supply'), array('controller' => 'supplies', 'action' => 'add')); ?> </li>
	</ul>
</div>
