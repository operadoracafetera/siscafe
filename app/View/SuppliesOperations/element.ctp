<div class="suppliesOperations form">
<?php echo $this->Form->create('SuppliesOperation'); ?>
	<fieldset>
            <legend><?php echo __('Registrar movimiento suministro sobre el embalaje '. $packagingCaffee['PackagingCaffee']['id']); ?></legend>
	<?php
		//echo $this->Form->input('observation',array('label'=>'Observaciones sobre el movimiento'));
		echo $this->Form->input('created_date',array('type' => 'hidden'));
		echo $this->Form->input('supplies_id',array('type' => 'select','options' =>$supplys));
                echo $this->Form->input('qta',array('label'=>'Cant. Unidades'));
		echo $this->Form->input('users_id',array('type' => 'hidden'));
	?>
        <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                        <th><?php echo $this->Paginator->sort('name','Suministro'); ?></th>
                        <th><?php echo $this->Paginator->sort('qta','Cant. Despachada'); ?></th>
                        <th><?php echo $this->Paginator->sort('created_date','Fecha Registro'); ?></th>
                        <th class="actions"><?php echo __('Acciones'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
            <?php foreach ($supplysOnPackaging as $supplyOnPackaging): ?>
            <tr>
                    <td><?php 
                    //debug($servicePackageHasItemsService);exit;
                    echo h($supplyOnPackaging["SuppliesOperation"]['id']); ?>&nbsp;</td>
                    <td><?php echo h($supplyOnPackaging['Supply']['name']); ?>&nbsp;</td>
                    <td><?php echo h($supplyOnPackaging['SuppliesOperation']['qta']); ?>&nbsp;</td>
                    <td><?php echo h($supplyOnPackaging['SuppliesOperation']['created_date']); ?>&nbsp;</td>
                    <td class="actions">
                            <li><?php  echo $this->Html->link(__('Borrar'), array('action' => 'delete','?' => 
                                ['servicesPackage_id' => $supplyOnPackaging['Supply']['ref_package_service'], 
                                 'oie_id' => $packagingCaffee['PackagingCaffee']['id'],
                                 'supplies_operation_id' => $supplyOnPackaging['SuppliesOperation']['id'],
                                 'supply_id'=>$supplyOnPackaging['Supply']['id']])); ?></li>
                    </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
    </fieldset>
<?php echo $this->Form->end(__('Registrar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
                <li><?php echo $this->Html->link(__('Menu'), array('controller'=>'Pages','action' => 'colaborador')); ?></li>
		<li><?php echo $this->Html->link(__('Regresar'), $this->request->referer()); ?> </li>
	</ul>
</div>
