<div class="slotStores form">
<?php echo $this->Form->create('SlotStore'); ?>
	<fieldset>
		<legend><?php echo __('Edit Slot Store'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name_space');
		echo $this->Form->input('stores_caffee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SlotStore.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('SlotStore.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Slot Stores'), array('action' => 'index')); ?></li>
	</ul>
</div>
