<div id="breadcrumbs">
    <h1><?php $this->Html->addCrumb('Gestión de Usuarios', array('controller' => 'users', 'action' => 'index'))?></h1>
    <h1><?php $this->Html->addCrumb('Agregar nuevo usuario');?></h1>
</div>
<div class="users form">
<?php echo $this->Form->create('User',array(
    'novalidate' => true
)); ?>
	<fieldset>
		<legend><?php echo __('Crear nuevo Usuario'); ?></legend>
	<?php
                echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('group_id', array('type'=>'select','label' =>'Grupo:','options'=>$groups));
		echo $this->Form->input('name', array('label'=>'Razon Social ó Nombre:'));
		echo $this->Form->input('email', array('label'=>'Correo Electronico:'));
		echo $this->Form->input('id_exportador', array('label'=>'Codigo Exportador:'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Salvar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Listado Usuarios'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Atras'), array('controller' => 'users', 'action' => 'index')); ?> </li>
	</ul>
</div>
