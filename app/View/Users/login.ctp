<div class="users form">
<?php echo $this->Form->create('User', array('controller' => 'users', 'login')); ?>
    <fieldset>
        <legend><?php echo __('Ingreso al sistema'); ?></legend>
        <?php echo $this->Form->input('username',array('label' => 'Nombre de usuario'));
        echo $this->Form->input('password',array('label' => 'Contraseña'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Login')); ?>
</div>