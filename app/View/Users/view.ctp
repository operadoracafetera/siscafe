<div id="breadcrumbs">
    <h1><?php $this->Html->addCrumb('Gestión de Usuarios', array('controller' => 'users', 'action' => 'index'))?></h1>
        <h1><?php $this->Html->addCrumb('Detalles del Usuario');?></h1>
</div>
<div class="users view">
<h2><?php echo __('Detalle del Usuario'); ?></h2>
	<dl>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Acceso'); ?></dt>
		<dd>
			<?php echo h($user['User']['date_access']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Grupo'); ?></dt>
		<dd>
			<?php echo $user['Group']['name']; ?>
		</dd>
		<dt><?php echo __('Razon social ó Nombre'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Correo Electronico'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip conexion cliente'); ?></dt>
		<dd>
			<?php echo h($user['User']['ip_remote_server']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Id Exportador'); ?></dt>
		<dd>
			<?php echo h($user['User']['id_exportador']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Modificar Usuario'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Borrar Usuario'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Esta seguro de eliminar el usuario con id # %s?', $user['User']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('Atras'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        </ul>
</div>
