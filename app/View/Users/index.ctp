<div id="breadcrumbs">
    <h1><?php $this->Html->addCrumb('Gestión de Usuarios')?></h1>
</div>
<div class="users index">
	<h2><?php echo __('Listado Maestro de Usuarios'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id_rol', 'Grupo'); ?></th>
			<th><?php echo $this->Paginator->sort('id_exportador', 'Id FNC'); ?></th>
                        <th><?php echo $this->Paginator->sort('username', 'Username'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'Razon social'); ?></th>
			<th><?php echo $this->Paginator->sort('date_access', 'Ultimo Acceso'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
                <td>
			<?php echo $user['Group']['name']; ?>
                </td>
                <td><?php echo h($user['User']['id_exportador']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['date_access']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Detalles'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Pagina {:page} de {:pages}, mostrando {:current} de un registro total de {:count}, iniciando con {:start}, finalizando con el {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Antes'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo Usuario'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Menú Principal'), array('controller' => 'pages', 'action' => 'colaborador')); ?> </li>
	</ul>
</div>
