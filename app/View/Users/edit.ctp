<div id="breadcrumbs">
        <h1><?php $this->Html->addCrumb('Editar usuario');?></h1>
</div>
<div class="users form">
<?php echo $this->Form->create('User',array(
    'novalidate' => true
)); ?>
	<fieldset>
		<legend><?php echo __('Parametros del Usuario'); ?></legend>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('ref_store',array('label'=>'Bodega de operación'));
		echo $this->Form->input('departaments_id',array('label'=>'Bodega de operación','options'=>$departaments));
                echo $this->Form->input('bascule_id',array('label'=>'Basculas operación','empty'=>'(Seleccione...)', 'required' => true,'options' =>$bascules));
                echo $this->Form->input('bascule_selected',array('label'=>'Bascula Seleccionada','disabled' => 'disabled','value'=>$this->request->data['Bascule']['name']));
                echo $this->Form->input('bascule_url',array('label'=>'URL Bascula','disabled' => 'disabled','value'=>$this->request->data['Bascule']['bascule']));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Editar')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Menu'), array('controller' => 'Pages', 'action' => $this->Session->read('User.profile'))); ?> </li>
	</ul>
</div>
