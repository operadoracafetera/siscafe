<?php
App::uses('AppModel', 'Model');
/**
 * InfoNavy Model
 *
 */
class DetailPackakgingCrossdocking extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'detail_packakging_crossdocking';
	
	public $belongsTo = array( 
		'PackagingCaffee' => array( 'className' => 'PackagingCaffee', 
		'foreignKey' => 'packaging_caffee_id', 
		'conditions' => '', 'fields' => '', 
		'order' => '' )); 
}
