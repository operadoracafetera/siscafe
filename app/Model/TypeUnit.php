<?php
App::uses('AppModel', 'Model');
/**
 * TypeUnit Model
 *
 */
class TypeUnit extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'type_units';

	public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));
        
        public $virtualFields = array('name' => "CONCAT(TypeUnit.id,' - ', TypeUnit.name)");

}
