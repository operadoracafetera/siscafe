<?php
App::uses('AppModel', 'Model');
/**
 * StateOperation Model
 *
 */
class StateOperation extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'state_operation';

        public $hasMany = array(
	'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	));

}
