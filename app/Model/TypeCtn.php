<?php
App::uses('AppModel', 'Model');
/**
 * TypeCtn Model
 *
 */
class TypeCtn extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'type_ctn';

}
