<?php
App::uses('AppModel', 'Model');
/**
 * MaterialCoffee Model
 *
 */
class MaterialCoffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'material_coffee';
        
        public $hasMany = array(
	'RemittancesCaffee' => array(
			'className' => 'MaterialCoffee',
			'foreignKey' => 'cod_material',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	));

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
        
        //public $virtualFields = array('name' => 'CONCAT(MaterialCoffee.cod_material, " ", MaterialCoffee.name)');

}
