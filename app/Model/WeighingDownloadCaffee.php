<?php
App::uses('AppModel', 'Model');
/**
 * WeighingDownloadCaffee Model
 *
 */
class WeighingDownloadCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	var $name = "WeighingDownloadCaffee";
	public $useTable = 'weighing_download_caffee';
	
}
