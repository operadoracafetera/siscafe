<?php
App::uses('AppModel', 'Model');
/**
 * SampleRemmitance Model
 *
 */
class SampleRemmitance extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'coffee_sample_id,remittances_caffee_id';
        
        public $belongsTo = array( 
	  'RemittancesCaffee' => array( 'className' => 'RemittancesCaffee', 
	  'foreignKey' => 'remittances_caffee_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ),
	  'CoffeeSample' => array( 'className' => 'CoffeeSample', 
	  'foreignKey' => 'coffee_sample_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' )); 

}
