<?php
App::uses('AppModel', 'Model');
/**
 * ShippingSample Model
 *
 */
class ShippingSample extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'shipping_sample';


}
