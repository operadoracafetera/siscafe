<?php
App::uses('AppModel', 'Model');
/**
 * Bascule Model
 *
 */
class Bill extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bill';

/**
 * Display field
 *
 * @var string
 */
         public $hasMany = array(
            'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		));

}
