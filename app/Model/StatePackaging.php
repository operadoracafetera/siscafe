<?php

App::uses('AppModel', 'Model');

/**
 * Custom Model
 *
 */
class StatePackaging extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'state_packaging';
    
    
    public $hasMany = array(
            'PackagingCaffee' => array(
                            'className' => 'PackagingCaffee',
                            'foreignKey' => 'id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            )
	);

}
