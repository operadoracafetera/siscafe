<?php

App::uses('AppModel', 'Model');

/**
 * ShippingLines Model
 *
 */
class ShippingLine extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    //public $useTable = 'customs';
    
    public $hasMany = array(
		'InfoNavy' => array(
			'className' => 'InfoNavy',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);

}
