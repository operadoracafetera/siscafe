<?php
App::uses('AppModel', 'Model');
/**
 * PackagingCaffee Model
 *
 */
class PackagingCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'packaging_caffee';
	
	public $belongsTo = array(
		'ReadyContainer' => array(
			'className' => 'ReadyContainer',
			'foreignKey' => 'ready_container_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'CheckingCtn' => array(
			'className' => 'CheckingCtn',
			'foreignKey' => 'checking_ctn_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'StatePackaging' => array(
			'className' => 'StatePackaging',
			'foreignKey' => 'state_packaging_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'ServicesOrder' => array(
			'className' => 'ServicesOrder',
			'foreignKey' => 'rem_ref',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'InfoNavy' => array(
			'className' => 'InfoNavy',
			'foreignKey' => 'info_navy_id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	),
            'TraceabilityPackaging' => array(
			'className' => 'TraceabilityPackaging',
			'foreignKey' => 'traceability_packaging_id',
			'conditions' => '',
			'dependent' => false,
			'fields' => '',
			'order' => ''
	));
		
	public $hasMany = array(
            'DetailPackagingCaffee' => array(
                            'className' => 'DetailPackagingCaffee',
                            'foreignKey' => 'packaging_caffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
			),
			'DetailPackakgingCrossdocking' => array(
				'className' => 'DetailPackakgingCrossdocking',
				'foreignKey' => 'packaging_caffee_id',
				'conditions' => '',
				'dependent' => false,
				'fields' => '',
				'order' => ''
)
	);

}
