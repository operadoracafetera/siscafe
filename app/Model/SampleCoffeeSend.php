<?php
App::uses('AppModel', 'Model');
/**
 * SampleCoffeeSend Model
 *
 */
class SampleCoffeeSend extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sample_coffee_send';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'coffee_sample_id,shipping_sample_id';
        
        public $belongsTo = array( 
	  'CoffeeSample' => array( 'className' => 'CoffeeSample', 
	  'foreignKey' => 'coffee_sample_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ),
	  'RemittancesCaffee' => array( 'className' => 'RemittancesCaffee', 
	  'foreignKey' => 'shipping_sample_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' )); 

}
