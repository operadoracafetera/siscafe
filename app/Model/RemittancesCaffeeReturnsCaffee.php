<?php
App::uses('AppModel', 'Model');
/**
 * RemittancesCaffeeReturnsCaffee Model
 *
 * @property RemittancesCaffee $RemittancesCaffee
 * @property ReturnsCaffee $ReturnsCaffee
 */
class RemittancesCaffeeReturnsCaffee extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'remittances_caffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ReturnsCaffee' => array(
			'className' => 'ReturnsCaffee',
			'foreignKey' => 'returns_caffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
