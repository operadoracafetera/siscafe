<?php
App::uses('AppModel', 'Model');
/**
 * DetailPackagingCaffee Model
 *
 */
class DetailPackagingCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'detail_packaging_caffee';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'remittances_caffee_id,packaging_caffee_id';

	public $belongsTo = array( 
	  'PackagingCaffee' => array( 'className' => 'PackagingCaffee', 
	  'foreignKey' => 'packaging_caffee_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ), 
	  'RemittancesCaffee' => array( 'className' => 'RemittancesCaffee', 
	  'foreignKey' => 'remittances_caffee_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ) ); 
}
