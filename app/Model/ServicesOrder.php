<?php

App::uses('AppModel', 'Model');

/**
 * ServicesOrder Model
 *
 */
class ServicesOrder extends AppModel {

   
    public $belongsTo = array(
		'PackagingCaffee' => array(
			'className' => 'PackagingCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));
    
    public $hasMany = array(
            'DetailsServicesToCaffee' => array(
                            'className' => 'DetailsServicesToCaffee',
                            'foreignKey' => 'services_orders_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ));
}
