<?php
App::uses('AppModel', 'Model');
/**
 * NoveltyInCaffee Model
 *
 */
class NoveltyInCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'novelty_in_caffee';
        
        public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
