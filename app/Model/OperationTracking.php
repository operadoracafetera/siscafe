<?php
App::uses('AppModel', 'Model');
/**
 * OperationTracking Model
 *
 */
class OperationTracking extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'operation_tracking';
	
	public $belongsTo = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'expo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
