<?php
App::uses('AppModel', 'Model');
/**
 * ChargeMassiveSampleCoffee Model
 *
 */
class ChargeMassiveSampleCoffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'charge_massive_sample_coffee';
        
        public $belongsTo = array(
		'Users' => array(
			'className' => 'Users',
			'foreignKey' => 'user_reg',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
