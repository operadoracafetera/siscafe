<?php
App::uses('AppModel', 'Model');
/**
 * RemittancesCaffeeHasFumigationService Model
 *
 */
class RemittancesCaffeeHasFumigationService extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'fumigation_services_id,remittances_caffee_id';
        
	public $belongsTo = array( 
	  'FumigationService' => array( 'className' => 'FumigationService', 
	  'foreignKey' => 'fumigation_services_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ),
	  'RemittancesCaffee' => array( 'className' => 'RemittancesCaffee', 
	  'foreignKey' => 'remittances_caffee_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' )); 

}
