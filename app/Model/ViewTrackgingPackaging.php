<?php
App::uses('AppModel', 'Model');
/**
 * ViewTrackgingPackaging Model
 *
 */
class ViewTrackgingPackaging extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'view_trackging_packaging';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'oie';

}
