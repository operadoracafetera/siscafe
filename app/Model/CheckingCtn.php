<?php
App::uses('AppModel', 'Model');
/**
 * CheckingCtn Model
 *
 */
class CheckingCtn extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'checking_ctn';
        
        public $belongsTo = array(
		'PackagingCaffee' => array(
			'className' => 'PackagingCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'User' => array(
			'className' => 'User',
			'foreignKey' => 'users_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
