<?php
App::uses('AppModel', 'Model');
/**
 * ViewPackaging Model
 *
 */
class ViewPackaging extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'view_packaging';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'oie';

}
