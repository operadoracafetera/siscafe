<?php
App::uses('AppModel', 'Model');
/**
 * VehiculeInspection Model
 *
 */
class VehiculeInspection extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'vehicule_inspection';
        
        public $belongsTo = array(
          'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ));

}
