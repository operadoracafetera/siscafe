<?php
App::uses('AppModel', 'Model');
/**
 * CoffeeSample Model
 *
 */
class CoffeeSample extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'coffee_sample';
        
        public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'register_users_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'sampler_users_id'
        ),
        'Client' => array(
            'className' => 'Client',
            'foreignKey' => 'client_id'
        ),);
        
        public $hasMany = array(
            'SampleRemmitance' => array(
                            'className' => 'SampleRemmitance',
                            'foreignKey' => 'coffee_sample_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ),
            'SampleCoffeeHasTypeSample' => array(
                            'className' => 'SampleCoffeeHasTypeSample',
                            'foreignKey' => 'sample_coffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ));

}
