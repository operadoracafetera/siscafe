<?php
App::uses('AppModel', 'Model');
/**
 * WeighingReturnCoffee Model
 *
 * @property RemittancesCaffee $RemittancesCaffee
 */
class WeighingReturnCoffee extends AppModel {

    /**
 * Use table
 *
 * @var mixed False or table name
 */
	var $name = "WeighingReturnCoffee";
	public $useTable = 'weighing_return_coffee';
	
}
