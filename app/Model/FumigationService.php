<?php
App::uses('AppModel', 'Model');
/**
 * FumigationService Model
 *
 */
class FumigationService extends AppModel {
    /**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'fumigation_services';
        
        
        public $belongsTo = array(
		'HookupStatus' => array(
			'className' => 'HookupStatus',
			'foreignKey' => 'hookup_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Departament' => array(
			'className' => 'Departament',
			'foreignKey' => 'departament_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		);
        
        public $hasMany = array(
		'RemittancesCaffeeHasFumigationService' => array(
			'className' => 'RemittancesCaffeeHasFumigationService',
			'foreignKey' => 'fumigation_services_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ScheduleCoffeeFumigation' => array(
			'className' => 'ScheduleCoffeeFumigation',
			'foreignKey' => 'fumigation_services_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);
}
