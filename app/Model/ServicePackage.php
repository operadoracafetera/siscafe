<?php
App::uses('AppModel', 'Model');
/**
 * ServicePackage Model
 *
 */
class ServicePackage extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'service_package';
        
        public $hasMany = array(
            'ServicePackageHasItemsService' => array(
                            'className' => 'ServicePackageHasItemsService',
                            'foreignKey' => 'service_package_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            )); 

}
