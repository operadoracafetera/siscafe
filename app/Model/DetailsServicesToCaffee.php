<?php

App::uses('AppModel', 'Model');

/**
 * DetailsServicesToCaffe Model
 *
 */
class DetailsServicesToCaffee extends AppModel {

    public $useTable = 'details_services_to_caffee';
    
    public $belongsTo = array(
        'ItemsService' => array(
            'className' => 'ItemsService',
            'foreignKey' => 'items_services_id'
        ),
        'ServicePackage' => array(
            'className' => 'ServicePackage',
            'foreignKey' => 'service_package_id'
        ),
        'ServicesOrder' => array(
            'className' => 'ServicesOrder',
            'foreignKey' => 'services_orders_id'
        )
    );
}
