<?php
App::uses('AppModel', 'Model');
/**
 * Supply Model
 *
 */
class Supply extends AppModel {

    public $hasMany = array(
            'SuppliesOperation' => array(
                            'className' => 'SuppliesOperation',
                            'foreignKey' => 'supplies_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ));

}
