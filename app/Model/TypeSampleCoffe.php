<?php
App::uses('AppModel', 'Model');
/**
 * TypeSampleCoffe Model
 *
 */
class TypeSampleCoffe extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'type_sample_coffe';

        
         public $hasMany = array(
            'CoffeeSample' => array(
                            'className' => 'CoffeeSample',
                            'foreignKey' => 'coffee_sample_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ),
            'SampleCoffeeHasTypeSample' => array(
                            'className' => 'SampleCoffeeHasTypeSample',
                            'foreignKey' => 'sample_coffee_id',
                            'conditions' => '',
                            'dependent' => false,
                            'fields' => '',
                            'order' => ''
            ));

}
