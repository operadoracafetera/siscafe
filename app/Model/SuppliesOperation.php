<?php
App::uses('AppModel', 'Model');
/**
 * SuppliesOperation Model
 *
 */
class SuppliesOperation extends AppModel {

    public $belongsTo = array(
        'Supply' => array(
            'className' => 'Supply',
            'foreignKey' => 'supplies_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'users_id'
        ));
}
