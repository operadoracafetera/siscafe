<?php
App::uses('AppModel', 'Model');
/**
 * ViewLotCoffee Model
 *
 */
class ViewLotCoffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'view_lot_coffee';


}
