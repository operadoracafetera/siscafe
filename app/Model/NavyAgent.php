<?php

App::uses('AppModel', 'Model');

/**
 * Custom Model
 *
 */
class NavyAgent extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'navy_agent';

}
