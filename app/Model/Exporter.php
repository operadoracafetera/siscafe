<?php
App::uses('AppModel', 'Model');
/**
 * Exporter Model
 *
 */
class Exporter extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
