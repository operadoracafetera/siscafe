<?php
App::uses('AppModel', 'Model');
/**
 * BulkUploadRemmittancesCoffee Model
 *
 */
class BulkUploadRemmittancesCoffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bulk_upload_remmittances_coffee';
        
        public $belongsTo = array(
		'Users' => array(
			'className' => 'Users',
			'foreignKey' => 'user_reg',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
