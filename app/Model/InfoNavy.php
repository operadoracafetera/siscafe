<?php
App::uses('AppModel', 'Model');
/**
 * InfoNavy Model
 *
 */
class InfoNavy extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'info_navy';
	
	public $hasMany = array(
		'PackagingCaffee' => array(
			'className' => 'PackagingCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);
        
        public $belongsTo = array(
		'NavyAgent' => array(
			'className' => 'NavyAgent',
			'foreignKey' => 'navy_agent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'ShippingLine' => array(
			'className' => 'ShippingLine',
			'foreignKey' => 'shipping_lines_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'Custom' => array(
			'className' => 'Custom',
			'foreignKey' => 'customs_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
            );

}
