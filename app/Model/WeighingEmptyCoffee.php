<?php
App::uses('AppModel', 'Model');
/**
 * WeighingEmptyCoffee Model
 *
 */
class WeighingEmptyCoffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'weighing_empty_coffee';

}
