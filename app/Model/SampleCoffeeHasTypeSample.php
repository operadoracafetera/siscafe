<?php
App::uses('AppModel', 'Model');
/**
 * SampleCoffeeHasTypeSample Model
 *
 */
class SampleCoffeeHasTypeSample extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sample_coffee_has_type_sample';

/**
 * Primary key field
 *
 * @var string
 */
	//public $primaryKey = 'sample_coffee_id';
        
        public $belongsTo = array(
		'CoffeeSample' => array(
			'className' => 'CoffeeSample',
			'foreignKey' => 'sample_coffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TypeSampleCoffe' => array(
			'className' => 'TypeSampleCoffe',
			'foreignKey' => 'type_sample_coffee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
