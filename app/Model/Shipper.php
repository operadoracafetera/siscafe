<?php
App::uses('AppModel', 'Model');
/**
 * Shipper Model
 *
 */
class Shipper extends AppModel {

    public $useTable = 'shippers';

	public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));
    
    //public $virtualFields = array('name' => 'CONCAT(shipper.id, " - ", shipper.business_name)');
    
}
