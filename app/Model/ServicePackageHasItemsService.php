<?php
App::uses('AppModel', 'Model');
/**
 * ServicePackageHasItemsService Model
 *
 */
class ServicePackageHasItemsService extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'n';
        
        
        public $belongsTo = array( 
	  'ServicePackage' => array( 'className' => 'ServicePackage', 
	  'foreignKey' => 'service_package_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' ),
	  'ItemsService' => array( 'className' => 'ItemsService', 
	  'foreignKey' => 'items_services_id', 
	  'conditions' => '', 'fields' => '', 
	  'order' => '' )); 

}
