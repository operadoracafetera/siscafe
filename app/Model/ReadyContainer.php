<?php
App::uses('AppModel', 'Model');
/**
 * ReadyContainer Model
 *
 */
class ReadyContainer extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ready_container';
	
	public $belongsTo = array(
		'PackagingCaffee' => array(
			'className' => 'PackagingCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		);

}
