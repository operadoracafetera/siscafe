<?php
App::uses('AppModel', 'Model');
/**
 * UnitsCaffee Model
 *
 */
class UnitsCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'units_caffee';
        

	public $hasMany= array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));
        
        /**
 * Display field
 *
 * @var string
 */
        public $virtualFields = array('name' => "CONCAT(UnitsCaffee.id,' - ', UnitsCaffee.name_unit)");

}
