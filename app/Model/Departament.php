<?php
App::uses('AppModel', 'Model');
/**
 * Departament Model
 *
 */
class Departament extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $hasMany = array(
		'InfoNavy' => array(
			'className' => 'InfoNavy',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ReturnsCaffee' => array(
			'className' => 'ReturnsCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		);

	

}
