<?php
App::uses('AppModel', 'Model');
/**
 * RemittancesCaffeeHasNoveltysCaffee Model
 *
 */
class RemittancesCaffeeHasNoveltysCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'remittances_caffee_has_noveltys_caffee';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'remittances_caffee_id,noveltys_caffee_id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'remittances_caffee_id';
        
        public $belongsTo = array(
        'NoveltysCoffee' => array(
            'className' => 'NoveltysCoffee',
            'foreignKey' => 'noveltys_caffee_id'
        ),
        'RemittancesCaffee' => array(
            'className' => 'RemittancesCaffee',
            'foreignKey' => 'remittances_caffee_id'
        )
    );

}
