<?php
App::uses('AppModel', 'Model');
/**
 * PackingCaffee Model
 *
 */
class PackingCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'packing_caffee';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        
        public $hasMany = array(
		'RemittancesCaffee' => array(
			'className' => 'RemittancesCaffee',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		));

}
