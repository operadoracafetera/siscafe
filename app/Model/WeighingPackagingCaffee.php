<?php
App::uses('AppModel', 'Model');
/**
 * WeighingDownloadCaffee Model
 *
 */
class WeighingPackagingCaffee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	var $name = "WeighingPackagingCaffee";
	public $useTable = 'weighing_packaging_caffee';
	
}
