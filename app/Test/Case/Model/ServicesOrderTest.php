<?php
App::uses('ServicesOrder', 'Model');

/**
 * ServicesOrder Test Case
 */
class ServicesOrderTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.services_order'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServicesOrder = ClassRegistry::init('ServicesOrder');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServicesOrder);

		parent::tearDown();
	}

}
