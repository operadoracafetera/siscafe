<?php
App::uses('ViewPackaging2', 'Model');

/**
 * ViewPackaging2 Test Case
 */
class ViewPackaging2Test extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.view_packaging2'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ViewPackaging2 = ClassRegistry::init('ViewPackaging2');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ViewPackaging2);

		parent::tearDown();
	}

}
