<?php
App::uses('OperationTracking', 'Model');

/**
 * OperationTracking Test Case
 */
class OperationTrackingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.operation_tracking'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OperationTracking = ClassRegistry::init('OperationTracking');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OperationTracking);

		parent::tearDown();
	}

}
