<?php
App::uses('DetailPackagingCaffee', 'Model');

/**
 * DetailPackagingCaffee Test Case
 */
class DetailPackagingCaffeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.detail_packaging_caffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DetailPackagingCaffee = ClassRegistry::init('DetailPackagingCaffee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DetailPackagingCaffee);

		parent::tearDown();
	}

}
