<?php
App::uses('NoveltyInCaffee', 'Model');

/**
 * NoveltyInCaffee Test Case
 */
class NoveltyInCaffeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.novelty_in_caffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NoveltyInCaffee = ClassRegistry::init('NoveltyInCaffee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NoveltyInCaffee);

		parent::tearDown();
	}

}
