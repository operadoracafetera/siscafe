<?php
App::uses('ItemsService', 'Model');

/**
 * ItemsService Test Case
 */
class ItemsServiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.items_service'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ItemsService = ClassRegistry::init('ItemsService');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ItemsService);

		parent::tearDown();
	}

}
