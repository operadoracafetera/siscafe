<?php
App::uses('Supply', 'Model');

/**
 * Supply Test Case
 */
class SupplyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.supply'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Supply = ClassRegistry::init('Supply');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Supply);

		parent::tearDown();
	}

}
