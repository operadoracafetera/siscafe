<?php
App::uses('Embalaje', 'Model');

/**
 * Embalaje Test Case
 */
class EmbalajeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.embalaje'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Embalaje = ClassRegistry::init('Embalaje');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Embalaje);

		parent::tearDown();
	}

}
