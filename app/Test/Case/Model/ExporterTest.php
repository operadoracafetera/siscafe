<?php
App::uses('Exporter', 'Model');

/**
 * Exporter Test Case
 */
class ExporterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.exporter'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Exporter = ClassRegistry::init('Exporter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Exporter);

		parent::tearDown();
	}

}
