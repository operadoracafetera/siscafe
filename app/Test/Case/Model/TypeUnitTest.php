<?php
App::uses('TypeUnit', 'Model');

/**
 * TypeUnit Test Case
 */
class TypeUnitTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.type_unit'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TypeUnit = ClassRegistry::init('TypeUnit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TypeUnit);

		parent::tearDown();
	}

}
