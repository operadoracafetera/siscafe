<?php
App::uses('ReadyContainer', 'Model');

/**
 * ReadyContainer Test Case
 */
class ReadyContainerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ready_container'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ReadyContainer = ClassRegistry::init('ReadyContainer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ReadyContainer);

		parent::tearDown();
	}

}
