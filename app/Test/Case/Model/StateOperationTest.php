<?php
App::uses('StateOperation', 'Model');

/**
 * StateOperation Test Case
 */
class StateOperationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.state_operation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StateOperation = ClassRegistry::init('StateOperation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StateOperation);

		parent::tearDown();
	}

}
