<?php
App::uses('ViewTrackgingPackaging', 'Model');

/**
 * ViewTrackgingPackaging Test Case
 */
class ViewTrackgingPackagingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.view_trackging_packaging'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ViewTrackgingPackaging = ClassRegistry::init('ViewTrackgingPackaging');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ViewTrackgingPackaging);

		parent::tearDown();
	}

}
