<?php
App::uses('TrackingHasPhoto', 'Model');

/**
 * TrackingHasPhoto Test Case
 */
class TrackingHasPhotoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tracking_has_photo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TrackingHasPhoto = ClassRegistry::init('TrackingHasPhoto');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TrackingHasPhoto);

		parent::tearDown();
	}

}
