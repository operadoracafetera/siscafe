<?php
App::uses('TypeCtn', 'Model');

/**
 * TypeCtn Test Case
 */
class TypeCtnTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.type_ctn'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TypeCtn = ClassRegistry::init('TypeCtn');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TypeCtn);

		parent::tearDown();
	}

}
