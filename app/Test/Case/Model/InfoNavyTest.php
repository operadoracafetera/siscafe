<?php
App::uses('InfoNavy', 'Model');

/**
 * InfoNavy Test Case
 */
class InfoNavyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.info_navy'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InfoNavy = ClassRegistry::init('InfoNavy');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InfoNavy);

		parent::tearDown();
	}

}
