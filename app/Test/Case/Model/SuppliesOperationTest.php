<?php
App::uses('SuppliesOperation', 'Model');

/**
 * SuppliesOperation Test Case
 */
class SuppliesOperationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.supplies_operation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SuppliesOperation = ClassRegistry::init('SuppliesOperation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SuppliesOperation);

		parent::tearDown();
	}

}
