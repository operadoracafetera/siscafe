<?php
App::uses('TallyPackaging', 'Model');

/**
 * TallyPackaging Test Case
 */
class TallyPackagingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tally_packaging'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TallyPackaging = ClassRegistry::init('TallyPackaging');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TallyPackaging);

		parent::tearDown();
	}

}
