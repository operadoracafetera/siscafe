<?php
App::uses('ServicePackageHasItemsService', 'Model');

/**
 * ServicePackageHasItemsService Test Case
 */
class ServicePackageHasItemsServiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.service_package_has_items_service'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServicePackageHasItemsService = ClassRegistry::init('ServicePackageHasItemsService');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServicePackageHasItemsService);

		parent::tearDown();
	}

}
