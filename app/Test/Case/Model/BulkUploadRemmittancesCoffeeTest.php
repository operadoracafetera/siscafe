<?php
App::uses('BulkUploadRemmittancesCoffee', 'Model');

/**
 * BulkUploadRemmittancesCoffee Test Case
 */
class BulkUploadRemmittancesCoffeeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bulk_upload_remmittances_coffee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BulkUploadRemmittancesCoffee = ClassRegistry::init('BulkUploadRemmittancesCoffee');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BulkUploadRemmittancesCoffee);

		parent::tearDown();
	}

}
