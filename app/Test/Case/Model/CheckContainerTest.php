<?php
App::uses('CheckContainer', 'Model');

/**
 * CheckContainer Test Case
 */
class CheckContainerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.check_container'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CheckContainer = ClassRegistry::init('CheckContainer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CheckContainer);

		parent::tearDown();
	}

}
