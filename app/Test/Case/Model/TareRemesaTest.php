<?php
App::uses('TareRemesa', 'Model');

/**
 * TareRemesa Test Case
 */
class TareRemesaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tare_remesa'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TareRemesa = ClassRegistry::init('TareRemesa');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TareRemesa);

		parent::tearDown();
	}

}
