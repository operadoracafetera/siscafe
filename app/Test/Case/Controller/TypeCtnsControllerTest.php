<?php
App::uses('TypeCtnsController', 'Controller');

/**
 * TypeCtnsController Test Case
 */
class TypeCtnsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.type_ctn'
	);

}
