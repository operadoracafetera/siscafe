<?php
App::uses('ExportersController', 'Controller');

/**
 * ExportersController Test Case
 */
class ExportersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.exporter'
	);

}
