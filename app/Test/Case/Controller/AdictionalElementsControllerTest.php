<?php
App::uses('AdictionalElementsController', 'Controller');

/**
 * AdictionalElementsController Test Case
 */
class AdictionalElementsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adictional_element'
	);

}
