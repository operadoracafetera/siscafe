<?php
/**
 * PackagingCaffee Fixture
 */
class PackagingCaffeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'packaging_caffee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'weight_to_out' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'state_packaging_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'long_container' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ready_container_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'cause_emptied_ctn_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'info_navy_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'observation' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'packaging_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'cancelled_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'iso_ctn' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seal_1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seal_2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'users_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'seal_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_packaging_caffee_state_packaging1_idx' => array('column' => 'state_packaging_id', 'unique' => 0),
			'fk_packaging_caffee_ready_container1_idx' => array('column' => 'ready_container_id', 'unique' => 0),
			'fk_packaging_caffee_cause_emptied_ctn1_idx' => array('column' => 'cause_emptied_ctn_id', 'unique' => 0),
			'fk_packaging_caffee_info_navy1_idx' => array('column' => 'info_navy_id', 'unique' => 0),
			'fk_packaging_caffee_users1_idx' => array('column' => 'users_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'created_date' => '2017-03-15 16:18:07',
			'weight_to_out' => 1,
			'state_packaging_id' => 1,
			'long_container' => '',
			'ready_container_id' => 1,
			'active' => 1,
			'cause_emptied_ctn_id' => 1,
			'info_navy_id' => 1,
			'observation' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'packaging_date' => '2017-03-15 16:18:07',
			'cancelled_date' => '2017-03-15 16:18:07',
			'iso_ctn' => 'Lorem ipsum dolor sit amet',
			'seal_1' => 'Lorem ipsum dolor sit amet',
			'seal_2' => 'Lorem ipsum dolor sit amet',
			'users_id' => 1,
			'seal_date' => '2017-03-15 16:18:07'
		),
	);

}
