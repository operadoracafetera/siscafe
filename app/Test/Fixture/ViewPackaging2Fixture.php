<?php
/**
 * ViewPackaging2 Fixture
 */
class ViewPackaging2Fixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'view_packaging2';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'OIE' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 10, 'unsigned' => true),
		'AUTORIZACIÓN' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'LOTES' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'FECHA_REGISTROS' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'BOOKING' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MOTONAVA' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'NAVIERO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ADUANAS' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'VIAJE' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'EXPORTADOR' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 300, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'LINEA' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'CTA_CAFE' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'PESO_CTN' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'SELLO_1' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SELLO_2' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'PUERTO' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 300, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ESTADO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ISO_CTN' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BIC_CTN' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MODALIDAD' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'TIPO_EMBALAJE' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DESTINO' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ELEMENTOS_EMBALAJE' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			
		),
		'tableParameters' => array('comment' => 'VIEW')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'OIE' => 1,
			'AUTORIZACIÓN' => 1,
			'LOTES' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'FECHA_REGISTROS' => '2019-09-13 15:09:49',
			'BOOKING' => 'Lorem ipsum dolor sit amet',
			'MOTONAVA' => 'Lorem ipsum dolor sit amet',
			'NAVIERO' => 'Lorem ipsum dolor sit amet',
			'ADUANAS' => 'Lorem ipsum dolor sit amet',
			'VIAJE' => 'Lorem ipsum dolor sit amet',
			'EXPORTADOR' => 'Lorem ipsum dolor sit amet',
			'LINEA' => 'Lorem ipsum dolor sit amet',
			'CTA_CAFE' => 1,
			'PESO_CTN' => 1,
			'SELLO_1' => 'Lorem ipsum dolor sit amet',
			'SELLO_2' => 'Lorem ipsum dolor sit amet',
			'PUERTO' => 'Lorem ipsum dolor sit amet',
			'ESTADO' => 'Lorem ipsum dolor sit amet',
			'ISO_CTN' => 'Lorem ipsum dolor sit amet',
			'BIC_CTN' => 'Lorem ips',
			'MODALIDAD' => 'Lorem ipsum dolor sit amet',
			'TIPO_EMBALAJE' => 'Lorem ipsum dolor sit amet',
			'DESTINO' => 'Lorem ipsum dolor sit amet',
			'ELEMENTOS_EMBALAJE' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
