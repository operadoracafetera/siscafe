<?php
/**
 * SuppliesOperation Fixture
 */
class SuppliesOperationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'operation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'qta' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'observation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 345, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'supplies_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'users_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_supplies_debits_supplies1_idx' => array('column' => 'supplies_id', 'unique' => 0),
			'fk_supplies_debits_users1_idx' => array('column' => 'users_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'operation' => 'Lorem ipsum dolor sit amet',
			'qta' => 1,
			'observation' => 'Lorem ipsum dolor sit amet',
			'created_date' => '2017-08-16 15:51:07',
			'supplies_id' => 1,
			'users_id' => 1
		),
	);

}
