<?php
/**
 * RemittancesCaffee Fixture
 */
class RemittancesCaffeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'remittances_caffee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 7, 'unsigned' => true, 'key' => 'primary'),
		'lot_caffee' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'quantity_bag_in_store' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'quantity_bag_out_store' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'quantity_in_pallet_caffee' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true),
		'quantity_out_pallet_caffee' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true),
		'quantity_radicated_bag_in' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'total_weight_net_nominal' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'total_weight_net_real' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'tare_download' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'tare_packaging' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'source_location' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true),
		'auto_otm' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'vehicle_plate' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'download_caffee_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'packaging_caffee_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_dated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'is_active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'guide_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'client_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'units_cafee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'packing_cafee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'mark_cafee_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'city_source_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'shippers_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'slot_store_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'staff_sample_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'staff_wt_in_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'staff_wt_out_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'staff_driver_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'observation' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'details_weight' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'packaging_caffee_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'total_tare' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_pallet_packaging_total' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'quantity_radicated_bag_out' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'state_operation_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'key' => 'index'),
		'ref_download' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ref_packaging' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'quantity_radicated_bag_out_total' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'total_bag_out' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'custom_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'total_rad_bag_out' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_remittances_caffee_clients1_idx' => array('column' => 'client_id', 'unique' => 0),
			'fk_remittances_caffee_units_cafee1_idx' => array('column' => 'units_cafee_id', 'unique' => 0),
			'fk_remittances_caffee_packing_cafee1_idx' => array('column' => 'packing_cafee_id', 'unique' => 0),
			'fk_remittances_caffee_mark_cafee1_idx' => array('column' => 'mark_cafee_id', 'unique' => 0),
			'fk_remittances_caffee_city_source1_idx' => array('column' => 'city_source_id', 'unique' => 0),
			'fk_remittances_caffee_shippers1_idx' => array('column' => 'shippers_id', 'unique' => 0),
			'fk_remittances_caffee_slot_store1_idx' => array('column' => 'slot_store_id', 'unique' => 0),
			'fk_remittances_caffee_users1_idx' => array('column' => 'staff_sample_id', 'unique' => 0),
			'fk_remittances_caffee_users2_idx' => array('column' => 'staff_wt_in_id', 'unique' => 0),
			'fk_remittances_caffee_users3_idx' => array('column' => 'staff_wt_out_id', 'unique' => 0),
			'fk_remittances_caffee_users4_idx' => array('column' => 'staff_driver_id', 'unique' => 0),
			'fk_remittances_caffee_packaging_caffee1_idx' => array('column' => 'packaging_caffee_id', 'unique' => 0),
			'fk_remittances_caffee_state_operation1' => array('column' => 'state_operation_id', 'unique' => 0),
			'custom_id' => array('column' => 'custom_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'lot_caffee' => 'Lorem ip',
			'quantity_bag_in_store' => 1,
			'quantity_bag_out_store' => 1,
			'quantity_in_pallet_caffee' => 1,
			'quantity_out_pallet_caffee' => 1,
			'quantity_radicated_bag_in' => 1,
			'total_weight_net_nominal' => 1,
			'total_weight_net_real' => 1,
			'tare_download' => 1,
			'tare_packaging' => 1,
			'source_location' => 1,
			'auto_otm' => 'Lorem ipsum dolor sit amet',
			'vehicle_plate' => 'Lorem ip',
			'download_caffee_date' => '2017-03-15 17:22:58',
			'packaging_caffee_date' => '2017-03-15 17:22:58',
			'created_date' => '2017-03-15 17:22:58',
			'updated_dated' => '2017-03-15 17:22:58',
			'is_active' => 1,
			'guide_id' => 'Lorem ipsum dolor sit amet',
			'client_id' => 1,
			'units_cafee_id' => 1,
			'packing_cafee_id' => 1,
			'mark_cafee_id' => 1,
			'city_source_id' => 1,
			'shippers_id' => 1,
			'slot_store_id' => 1,
			'staff_sample_id' => 1,
			'staff_wt_in_id' => 1,
			'staff_wt_out_id' => 1,
			'staff_driver_id' => 1,
			'observation' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'details_weight' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'packaging_caffee_id' => 1,
			'total_tare' => 1,
			'weight_pallet_packaging_total' => 1,
			'quantity_radicated_bag_out' => 1,
			'state_operation_id' => 1,
			'ref_download' => 'Lorem ipsum dolor sit amet',
			'ref_packaging' => 'Lorem ipsum dolor sit amet',
			'quantity_radicated_bag_out_total' => 1,
			'total_bag_out' => 1,
			'custom_id' => 1,
			'total_rad_bag_out' => 1
		),
	);

}
