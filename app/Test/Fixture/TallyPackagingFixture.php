<?php
/**
 * TallyPackaging Fixture
 */
class TallyPackagingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'tally_packaging';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'bic_container' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'seals_line_img' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seals_line_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seals_policy_img' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'seals_policy_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'users_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_tally_packaging_users1_idx' => array('column' => 'users_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'bic_container' => 'Lorem ips',
			'created_date' => '2016-07-29 17:47:25',
			'seals_line_img' => 'Lorem ipsum dolor sit amet',
			'seals_line_code' => 'Lorem ipsum dolor sit amet',
			'seals_policy_img' => 'Lorem ipsum dolor sit amet',
			'seals_policy_code' => 'Lorem ipsum dolor sit amet',
			'users_id' => 1
		),
	);

}
