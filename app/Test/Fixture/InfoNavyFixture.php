<?php
/**
 * InfoNavy Fixture
 */
class InfoNavyFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'info_navy';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'proforma' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'packaging_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'motorship_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'packaging_mode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'booking' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'travel_num' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'destiny' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'navy_agent_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'shipping_lines_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'customs_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'jornally' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'observation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 300, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_info_navy_navy_agent1_idx' => array('column' => 'navy_agent_id', 'unique' => 0),
			'fk_info_navy_shipping_lines1_idx' => array('column' => 'shipping_lines_id', 'unique' => 0),
			'fk_info_navy_customs1_idx' => array('column' => 'customs_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'proforma' => 'Lorem ipsum dolor sit amet',
			'packaging_type' => 'Lorem ipsum dolor sit amet',
			'motorship_name' => 'Lorem ipsum dolor sit amet',
			'packaging_mode' => 'Lorem ipsum dolor sit amet',
			'booking' => 'Lorem ipsum dolor sit amet',
			'travel_num' => 'Lorem ipsum dolor sit amet',
			'destiny' => 'Lorem ipsum dolor sit amet',
			'navy_agent_id' => 1,
			'shipping_lines_id' => 1,
			'customs_id' => 1,
			'jornally' => 'Lor',
			'observation' => 'Lorem ipsum dolor sit amet'
		),
	);

}
