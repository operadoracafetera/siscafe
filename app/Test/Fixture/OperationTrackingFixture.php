<?php
/**
 * OperationTracking Fixture
 */
class OperationTrackingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'operation_tracking';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'ref_text' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 145, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'observation' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 400, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'ref_text' => 'Lorem ipsum dolor sit amet',
			'observation' => 'Lorem ipsum dolor sit amet',
			'created_date' => '2019-07-17 08:50:50',
			'updated_date' => '2019-07-17 08:50:50'
		),
	);

}
