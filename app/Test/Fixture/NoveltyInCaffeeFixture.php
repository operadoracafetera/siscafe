<?php
/**
 * NoveltyInCaffee Fixture
 */
class NoveltyInCaffeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'novelty_in_caffee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'img1' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 245, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'img2' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 245, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'img3' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 245, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'img4' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 245, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'observation' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'img1' => 'Lorem ipsum dolor sit amet',
			'img2' => 'Lorem ipsum dolor sit amet',
			'img3' => 'Lorem ipsum dolor sit amet',
			'img4' => 'Lorem ipsum dolor sit amet',
			'observation' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created_date' => '2017-06-12 18:54:43'
		),
	);

}
