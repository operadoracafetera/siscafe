<?php
/**
 * CheckContainer Fixture
 */
class CheckContainerFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'check_container';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'check_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'question_1' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'question_2' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'question_3' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'question_4' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'question_5' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'question_6' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'state_container' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'observation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bic_container' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'users_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_check_container_users1_idx' => array('column' => 'users_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'check_date' => '2016-07-29 02:35:38',
			'question_1' => 'Lorem ipsum dolor sit amet',
			'question_2' => 'Lorem ipsum dolor sit amet',
			'question_3' => 'Lorem ipsum dolor sit amet',
			'question_4' => 'Lorem ipsum dolor sit amet',
			'question_5' => 'Lorem ipsum dolor sit amet',
			'question_6' => 'Lorem ipsum dolor sit amet',
			'state_container' => 'Lorem ipsum dolor sit amet',
			'observation' => 'Lorem ipsum dolor sit amet',
			'bic_container' => 'Lorem ips',
			'users_id' => 1
		),
	);

}
