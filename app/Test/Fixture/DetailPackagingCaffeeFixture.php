<?php
/**
 * DetailPackagingCaffee Fixture
 */
class DetailPackagingCaffeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'detail_packaging_caffee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'remittances_caffee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 7, 'unsigned' => true, 'key' => 'primary'),
		'packaging_caffee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'quantity_radicated_bag_out' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'state' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('remittances_caffee_id', 'packaging_caffee_id'), 'unique' => 1),
			'fk_remittances_caffee_has_packaging_caffee_packaging_caffee_idx' => array('column' => 'packaging_caffee_id', 'unique' => 0),
			'fk_remittances_caffee_has_packaging_caffee_remittances_caff_idx' => array('column' => 'remittances_caffee_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'remittances_caffee_id' => 1,
			'packaging_caffee_id' => 1,
			'quantity_radicated_bag_out' => 1,
			'created_date' => '2017-03-15 17:10:37',
			'updated_date' => '2017-03-15 17:10:37',
			'state' => ''
		),
	);

}
