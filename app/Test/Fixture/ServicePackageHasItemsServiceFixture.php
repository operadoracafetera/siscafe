<?php
/**
 * ServicePackageHasItemsService Fixture
 */
class ServicePackageHasItemsServiceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'service_package_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'items_services_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'qta' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => array('service_package_id', 'items_services_id'), 'unique' => 1),
			'fk_service_package_has_items_services_items_services1_idx' => array('column' => 'items_services_id', 'unique' => 0),
			'fk_service_package_has_items_services_service_package1_idx' => array('column' => 'service_package_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'service_package_id' => 1,
			'items_services_id' => 1,
			'qta' => 1
		),
	);

}
