<?php
/**
 * BulkUploadRemmittancesCoffee Fixture
 */
class BulkUploadRemmittancesCoffeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'bulk_upload_remmittances_coffee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 20, 'unsigned' => true, 'key' => 'primary'),
		'name_file' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'date_reg' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'count_insert' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2, 'unsigned' => false),
		'user_reg' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'msg_error' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name_file' => 'Lorem ipsum dolor sit amet',
			'date_reg' => '2019-10-03 17:31:39',
			'count_insert' => 1,
			'user_reg' => 1,
			'msg_error' => 'Lorem ipsum dolor sit amet'
		),
	);

}
