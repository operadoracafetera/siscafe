<?php
/**
 * ServicePackage Fixture
 */
class ServicePackageFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'service_package';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'name_package' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 145, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'price_per_bag' => array('type' => 'decimal', 'null' => false, 'default' => '0', 'length' => 10, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name_package' => 'Lorem ipsum dolor sit amet',
			'active' => 1,
			'created_date' => '2017-06-28 16:08:01',
			'updated_date' => '2017-06-28 16:08:01',
			'price_per_bag' => ''
		),
	);

}
