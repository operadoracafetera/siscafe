<?php
/**
 * TrackingHasPhoto Fixture
 */
class TrackingHasPhotoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'tracking_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'name_file' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 245, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'tracking_has_photos_indx1' => array('column' => 'tracking_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'tracking_id' => 1,
			'name_file' => 'Lorem ipsum dolor sit amet',
			'created_date' => '2019-07-17 08:51:17',
			'updated_date' => '2019-07-17 08:51:17'
		),
	);

}
