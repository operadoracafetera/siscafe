<?php
App::uses('AppController', 'Controller');
/**
 * SampleRemmitances Controller
 *
 * @property SampleRemmitance $SampleRemmitance
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property yComponent $y
 * @property SessionComponent $Session
 */
class SampleRemmitancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Y', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SampleRemmitance->recursive = 0;
		$this->set('sampleRemmitances', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SampleRemmitance->exists($id)) {
			throw new NotFoundException(__('Invalid sample remmitance'));
		}
		$options = array('conditions' => array('SampleRemmitance.' . $this->SampleRemmitance->primaryKey => $id));
		$this->set('sampleRemmitance', $this->SampleRemmitance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SampleRemmitance->create();
			if ($this->SampleRemmitance->save($this->request->data)) {
				$this->Flash->success(__('The sample remmitance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The sample remmitance could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SampleRemmitance->exists($id)) {
			throw new NotFoundException(__('Invalid sample remmitance'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SampleRemmitance->save($this->request->data)) {
				$this->Flash->success(__('The sample remmitance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The sample remmitance could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SampleRemmitance.' . $this->SampleRemmitance->primaryKey => $id));
			$this->request->data = $this->SampleRemmitance->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SampleRemmitance->id = $id;
		if (!$this->SampleRemmitance->exists()) {
			throw new NotFoundException(__('Invalid sample remmitance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SampleRemmitance->delete()) {
			$this->Flash->success(__('The sample remmitance has been deleted.'));
		} else {
			$this->Flash->error(__('The sample remmitance could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
