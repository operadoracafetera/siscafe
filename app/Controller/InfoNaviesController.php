<?php

App::uses('AppController', 'Controller');

/**
 * InfoNavies Controller
 *
 * @property InfoNavy $InfoNavy
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class InfoNaviesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->InfoNavy->recursive = 3;
        $this->paginate = array(
                                'limit' => 30,
                                'order' => array('InfoNavy.id'=>'desc')
                            );
        $data = $this->paginate('InfoNavy');
        $this->set('infoNavies', $data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->InfoNavy->exists($id)) {
            throw new NotFoundException(__('Invalid info navy'));
        }
        $options = array('conditions' => array('InfoNavy.' . $this->InfoNavy->primaryKey => $id));
        $this->set('infoNavy', $this->InfoNavy->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->InfoNavy->create();
            date_default_timezone_set('America/Bogota');
            $this->request->data['InfoNavy']['created_date'] = date('Y-m-d h:i:s');
            if ($this->InfoNavy->save($this->request->data)) {
                $this->Flash->success(__('La Proforma ha sido guardada'));
                return $this->redirect(array('controller'=>'PackagingCaffees','action' => 'add'));
            } else {
                $this->Flash->error(__('La Proforma no pudo ser guardada'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->InfoNavy->exists($id)) {
            throw new NotFoundException(__('Invalid info navy'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is(array('post', 'put'))) {
            if ($this->InfoNavy->save($this->request->data)) {
                $this->Flash->success(__('Se actualizo el registro exitosamente'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('No se registro la información. Por favor intentelo nuevamente.'));
            }
        } else {
            $options = array('conditions' => array('InfoNavy.' . $this->InfoNavy->primaryKey => $id));
            $this->request->data = $this->InfoNavy->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->InfoNavy->id = $id;
        if (!$this->InfoNavy->exists()) {
            throw new NotFoundException(__('Invalid info navy'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->InfoNavy->delete()) {
            $this->Flash->success(__('The info navy has been deleted.'));
        } else {
            $this->Flash->error(__('The info navy could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function findInfoNavyByProforma($proforma=null) {
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->autoRender = false;
        $dataInfoNavy = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.proforma' => $proforma)));
        if($dataInfoNavy) {
            $this->loadModel('NavyAgent');
            $data = array('InfoNavy' => $dataInfoNavy,
            'InfoNavyAgent' => $this->NavyAgent->find('first', array('conditions' => array('NavyAgent.id' => $dataInfoNavy['InfoNavy']['navy_agent_id']))));
            return json_encode($data);
        }
        else{
            return "";
        }
    }
    
    public function findInfoNavyByBooking($booking=null) {
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->autoRender = false;
        $this->InfoNavy->recursive = 0;
        $dataInfoNavy = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.booking' => $booking)));
        if($dataInfoNavy) {
            $data = array($dataInfoNavy);
            return json_encode($data);
        }
        else{
            return "";
        }
    }
    
    public function searchNavies(){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('AdictionalElementsHasPackagingCaffee');
        $searchInfoList = array();
        $adictionalInfoList = array();
        if ($this->request->is('post')) {
            $this->InfoNavy->recursive = 3;
            if ($this->request->data['InfoNavy']['codigo']) {
                $searchInfoList = $this->InfoNavy->find('all', array('conditions' => array('InfoNavy.id' => $this->request->data['InfoNavy']['codigo'])));
                $adictionalInfoList = $this->AdictionalElementsHasPackagingCaffee->find('all',array('conditions' => array('AdictionalElementsHasPackagingCaffee.info_navy_id' => $this->request->data['InfoNavy']['codigo'])));
            } else if ($this->request->data['InfoNavy']['proforma']) {
                $searchInfoList = $this->InfoNavy->find('all', array('conditions' => array('InfoNavy.proforma' => $this->request->data['InfoNavy']['proforma'])));
                $searchInfo = $this->InfoNavy->find('list', array('fields' => 'InfoNavy.id','conditions' => array('InfoNavy.proforma' => $this->request->data['InfoNavy']['proforma'])));
                $adictionalInfoList = $this->AdictionalElementsHasPackagingCaffee->find('all',array('conditions' => array('AdictionalElementsHasPackagingCaffee.info_navy_id' => $searchInfo)));
            } else if ($this->request->data['InfoNavy']['booking']) {
                $searchInfoList = $this->InfoNavy->find('all', array('conditions' => array('InfoNavy.booking' => $this->request->data['InfoNavy']['booking'])));
                $searchInfo = $this->InfoNavy->find('list', array('fields' => 'InfoNavy.id','conditions' => array('InfoNavy.booking' => $this->request->data['InfoNavy']['booking'])));
                $adictionalInfoList = $this->AdictionalElementsHasPackagingCaffee->find('all',array('conditions' => array('AdictionalElementsHasPackagingCaffee.info_navy_id' => $searchInfo)));
            }else if ($this->request->data['InfoNavy']['motorship_name']) {
                $searchInfoList = $this->InfoNavy->find('all', array('conditions' => array('InfoNavy.motorship_name' => $this->request->data['InfoNavy']['motorship_name'])));
            }
        }
        $this->set('searchInfoList', $searchInfoList);
        $this->set('adictionalInfoList', $adictionalInfoList);
    }
    
    public function elementAdicional($id = null) {
		$this->loadModel('SuppliesOperation');
		$this->loadModel('Supply');
		$this->loadModel('PackagingCaffee');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->InfoNavy->exists($id)) {
            throw new NotFoundException(__('Invalid info navy'));
        }
        $this->layout = 'colaborador';
		
        if ($this->request->is(array('post', 'put'))) {
			$this->SuppliesOperation->create();
			$dataCtn = $this->PackagingCaffee->find('first',['conditions'=>['PackagingCaffee.info_navy_id'=>$id]]);
			$ctnJetty = $dataCtn['PackagingCaffee']['jetty'];
			$dataSuppliesOperation = $this->SuppliesOperation->find('first', ['conditions'=>['SuppliesOperation.info_navy_id'=>$id]]);
			$ctaCtns = $this->request->data['InfoNavy']['num_ctn_real'];
            $dataInfoNavy = $this->InfoNavy->find('first',['conditions'=>['InfoNavy.id'=>$id]]);
            $idSupplyMT = null;
            $dataSupply = null;
            $numMT = null;
            if($this->request->data['InfoNavy']['adictional_num_manta'] != 0){
                $numMT = $this->request->data['InfoNavy']['adictional_num_manta']*$ctaCtns;
                if($ctnJetty == 'BUN' || $ctnJetty == 'SPIA'){$idSupplyMT = 24;}
                else if( $ctnJetty == 'CONTECAR' or $ctnJetty == 'SPRC' or $ctnJetty == 'COMPAS' or $ctnJetty == 'CTG' or $ctnJetty == 'BLOCPORT'){$idSupplyMT = 25;}
                $dataSupply = $this->Supply->find('first',['conditions'=>['Supply.id'=>$idSupplyMT]]);
            }
            else if($this->request->data['InfoNavy']['mantas_vdry_40'] != 0){
                $numMT = $this->request->data['InfoNavy']['mantas_vdry_40']*$ctaCtns;
                if($ctnJetty == 'BUN' || $ctnJetty == 'SPIA'){$idSupplyMT = 27;}
                else if( $ctnJetty == 'CONTECAR' or $ctnJetty == 'SPRC' or $ctnJetty == 'COMPAS' or $ctnJetty == 'CTG' or $ctnJetty == 'BLOCPORT'){$idSupplyMT = 28;}
                $dataSupply = $this->Supply->find('first',['conditions'=>['Supply.id'=>$idSupplyMT]]);
            }
			//Borra
			if($dataSuppliesOperation && $idSupplyMT){
				$allOperations = $this->SuppliesOperation->find('all', ['conditions'=>['SuppliesOperation.supplies_id'=>$idSupplyMT]]);
				$saldoElemento = 0;
				foreach($allOperations as $operation){
					if($operation['SuppliesOperation']['operation'] == 'INGRESO'){
						$saldoElemento = $saldoElemento + $operation['SuppliesOperation']['qta'];
					}
					else if($operation['SuppliesOperation']['operation'] == 'SALIDA'){
						$saldoElemento = $saldoElemento - $operation['SuppliesOperation']['qta'];
					}
				}
				$saldoElementoAntes = $saldoElemento+$dataSuppliesOperation['SuppliesOperation']['qta'];
				if($saldoElementoAntes-$numMT >= 0){
					$this->SuppliesOperation->deleteAll(['SuppliesOperation.id'=>$dataSuppliesOperation['SuppliesOperation']['id']],false);
					$saldoElemento = $saldoElemento+$dataSuppliesOperation['SuppliesOperation']['qta'];
					$this->Supply->updateAll(['Supply.saldo'=>$saldoElemento],['Supply.id'=>$idSupplyMT]);
				}
				
				$dataSupply = $this->Supply->find('first',['conditions'=>['Supply.id'=>$idSupplyMT]]);
			}
			
			if($numMT != 0 && $idSupplyMT){
				if($dataSupply['Supply']['saldo']-$numMT < 0){
					$this->Flash->error(__('Saldo insuficiente en el suministro de Mantas Termicas.'));
					return $this->redirect(array('action' => 'elementAdicional',$id));
				}
				$newOperation = [
					'operation'=>'SALIDA', 
					'qta'=>$numMT,
					'observation' => 'Uso en proforma de embalaje '.$dataInfoNavy['InfoNavy']['proforma'],
					'created_date' => date('Y-m-d h:i:s'), 
					'supplies_id'=> $idSupplyMT, 
					'users_id'=> $this->Session->read('User.id'),
					'saldo_before'=> $dataSupply['Supply']['saldo'],
					'info_navy_id'=> $id,
					];
				if($this->SuppliesOperation->save($newOperation)){
					$this->Supply->updateAll(['Supply.saldo'=>$dataSupply['Supply']['saldo']-$numMT],['Supply.id'=>$idSupplyMT]);
				}
			}
			
			
            if ($this->InfoNavy->save($this->request->data)) {
                $this->Flash->success(__('Se actualizo el registro exitosamente'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('No se registro la informaci�n. Por favor intentelo nuevamente.'));
            }
        } else {
            $options = array('conditions' => array('InfoNavy.' . $this->InfoNavy->primaryKey => $id));
            $this->request->data = $this->InfoNavy->find('first', $options);
        }
    }
    
}
