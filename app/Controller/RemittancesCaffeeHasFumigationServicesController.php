<?php
App::uses('AppController', 'Controller');
/**
 * RemittancesCaffeeHasFumigationServices Controller
 *
 * @property RemittancesCaffeeHasFumigationService $RemittancesCaffeeHasFumigationService
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RemittancesCaffeeHasFumigationServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    //public $components = array('Paginator', 'Flash', 'Session');


    public function beforeFilter(){
        $this->Session = $this->Components->load('Session');
        $this->Paginator = $this->Components->load('Paginator');
        $this->Flash = $this->Components->load('Flash');
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RemittancesCaffeeHasFumigationService->recursive = 0;
		$this->set('remittancesCaffeeHasFumigationServices', $this->Paginator->paginate());
    }


    /**
 * findLotCoffeeFumigation method
 *
 * @return void
 */
    public function findLotCoffeeFumigation($lotCoffee = null){
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->autoRender = false;
        $this->loadModel('ScheduleCoffeeFumigation');        
                $dataScheduleCoffeeFumigation = $this->ScheduleCoffeeFumigation->find('all',['conditions'=>['and'=>['ScheduleCoffeeFumigation.status'=>1,'ScheduleCoffeeFumigation.lot_coffee LIKE'=>'%'.$lotCoffee.'%']]]);

        if($dataScheduleCoffeeFumigation){
            echo json_encode($dataScheduleCoffeeFumigation);
        }
        else{
            echo json_encode("");
        }
    }
        


/**
 * add method
 *
 * @return void
 */
	public function add() {
                if(!$this->Session->read('User.id')){
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                }
                $this->layout = 'colaborador';
                $this->loadModel('FumigationService');
                $this->loadModel('ServicesOrder');
                $this->loadModel('Client');
                $this->loadModel('RemittancesCaffeeHasFumigationService');
                $this->loadModel('RemittancesCaffee');
                $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
                $this->RemittancesCaffeeHasFumigationService->recursive = 2;
                $orderId = isset($this->request->query['services_orders_id']) ? $this->request->query['services_orders_id'] : null;
                $fumigationId = isset($this->request->query['fumigation_services_id']) ? $this->request->query['fumigation_services_id'] : null;
                $exporter_id = isset($this->request->query['exporter_id']) ? $this->request->query['exporter_id'] : null; 
                $db = $this->FumigationService->getDataSource();
		if ($this->request->is('post')) {
                    //debug($this->request->data);exit;
			$this->RemittancesCaffeeHasFumigationService->create();
                        $this->request->data['RemittancesCaffeeHasFumigationService']['remittances_caffee_id']=$this->request->data['RemittancesCaffeeHasFumigationService']['remittances_caffee_id'];
                        $this->request->data['RemittancesCaffeeHasFumigationService']['fumigation_services_id']=$this->request->data['RemittancesCaffeeHasFumigationService']['fumigation_services_id'];
			$remittancesCaffee = $this->RemittancesCaffee->find('first',array('conditions' => array('RemittancesCaffee.id' => $this->request->data['RemittancesCaffeeHasFumigationService']['remittances_caffee_id'])));
                        $remittancesCaffeeHasFumigationService=$this->RemittancesCaffeeHasFumigationService->find('first',array('conditions' => array('RemittancesCaffeeHasFumigationService.remittances_caffee_id' => $this->request->data['RemittancesCaffeeHasFumigationService']['remittances_caffee_id'])));
                        if($remittancesCaffee != null && $remittancesCaffeeHasFumigationService == null){
                            $fumigationService = $this->FumigationService->find('first',array('conditions' => array('FumigationService.id' => $fumigationId)));
                            $dosis = $this->request->data['RemittancesCaffeeHasFumigationService']['dosis'];
                            $quimico = $this->request->data['RemittancesCaffeeHasFumigationService']['quimico'];
                            unset($this->request->data['RemittancesCaffeeHasFumigationService']['completed'],$this->request->data['RemittancesCaffeeHasFumigationService']['quimico'],
                                    $this->request->data['RemittancesCaffeeHasFumigationService']['remesa'],$this->request->data['RemittancesCaffeeHasFumigationService']['dosis']);
                            $volumenRemittancesCaffee = ($remittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'])*(3.5625);
                            $volumenRemittancesCaffee += $fumigationService['FumigationService']['volumen_fumigated'];
                            $horas_elevador = $this->request->data['RemittancesCaffeeHasFumigationService']['hour_elevator'];
                            $weightCaffee = $remittancesCaffee['RemittancesCaffee']['total_weight_net_real'];
                            $weightCaffee += $fumigationService['FumigationService']['weight_caffee'];
                            $cta_poison_tablet = ($volumenRemittancesCaffee*$dosis);
                            $cta_bandejas_poison = ($cta_poison_tablet/30);
                            $ppm_teorico = ($dosis * 659);
                            if ($this->FumigationService->updateAll(
                                    array(
                                        'FumigationService.ppm_teory'=>$ppm_teorico,
                                        'FumigationService.bandejas_used'=>$cta_bandejas_poison,
                                        'FumigationService.hour_elevator'=>$horas_elevador,
                                        'FumigationService.quantity_poison_used'=>$cta_poison_tablet,
                                        'FumigationService.volumen_fumigated'=> $volumenRemittancesCaffee,
                                        'FumigationService.qta_bags'=> $fumigationService['FumigationService']['qta_bags']+$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'],
                                        'FumigationService.dosis'=> $dosis,
                                        'FumigationService.product_fumigation'=> $db->value($quimico,'string'),
                                        'FumigationService.weight_caffee' => $weightCaffee),
                                    array('FumigationService.id' => $fumigationId))) {
                                    $this->RemittancesCaffeeHasFumigationService->save($this->request->data);
                                    $remittancesCaffe = array('remittances_caffee_id'=>$remittancesCaffee['RemittancesCaffee']['id'],
                                                'noveltys_caffee_id'=>1,
                                                'created_date'=>date('Y-m-d h:i:s'),
                                                'active'=>1);
                                    $this->RemittancesCaffeeHasNoveltysCaffee->save($remittancesCaffe);
                                    $this->Flash->success(__('Se agrego con exito el lote a fumigar'));
                                    return $this->redirect(array('controller' => 'FumigationServices','action' => 'index'));
                            } else {
                                    $this->Flash->error(__('The remittances caffee has fumigation service could not be saved. Please, try again.'));
                            } 
                        }
                        else{
                            $fumigationService = $this->FumigationService->find('first',array('conditions' => array('FumigationService.id' => $fumigationId)));
                            $dosis = $this->request->data['RemittancesCaffeeHasFumigationService']['dosis'];
                            $quimico = $this->request->data['RemittancesCaffeeHasFumigationService']['quimico'];
                            $volumenRemittancesCaffee = $this->request->data['RemittancesCaffeeHasFumigationService']['volumen'];
                            $horas_elevador = $this->request->data['RemittancesCaffeeHasFumigationService']['hour_elevator'];
                            $weightCaffee = $this->request->data['RemittancesCaffeeHasFumigationService']['weight'];
                            $cta_poison_tablet = ($volumenRemittancesCaffee*$dosis);
                            $cta_bandejas_poison = ($cta_poison_tablet/30);
                            $ppm_teorico = ($dosis * 659);
                            if ($this->FumigationService->updateAll(
                                    array(
                                        'FumigationService.ppm_teory'=>$ppm_teorico,
                                        'FumigationService.hour_elevator'=>$horas_elevador,
                                        'FumigationService.bandejas_used'=>$cta_bandejas_poison,
                                        'FumigationService.quantity_poison_used'=>$cta_poison_tablet,
                                        'FumigationService.volumen_fumigated'=> $volumenRemittancesCaffee,
                                        'FumigationService.qta_bags'=> $fumigationService['FumigationService']['qta_bags']+$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'],
                                        'FumigationService.dosis'=> $dosis,
                                        'FumigationService.product_fumigation'=> $db->value($quimico,'string'),
                                        'FumigationService.weight_caffee' => $weightCaffee),
                                    array('FumigationService.id' => $fumigationId))) {
                                    $this->Flash->success(__('Se actualizo los datos de la fumigación'));
                                    return $this->redirect(array('controller' => 'FumigationServices','action' => 'index'));
                            }
                        }
		}
                $this->set('idFumigation',$fumigationId);
                $options = array('conditions' => array('RemittancesCaffeeHasFumigationService.fumigation_services_id' => $fumigationId));
                $RemittancesCaffeeHasFumigationServiceind = $this->RemittancesCaffeeHasFumigationService->find('all',$options);
                $this->set('remittancesCaffeeHasFumigationService',$RemittancesCaffeeHasFumigationServiceind);
                $fumigationService = $this->FumigationService->find('first',array('conditions' => array('FumigationService.id' => $fumigationId)));
                $this->set('remittancesCaffees', 
                        $this->paginate('RemittancesCaffee', 
                                array(
                                    'RemittancesCaffee.client_id' => $fumigationService['FumigationService']['exporter_id'],
                                    'RemittancesCaffee.is_active' => '1'))
                        );
                $this->set('client',$this->Client->find('first',array('conditions'=>array('Client.id'=>$fumigationService['FumigationService']['exporter_id']))));
                $this->set('fumigationService',$fumigationService);
                $this->set('orderServiceId',$orderId);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
                if(!$this->Session->read('User.id')){
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                }
                $this->layout = 'colaborador';
                $this->loadModel('FumigationService');
                $this->loadModel('RemittancesCaffee');
                $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
                $idFumigation = isset($this->request->query['fumigation_services_id']) ? $this->request->query['fumigation_services_id']: null;
                $idRemittancesCaffee = isset($this->request->query['remittances_caffee_id']) ? $this->request->query['remittances_caffee_id']: null;
                $remittancesCaffee = $this->RemittancesCaffee->find('first',array('conditions' => array('RemittancesCaffee.id' => $idRemittancesCaffee)));
                $fumigationService = $this->FumigationService->find('first',array('conditions' => array('FumigationService.id' => $idFumigation)));
                $dosis = $fumigationService['FumigationService']['dosis'];
                $volumenRemittancesCaffee = $fumigationService['FumigationService']['volumen_fumigated'];
                $volumenRemittancesCaffee -= ($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']/$remittancesCaffee['UnitsCaffee']['quantity'])*(3.5625);
                $weightCaffee = $fumigationService['FumigationService']['weight_caffee'];
                $weightCaffee -= $remittancesCaffee['RemittancesCaffee']['total_weight_net_real'];
                $cta_poison_tablet = ($volumenRemittancesCaffee*$dosis);
                $cta_bandejas_poison = ($cta_poison_tablet/30);
                $ppm_teorico = ($dosis * 659);
                if($this->RemittancesCaffeeHasFumigationService->deleteAll([
                    'RemittancesCaffeeHasFumigationService.remittances_caffee_id' => $idRemittancesCaffee,
                    'RemittancesCaffeeHasFumigationService.fumigation_services_id' => $idFumigation],false)){
                $this->FumigationService->updateAll(
                        array(
                            'FumigationService.ppm_teory'=>$ppm_teorico,
                            'FumigationService.bandejas_used'=>$cta_bandejas_poison,
                            'FumigationService.quantity_poison_used'=>$cta_poison_tablet,
                            'FumigationService.volumen_fumigated'=> $volumenRemittancesCaffee,
                            'FumigationService.qta_bags'=> $fumigationService['FumigationService']['qta_bags']-$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'],
                            'FumigationService.dosis'=> $dosis,
                            'FumigationService.weight_caffee' => $weightCaffee),
                        array('FumigationService.id' => $idFumigation));
                        $this->RemittancesCaffeeHasNoveltysCaffee->deleteAll(array(
                            'RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id' => $idRemittancesCaffee,
                            'RemittancesCaffeeHasNoveltysCaffee.noveltys_caffee_id' => 1), false);
                        $this->RemittancesCaffeeHasFumigationService->save($this->request->data);
                        $this->Flash->success(__('Se quito el lote de la fumigación con exito'));
                        return $this->redirect(array('controller' => 'FumigationServices','action' => 'index'));
                    }
    }

    /**
     * updateFumigation method
     *
     * @throws NotFoundException
         * @param string $id
         * @return void
     */
    function updateFumigation($remittancesCaffee = null, $idFumigation = null){
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->autoRender = false;
        $this->loadModel('FumigationServices');

        $fumigationServices = $this->FumigationServices->find('first',
        ['conditions'=>['FumigationServices.id'=>$idFumigation]]);

        $numPallets = ceil($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']*$remittancesCaffee['UnitsCaffee']['quantity']/1750);
        $volumenCoffee = (1.25*1.9*1.15)*$numPallets;
        $tabletPoison = $volumenCoffee*$fumigationServices['FumigationServices']['dosis'];
        $numberRacks = $tabletPoison/30;
        $qta_bags = $remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'];
        $ppmTheoric = $fumigationServices['FumigationServices']['dosis']*659;

        
        $fumigationServices['FumigationServices']['weight_caffee']=($fumigationServices['FumigationServices']['weight_caffee']+$remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in']*$remittancesCaffee['UnitsCaffee']['quantity']);
        $fumigationServices['FumigationServices']['bandejas_used']=($fumigationServices['FumigationServices']['bandejas_used']+$numberRacks);
        $fumigationServices['FumigationServices']['quantity_poison_used']=($fumigationServices['FumigationServices']['quantity_poison_used']+$tabletPoison);
        $fumigationServices['FumigationServices']['ppm_teory']=($fumigationServices['FumigationServices']['ppm_teory']+$ppmTheoric);
        $fumigationServices['FumigationServices']['volumen_fumigated']=($fumigationServices['FumigationServices']['volumen_fumigated']+$volumenCoffee);
        $fumigationServices['FumigationServices']['qta_bags']=($fumigationServices['FumigationServices']['qta_bags']+$qta_bags);
        $this->FumigationServices->save($fumigationServices);
    }
    

    /**
         * addCoffeeFumigation method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
     */
    public function addCoffeeFumigation($request = null){
        if(!$this->Session){
            $this->beforeFilter();
        };
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->autoRender = false;
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('FumigationServices');
        $this->loadModel('RemittancesCaffeeHasFumigationServices');
        
        $dataArray = explode("-",$request);

        $remittancesCaffeeHasFumigationService = $this->RemittancesCaffeeHasFumigationServices->create();
        $fumigationServices = $this->FumigationServices->find('first',
        ['conditions'=>['FumigationServices.id'=>$dataArray[0]]]);

        if($fumigationServices && sizeof($dataArray) == 3 && $dataArray[0] && $dataArray[1] && $dataArray[2]){

	    $dataCoffeeDownload = $this->RemittancesCaffeeHasFumigationServices->find('first',['conditions'=>
            ['RemittancesCaffeeHasFumigationServices.remittances_caffee_id'=>$dataArray[1]]]);

            if($dataCoffeeDownload){
                $this->RemittancesCaffeeHasFumigationServices->deleteAll(['id'=>$dataCoffeeDownload['RemittancesCaffeeHasFumigationServices']['id']]);
            }

            $data= [
                'fumigation_services_id'=>$dataArray[0],
                'remittances_caffee_id'=>$dataArray[1],
                'qta_coffee'=>$dataArray[2],
                'reg_date'=>date('Y-m-d h:i:s')
            ];
            
            if($this->RemittancesCaffeeHasFumigationServices->save($data)){
    
                $fumigationServices['FumigationServices']['weight_caffee']=0;
                $fumigationServices['FumigationServices']['bandejas_used']=0;
                $fumigationServices['FumigationServices']['quantity_poison_used']=0;
                $fumigationServices['FumigationServices']['ppm_teory']=0;
                $fumigationServices['FumigationServices']['qta_bags']=0;
                $fumigationServices['FumigationServices']['volumen_fumigated']=0;
                $this->FumigationServices->save($fumigationServices);
    
                $dataRemittancesCaffeeHasFumigationServices = $this->RemittancesCaffeeHasFumigationServices
                ->find('all',
                ['conditions'=>
                    ['RemittancesCaffeeHasFumigationServices.fumigation_services_id'=>$dataArray[0]]]);
    
                foreach($dataRemittancesCaffeeHasFumigationServices as $value){
                    $remittancesCaffee = $this->RemittancesCaffee->find('first',
                        ['conditions'=>['RemittancesCaffee.id'=>$value['RemittancesCaffeeHasFumigationServices']['remittances_caffee_id']]]);
                        $this->updateFumigation($remittancesCaffee, $dataArray[0]);
                }
    
            }

        }

    }
}
