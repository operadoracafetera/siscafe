<?php
App::uses('AppController', 'Controller');
/**
 * ServicePackageHasItemsServices Controller
 *
 * @property ServicePackageHasItemsService $ServicePackageHasItemsService
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ServicePackageHasItemsServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');


/**
 * add method
 *
 * @return void
 */
	public function add($idPackage = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
            if ($this->request->is('post')) {
                    $this->ServicePackageHasItemsService->create();
                    $this->request->data['ServicePackageHasItemsService']['service_package_id'] = $idPackage;
                    //debug($this->request->data['ServicePackageHasItemsService']);exit;
                    if ($this->ServicePackageHasItemsService->save($this->request->data)) {
                            $this->Flash->success(__('El item de servicio fue registrado.'));
                            return $this->redirect(array('action' => 'add',$idPackage));
                    } else {
                            $this->Flash->error(__('The service package has items service could not be saved. Please, try again.'));
                    }
            }
            //debug($this->ServicePackageHasItemsService);exit;
            $this->loadModel('ServicePackage');
            $servicePackage = $this->ServicePackage->find('first',array('conditions' => array('ServicePackage.id = ' => $idPackage)));
            //debug($servicePack);exit;
            $servicePackageHasItemsServices = $this->ServicePackageHasItemsService->find('all',array('conditions' => array('ServicePackage.id = ' => $idPackage)));
            $this->set(compact('servicePackage',$servicePackage));
            $this->set(compact('servicePackageHasItemsServices',$servicePackageHasItemsServices));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
                $service_package_id = isset($this->request->query['service_package_id']) ? $this->request->query['service_package_id']: null;
		$items_services_id = isset($this->request->query['items_services_id']) ? $this->request->query['items_services_id']: null;
		if ($this->ServicePackageHasItemsService->deleteAll(['ServicePackageHasItemsService.service_package_id' => $service_package_id,
		'ServicePackageHasItemsService.items_services_id' => $items_services_id],false)) {
			$this->Flash->success(__('El item de servicio ha sido borrado.'));
		} else {
			$this->Flash->error(__('Se genero un error. Por favor intente nuevamente.'));
		}
		return $this->redirect(array('action' => 'add',$service_package_id));
	}
}
