<?php
App::uses('AppController', 'Controller');
App::import('Vendor','ShippingUtil');
App::uses('HttpSocket', 'Network/Http');
App::uses('CakeEmail', 'Network/Email');
require_once("../Vendor/aws/aws-autoloader.php");
 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/**
 * CoffeeSamples Controller
 *
 * @property CoffeeSample $CoffeeSample
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class CoffeeSamplesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index($id= null) {
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                $this->loadModel('TypeSampleCoffe');
                $this->loadModel('User');
                $this->loadModel('RemittancesCaffee');
                if($this->Session->read('User.profiles_id')==9){
		    $this->CoffeeSample->recursive = 3;
                    $this->layout = 'invitado';
                    $this->Paginator->settings = array(
                    'limit' => 30,
		    'joins' => array(
        	        array(
            		'table' => 'sample_coffee_has_type_sample',
            		'alias' => 'SampleHasType',
            		'type' => 'INNER',
            		'conditions' => array(
                 	   'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  )
        		)
    		    ),
		    'conditions' => array(
        		'SampleHasType.type_sample_coffee_id' => 2
    		    ),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));
                }
                else{
   	 	  $this->layout = 'colaborador';
                    $this->Paginator->settings = array(
                    'limit' => 30,
                    'conditions' => array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));
                }
                
		
		
		if($id == 1){	
	    
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => 2),
                        	array('CoffeeSample.status'=>"REGISTRADA")
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));


		  }
		  else if($id == 2){
		    
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => 2),
                        	array('or'=>array('CoffeeSample.status'=>"COMPLETADA",'CoffeeSample.status'=>"CERRADA"))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 3){
			
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => array(1,4,5,6,7,8,9,10,11,13,14,15,16)),
				array('CoffeeSample.get_sample' => 1),
                        	array('CoffeeSample.status'=>"REGISTRADA")
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 4){

			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => array(1,4,5,6,7,8,9,10,11,13,14,15,16)),
				array('CoffeeSample.get_sample' => 1),
                        	array('or'=>array(array('CoffeeSample.status'=>"COMPLETADA"),array('CoffeeSample.status'=>"CERRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 5){

			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => 3),
                        	array('CoffeeSample.status'=>"REGISTRADA")
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 6){
		  
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => 3),
                        	array('OR'=>array(array('CoffeeSample.status'=>"COMPLETADA"),array('CoffeeSample.status'=>"CERRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 7){
		  
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
				array('SampleHasType.type_sample_coffee_id' => 19),
                        	array('OR'=>array(array('CoffeeSample.status'=>"REGISTRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 8){
		  
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
				array('SampleHasType.type_sample_coffee_id' => 19),
                        	array('OR'=>array(array('CoffeeSample.status'=>"COMPLETADA"),array('CoffeeSample.status'=>"CERRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 9){
		  
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
				array('SampleHasType.type_sample_coffee_id' => 12),
				array('CoffeeSample.get_sample' => 1),
                        	array('OR'=>array(array('CoffeeSample.status'=>"REGISTRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }
		  else if($id == 10){
		  
			$this->CoffeeSample->recursive = 3;
                    	$this->layout = 'colaborador';
                    	$this->Paginator->settings = array(
                    	'limit' => 30,
		    	'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   		'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  		)
        			)
    		    	),
		    	'conditions' => array('and'=>
                        	array('CoffeeSample.operation_center'=>$this->Session->read('User.jetty')),
				array('SampleHasType.type_sample_coffee_id' => 12),
                        	array('OR'=>array(array('CoffeeSample.status'=>"COMPLETADA"),array('CoffeeSample.status'=>"CERRADA")))
    		    	),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));

		  }


                $this->CoffeeSample->recursive = 2;
                $dataCoffeeSamples = $this->Paginator->paginate();
                $dataCoffeeSamplesResult = array();
                foreach($dataCoffeeSamples as $dataCoffeeSample){
                    $refLotCoffeeSamples = explode("-",$dataCoffeeSample['CoffeeSample']['lot_coffee_ref']);
                    $fieldLotCoffee = $refLotCoffeeSamples[1];
                    $fieldExporterId = $dataCoffeeSample['CoffeeSample']['client_id'];
                    $fieldYearCurrent = date('Y');
                    $this->RemittancesCaffee->recursive = 2;
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('all',array('conditions' => 
                        array('and'=>array(
                            'RemittancesCaffee.lot_caffee'=>$fieldLotCoffee,
                            'RemittancesCaffee.client_id'=>$fieldExporterId,
			    'RemittancesCaffee.created_date >' => '2020-01-01')
                        
                    )));
                    array_push($dataCoffeeSample, array('remittancesCaffee'=>$dataRemittancesCaffee));
                    array_push($dataCoffeeSamplesResult, $dataCoffeeSample);
                }
		$this->set('coffeeSamples', $dataCoffeeSamplesResult);
                $this->set('index',$id);
                $this->set('option_index',$id);
                $this->set('userCurrent', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
                $this->set('typeSampleCoffes',$typeSampleCoffes);
	}
        
        
        /**
 * invoice method
 *
 * @return void
 */
	public function invoice($id=1) {
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                $this->loadModel('TypeSampleCoffe');
                $this->loadModel('User');
                if($this->Session->read('User.profiles_id')==9){
                        $this->layout = 'invitado';
                        $this->Paginator->settings = array(
                    'limit' => 30,
		    'joins' => array(
        	        array(
            		'table' => 'sample_coffee_has_type_sample',
            		'alias' => 'SampleHasType',
            		'type' => 'INNER',
            		'conditions' => array(
                 	   'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  )
        		)
    		    ),
		    'conditions' => array(
        		'SampleHasType.type_sample_coffee_id' => 2
    		    ),
                    'order' => array('CoffeeSample.created_date' => 'DESC'));
                }
                else{
                        $this->layout = 'colaborador';
                        $this->Paginator->settings = array(
                        'limit' => 15,
                        'order' => array('CoffeeSample.created_date' => 'DESC')
                    );
                }
                $this->CoffeeSample->recursive = 2;
		$this->set('coffeeSamples', $this->Paginator->paginate());
                $this->set('index',$id);
                $this->set('option_index',$id);
                $this->set('userCurrent', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
                $this->set('typeSampleCoffes',$typeSampleCoffes);
	}
        
        
        /**
        * getTrackingOie method
        *
        * @return void
        */
       public function getTrackingRequest($idRequest = null){
           if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;

           $bucket = 'requestcustomercoffee';
           $keyname = $idRequest.".pdf";

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-2',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__('No se encontro el archivo!'));
           }

       }
       
       /**
        * getTrackingOie method
        *
        * @return void
        */
       public function getTracking($idRequest = null){
           if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;

           $bucket = 'requestcustomercoffee';
           $keyname = "R-".$idRequest.".zip";

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-2',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__('No se encontro el archivo!'));
           }
       }
       
       /**
        * getTrackingOie method
        *
        * @return void
        */
       public function getTrackingEvidence($idRequest = null){
           if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;

           $bucket = 'requestcustomercoffee';
           $keyname = "E-".$idRequest.".jpg";

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-2',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__('No se encontro el archivo!'));
           }
       }
       
       
       /**
         * findCoffeeSample method
         *
         * @throws NotFoundException
         * @param string $lotCoffee
         * @return void
         */
       public function search(){
           if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            if($this->Session->read('User.profiles_id')==9){
                $this->layout = 'invitado';
            }
            else{
                $this->layout = 'colaborador';
            }
       }
       
       /**
         * findCoffeeSample method
         *
         * @throws NotFoundException
         * @param string $lotCoffee
         * @return void
         */
       public function findCoffeeSample($lotCoffee = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('TypeSampleCoffe');
            $this->autoRender=false;
            $dataSampledataCoffee = array();
            $dataTypeCoffee = null;
            if($this->Session->read('User.profiles_id') == 1){
                $dataCoffeeSample = $this->CoffeeSample->find('all',
                        array('conditions' =>
							array(
			    'and'=>array(
				'CoffeeSample.lot_coffee_ref'=>$lotCoffee,
				'CoffeeSample.get_sample' => true
				)
			  ))
						);
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
                foreach($dataCoffeeSample as $dataSample){
                    foreach($dataSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                        $key = $typeSampleCoffe['type_sample_coffee_id'];
                        $dataTypeCoffee .= $typeSampleCoffes[$key]." ";
                    }
                    array_push($dataSample, $dataTypeCoffee);
                    array_push($dataSampledataCoffee, $dataSample);
                }
                return json_encode($dataSampledataCoffee);
            }
            else if($this->Session->read('User.profiles_id') == 9){
                $dataCoffeeSample = $this->CoffeeSample->find('all',
                        array(
			  'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   	   'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  	 )
        			)
    		    	  ),
			  'conditions' =>array(
			    'and'=>array(
				'CoffeeSample.lot_coffee_ref'=>$lotCoffee,
				'CoffeeSample.get_sample'=>1,
				'SampleHasType.type_sample_coffee_id' => 2
				)
			  )
			));
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
                foreach($dataCoffeeSample as $dataSample){
                    foreach($dataSample['SampleCoffeeHasTypeSample'] as $typeSampleCoffe){
                        $key = $typeSampleCoffe['type_sample_coffee_id'];
                        $dataTypeCoffee .= $typeSampleCoffes[$key]." ";
                    }
                    array_push($dataSample, $dataTypeCoffee);
                    array_push($dataSampledataCoffee, $dataSample);
                }
                return json_encode($dataSampledataCoffee);
            }
            return json_encode($dataCoffeeSample);
       }
       
       /**
         * findCoffeeSample method
         *
         * @throws NotFoundException
         * @param string $lotCoffee
         * @return void
         */
       public function findCoffeeSample2($lotCoffee = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->autoRender=false;
            $this->loadModel('TypeSampleCoffe');       
            $dataCoffeeSample = $this->CoffeeSample->find('all',
                        array(
                          'fields' => array(
                              'CoffeeSample.id','CoffeeSample.get_sample','CoffeeSample.lot_coffee_ref',
                              'CoffeeSample.status','CoffeeSample.qta_bag','CoffeeSample.created_date',
                              'CoffeeSample.operation_center','TypeSampleCoffee.name'
                          ),
			  'joins' => array(
        	        	array(
            			'table' => 'sample_coffee_has_type_sample',
            			'alias' => 'SampleHasType',
            			'type' => 'INNER',
            			'conditions' => array(
                 	   	   'SampleHasType.sample_coffee_id = CoffeeSample.id'
            		  	 )
        			),
                                array(
                                'table' => 'type_sample_coffe',
                                'alias' => 'TypeSampleCoffee',
                                'type' => 'INNER',
                                'conditions' => array(
                                   'SampleHasType.type_sample_coffee_id = TypeSampleCoffee.id'
                                  )
                                )
    		    	  ),
			  'conditions' =>array(
			    'and'=>array(
				'CoffeeSample.lot_coffee_ref'=>$lotCoffee,
                                'CoffeeSample.status'=>"REGISTRADA",
				'TypeSampleCoffee.id' => array(1,2,3,4,5,6,7,8,9,11,12,19)
				)
			  )
			));
            return json_encode($dataCoffeeSample);
       }
       
       
       /**
         * delete method
         *
         * @throws NotFoundException
         * @param string $option
         * @return void
         */
       public function deleteItem($option = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->loadModel('SampleRemmitance');
            $this->layout = 'colaborador';
            $this->autoRender=false;
            $parameter = explode("-", $option);
            $dataResult = $this->SampleRemmitance->deleteAll(
                    array('SampleRemmitance.coffee_sample_id'=>$parameter[0],
                          'SampleRemmitance.remittances_caffee_id'=>$parameter[1]),false);
            if($dataResult){
                $this->Flash->success(__('Id descargue retirado correctamente!'));
                return $this->redirect(array('action' => 'edit',$parameter[0])); 
            }
       }

        /**
         * view method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            
            if($this->Session->read('User.profiles_id')==9){
                $this->layout = 'invitado';
            }
            else{
                $this->layout = 'colaborador';
            }
            $this->loadModel('SampleRemmitance');
            $this->loadModel('User');
            $this->loadModel('TypeSampleCoffe');
            if (!$this->CoffeeSample->exists($id)) {
                    throw new NotFoundException(__('Invalid coffee sample'));
            }
            $options = array('conditions' => array('CoffeeSample.' . $this->CoffeeSample->primaryKey => $id));
            $this->SampleRemmitance->recursive = 2;
            $dataSampleRemmitance = $this->SampleRemmitance->find('all',array('conditions'=>array('SampleRemmitance.coffee_sample_id'=>$id)));
            $this->set('dataSampleRemmitances',$dataSampleRemmitance);
            $this->set('coffeeSample', $this->CoffeeSample->find('first', $options));
            $this->set('userCurrent', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
            $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
            $this->set('typeSampleCoffes',$typeSampleCoffes);
	}

        /**
         * add method
         *
         * @return void
         */
	public function add($st_lot_coffee=null) {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
                debug($st_lot_coffee);exit;
                $parameters = explode("-", $st_lot_coffee);
                $lot_coffee = $parameters[0];
                $client_id = $parameters[1];
                $this->loadModel('ServicePackage');
                $this->loadModel('RemittancesCaffee');
                $this->loadModel('SampleRemmitances');
                $sample_coffee_id = $parameters[2];
                
                $remittancesCaffees = $this->RemittancesCaffee->find('all',
                        array('conditions' => array('AND' => 
                            array(
                                'RemittancesCaffee.lot_caffee' =>$lot_coffee,
                                'RemittancesCaffee.client_id' =>$client_id,
                            ))));
		if ($this->request->is('post')) {
                        $db = $this->CoffeeSample->getDataSource();
                        if($sample_coffee_id != 0){
                            $arraySampleRemmitances = $this->SampleRemmitances->find('all',array('conditions' => array('AND' => 
                            array(
                                'SampleRemmitances.coffee_sample_id' =>$sample_coffee_id
                            ))));
                            if (empty($arraySampleRemmitances)){
                                $i=0;
                                foreach($remittancesCaffees as $remittancesCaffee){
                                   $this->SampleRemmitances->create();
                                   $sampleRemmitances = array('SampleRemmitances'=>array(
                                       'coffee_sample_id'=>$sample_coffee_id,
                                       'remittances_caffee_id'=>$remittancesCaffees[$i]['RemittancesCaffee']['id']
                                   ));
                                   //debug($sampleRemmitances);
                                   $this->SampleRemmitances->save($sampleRemmitances);
                                   $i++;
                                }
                                //exit;
                                $this->CoffeeSample->updateAll(array('CoffeeSample.status'=>$db->value('COMPLETADA','string'),'CoffeeSample.end_date'=>$db->value(date("Y-m-d h:i:s"),'string')),array('CoffeeSample.id'=>$sample_coffee_id));
                                $this->Flash->success(__('Toma de muestra completada'));
                                return $this->redirect(array('action' => 'view',$sample_coffee_id."-V"));
                            }
                            else{
                                $this->CoffeeSample->updateAll(array('CoffeeSample.status'=>$db->value('COMPLETADA','string'),'CoffeeSample.end_date'=>$db->value(date("Y-m-d h:i:s"),'string')),array('CoffeeSample.id'=>$sample_coffee_id));
                                return $this->redirect(array('action' => 'view',$sample_coffee_id."-V"));
                            }
                        }
                        else{
                            $this->CoffeeSample->create();
                            $this->request->data['CoffeeSample']['register_users_id']=$this->Session->read('User.id');
                            $this->request->data['CoffeeSample']['status']='ASIGNADA';
                            $this->request->data['CoffeeSample']['client_id']=$client_id;
                            $this->request->data['CoffeeSample']['created_date']=date('Y-m-d h:i:s');
                            $file_seal1_path = $this->request->data['CoffeeSample']['img_request_sample'];
                            $this->request->data['CoffeeSample']['img_request_sample'] = '';
                            $this->request->data['CoffeeSample']['lot_coffee_ref']= substr($st_lot_coffee, 0, strlen($st_lot_coffee)-2);
                            $db = $this->CoffeeSample->getDataSource();
                            if ($this->CoffeeSample->save($this->request->data)) {
                                $pathDirectory = 'data/samplesCoffee/' . $this->CoffeeSample->id . "/" . $this->request->data['CoffeeSample']['lot_coffee_ref'];
                                if (!is_dir(WWW_ROOT . 'img/' . $pathDirectory)) {
                                    mkdir(WWW_ROOT . 'img/' . $pathDirectory, 0777, true);
                                }
                                $pathSeal1 = $pathDirectory . "/" . $file_seal1_path['name'];
                                $resultSeal1 = move_uploaded_file($file_seal1_path['tmp_name'], WWW_ROOT . 'img/' . $pathSeal1);
                                $i=0;
                                $qtaBags=0;
                                foreach($remittancesCaffees as $remittancesCaffee){
                                   $this->SampleRemmitances->create();
                                   $sampleRemmitances = array('SampleRemmitances'=>array(
                                       'coffee_sample_id'=>$this->CoffeeSample->id,
                                       'remittances_caffee_id'=>$remittancesCaffees[$i]['RemittancesCaffee']['id']
                                   ));
                                   $qtaBags+=$remittancesCaffees[$i]['RemittancesCaffee']['quantity_bag_in_store'];
                                   //debug($sampleRemmitances);
                                   $this->SampleRemmitances->save($sampleRemmitances);
                                   $i++;
                                }
                                $this->CoffeeSample->updateAll(array('CoffeeSample.img_request_sample'=>$db->value($pathSeal1, 'string'),'CoffeeSample.qta_bag'=>$qtaBags),array('CoffeeSample.id'=>$this->CoffeeSample->id));
                                $this->Flash->success(__('Se asigno correctamente la toma de muestra de café'));
                                return $this->redirect(array('action' => 'index',1));
                            }else {
                                $this->Flash->error(__('Problemas con el registros. Intentelo nuevamente'));
                            } 
                        }
		}
                $remittancesCaffeesIds = $this->RemittancesCaffee->find('list',
                        array('conditions' => array('AND' => 
                            array(
                                'RemittancesCaffee.lot_caffee' =>$lot_coffee,
                                'RemittancesCaffee.client_id' =>$client_id,
                            ))));
                if($sample_coffee_id != 0 && $remittancesCaffees == null){
                    $this->Flash->error(__('Lote de café No relacionado!. Por favor verificar información.'));
                    return $this->redirect(array('action' => 'view',$sample_coffee_id."-V")); 
                }
		$typeSampleCoffes = $this->CoffeeSample->TypeSampleCoffe->find('list');
                $coffeeSamples = $this->SampleRemmitances->find('all',array('conditions' => array('SampleRemmitances.remittances_caffee_id'=>$remittancesCaffeesIds)));
		$this->set(compact('typeSampleCoffes','sample_coffee_id','remittancesCaffees','coffeeSamples'));
                if(isset($parameters[3])){
                    $this->set('flag', $parameters[3]);
                }
                $this->set('servicePackages', $this->ServicePackage->find('list',array('conditions'=>array('ServicePackage.name_package LIKE'=> '%TOMA%'),'fields' => array('ServicePackage.name_package'))));
	}
        
        /**
         * add preassignment
         *
         * @return void
         */
	public function preassignment($option = null) {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                if($this->Session->read('User.profiles_id')==1){
                    $this->layout = 'colaborador';
                }
                else if($this->Session->read('User.profiles_id')==9){
                    $this->layout = 'invitado';
                }
                $dataFile = null;
                $this->loadModel('User');
                $this->loadModel('SampleRemmitance');
                $this->loadModel('TypeSampleCoffe');
                $this->loadModel('SampleCoffeeHasTypeSample');
		if ($this->request->is('post')) {
                    //debug($this->request->data);exit;
                    $this->CoffeeSample->create();
                    $this->request->data['CoffeeSample']['register_users_id']=$this->Session->read('User.id');
                    $this->request->data['CoffeeSample']['status']='REGISTRADA';
                    $this->request->data['CoffeeSample']['created_date']=date('Y-m-d H:i:s');
                    $this->request->data['CoffeeSample']['lot_coffee_ref']=$this->request->data['CoffeeSample']['lot'];
                    $dataFile = $this->request->data['CoffeeSample']['img_request_sample'];
                    $this->request->data['CoffeeSample']['img_request_sample'] = date('Y-m-d H:i:s');
                    if ($this->CoffeeSample->save($this->request->data)) {
                        
                        if(isset($this->request->data['dataRemittancesCaffee'])){
                            foreach($this->request->data['dataRemittancesCaffee'] as $dataRemittancesCoffee){
                                $this->SampleRemmitance->create();
                                $arrayData = array('coffee_sample_id'=>$this->CoffeeSample->id,'remittances_caffee_id'=>$dataRemittancesCoffee);
                                $this->SampleRemmitance->save($arrayData);
                            }
                        }
                        
                        foreach($this->request->data['CoffeeSample']['type_sample_coffe_id'] as $dataTypeSamples){
                                    $this->SampleCoffeeHasTypeSample->create();
                                    $arrayData = array('sample_coffee_id'=>$this->CoffeeSample->id,'type_sample_coffee_id'=>$dataTypeSamples);
                                    try{
                                        $this->SampleCoffeeHasTypeSample->save($arrayData);
                                    }
                                    catch(Exception $e){
                                        $dataMessage .= "Ya tiene asociado el tipo de muestra! ";
                                    }
                        }
                        
                        
                        $temp_file_location = $dataFile['tmp_name']; 
                        $bucket = 'requestcustomercoffee';
                        $keyname = $this->CoffeeSample->id.".pdf";
                        if(!empty($temp_file_location)){
                            $s3 = new S3Client([
                                'version' => 'latest',
                                'region'  => 'us-east-2',
                                'credentials' => [
                                'key'    => "AKIAJZHFB4GIIF3FDWMA",
                                'secret' => "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",]]);
                            try
                            {
                                $result = $s3->putObject([
                                        'Bucket' => $bucket,
                                        'Key'    => $keyname,
                                        'SourceFile' => $temp_file_location
                                ]);
                            } catch (S3Exception $e) {
                                $this->Flash->error(__($e->getMessage()));
                            }
                        }
                        
                        $this->Flash->success(__('Solicitud registra exitosamente'));
                        return $this->redirect(array('action' => 'view',$this->CoffeeSample->id));
                        
                    }
                }
                $dataCoffeeSample = array();
                $dataSampleRemmitances = array();
                $this->set(compact('dataSampleRemmitances'));
                $this->set(compact('dataCoffeeSample'));
		$typeSampleCoffes = $this->TypeSampleCoffe->find('list');
		$this->set(compact('typeSampleCoffes'));
                $this->set('userCurrent', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
	}
        
        /**
         * complete method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        
        public function complete($id = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('User');
            $coffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
            if($coffeeSample['CoffeeSample']['status']=="COMPLETADA"){
                $dataResult1 = $this->CoffeeSample->updateAll(
                            array(
                                'status' => "'CERRADA'",
                                'end_date' => "'".date('Y-m-d H:i:s')."'",
                                'send_date'=> "'".date('Y-m-d H:i:s')."'"),
                            array('CoffeeSample.id'=>$id));
                $user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id'))));
                $arrayEmail1 = explode(";",$coffeeSample['CoffeeSample']['email_alt']);
                $arrayEmail2 = explode(";",$user['User']['email']);
                for($i=1;$i<3;$i++){
                    if($i == 1){
                        foreach($arrayEmail1 as $emails1){
                            $Email = new CakeEmail();
                            $Email->config('gmail');
                            $Email->from(array('asistentetic@operadoracafetera.com' => 'SISCAFE'))
                                ->to($emails1)
                                ->subject('Solicitud CERRADA fin del proceso (Muestre, Trazabilidad) id #'.$id)
                                ->send('En el siguiente mensaje, se esta comunicando que se ha CERRADO la solicitud del lote '.$coffeeSample['CoffeeSample']['lot_coffee_ref'].'. Por favor revisar.\n\n'
                                        . 'Atentamente,\n'
                                        . 'Operaciones COPCSA');
                        }
                    }
                    else if($i == 2){
                        foreach($arrayEmail2 as $emails2){
                            $Email = new CakeEmail();
                            $Email->config('gmail');
                            $Email->from(array('asistentetic@operadoracafetera.com' => 'SISCAFE'))
                                ->to($emails2)
                                ->subject('Solicitud CERRADA fin del proceso (Muestre, Trazabilidad) id #'.$this->CoffeeSample->id)
                                ->send('En el siguiente mensaje, se esta comunicando que se ha CERRADO la solicitud del lote '.$coffeeSample['CoffeeSample']['lot_coffee_ref'].'. Por favor revisar.'
                                        . 'Atentamente,\n'
                                        . 'Operaciones COPCSA');
                        }
                    }
                }
                $dataMessage = "Solicitud CERRADA fin del proceso\n";
                $this->Flash->success(__($dataMessage));
                return $this->redirect(array('action' => 'index',$this->Session->read('User.id'))); 
            }
        }
        
        
        /**
         * make method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        
        public function make($id = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
                $this->loadModel('User');
                $this->loadModel('SampleRemmitance');
                $this->loadModel('User');
                $this->loadModel('TypeSampleCoffe');
		if ($this->request->is('post')) {
                    $dataFile = null;
                    //debug($this->request->data);exit;
                    $db = $this->CoffeeSample->getDataSource();
                    $dataUploadResponse = null;
                    $dataUploadEvidence = null;
                    if(!empty($this->request->data['CoffeeSample']['doc_sample']['tmp_name'])){
                        $dataFile = $this->request->data['CoffeeSample']['doc_sample'];
                        unset($this->request->data['CoffeeSample']['doc_sample']);
                        $bucket = 'requestcustomercoffee';
                        $keyname = "R-".$id.".zip";
                        $temp_file_location = $dataFile['tmp_name'];
                        $s3 = new S3Client([
                            'version' => 'latest',
                            'region'  => 'us-east-2',
                            'credentials' => [
                            'key'    => "AKIAJZHFB4GIIF3FDWMA",
                            'secret' => "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",]]);

                            $result = $s3->putObject([
                                    'Bucket' => $bucket,
                                    'Key'    => $keyname,
                                    'SourceFile' => $temp_file_location
                            ]);
                            if($result){
                              $dataUploadResponse = "'".date('Y-m-d H:i:s')."'";
                            }
                    }
                    else if(!empty($this->request->data['CoffeeSample']['img_response_sample']['tmp_name'])){
                        $dataFile = $this->request->data['CoffeeSample']['img_response_sample'];
                        unset($this->request->data['CoffeeSample']['img_response_sample']);
                        $bucket = 'requestcustomercoffee';
                        $keyname = "E-".$id.".jpg";
                        $temp_file_location = $dataFile['tmp_name'];
                        $s3 = new S3Client([
                            'version' => 'latest',
                            'region'  => 'us-east-2',
                            'credentials' => [
                            'key'    => "AKIAJZHFB4GIIF3FDWMA",
                            'secret' => "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",]]);

                            $result = $s3->putObject([
                                    'Bucket' => $bucket,
                                    'Key'    => $keyname,
                                    'SourceFile' => $temp_file_location
                            ]);
                            if($result){
                              $dataUploadEvidence = "'".date('Y-m-d H:i:s')."'";
                            }
                    }

		    $dataCoffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
                    
		    if(!$dataCoffeeSample['CoffeeSample']['end_date']){

			$dataResult1 = $this->CoffeeSample->updateAll(
                        array(
                            'sampler_users_id' => $this->request->data['CoffeeSample']['user_sampler'],
                            'user_manager' => $this->Session->read('User.id'),
                            'email_alt'=>"'".$this->request->data['CoffeeSample']['email_alt']."'",
			    'end_date' => "'".date('Y-m-d H:i:s')."'",
                            'status'=>"'COMPLETADA'",
                            'img_response_sample'=> $dataUploadEvidence,
                            'img_finish_sample'=>$dataUploadResponse,
                            'request_reponse' => $db->value($this->request->data['CoffeeSample']['request_reponse'], 'string'),
                            'value_bill' => $this->request->data['CoffeeSample']['value_bill'],
                            'sample_packing' => $db->value($this->request->data['CoffeeSample']['sample_packing'], 'string'),
                            'supply_company' => $db->value($this->request->data['CoffeeSample']['supply_company'], 'string'),
                            'tracking_supply' => $this->request->data['CoffeeSample']['tracking_supply']),                            
                        array('CoffeeSample.id'=>$id));
			
                    $user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id'))));
		    $dataCoffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
		    
                    $arrayEmail1 = explode(";",$dataCoffeeSample['CoffeeSample']['email_alt']);
                    $arrayEmail2 = explode(";",$user['User']['email']);
                    for($i=1;$i<3;$i++){
                        if($i == 1){
                            foreach($arrayEmail1 as $emails1){
                                $Email = new CakeEmail();
                                $Email->config('gmail');
                                $Email->from(array('asistentetic@operadoracafetera.com' => 'SISCAFE'))
                                    ->to($emails1)
                                    ->subject('Solicitud COMPLETADA la solicitud registrada con el ID #'.$id)
                                    ->send('En el siguiente mensaje, se esta comunicando que se ha COMPLETADO la solicitud del lote '.$dataCoffeeSample['CoffeeSample']['lot_coffee_ref'].'. Por favor revisar.\n\n'
                                            . 'Atentamente,\n'
                                            . 'Operaciones COPCSA');
                            }
                        }
                        else if($i == 2){
                            foreach($arrayEmail2 as $emails2){
                                $Email = new CakeEmail();
                                $Email->config('gmail');
                                $Email->from(array('asistentetic@operadoracafetera.com' => 'SISCAFE'))
                                    ->to($emails2)
                                    ->subject('Solicitud COMPLETADA la solicitud registrada con el ID #'.$id)
                                    ->send('En el siguiente mensaje, se esta comunicando que se ha COMPLETADO la solicitud del lote '.$dataCoffeeSample['CoffeeSample']['lot_coffee_ref'].'. Por favor revisar.\n\n'
                                            . 'Atentamente,\n'
                                            . 'Operaciones COPCSA');
                            }
                        }
                    }
		    }
		    else{
			$dataResult1 = $this->CoffeeSample->updateAll(
                        array(
                            'sampler_users_id' => $this->request->data['CoffeeSample']['user_sampler'],
                            'user_manager' => $this->Session->read('User.id'),
                            'email_alt'=>"'".$this->request->data['CoffeeSample']['email_alt']."'",
                            'status'=>"'COMPLETADA'",
                            'img_response_sample'=> $dataUploadEvidence,
                            'img_finish_sample'=>$dataUploadResponse,
                            'request_reponse' => $db->value($this->request->data['CoffeeSample']['request_reponse'], 'string'),
                            'value_bill' => $this->request->data['CoffeeSample']['value_bill'],
                            'sample_packing' => $db->value($this->request->data['CoffeeSample']['sample_packing'], 'string'),
                            'supply_company' => $db->value($this->request->data['CoffeeSample']['supply_company'], 'string'),
                            'tracking_supply' => $this->request->data['CoffeeSample']['tracking_supply']),                            
                        array('CoffeeSample.id'=>$id));
			}
                    
                    $dataMessage = "Solicitud ".$id." COMPLETADA exitosamente\n";
                    $this->Flash->success(__($dataMessage));
                    return $this->redirect(array('action' => 'index',1)); 
                }
                $this->SampleRemmitance->recursive = 2;
                $dataCoffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
                $dataSampleRemmitance = $this->SampleRemmitance->find('all',array('conditions'=>array('SampleRemmitance.coffee_sample_id'=>$id)));
                $this->set('dataSampleRemmitances',$dataSampleRemmitance);
                $this->set('dataCoffeeSample',$dataCoffeeSample);
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
		$this->set(compact('typeSampleCoffes'));
                $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and'=> array('User.departaments_id' => $this->Session->read('User.centerId'),'User.profiles_id' => 4))));
                $this->set(compact('usersSamplers'));
        }
               
        
        /**
         * edit method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
        
        public function edit($id = null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                if($this->Session->read('User.profiles_id')==1){
                    $this->layout = 'colaborador';
                }
                else if($this->Session->read('User.profiles_id')==9){
                    $this->layout = 'invitado';
                }
                $this->loadModel('User');
                $this->loadModel('SampleRemmitance');
                $this->loadModel('TypeSampleCoffe');
                $this->loadModel('SampleCoffeeHasTypeSample');
		if ($this->request->is('post')) {
                    //debug($this->request->data);exit;
                    $dateChangeFile = null;
                    $CoffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
                    if($CoffeeSample['CoffeeSample']['status']=="REGISTRADA"){
                        $db = $this->CoffeeSample->getDataSource();
                        if(!empty($this->request->data['CoffeeSample']['img_request_sample']['tmp_name'])){
                            $dataFile = $this->request->data['CoffeeSample']['img_request_sample'];
                            $dateChangeFile = date('Y-m-d H:i:s');
                            $bucket = 'requestcustomercoffee';
                            $keyname = $id.".pdf";
                            if($dataFile['tmp_name']){
                                $s3 = new S3Client([
                                    'version' => 'latest',
                                    'region'  => 'us-east-2',
                                    'credentials' => [
                                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                                    'secret' => "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",]]);
                                try
                                {
                                    $result = $s3->putObject([
                                            'Bucket' => $bucket,
                                            'Key'    => $keyname,
                                            'SourceFile' => $dataFile['tmp_name']
                                    ]);
                                } catch (S3Exception $e) {
                                    $this->Flash->error(__($e->getMessage()));
                                }
                            }
                        }
                        unset($this->request->data['CoffeeSample']['img_request_sample']);
                        $dataResult1 = $this->CoffeeSample->updateAll(
                            array(
                                'qta_bag' => $this->request->data['CoffeeSample']['qta_bag'],
                                'lot_coffee_ref' => $db->value($this->request->data['CoffeeSample']['lot'], 'string'),
                                'client_id' => $this->request->data['CoffeeSample']['client_id'],
                                'img_request_sample'=>"'".$dateChangeFile."'",
                                'updated_date'=>"'".date('Y-m-d H:i:s')."'",
                                'operation_center' => $db->value($this->request->data['CoffeeSample']['operation_center'], 'string'),
				'terminal' => $db->value($this->request->data['CoffeeSample']['terminal'], 'string'),
                                'record_pictures' => $this->request->data['CoffeeSample']['record_pictures'],
                                'tracking_packaging'=> $this->request->data['CoffeeSample']['tracking_packaging'],
                                'observation' => "'".$this->request->data['CoffeeSample']['observation']."'"),
                            array('CoffeeSample.id'=>$id));
                        $dataMessage = "Solicitud actualizada exitosamente\n";
                        if(isset($this->request->data['dataRemittancesCaffee'])){
                            foreach($this->request->data['dataRemittancesCaffee'] as $dataRemittancesCoffee){
                                    $this->SampleRemmitance->create();
                                    $arrayData = array('coffee_sample_id'=>$id,'remittances_caffee_id'=>$dataRemittancesCoffee);
                                    try{
                                        $this->SampleRemmitance->save($arrayData);
                                    }
                                    catch(Exception $e){
                                        $dataMessage .= "Ya existe en la actual solicitud el id de Café ".$dataRemittancesCoffee;
                                    }
                            }
                        }
                        $this->SampleCoffeeHasTypeSample->deleteAll(array('SampleCoffeeHasTypeSample.sample_coffee_id'=>$id),false);
                        foreach($this->request->data['CoffeeSample']['type_sample_coffe_id'] as $dataTypeSamples){
                                    $this->SampleCoffeeHasTypeSample->create();
                                    $arrayData = array('sample_coffee_id'=>$id,'type_sample_coffee_id'=>$dataTypeSamples);
                                    try{
                                        $this->SampleCoffeeHasTypeSample->save($arrayData);
                                    }
                                    catch(Exception $e){
                                        $dataMessage .= "Ya tiene asociado el tipo de muestra! ";
                                    }
                        }
                        $this->Flash->success(__($dataMessage));
                        return $this->redirect(array('action' => 'edit',$id)); 
                    }
                    else{
                       $this->Flash->error(__("Solicitud NO ha sido completada hasta el momento!")); 
                    }
                }
                $this->SampleRemmitance->recursive = 2;
                $dataCoffeeSample = $this->CoffeeSample->find('first',array('conditions'=>array('CoffeeSample.id'=>$id)));
                $dataSampleRemmitance = $this->SampleRemmitance->find('all',array('conditions'=>array('SampleRemmitance.coffee_sample_id'=>$id)));
                $this->set('dataSampleRemmitances',$dataSampleRemmitance);
                $this->set('dataCoffeeSample',$dataCoffeeSample);
                $typeSampleCoffes = $this->TypeSampleCoffe->find('list');
                $this->set('typeSampleCoffes',$typeSampleCoffes);
                $this->set('userCurrent', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
        }

        /**
         * send_sample method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
	public function send_sample($id = null) {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
		if (!$this->CoffeeSample->exists($id)) {
			throw new NotFoundException(__('Invalid coffee sample'));
		}
		if ($this->request->is(array('post', 'put'))) {
                        $db = $this->CoffeeSample->getDataSource();
                        $file_seal1_path = $this->request->data['CoffeeSample']['img_finish_sample'];
                        $this->request->data['CoffeeSample']['img_finish_sample']='';
                        $pathDirectory = 'data/samplesCoffee/' . $this->CoffeeSample->id . "/" . $this->request->data['CoffeeSample']['lot_coffee_ref'];
                        if (!is_dir(WWW_ROOT . 'img/' . $pathDirectory)) {
                            mkdir(WWW_ROOT . 'img/' . $pathDirectory, 0777, true);
                        }
                        $pathSeal1 = $pathDirectory . "/" . $file_seal1_path['name'];
                        $resultSeal1 = move_uploaded_file($file_seal1_path['tmp_name'], WWW_ROOT . 'img/' . $pathSeal1);
                        /*$serviceOrder = array(
                            'created_date' => $db->value(date('Y-m-d h:i:s'), 'string'),
                            'exporter_code' => $this->request->data['PackagingCaffee']['exporter_id'],
                            'create_user' => $this->Session->read('User.id')
                        );
                        $this->ServicesOrder->create();
                        $this->ServicesOrder->save($serviceOrder);*/
                        $wsShippingUtil = new ShippingUtil;
                        $arrayData = array(
                            'Shipping'=>$this->request->data['CoffeeSample']['supply_company'],
                            'Operation'=>'SEND',
                            'Num_ValorDeclaradoTotal'=> ($this->request->data['CoffeeSample']['sample_packing'] == 'SOBRE')? "25000":"50000",
                            'Des_Telefono'=>$this->request->data['CoffeeSample']['phone_des'],
                            'Des_Ciudad'=>$this->request->data['CoffeeSample']['city_des'],
                            'Ori_Ciudad'=>$this->Session->read('User.center_departament'),
                            'Des_Direccion'=>$this->request->data['CoffeeSample']['address'],
                            'Nom_Contacto'=>$this->request->data['CoffeeSample']['name_contact'],
                            'Des_DireccionRemitente'=>$this->Session->read('User.addressCenterOperation'),
                            'Des_DepartamentoDestino'=>$this->request->data['CoffeeSample']['departament_des'],
                            'Des_DepartamentoOrigen'=>$this->Session->read('User.center_departament'),
                            'Des_CorreoElectronico'=>$this->request->data['CoffeeSample']['email_contact'],
                            'Des_codigo_postal'=>$this->request->data['CoffeeSample']['des_code_postal'],
                        );
                        $gEnvio = $wsShippingUtil->sd_SampleCoffee($arrayData);
                        $resultxml = simplexml_load_string($gEnvio);
                        $admisionXml = $resultxml->ADMISIONES->RESPUESTA_ADMISION;
                        $arrayEnvio = json_decode( json_encode($admisionXml) , 1);
                        if($arrayEnvio['NUMERO_ENVIO']){
                            if($this->CoffeeSample->updateAll(
                                array(
                                    'CoffeeSample.tracking_supply_id'=>$db->value($arrayEnvio['NUMERO_ENVIO'], 'string'),
                                    'CoffeeSample.img_finish_sample'=>$db->value($pathSeal1, 'string'),
                                    'CoffeeSample.send_date'=>$db->value(date('Y-m-d h:i:s'), 'string'),
                                    'CoffeeSample.status'=>$db->value('DESPACHADA', 'string'),
                                    'CoffeeSample.sample_packing'=>$db->value($this->request->data['CoffeeSample']['sample_packing'], 'string')),
                                array('CoffeeSample.id'=>$id))) {
                                    $this->Flash->success(__('Muestra despachada exitosamente'));
                                    return $this->redirect(array('action' => 'view',$id."-V"));
                            }
			} else {
                            $this->Flash->error(__('No se genero ningun despacho. Por favor intentelo nuevamente'));
			}
		}
                $this->Paginator->settings = array(
                        'limit' => 10,
                        'conditions' => array('CoffeeSample.status'=>'COMPLETADA'),
                        'order' => array('CoffeeSample.created_date' => 'DESC')
                    );
                $this->set('coffeeSamples',$this->Paginator->paginate());
	}
        
        
        public function download_shipping($id =null){
            if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            $this->layout = 'colaborador';
            if (!$this->CoffeeSample->exists($id)) {
                    throw new NotFoundException(__('Invalid coffee sample'));
            }
            $options = array('conditions' => array('CoffeeSample.' . $this->CoffeeSample->primaryKey => $id));
            $coffeeSample = $this->CoffeeSample->find('first', $options);
            $wsShippingUtil = new ShippingUtil;
                $arrayData = array(
                    'Shipping'=>$coffeeSample['CoffeeSample']['supply_company'],
                    'Operation'=>'TICKET_PRINT',
                    'Tracking'=>$coffeeSample['CoffeeSample']['tracking_supply_id']
                );
            $wsShippingUtil->sd_SampleCoffee($arrayData);
        }
        
        
        public function delete($id = null) {
                $this->loadModel('SampleRemmitance');
                $this->loadModel('SampleCoffeeHasTypeSample');
		$this->CoffeeSample->id = $id;
		if (!$this->CoffeeSample->exists()) {
			throw new NotFoundException(__('Invalid charge massive sample coffee'));
		}
                $this->SampleRemmitance->deleteAll(array('SampleRemmitance.coffee_sample_id'=>$id),false);
                $this->SampleCoffeeHasTypeSample->deleteAll(array('SampleCoffeeHasTypeSample.sample_coffee_id'=>$id),false);
                $this->CoffeeSample->deleteAll(array('CoffeeSample.id'=>$id),false);
                $this->Flash->success(__('Solicitud eliminada correctamente! ID-'.$id));
		return $this->redirect(array('action' => 'index',$this->Session->read('User.id')));
	}
}
