<?php
App::uses('AppController', 'Controller');
/**
 * ViewTrackgingPackagings Controller
 *
 * @property ViewTrackgingPackaging $ViewTrackgingPackaging
 * @property PaginatorComponent $Paginator
 */
class ViewTrackgingPackagingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
		$this->ViewTrackgingPackaging->recursive = 0;
                $this->Paginator->settings = array(
                    'limit' => 50,
                    'order' => array('ViewTrackgingPackaging.date_oie' => 'DESC'));
		$this->set('viewTrackgingPackagings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ViewTrackgingPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid view trackging packaging'));
		}
		$options = array('conditions' => array('ViewTrackgingPackaging.' . $this->ViewTrackgingPackaging->primaryKey => $id));
		$this->set('viewTrackgingPackaging', $this->ViewTrackgingPackaging->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function search() {
            if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->ViewTrackgingPackaging->create();
			if ($this->ViewTrackgingPackaging->save($this->request->data)) {
				$this->Flash->success(__('The view trackging packaging has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The view trackging packaging could not be saved. Please, try again.'));
			}
		}
	}
        
        
        /**
 * add method
 *
 * @return void
 */
	public function findUnits($lotCoffee = null) {
            if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->autoRender=false;
	    $dataViewTrackgingPackaging = $this->ViewTrackgingPackaging->find('all',
                    array('conditions' => array('and'=>
                        array('ViewTrackgingPackaging.lotes LIKE '=>"%".$lotCoffee."%"),array('YEAR(ViewTrackgingPackaging.date_oie)'=>date("Y")))));
            //debug($dataViewTrackgingPackaging);            
            return json_encode($dataViewTrackgingPackaging);
	}


}
