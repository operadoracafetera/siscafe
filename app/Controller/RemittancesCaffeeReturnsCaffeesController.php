<?php
App::uses('AppController', 'Controller');
/**
 * RemittancesCaffeeReturnsCaffees Controller
 *
 * @property RemittancesCaffeeReturnsCaffee $RemittancesCaffeeReturnsCaffee
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class RemittancesCaffeeReturnsCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RemittancesCaffeeReturnsCaffee->recursive = 0;
		$this->set('remittancesCaffeeReturnsCaffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RemittancesCaffeeReturnsCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid remittances caffee returns caffee'));
		}
		$options = array('conditions' => array('RemittancesCaffeeReturnsCaffee.' . $this->RemittancesCaffeeReturnsCaffee->primaryKey => $id));
		$this->set('remittancesCaffeeReturnsCaffee', $this->RemittancesCaffeeReturnsCaffee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RemittancesCaffeeReturnsCaffee->create();
			if ($this->RemittancesCaffeeReturnsCaffee->save($this->request->data)) {
				$this->Flash->success(__('The remittances caffee returns caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The remittances caffee returns caffee could not be saved. Please, try again.'));
			}
		}
		$remittancesCaffees = $this->RemittancesCaffeeReturnsCaffee->RemittancesCaffee->find('list');
		$returnsCaffees = $this->RemittancesCaffeeReturnsCaffee->ReturnsCaffee->find('list');
		$this->set(compact('remittancesCaffees', 'returnsCaffees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RemittancesCaffeeReturnsCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid remittances caffee returns caffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RemittancesCaffeeReturnsCaffee->save($this->request->data)) {
				$this->Flash->success(__('The remittances caffee returns caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The remittances caffee returns caffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RemittancesCaffeeReturnsCaffee.' . $this->RemittancesCaffeeReturnsCaffee->primaryKey => $id));
			$this->request->data = $this->RemittancesCaffeeReturnsCaffee->find('first', $options);
		}
		$remittancesCaffees = $this->RemittancesCaffeeReturnsCaffee->RemittancesCaffee->find('list');
		$returnsCaffees = $this->RemittancesCaffeeReturnsCaffee->ReturnsCaffee->find('list');
		$this->set(compact('remittancesCaffees', 'returnsCaffees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RemittancesCaffeeReturnsCaffee->id = $id;
		if (!$this->RemittancesCaffeeReturnsCaffee->exists()) {
			throw new NotFoundException(__('Invalid remittances caffee returns caffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->RemittancesCaffeeReturnsCaffee->delete()) {
			$this->Flash->success(__('The remittances caffee returns caffee has been deleted.'));
		} else {
			$this->Flash->error(__('The remittances caffee returns caffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
