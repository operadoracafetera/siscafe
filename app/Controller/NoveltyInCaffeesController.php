<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * NoveltyInCaffees Controller
 *
 * @property NoveltyInCaffee $NoveltyInCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class NoveltyInCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->NoveltyInCaffee->recursive = 0;
		$this->set('noveltyInCaffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->NoveltyInCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid novelty in caffee'));
		}
		$options = array('conditions' => array('NoveltyInCaffee.' . $this->NoveltyInCaffee->primaryKey => $id));
		$this->set('noveltyInCaffee', $this->NoveltyInCaffee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idRemittanceCaffee = null) {
                if(!$this->Session->read('User.id')){
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                }
                $this->layout = 'colaborador';
                $this->loadModel('RemittancesCaffee');
                $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
                $RemittancesCaffee = $this->RemittancesCaffee->find('first',array('conditions' => array('RemittancesCaffee.id' => $idRemittanceCaffee)));
		if ($this->request->is('post')) {
			$this->NoveltyInCaffee->create();
                        $this->request->data['NoveltyInCaffee']['created_date']=date("Y-m-d H:i:s");
                        $file_img1_path = $this->request->data['NoveltyInCaffee']['img1'];
                        $file_img2_path = $this->request->data['NoveltyInCaffee']['img2'];
                        $file_img3_path = $this->request->data['NoveltyInCaffee']['img3'];
                        $file_img4_path = $this->request->data['NoveltyInCaffee']['img4'];
                        $pathDirectory='data/noveltyCaffee'."/".$idRemittanceCaffee;
                        if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                            mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                        }
                        $pathImg1=$pathDirectory."/".$file_img1_path['name'];
                        $resultImg1 = move_uploaded_file($file_img1_path['tmp_name'], WWW_ROOT.'img/'.$pathImg1);
                        $this->request->data['NoveltyInCaffee']['img1']=$pathImg1;
                        $pathImg2=$pathDirectory."/".$file_img2_path['name'];
                        $resultImg2 = move_uploaded_file($file_img2_path['tmp_name'], WWW_ROOT.'img/'.$pathImg2);
                        $this->request->data['NoveltyInCaffee']['img2']=$pathImg2;
                        $pathImg3=$pathDirectory."/".$file_img3_path['name'];
                        $resultImg3 = move_uploaded_file($file_img3_path['tmp_name'], WWW_ROOT.'img/'.$pathImg3);
                        $this->request->data['NoveltyInCaffee']['img3']=$pathImg3;
                        $pathImg4=$pathDirectory."/".$file_img4_path['name'];
                        $resultImg4 = move_uploaded_file($file_img4_path['tmp_name'], WWW_ROOT.'img/'.$pathImg4);
                        $this->request->data['NoveltyInCaffee']['img4']=$pathImg4;
                        //debug($this->request->data['NoveltyInCaffee']);exit;
			if ($this->NoveltyInCaffee->save($this->request->data)) {
                                /*if($this->request->data['NoveltyInCaffee']['mojado'] == true){
                                    $noveltyCaffe = array('RemittancesCaffeeHasNoveltysCaffee'=> 
                                        array(
                                            'remittances_caffee_id'=>$RemittancesCaffee['RemittancesCaffee']['id'],
                                            'noveltys_caffee_id'=>4,
                                            'created_date'=>date('Y-m-d H:i:s'),
                                            'active'=>1,
                                        ));
                                    $this->RemittancesCaffeeHasNoveltysCaffee->save($noveltyCaffe);
                                }*/
				$this->Flash->success(__('Novedad registrada correctamente!'));
                                $RemittancesCaffee['RemittancesCaffee']['novelty_in_caffee_id']=$this->NoveltyInCaffee->id;
                                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.novelty_in_caffee_id'=>$this->NoveltyInCaffee->id),array('RemittancesCaffee.id'=>$idRemittanceCaffee));
				if($RemittancesCaffee['Client']['notify_email'] == 1){
                                    $this->sendEmail(
                                            $RemittancesCaffee['Client']['emails'],
                                            "3-".$RemittancesCaffee['Client']['exporter_code']."-".$RemittancesCaffee['RemittancesCaffee']['lot_caffee'],
                                            $this->request->data['NoveltyInCaffee']['observation']);
                                    
                                    return $this->redirect(array('controller'=>'RemittancesCaffees','action' => 'index')); 
                                }
                                else{
                                   return $this->redirect(array('controller'=>'RemittancesCaffees','action' => 'index')); 
                                }
			} else {
				$this->Flash->error(__('No se pudo registrar la novedad. Por favor intento nuevamente.'));
			}
		}
                //debug($RemittancesCaffee);exit;
                $this->set('RemittancesCaffee', $RemittancesCaffee);
                
	}

        public function sendEmail($to = null,$refCafe = null, $novelty = null){
            $Email = new CakeEmail();
            $Email->config('gmail');
            $Email->from(array('servicioalcliente@operadoracafetera.com' => 'Operaciones Café - COPCSA'));
            $Email->to($to);
            $Email->subject("Novedad en el descargue de Café - ".$refCafe);
            $Email->send("Se genero una novedad en el descargue de café;\n"
                    . "Lote de Café: ".$refCafe."\n"
                    . "Novedad: ".$novelty."\n"
                    . "Para mas detalles, Por favor ingresar  al Portal del Exportador, opción\n"
                    . "Descargues Café -> Novedades.");
        }
        
}
