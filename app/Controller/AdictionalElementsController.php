<?php
App::uses('AppController', 'Controller');
/**
 * AdictionalElements Controller
 *
 * @property AdictionalElement $AdictionalElement
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class AdictionalElementsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AdictionalElement->recursive = 0;
		$this->set('adictionalElements', $this->Paginator->paginate());
	}
	
/**
 * find method
 *
 * @return void
 */
	public function findAllAdictionalElements() {
		return $this->AdictionalElement->find('list');
	}
	
/**
 * find method
 *
 * @return void
 */
	public function findAll() {
		return $this->AdictionalElement->find('all');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AdictionalElement->exists($id)) {
			throw new NotFoundException(__('Invalid adictional element'));
		}
		$options = array('conditions' => array('AdictionalElement.' . $this->AdictionalElement->primaryKey => $id));
		$this->set('adictionalElement', $this->AdictionalElement->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AdictionalElement->create();
			if ($this->AdictionalElement->save($this->request->data)) {
				$this->Flash->success(__('The adictional element has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The adictional element could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AdictionalElement->exists($id)) {
			throw new NotFoundException(__('Invalid adictional element'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AdictionalElement->save($this->request->data)) {
				$this->Flash->success(__('The adictional element has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The adictional element could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AdictionalElement.' . $this->AdictionalElement->primaryKey => $id));
			$this->request->data = $this->AdictionalElement->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AdictionalElement->id = $id;
		if (!$this->AdictionalElement->exists()) {
			throw new NotFoundException(__('Invalid adictional element'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AdictionalElement->delete()) {
			$this->Flash->success(__('The adictional element has been deleted.'));
		} else {
			$this->Flash->error(__('The adictional element could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
