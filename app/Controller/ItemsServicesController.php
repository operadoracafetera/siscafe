<?php
App::uses('AppController', 'Controller');
/**
 * ItemsServices Controller
 *
 * @property ItemsService $ItemsService
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ItemsServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ItemsService->recursive = 0;
		$this->set('itemsServices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ItemsService->exists($id)) {
			throw new NotFoundException(__('Invalid items service'));
		}
		$options = array('conditions' => array('ItemsService.' . $this->ItemsService->primaryKey => $id));
		$this->set('itemsService', $this->ItemsService->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ItemsService->create();
			if ($this->ItemsService->save($this->request->data)) {
				$this->Flash->success(__('The items service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The items service could not be saved. Please, try again.'));
			}
		}
	}
        
       /**
 * add method
 *
 * @return void
 */
	public function findByIdItem($idItemService = null) {
            if (!$this->ItemsService->exists($idItemService)) {
                $this->set('itemsService', null);
            }
            $this->autoRender = false;
            $options = array('conditions' => array('ItemsService.unoee_ref' => $idItemService));
            $itemService = $this->ItemsService->find('first', $options);
            return json_encode($itemService);
	} 

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ItemsService->exists($id)) {
			throw new NotFoundException(__('Invalid items service'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ItemsService->save($this->request->data)) {
				$this->Flash->success(__('The items service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The items service could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ItemsService.' . $this->ItemsService->primaryKey => $id));
			$this->request->data = $this->ItemsService->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ItemsService->id = $id;
		if (!$this->ItemsService->exists()) {
			throw new NotFoundException(__('Invalid items service'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ItemsService->delete()) {
			$this->Flash->success(__('The items service has been deleted.'));
		} else {
			$this->Flash->error(__('The items service could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
