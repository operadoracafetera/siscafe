<?php
App::uses('AppController', 'Controller');
/**
 * ReturnsCaffees Controller
 *
 * @property ReturnsCaffee $ReturnsCaffee
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ReturnsCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		if (!$this->Session->read('User.id')) {
                	return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            	}
                $this->layout ='colaborador';	
                $this->ReturnsCaffee->recursive = 0;
		$this->paginate = array(
                    'limit' => 20,
                    'order' => array('ReturnsCaffee.id'=>'desc'),
		    'conditions' => array('Departament.cod_city'=>$this->Session->read('User.jetty'))
                );
		$this->set('returnsCaffees', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ReturnsCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid returns caffee'));
		}
                $this->layout ='colaborador';	
		$options = array('conditions' => array('ReturnsCaffee.' . $this->ReturnsCaffee->primaryKey => $id));
		$this->set('returnsCaffee', $this->ReturnsCaffee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->loadModel('ServicesOrder');
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('User');
	    $this->loadModel('Departaments');
            $this->loadModel('Client');
            $this->layout ="colaborador";	
            $db = $this->RemittancesCaffee->getDataSource();
            if ($this->request->is('post')) {
                    $this->ReturnsCaffee->create();
                    $this->request->data['ReturnsCaffee']['return_date'] = date('Y-m-d H:i:s');
                    $this->request->data['ReturnsCaffee']['user_register'] = $this->Session->read('User.id');
                    $this->request->data['ReturnsCaffee']['state_return'] = 'REGISTRADA';
                    $client = $this->Client->find('first',array('conditions'=>array('Client.id'=>$this->request->data['ReturnsCaffee']['client_id'])));
                    if ($this->ReturnsCaffee->save($this->request->data)) {
                            $this->ServicesOrder->create();
                            $servicesOrder= array(
                                'created_date' => date('Y-m-d h:i:s'),
                                'exporter_code'=> $client['Client']['exporter_code'],
                                'create_user' => $this->Session->read('User.id'),
                            );
                            $this->ServicesOrder->save($servicesOrder);
                            $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.order_service'=>$this->ServicesOrder->id),array('ReturnsCaffee.id'=>$this->ReturnsCaffee->id));
                            $this->Flash->success(__('Se registro una devolucion de café, por favor liste que remesas a relacionar...'));
                            return $this->redirect(array('controller'=>'ReturnsCaffees','action' => 'return_lot',$this->ReturnsCaffee->id));

                    } else {
                        $this->Flash->error(__('No se puede devolver la remesa ' . $id));
                    }
            }
            $remittancesCaffees = $this->ReturnsCaffee->RemittancesCaffee->find('list');

            $this->set(compact('remittancesCaffees')); 
	    $deparaments = $this->Departaments->find('list',array('conditions'=>array('Departaments.cod_city'=>$this->Session->read('User.jetty'))));
	    $this->set(compact('deparaments'));
	}

        /**
 * add method
 *
 * @return void
 */
	public function return_lot($idReturn = null) {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }           
	    $this->loadModel('ServicesOrder');
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('User');
            $this->loadModel('Client');
            $this->loadModel('RemittancesCaffeeReturnsCaffee');
            $this->loadModel('PackingCaffee');
            $this->loadModel('ServicePackage');
            $this->loadModel('DetailsServicesToCaffee');
            $this->layout ='colaborador';	
            $db = $this->RemittancesCaffee->getDataSource();
            if ($this->request->is(array('post', 'put'))) {
                $dataRemesa = $this->RemittancesCaffeeReturnsCaffee->find('all',array('conditions'=>array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $idReturn)));
                $returnCoffee = $this->ReturnsCaffee->find('first',array('conditions'=>array('ReturnsCaffee.id'=>$idReturn)));
                if($dataRemesa){
                    if($returnCoffee['ReturnsCaffee']['client_id'] == 1){
                        foreach ($dataRemesa as $remesa) {
                            $servicePackage = $this->ServicePackage->find('first',array('conditions'=>array('ServicePackage.id' => '00138')));
                            $totalServiceOrder=0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices){
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor']*$remesa['RemittancesCaffee']['quantity_radicated_bag_in'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $remesa['ReturnsCaffee']['order_service'],
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $remesa['RemittancesCaffee']['id'],
                                    'qta_bag_to_work' => $remesa['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'observation' => 'DEVOLUCION DE LOTE '.'3-'.$returnCoffee['Client']['exporter_code'].'-'.$remesa['RemittancesCaffee']['lot_caffee'].' POR '.$remesa['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date'=>date('Y-m-d h:i:s')
                                );
                                $totalServiceOrder+=$valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor'=>$totalServiceOrder),array('ServicesOrder.id' =>$remesa['ReturnsCaffee']['order_service']));
                                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id'=>9),array('RemittancesCaffee.id'=>$remesa['RemittancesCaffee']['id']));
                                }
                        }
                        $returnCoffee = $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.state_return'=>"'COMPLETADA'"),array('ReturnsCaffee.id'=>$idReturn));
                    }
                    else{
                        foreach ($dataRemesa as $remesa) {
                            $servicePackage = $this->ServicePackage->find('first',array('conditions'=>array('ServicePackage.id' => '00139')));
                            $totalServiceOrder=0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices){
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor']*$remesa['RemittancesCaffee']['quantity_radicated_bag_in'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $remesa['ReturnsCaffee']['order_service'],
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $remesa['RemittancesCaffee']['id'],
                                    'qta_bag_to_work' => $remesa['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'observation' => 'DEVOLUCION DE LOTE '.'3-'.$returnCoffee['Client']['exporter_code'].'-'.$remesa['RemittancesCaffee']['lot_caffee'].' POR '.$remesa['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date'=>date('Y-m-d H:i:s')
                                );
                                $totalServiceOrder+=$valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor'=>$totalServiceOrder),array('ServicesOrder.id' =>$remesa['ReturnsCaffee']['order_service']));
                                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id'=>9),array('RemittancesCaffee.id'=>$remesa['RemittancesCaffee']['id']));
                            }
                        }
                        $returnCoffee = $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.state_return'=>"'COMPLETADA'"),array('ReturnsCaffee.id'=>$idReturn));
                    }
                    $this->Flash->success(__('Se registro una devolucion de café. '));
                    return $this->redirect(array('controller'=>'ReturnsCaffees','action' => 'index',$idReturn));
                }
                else{
                    $this->Flash->error(__('No hay ninguna remesa relacionada para devolver. por favor verificar. '));
                }
            }
            $this->Paginator->settings = array(
                'limit' => 15,
                'conditions' => array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $idReturn)
            );
            $returnCoffee = $this->ReturnsCaffee->find('first',array('conditions'=>array('ReturnsCaffee.id'=>$idReturn)));
            $this->set(compact('returnCoffee'));    
            $remittancesCaffeeReturnsCaffee = $this->Paginator->paginate('RemittancesCaffeeReturnsCaffee');
            $this->set('remittancesCaffeeReturnsCaffees',$remittancesCaffeeReturnsCaffee);       
	}
        
        public function cancelled($idReturn=null){
            $this->autoRender = false;
            $this->loadModel('RemittancesCaffeeReturnsCaffee');
            $this->loadModel('ServicesOrder');
            $this->loadModel('RemittancesCaffee');
            $returnsCaffee = $this->ReturnsCaffee->find('first',array('condictions'=>array('ReturnsCaffee.id'=>$idReturn)));
            $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.state_return'=>"'CANCELADA'"),array('ReturnsCaffee.id'=>$idReturn));
            $dataRemesa = $this->RemittancesCaffeeReturnsCaffee->find('all',array('conditions'=>array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $idReturn)));
	    $this->ServicesOrder->updateAll(array('ServicesOrder.active'=>false),array('ServicesOrder.id' =>$returnsCaffee['ReturnsCaffee']['order_service']));
            foreach ($dataRemesa as $remesa){
                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id'=>2),array('RemittancesCaffee.id' =>$remesa['RemittancesCaffee']['id']));
            }
            return $this->redirect(array('controller'=>'ReturnsCaffees','action' => 'index')); 
        }

        /**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ReturnsCaffee->exists($id)) {
			throw new NotFoundException(__('Invalid returns caffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ReturnsCaffee->save($this->request->data)) {
				$this->Flash->success(__('The returns caffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The returns caffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ReturnsCaffee.' . $this->ReturnsCaffee->primaryKey => $id));
			$this->request->data = $this->ReturnsCaffee->find('first', $options);
		}
		$remittancesCaffees = $this->ReturnsCaffee->RemittancesCaffee->find('list');
		$this->set(compact('remittancesCaffees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ReturnsCaffee->id = $id;
		if (!$this->ReturnsCaffee->exists()) {
			throw new NotFoundException(__('Invalid returns caffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ReturnsCaffee->delete()) {
			$this->Flash->success(__('The returns caffee has been deleted.'));
		} else {
			$this->Flash->error(__('The returns caffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function searchReturns(){
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('RemittancesCaffeeReturnsCaffee');
            $this->loadModel('Client');
            $this->loadModel('RemittancesCaffee');           
            $searchInfoList = array();
            if ($this->request->is('post')) {
                $this->ReturnsCaffee->recursive = 3;
                if ($this->request->data['ReturnsCaffee']['codigo']) {
                    $searchInfoList = $this->ReturnsCaffee->find('all', array('conditions' => array('ReturnsCaffee.id' => $this->request->data['ReturnsCaffee']['codigo'])));
                }else if ($this->request->data['ReturnsCaffee']['vehiculo']) {
                    $searchInfoList = $this->ReturnsCaffee->find('all', array('conditions' => array('ReturnsCaffee.vehicle_plate' => $this->request->data['ReturnsCaffee']['vehiculo'])));
                }else if ($this->request->data['ReturnsCaffee']['remesa']) {
                    $searchInfoList = $this->RemittancesCaffeeReturnsCaffee->find('all', array('conditions' => array('RemittancesCaffeeReturnsCaffee.remittances_caffee_id' => $this->request->data['ReturnsCaffee']['remesa'])));
                }else if ($this->request->data['ReturnsCaffee']['lote']) {
                    $porciones = explode("-", $this->request->data['ReturnsCaffee']['lote']);
                    $client = $this->Client->find('all',array('conditions' => array('Client.exporter_code'=>$porciones[0])));
                    $dataRemittances = $this->RemittancesCaffee->find('list',array('fields'=>array('RemittancesCaffee.id'),'conditions' => array( 'AND' =>array(
                                                                                        'RemittancesCaffee.lot_caffee' => $porciones[1],
                                                                                        'RemittancesCaffee.client_id' => $client[0]['Client']['id']
                                                                                     )))); 
                    $searchInfoList = $this->RemittancesCaffeeReturnsCaffee->find('all', array('conditions' => array('RemittancesCaffeeReturnsCaffee.remittances_caffee_id' => $dataRemittances)));
                    //debug($searchInfoList);exit;    
                }   
            }
            $this->set('searchInfoList', $searchInfoList);
        } 
        
}
