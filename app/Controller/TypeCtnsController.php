<?php
App::uses('AppController', 'Controller');
/**
 * TypeCtns Controller
 *
 * @property TypeCtn $TypeCtn
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TypeCtnsController extends AppController {

	
	/**
 * findAll method
 *
 * @return void
 */
	public function findAll() {
		return $this->TypeCtn->find('list', array('fields'=>array('TypeCtn.id','TypeCtn.type_ctn')));
	}

		/**
 * findById method
 *
 * @return void
 */
	public function findById($id) {
		$option=array('conditions' => array('TypeCtn.' . $this->TypeCtn->primaryKey => $id));
		return $this->TypeCtn->find('first', $option);
	}
}
