<?php

App::uses('AppController', 'Controller');

/**
 * DetailsServicesToCaffees Controller
 *
 * @property DetailsServicesToCaffee $DetailsServicesToCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DetailsServicesToCaffeesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->DetailsServicesToCaffee->recursive = 0;
        $this->set('detailsServicesToCaffee', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->DetailsServicesToCaffeee->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $options = array('conditions' => array('DetailsServicesToCaffee.' . $this->DetailsServicesToCaffee->primaryKey => $id));
        $this->set('detailsServicesToCaffeee', $this->DetailsServicesToCaffee->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($serviceOrderid = null, $exportCode = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('ServicePackage');
        $this->loadModel('ServicesOrder');
        $this->loadModel('RemittancesCaffee');
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $servicePackages = $this->ServicePackage->find('list', array('fields' => array('ServicePackage.name_package', 'ServicePackage.id')));
            //debug($this->request->data['DetailsServicesToCaffee']);exit;
            $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $serviceOrderid)));
            
            if($this->request->data['DetailsServicesToCaffee']['lock_remesa']){
                if($this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'] == null){
                    $this->Flash->error(__('Esta tratando de bloquear una fracción no seleccionada. Por favor asocie una remesa'));
                    return $this->redirect(array('controller' => 'DetailsServicesToCaffees', 'action' => 'add',$serviceOrder['ServicesOrder']['id'],$exportCode));
                }
                $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
                $noveltys = $this->RemittancesCaffeeHasNoveltysCaffee->find('first', ['conditions' => ['remittances_caffee_id' => $this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'],
                        'noveltys_caffee_id' => 4]]);
                if (empty($noveltys)) {
                    $noveltys = $this->RemittancesCaffeeHasNoveltysCaffee->create([
                        'remittances_caffee_id' => $this->request->data['DetailsServicesToCaffee']['remittances_caffee_id'],
                        'noveltys_caffee_id' => 4,
                        'created_date' => date('Y-m-d h:i:s'),
                        'active' => 1]);
                    $this->RemittancesCaffeeHasNoveltysCaffee->save($noveltys);
                }
            }
            
            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => $servicePackages[$this->request->data['DetailsServicesToCaffee']['service_package']])));
            $file_img1_path = $this->request->data['DetailsServicesToCaffee']['document'];
            $pathDirectory = 'data/service_orders' . "/" . $this->request->data['DetailsServicesToCaffee']['services_orders_id'];
            if (!is_dir(WWW_ROOT . 'img/' . $pathDirectory)) {
                mkdir(WWW_ROOT . 'img/' . $pathDirectory, 0777, true);
            }
            $pathImg1 = 'img/' . $pathDirectory . "/" . $file_img1_path['name'];
            $resultImg1 = move_uploaded_file($file_img1_path['tmp_name'], WWW_ROOT . $pathImg1);
            $total_valor = 0;
            $bag_to_work = $this->request->data['DetailsServicesToCaffee']['qta_bag_to_work'];
            foreach ($servicePackage['ServicePackageHasItemsService'] as $value) {
                $this->DetailsServicesToCaffee->create();
                date_default_timezone_set('America/Bogota');
                $this->request->data['DetailsServicesToCaffee']['created_date'] = date('Y-m-d h:i:s');
                $this->request->data['DetailsServicesToCaffee']['updated_date'] = date('Y-m-d h:i:s');
                $this->request->data['DetailsServicesToCaffee']['service_package_id'] = $servicePackages[$this->request->data['DetailsServicesToCaffee']['service_package']];
                $this->request->data['DetailsServicesToCaffee']['document'] = $pathImg1;
                $this->request->data['DetailsServicesToCaffee']['items_services_id'] = $value['items_services_id'];
                if ($value['flag_suma'] == true) {
                    $this->request->data['DetailsServicesToCaffee']['value'] = $value['valor'];
                    $this->request->data['DetailsServicesToCaffee']['qta_bag_to_work'] = $value['qta'];
                }
                if ($value['flag_multiplica'] == true) {
                    $this->request->data['DetailsServicesToCaffee']['value'] = ($bag_to_work * $value['valor']);
                    $this->request->data['DetailsServicesToCaffee']['qta_bag_to_work'] = $bag_to_work;
                }
                $total_valor = $total_valor + $this->request->data['DetailsServicesToCaffee']['value'];
                if (!$this->DetailsServicesToCaffee->save($this->request->data)) {
                    $this->Flash->error(__('The services order could not be saved. Please, try again.'));
                    break;
                }
            }
            
            $this->ServicesOrder->updateAll(['total_valor' => ($total_valor + $serviceOrder['ServicesOrder']['total_valor'])], ['ServicesOrder.id' => $serviceOrderid]);
            $this->Flash->success(__('El detalle de servicio se ha guardado'));
            return $this->redirect(array('controller' => 'ServicesOrders', 'action' => 'view',$serviceOrder['ServicesOrder']['id']));
        }
        $this->set('servicePackages', $this->ServicePackage->find('list', array('fields' => array('ServicePackage.name_package'))));
        $this->set('serviceOrderId',$serviceOrderid);
        $this->set('serviceOrder', $this->ServicesOrder->find('first',array('conditions' =>array('ServicesOrder.id'=>$serviceOrderid))));
        $this->loadModel('Client');
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.exporter_code' => $exportCode)));
        $this->set('client', $dataClient);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null, $exportCode = null) {
        if (!$this->DetailsServicesToCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->loadModel('ServicePackage');
        if ($this->request->is(array('post', 'put'))) {

            if ($this->DetailsServicesToCaffee->save($this->request->data)) {
                $this->Flash->success(__('El detalle de servicio se ha guardado'));
                return $this->redirect(array('controller' => 'ServicesOrders', 'action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('DetailsServicesToCaffee.' . $this->DetailsServicesToCaffee->primaryKey => $id));
            $this->request->data = $this->DetailsServicesToCaffee->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($idDetaill = null,$servicesOrderId = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        //debug($servicesOrderId);
        //debug($idDetaill);exit;
        $this->request->allowMethod('post', 'delete');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('ServicesOrder');
        $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('all', ['fields' => ['sum(DetailsServicesToCaffee.value)'],
            'conditions' => ['DetailsServicesToCaffee.id' => $idDetaill]]);
        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $servicesOrderId)));
        $this->ServicesOrder->updateAll(
                ['total_valor' => ($serviceOrder['ServicesOrder']['total_valor'] - $detailsServicesToCaffee[0][0]['sum(`DetailsServicesToCaffee`.`value`)'])], ['ServicesOrder.id' => $servicesOrderId]);
        if ($this->DetailsServicesToCaffee->deleteAll([
                    'DetailsServicesToCaffee.id' => $idDetaill])) {
            $this->Flash->success(__('El detalle de servicio se ha borrado'));
        } else {
            $this->Flash->error(__('The services order could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('controller' => 'ServicesOrders', 'action' => 'view',$servicesOrderId));
    }

    public function completed($idDetaill = null,$servicesOrderId = null, $servicePackageId = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->request->allowMethod('post');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('ServicesOrder');
        $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
        date_default_timezone_set('America/Bogota');
        //debug($this->DetailsServicesToCaffee->find('first',array('conditions' => ['DetailsServicesToCaffee.id' => $idDetaill])));exit;
        if ($this->DetailsServicesToCaffee->updateAll(['completed' => 'true', 'updated_date' => "'" . date('Y-m-d h:i:s') . "'"], ['DetailsServicesToCaffee.id' => $idDetaill])) {
            $detailsCompleted = $this->DetailsServicesToCaffee->find('all', ['conditions' => ['DetailsServicesToCaffee.services_orders_id' => $servicesOrderId, 'DetailsServicesToCaffee.completed' => 0]]);
            if (empty($detailsCompleted)) {
                $this->ServicesOrder->updateAll(
                        ['closed' => 1, 'closed_date' => "'" . date('Y-m-d h:i:s') . "'", 'approve_user' => $this->Session->read('User.id')], ['ServicesOrder.id' => $servicesOrderId]);
            }
            $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('first',array('conditions' => ['DetailsServicesToCaffee.id' => $idDetaill]));
            if($detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id'] != null){
                $this->RemittancesCaffeeHasNoveltysCaffee->updateAll(array('RemittancesCaffeeHasNoveltysCaffee.active'=>0),array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['remittances_caffee_id']));
            }
            $this->Flash->success(__('Se ha completado con exito el paquete de servicio #'.$servicePackageId .' para la orden de servicio #'.$servicesOrderId));
        } else {
            $this->Flash->error(__('No se ha podido completar el servicio. Por favor intentelo nuevamente.'));
        }
        return $this->redirect(array('controller' => 'ServicesOrders', 'action' => 'view',$servicesOrderId));
    }

}
