<?php
App::uses('AppController', 'Controller');
/**
 * ServicePackages Controller
 *
 * @property ServicePackage $ServicePackage
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ServicePackagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
		$this->ServicePackage->recursive = 0;
		$this->set('servicePackages', $this->Paginator->paginate());
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->ServicePackage->create();
                        $this->request->data['ServicePackage']['created_date'] = date('Y-m-d h:i:s');
                        $this->request->data['ServicePackage']['updated_date'] = date('Y-m-d h:i:s');
			if ($this->ServicePackage->save($this->request->data)) {
				$this->Flash->success(__('Paquete de servicio registrado!'));
				return $this->redirect(array('controller'=>'ServicePackageHasItemsServices','action' => 'add',$this->ServicePackage->id));
			} else {
				$this->Flash->error(__('The service package could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
                if(!$this->Session->read('User.id')){
                    return $this->redirect(array('controller'=>'users','action' => 'login'));
                }
		if (!$this->ServicePackage->exists($id)) {
			throw new NotFoundException(__('Invalid service package'));
		}
                $this->layout = 'colaborador';
		if ($this->request->is(array('post', 'put'))) {
                        $this->request->data['ServicePackage']['updated_date'] = date('Y-m-d h:i:s');
			if ($this->ServicePackage->save($this->request->data)) {
				$this->Flash->success(__('Paquete de servicio actualizado!'));
				return $this->redirect(array('controller'=>'ServicePackageHasItemsServices','action' => 'add',$this->ServicePackage->id));
			} else {
				$this->Flash->error(__('The service package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ServicePackage.' . $this->ServicePackage->primaryKey => $id));
			$this->request->data = $this->ServicePackage->find('first', $options);
		}
	}

}
