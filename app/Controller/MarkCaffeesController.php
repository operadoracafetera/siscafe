<?php
App::uses('AppController', 'Controller');
/**
 * MarkCaffees Controller
 *
 * @property MarkCaffee $MarkCaffee
 * @property PaginatorComponent $Paginator
 */
class MarkCaffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

}
