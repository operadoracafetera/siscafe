<?php
App::uses('AppController', 'Controller');
/**
 * Exporters Controller
 *
 * @property Exporter $Exporter
 * @property PaginatorComponent $Paginator
 */
class DetailPackakgingCrossdockingController extends AppController
{

	public function findLotCoffeeCrossdocking($lotCoffee = null)
	{
		if (!$this->Session->read('User.id')) {
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
		$this->layout = 'colaborador';
		$this->autoRender = false;
		$this->loadModel('DetailPackakgingCrossdocking');
		$dataCoffeeCrossdocking = $this->DetailPackakgingCrossdocking->find(
			'all',
			[
				'recursive' => 3,				
				'conditions' => ['and' => ['DetailPackakgingCrossdocking.status' => 1, 'DetailPackakgingCrossdocking.lot_coffee' => $lotCoffee]]
			]
		);
		$dataReponse = [];
		foreach($dataCoffeeCrossdocking as $crossdocking){
			if($crossdocking['PackagingCaffee']['InfoNavy']['status_info_navy_id'] == 2 || $crossdocking['PackagingCaffee']['InfoNavy']['status_info_navy_id'] == 4){
				array_push($dataReponse,$crossdocking);
			}
		}
		if ($dataReponse) {
			echo json_encode($dataReponse);
		} else {
			echo json_encode("");
		}
	}

	public function updateDetailPackakgingCrossdocking($detailCrossdockingId, $remittancesCaffeeId)
	{
		$dataToUpdate = ['remittances_caffee_id' => $remittancesCaffeeId];
		$this->DetailPackakgingCrossdocking->updateAll($dataToUpdate, ['DetailPackakgingCrossdocking.id' => $detailCrossdockingId]);
	}
}
