<?php

App::uses('AppController', 'Controller');
require_once("../Vendor/aws/aws-autoloader.php");

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

/**
 * PackagingCaffees Controller
 *
 * @property PackagingCaffee $PackagingCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PackagingCaffeesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->PackagingCaffee->recursive = 3;
        if ($this->Session->read('User.jetty') == 'BUN' || $this->Session->read('User.jetty') == 'SPIA') {
            $this->paginate = array(
                'limit' => 50,
                'order' => array('PackagingCaffee.id' => 'desc'),
                'conditions' =>
                array('and' => array(
                    array('PackagingCaffee.state_packaging_id = ' => 3),
                    array('PackagingCaffee.jetty' => $this->Session->read('User.jetty'))
                ))
            );
        } else if ($this->Session->read('User.jetty') == 'STM') {
            $this->paginate = array(
                'limit' => 50,
                'order' => array('PackagingCaffee.id' => 'desc'),
                'conditions' =>
                array('PackagingCaffee.jetty' => 'STM')
            );
        } else if ($this->Session->read('User.jetty') == 'CTG') {
            $this->Paginator->settings = array(
                'limit' => 30,
                'conditions' => array('or' => array(array('PackagingCaffee.jetty' => 'CONTECAR'), array('PackagingCaffee.jetty' => 'COMPAS'), array('PackagingCaffee.jetty' => 'SPRC'))),
                'order' => array('PackagingCaffee.created_date' => 'DESC')
            );
        } else if ($this->Session->read('User.jetty') == 'IMP') {
            $this->paginate = array(
                'limit' => 50,
                'order' => array('PackagingCaffee.id' => 'desc'),
                'conditions' =>
                array('and' => array(
                    array('PackagingCaffee.state_packaging_id = ' => 3),
                    array('PackagingCaffee.jetty' => $this->Session->read('User.jetty'))
                ))
            );
        }

        $data = $this->paginate('PackagingCaffee');
        //debug($this->paginate);exit;
        $this->set('packagingCaffees', $data);
    }

    /**
     * index method
     *
     * @return void
     */
    public function all()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'terminal';
        $this->loadModel('User');
        //$listUserByCenterOperation = $this->User->find('list',array('conditions' => array('and'=>array('User.departaments_id' => $this->Session->read('User.departaments_id')),array('User.profiles_id' => 1))));
        if ($this->Session->read('User.profiles_id') == 6) {
            $this->PackagingCaffee->recursive = 3;
            $this->paginate = array(
                'limit' => 50,
                'order' => array('PackagingCaffee.packaging_date' => 'desc'),
                'conditions' =>
                array('and' => array(
                    array('PackagingCaffee.state_packaging_id = ' => 3),
                    array('PackagingCaffee.packaging_date >' => '2019-02-03 00:00:00'),
                    array('PackagingCaffee.jetty' => 'BUN'),
                ))
            );
            $data = $this->paginate('PackagingCaffee');
        } else if ($this->Session->read('User.profiles_id') == 1) {
            $this->PackagingCaffee->recursive = 3;
            $this->paginate = array(
                'limit' => 50,
                'order' => array('PackagingCaffee.packaging_date' => 'desc'),
                'conditions' =>
                array('or' => array(
                    array('PackagingCaffee.state_packaging_id = ' => 3),
                    array('PackagingCaffee.state_packaging_id = ' => 7)
                ))
            );
            $data = $this->paginate('PackagingCaffee');
        }

        $this->set('packagingCaffees', $data);
    }

    /**
     * index method
     *
     * @return void
     */
    public function allctns()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->PackagingCaffee->recursive = 3;
        $this->paginate = array(
            'limit' => 50,
            'order' => array('PackagingCaffee.id' => 'desc')
        );
        $data = $this->paginate('PackagingCaffee');
        $this->set('packagingCaffees', $data);
    }


    /**
     * getTrackingOie method
     *
     * @return void
     */
    public function getTrackingOie($oie = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;

        $bucket = 'packagingtrackingbun';
        $keyname = $oie . ".zip";

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'us-east-2',
            'credentials' => [
                'key'    => "AKIAJZHFB4GIIF3FDWMA",
                'secret' => "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",
            ]
        ]);

        try {
            $result = $s3->getObject([
                'Bucket' => $bucket,
                'Key'    => $keyname,
            ]);

            header("Content-Type: {$result['ContentType']}");
            header('Content-Disposition: attachment; filename=' . $keyname);
            echo $result['Body'];
        } catch (S3Exception $e) {
            $this->Flash->error(__('No se encontro el archivo!'));
        }
    }

    /**
     * setTrackingOie method
     *
     * @return void
     */
    public function trackingOie($oie = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('ViewPackaging');
        if ($this->request->is('post')) {
            $file_track_oie = $this->request->data['PackagingCaffee']['file_track_oie'];
            $temp_file_location = $file_track_oie['tmp_name'];

            $bucket = 'packagingtrackingbun';
            $keyname = $oie . ".zip";

            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => 'us-east-2',
                'credentials' => [
                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                    'secret' => "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",
                ]
            ]);

            try {
                $result = $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $keyname,
                    'SourceFile' => $temp_file_location
                ]);
                $db = $this->PackagingCaffee->getDataSource();
                $this->PackagingCaffee->updateAll(
                    array('PackagingCaffee.upload_tracking_photos_date' => $db->value(date("Y-m-d H:i:s"), 'string')),
                    array('PackagingCaffee.id' => $oie)
                );
                $this->Flash->success("Trazabilidad cargada exitosamente!");
                return $this->redirect(array('action' => 'index'));
            } catch (S3Exception $e) {
                $this->Flash->error(__($e->getMessage()));
            }
        }
        $findPackagingCaffee = $this->ViewPackaging->find('first', array('conditions' => array('ViewPackaging.oie' => $oie)));
        $this->set('findPackagingCaffee', $findPackagingCaffee);
    }

    /**
     * index method
     *
     * @return void
     */
    public function empty_oie()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->PackagingCaffee->recursive = 3;
        $this->paginate = array(
            'limit' => 10,
            'conditions' =>
            array(
                'OR' =>
                array(
                    array('PackagingCaffee.state_packaging_id' => 4),
                    array('PackagingCaffee.state_packaging_id' => 8)
                )
            )
        );
        $data = $this->paginate('PackagingCaffee');
        $this->set('packagingCaffees', $data);
    }

    /**
     * index method
     *
     * @return void
     */
    public function cancelled_oie()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->PackagingCaffee->recursive = 3;
        $this->paginate = array('conditions' =>
        array(
            'OR' =>
            array(
                array('PackagingCaffee.state_packaging_id = ' => 6)
            )
        ));
        $data = $this->paginate('PackagingCaffee');
        $this->set('packagingCaffees', $data);
    }

    /**
     * index method
     *
     * @return void
     */
    public function confirm_oie()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->PackagingCaffee->recursive = 3;
        $this->paginate = array(
            'order' => array('PackagingCaffee.id' => 'desc'),
            'conditions' =>
            array(
                'AND' =>
                array(
                    array('PackagingCaffee.state_packaging_id = ' => 1),
                    array('PackagingCaffee.jetty' => $this->Session->read('User.jetty'))
                )
            )
        );
        $data = $this->paginate('PackagingCaffee');
        $this->set('packagingCaffees', $data);
    }

    /**
     * index method
     *
     * @return void
     */
    public function process_oie()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->PackagingCaffee->recursive = 3;
        $this->paginate = array('conditions' =>
        array(
            'AND' =>
            array(
                array('PackagingCaffee.state_packaging_id = ' => 5),
                array('PackagingCaffee.jetty' => $this->Session->read('User.jetty'))
            )
        ));
        $data = $this->paginate('PackagingCaffee');
        $this->set('packagingCaffees', $data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        $this->PackagingCaffee->recursive = 2;
        if (!$this->PackagingCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid packaging caffee'));
        }
        $options = array('conditions' => array('PackagingCaffee.' . $this->PackagingCaffee->primaryKey => $id));
        debug($this->PackagingCaffee->find('first', $options));
        exit;
        $this->set('packagingCaffee', $this->PackagingCaffee->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function seal($idOIE = null)
    {
        $this->loadModel('TypeCtn');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->PackagingCaffee->create();
            $typeCtn = $this->TypeCtn->find('first', array('conditions' => array('TypeCtn.id' => $this->request->data['PackagingCaffee']['iso_ctn'])));
            $findPackagingCaffee = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $idOIE)));
            $file_seal1_path = $this->request->data['PackagingCaffee']['seal1_path'];
            $file_seal2_path = $this->request->data['PackagingCaffee']['seal2_path'];
            $file_ctn_frontal = $this->request->data['PackagingCaffee']['img_ctn'];

            $dataFilesUpload = array($file_seal1_path, $file_seal2_path, $file_ctn_frontal);
            $dataPathFile = array("", "", "");
            $idImg = 0;

            $bucket = 'packagingtrackingbun';
            $keyname = $idOIE . "/" . $findPackagingCaffee['ReadyContainer']['bic_container'] . "/";
            $s3Client = new S3Client([
                'version' => 'latest',
                'region'  => 'us-east-2',
                'credentials' => [
                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                    'secret' => "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",
                ]
            ]);

            $s3Client->putObject(array(
                'Bucket' => $bucket,
                'Key'    => $keyname
            ));

            foreach ($dataFilesUpload as $uploadFile) {
                if ($uploadFile['name'] && $uploadFile['tmp_name']) {

                    $keyname = $uploadFile['name'];
                    $temp_file_location = $uploadFile['tmp_name'];
                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region'  => 'us-east-2',
                        'credentials' => [
                            'key'    => "AKIAJZHFB4GIIF3FDWMA",
                            'secret' => "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",
                        ]
                    ]);

                    $result = $s3->putObject([
                        'Bucket' => 'packagingtrackingbun',
                        'Key'    => $idOIE . "/" . $keyname,
                        'SourceFile' => $temp_file_location
                    ]);
                    $dataPathFile[$idImg] = $idOIE . "/" . $keyname;
                }
                $idImg = $idImg + 1;
            }
            $db = $this->PackagingCaffee->getDataSource();
            $observation = $this->request->data['PackagingCaffee']['observation'];
            $observation .= $findPackagingCaffee['PackagingCaffee']['observation'];
            if ($this->PackagingCaffee->updateAll(
                array(
                    'PackagingCaffee.seal_date' => $db->value(date("Y-m-d H:i:s"), 'string'),
                    'PackagingCaffee.seal_1' => $db->value($this->request->data['PackagingCaffee']['seal1']),
                    'PackagingCaffee.seal_2' => $db->value($this->request->data['PackagingCaffee']['seal2']),
                    'PackagingCaffee.users_id' => $this->Session->read('User.id'),
                    'PackagingCaffee.seal1_path' => $db->value($dataPathFile[0], 'string'),
                    'PackagingCaffee.seal2_path' => $db->value($dataPathFile[1], 'string'),
                    'PackagingCaffee.img_ctn' => $db->value($dataPathFile[2], 'string'),
                    'PackagingCaffee.observation' => $db->value($observation, 'string'),
                    'PackagingCaffee.long_ctn' => $db->value($typeCtn['TypeCtn']['long_ctn']),
                    'PackagingCaffee.cooperativa_ctg' => $db->value($this->request->data['PackagingCaffee']['cooperativa_ctg']),
                    'PackagingCaffee.iso_ctn' => $db->value($typeCtn['TypeCtn']['iso_ctn'])
                ),
                array('PackagingCaffee.id' => $idOIE)
            )) {
                $this->Flash->success(__('Se registraron los sellos existosamente!'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('Se genero un inconveniente con el registro. Por favor intento nuevamente'));
            }
        }
        $this->loadModel('TypeCtn');
        $typectn = $this->TypeCtn->find('list', array('fields' => array('TypeCtn.id', 'TypeCtn.type_ctn')));
        $findPackagingCaffee = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $idOIE)));
        $this->set('typectn', $typectn);
        $this->set('findPackagingCaffee', $findPackagingCaffee);
    }

    /**
     * ctnr method
     *
     * @throws NotFoundException
     * @param string $idOIE
     * @return void
     */
    public function ctnr($idOIE = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->loadModel('ReadyContainer');
            $options = array('conditions' => array('PackagingCaffee.' . $this->PackagingCaffee->primaryKey => $idOIE));
            $findPackagingCaffee = $this->PackagingCaffee->find('first', $options);
            $bic = $this->request->data['PackagingCaffee']['bic_container'];
            $readyContainer = array('bic_container' => $bic, 'date_registre' => date('Y-m-d H:i:s'), 'shipping_lines_id' => $findPackagingCaffee['InfoNavy']['shipping_lines_id']);
            if ($this->ReadyContainer->save($readyContainer)) {
                $findPackagingCaffee = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $idOIE)));
                $findPackagingCaffee['PackagingCaffee']['ready_container_id'] = $this->ReadyContainer->id;
                $this->PackagingCaffee->save($findPackagingCaffee);
                return $this->redirect(array('controller' => 'PackagingCaffees', 'action' => 'index'));
            } else {
                $this->Flash->error(__('No se pudo registrar el BIC CTNR. Intentelo nuevamente'));
            }
        }
    }


    /**
     * ctnr method
     *
     * @throws NotFoundException
     * @param string $idOIE
     * @return void
     */
    public function empty_ctnr($idOIE = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('CauseEmptiedCtn');
        $this->loadModel('ReadyContainer');
        $this->loadModel('ServicePackage');
        $this->loadModel('ServicesOrder');
        $this->loadModel('StatePackaging');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('WeighingDownloadCaffee');
        $this->loadModel('WeighingPackagingCaffee');
        $findPackagingCaffee = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $idOIE)));
        $totalServicePackage = 0;
        if ($this->request->is('post')) {
            if ($this->request->data['PackagingCaffee']['state_packaging_id'] == 7) {
                $this->PackagingCaffee->updateAll(
                    array(
                        'PackagingCaffee.state_packaging_id' => 7,
                        'PackagingCaffee.cause_emptied_ctn_id' => $this->request->data['PackagingCaffee']['cause_emptied_ctn_id']
                    ),
                    array('PackagingCaffee.id' => $idOIE)
                );
                $this->Flash->success(__('Se registro un vaciado del contenedor ' . $findPackagingCaffee['ReadyContainer']['bic_container']));
                return $this->redirect(array('controller' => 'PackagingCaffees', 'action' => 'index'));
            } else if ($this->request->data['PackagingCaffee']['state_packaging_id'] == 8) {
                $dataDetailPackagingCaffees = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)));
                $this->PackagingCaffee->updateAll(
                    array(
                        'PackagingCaffee.state_packaging_id' => 8,
                        'PackagingCaffee.cause_emptied_ctn_id' => $this->request->data['PackagingCaffee']['cause_emptied_ctn_id']
                    ),
                    array('PackagingCaffee.id' => $idOIE)
                );
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 8), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE));
                foreach ($dataDetailPackagingCaffees as $detailPackagingCaffee) {
                    $remesa = $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'];
                    $weighingPackagingCaffees = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('WeighingPackagingCaffee.id_oie' => $idOIE)));
                    $palletDevueltos = 0;
                    $sacosDevueltos = 0;
                    foreach ($weighingPackagingCaffees as $weighingPackagingCaffee) {
                        $palletDevueltos += $weighingPackagingCaffee['WeighingPackagingCaffee']['seq_weight_pallet'];
                        $sacosDevueltos += $weighingPackagingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet '];
                    }
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                    $this->RemittancesCaffee->updateAll(
                        array(
                            'RemittancesCaffee.quantity_bag_in_store' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] + $sacosDevueltos,
                            'RemittancesCaffee.quantity_bag_out_store' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] - $sacosDevueltos,
                            'RemittancesCaffee.quantity_in_pallet_caffee' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] + $palletDevueltos,
                            'RemittancesCaffee.quantity_out_pallet_caffee' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] - $palletDevueltos
                        ),
                        array('RemittancesCaffee.id' => $remesa)
                    );
                }
                $this->Flash->success(__('Se registro un vaciado del contenedor ' . $findPackagingCaffee['PackagingCaffee']['id']));
                return $this->redirect(array('controller' => 'PackagingCaffees', 'action' => 'process_oie'));
            }
            if ($this->PackagingCaffee->updateAll(
                array(
                    'PackagingCaffee.emptied_date' => "'" . date('Y-m-d H:i:s') . "'",
                    'PackagingCaffee.active' => 0,
                    'PackagingCaffee.state_packaging_id' => 4,
                    'PackagingCaffee.cause_emptied_ctn_id' => $this->request->data['PackagingCaffee']['cause_emptied_ctn_id']
                ),
                array('PackagingCaffee.id' => $idOIE)
            )) {
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 8), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE));
                foreach ($dataDetailPackagingCaffees as $detailPackagingCaffee) {
                    $remesa = $detailPackagingCaffee['DetailPackagingCaffee']['remittances_caffee_id'];
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.total_weight_net_real' => 0, 'RemittancesCaffee.slot_store_id' => null), array('RemittancesCaffee.id' => $remesa));
                    $this->WeighingDownloadCaffee->deleteAll(array('WeighingDownloadCaffee.remittances_caffee_id' => $remesa));
                    if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] > 0) {
                        $palletCoffee = array(
                            'seq_weight_pallet' => 1,
                            'weight_pallet' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] * $dataRemittancesCaffee['UnitsCaffee']['quantity'],
                            'quantity_bag_pallet' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'],
                            'weighing_date' => "'" . date('Y-m-d H:i:s') . "'",
                            'remittances_caffee_id' => $remesa
                        );
                        $this->WeighingDownloadCaffee->save($palletCoffee);
                    }
                }
                $this->Flash->success(__('Se registro un vaciado del contenedor ' . $findPackagingCaffee['ReadyContainer']['bic_container']));
                return $this->redirect(array('controller' => 'PackagingCaffees', 'action' => 'index'));
            } else {
                $this->Flash->error(__('No se pudo vaciar el contenedor. Intentelo nuevamente'));
            }
        }
        $this->set('findPackagingCaffee', $findPackagingCaffee);
        $this->set('causeEmptiedCtn', $this->CauseEmptiedCtn->find('list', array('fields' => array('CauseEmptiedCtn.id', 'CauseEmptiedCtn.name'))));
        $this->set('servicePackages', $this->ServicePackage->find('list', array('conditions' => array('ServicePackage.name_package LIKE' => '%CTG DESEMBALAJE%'), 'fields' => array('ServicePackage.name_package'))));
    }


    public function allOie($oie = null, $operation = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($operation) {
            if ($oie && $operation == 'cancel') {
                PackagingCaffeesController::cancelOIE($oie);
            }
            if ($oie && $operation == 'pack') {
                PackagingCaffeesController::packOIE($oie);
            }
        }
        $this->layout = 'colaborador';
        $this->PackagingCaffee->recursive = 3;
        $this->loadModel('InfoNavy');
        $this->loadModel('StatePackaging');
        $infoNavies = $this->InfoNavy->find('list', array('fields' => array('InfoNavy.id', 'InfoNavy.proforma')));
        $statePackaging = $this->StatePackaging->find('list', array('fields' => array('StatePackaging.id', 'StatePackaging.name')));
        $this->paginate = array(
            'conditions' => array('PackagingCaffee.jetty' => $this->Session->read('User.jetty')),
            'order' => array('PackagingCaffee.id' => 'DESC')
        );
        $this->set('packagingCaffees', $this->Paginator->paginate('PackagingCaffee'));
        $this->set('statePackaging', $statePackaging);
        $this->set('infoNavies', $infoNavies);
    }

    function cancelOIE($oie)
    {
        $infoOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        if ($infoOIE['PackagingCaffee']['state_packaging_id'] == 1 || $infoOIE['PackagingCaffee']['state_packaging_id'] == 2) {
            date_default_timezone_set('America/Bogota');
            $this->PackagingCaffee->updateAll(
                array(
                    'PackagingCaffee.state_packaging_id' => 6,
                    'PackagingCaffee.active' => 0,
                    'PackagingCaffee.cancelled_date' => "'" . date('Y-m-d H:i:s') . "'",
                ),
                array('PackagingCaffee.id' => $oie)
            );
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('DetailPackagingCaffee');
            $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie)));
            $this->DetailPackagingCaffee->updateAll(array(
                'DetailPackagingCaffee.state' => '4',
                'DetailPackagingCaffee.updated_date' => "'" . date('Y-m-d H:i:s') . "'"
            ), array('DetailPackagingCaffee.packaging_caffee_id' => $oie));
            foreach ($detailPackagingCaffee as $detail) {
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $detail['DetailPackagingCaffee']['remittances_caffee_id'])));
                $this->RemittancesCaffee->updateAll(array(
                    'RemittancesCaffee.total_rad_bag_out' => ($dataRemittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] - $detail['DetailPackagingCaffee']['quantity_radicated_bag_out']),
                    'RemittancesCaffee.state_operation_id' => 2
                ), array('RemittancesCaffee.id' => $detail['DetailPackagingCaffee']['remittances_caffee_id']));
            }
            $this->Flash->success(__('Se ha cancelado la OIE ' . $oie));
            return $this->redirect(array('action' => 'allOie'));
        } else {
            $this->Flash->error(__('No puede cancelar la OIE ' . $oie));
        }
    }

    function packOIE()
    {
        $this->loadModel('DetailPackagingCaffee');
        $this->autoRender = false;
        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;
        $datePack = isset($this->request->query['date']) ? $this->request->query['date'] : null;
        $jornada =  isset($this->request->query['jornally']) ? $this->request->query['jornally'] : null;
        $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie)));
        if ($detailPackagingCaffee) {
            $infoOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
            if ($infoOIE['PackagingCaffee']['state_packaging_id'] == 1 || $infoOIE['PackagingCaffee']['state_packaging_id'] == 2 && !PackagingCaffeesController::checkPackagingLock($oie)) {
                date_default_timezone_set('America/Bogota');
                $this->PackagingCaffee->updateAll(
                    array(
                        'PackagingCaffee.state_packaging_id' => 1,
                        'PackagingCaffee.created_date' => "'" . date_format(date_create($datePack), "Y-m-d H:i:s") . "'",
                        'PackagingCaffee.jornally' => "'" . $jornada . "'",
                    ),
                    array('PackagingCaffee.id' => $oie)
                );
                $this->loadModel('DetailPackagingCaffee');
                $this->loadModel('RemittancesCaffee');
                $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie)));
                $this->DetailPackagingCaffee->updateAll(array(
                    'DetailPackagingCaffee.state' => '1',
                    'DetailPackagingCaffee.updated_date' => "'" . date('Y-m-d H:i:s') . "'"
                ), array('DetailPackagingCaffee.packaging_caffee_id' => $oie));
                foreach ($detailPackagingCaffee as $detail) {
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id' => 3), array('RemittancesCaffee.id' => $detail['DetailPackagingCaffee']['remittances_caffee_id']));
                }

                $result = array('id_msj' => 1, 'msj' => 'Se ha confirmado la orden de embalaje con OIE' . $oie);
                return json_encode($result);
            } else {
                $result = array('id_msj' => 2, 'msj' => 'No puede embalar la OIE ' . $oie . ' debido a que hay bloqueos de remesa');
                return json_encode($result);
            }
        } else {
            $result = array('id_msj' => 3, 'msj' => 'No puede embalar la OIE ' . $oie . ' debido a que no hay remesas amparadas');
            return json_encode($result);
        }
    }

    function checkPackagingLock($oie)
    {
        $this->loadModel('DetailPackagingCaffee');
        $remittancesNoveltys = $this->DetailPackagingCaffee->find('all', array(
            'conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie),
            'joins' => array(
                array(
                    'table' => 'remittances_caffee_has_noveltys_caffee',
                    'alias' => 'RemittancesCaffeeHasNoveltysCaffee',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'DetailPackagingCaffee.remittances_caffee_id = RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id'
                    ),
                ),
                array(
                    'table' => 'noveltys_caffee',
                    'alias' => 'NoveltysCaffee',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'NoveltysCaffee.id = RemittancesCaffeeHasNoveltysCaffee.noveltys_caffee_id',
                    ),
                )
            ),
            'fields' => array(
                'RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id',
                'NoveltysCaffee.name',
                'RemittancesCaffeeHasNoveltysCaffee.active'
            )
        ));
        foreach ($remittancesNoveltys as $noveltys) {
            if ($noveltys['RemittancesCaffeeHasNoveltysCaffee']['active'] === true) {
                return true;
            }
        }
        return false;
    }

    public function add()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('InfoNavy');
        $this->loadModel('ReadyContainer');
        $this->loadModel('User');
        $this->loadModel('PackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('ShippingLine');
        $this->loadModel('ServicePackage');
        $this->loadModel('ServicesOrder');
        $this->loadModel('ShippingLine');
        $this->loadModel('Custom');
        $this->loadModel('Departaments');
        $this->loadModel('NavyAgent');

        $this->loadModel('DetailsServicesToCaffee');
        if ($this->request->is('post')) {
            $this->PackagingCaffee->create();
            $dataUser = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
            if ($this->Session->read('User.opera') == 1) {
                $infoNavies = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.proforma' => $this->request->data['PackagingCaffee']['info_navy_id'])));
                if ($infoNavies) {
                    $this->request->data['PackagingCaffee']['state_packaging_id'] = 2;
                    $this->request->data['PackagingCaffee']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['PackagingCaffee']['jetty'] = $dataUser['User']['cod_city_operation'];
                    $this->request->data['PackagingCaffee']['info_navy_id'] = $infoNavies['InfoNavy']['id'];
                    if ($this->PackagingCaffee->save($this->request->data)) {
                        $this->Flash->success(__('La OIE ha sido guardada con �xito'));
                        return $this->redirect(array('controller' => 'DetailPackagingCaffees', 'action' => 'add', '?' => ['oie_id' => $this->PackagingCaffee->id]));
                    } else {
                        $this->Flash->error(__('La OIE no pudo ser guardada.'));
                    }
                } else {
                    $this->Flash->error(__('La Proforma ingresada no existe'));
                }
            } else {
                $array_time_ini = $this->request->data['PackagingCaffee']['time_start'];
                $array_time_end = $this->request->data['PackagingCaffee']['time_end'];
                $this->InfoNavy->create();
                $infoNavy = array(
                    'packaging_type' => $this->request->data['PackagingCaffee']['mode_packaging'],
                    'motorship_name' => $this->request->data['PackagingCaffee']['info_navy_motonave'],
                    'packaging_mode' => $this->request->data['PackagingCaffee']['type_packaging'],
                    'booking' => strtoupper($this->request->data['PackagingCaffee']['info_navy_booking']),
                    'travel_num' => strtoupper($this->request->data['PackagingCaffee']['info_navy_trip']),
                    'destiny' => strtoupper($this->request->data['PackagingCaffee']['info_navy_pod']),
                    'navy_agent_id' => $this->request->data['PackagingCaffee']['info_navy_navy'],
                    'proforma' => 'N/A CTG',
                    'shipping_lines_id' => $this->request->data['PackagingCaffee']['info_navy_line'],
                    'customs_id' => $this->request->data['PackagingCaffee']['info_navy_customid'],
                    'created_date' => $this->request->data['PackagingCaffee']['created_date'],
                    'elements_adicional' => strtoupper($this->request->data['PackagingCaffee']['info_navy_adicional_element'])
                );

                $this->InfoNavy->save($infoNavy);
                $this->ReadyContainer->create();
                $registerContainer = array(
                    'bic_container' => strtoupper($this->request->data['PackagingCaffee']['bic_container']),
                    'shipping_lines_id' => $this->request->data['PackagingCaffee']['info_navy_line'],
                    'date_registre' => $this->request->data['PackagingCaffee']['created_date']
                );
                $this->ReadyContainer->save($registerContainer);

                $packagingCaffee = array(
                    'created_date' => $this->request->data['PackagingCaffee']['packaging_date'],
                    'state_packaging_id' => 3,
                    'jetty' => $this->request->data['PackagingCaffee']['jetty'],
                    'cooperativa_ctg' => $this->request->data['PackagingCaffee']['cooperativa_ctg'],
                    'ready_container_id' => $this->ReadyContainer->id,
                    'info_navy_id' => $this->InfoNavy->id,
                    'seal_3' => $this->request->data['PackagingCaffee']['seal_3'],
                    'seal_4' => $this->request->data['PackagingCaffee']['seal_4'],
                    //'observation' => strtoupper($this->request->data['PackagingCaffee']['observation']),
                    'autorization' => $this->request->data['PackagingCaffee']['autorizacion'],
                    'packaging_date' => $this->request->data['PackagingCaffee']['packaging_date'],
                    'iso_ctn' => $this->request->data['PackagingCaffee']['iso_ctn'],
                    'users_id' => $this->Session->read('User.id'),
                    'qta_bags_packaging' => $this->request->data['PackagingCaffee']['qta_unit'],
                    'weight_net_cont' => $this->request->data['PackagingCaffee']['weight_container'],
                    'exporter_cod' => $this->request->data['PackagingCaffee']['exporter_cod'],
                    'time_start' => $array_time_ini['hour'] . ':' . $array_time_ini['min'],
                    'time_end' => $array_time_end['hour'] . ':' . $array_time_end['min']
                );
                if ($this->PackagingCaffee->save($packagingCaffee)) {
                    $this->PackagingCaffee->updateAll(array('PackagingCaffee.rem_ref' => $this->ServicesOrder->id), array('PackagingCaffee.id' => $this->PackagingCaffee->id));
                    if ($this->request->data['PackagingCaffee']['lots']) {
                        $remittancesToPack = $this->request->data['PackagingCaffee']['lots'];
                        foreach ($remittancesToPack as $remittance) {
                            if (strpos($remittance, '-')) {
                                $infoRemittance = explode("-", $remittance);
                                $detailPackagingCaffee = [
                                    'remittances_caffee_id' => intval($infoRemittance[0]),
                                    'packaging_caffee_id' => $this->PackagingCaffee->id,
                                    'quantity_radicated_bag_out' => intval($infoRemittance[1]),
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'updated_date' => date('Y-m-d H:i:s'),
                                    'state' => '2',
                                    'tara_packaging' => 0,
                                ];
                                if ($this->DetailPackagingCaffee->save($detailPackagingCaffee)) {
                                    $dataToUpdate = ['state_operation_id' => 4];
                                    $this->RemittancesCaffee->updateAll($dataToUpdate, ['RemittancesCaffee.id' => intval($infoRemittance[0])]);
                                }
                            }
                        }
                    }
                    $this->Flash->success(__('El embalaje ha sido registrado correctamente'));
                    return $this->redirect(array('action' => 'index'));
                }
            }
        }
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 4))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 3))));
        $this->set('infoNavies', $this->InfoNavy->find('list', array('fields' => array('InfoNavy.proforma'))));
        $dataLines = $this->ShippingLine->find('all');
        foreach ($dataLines as $key => $value) {
            $listLines[$value['ShippingLine']['id']] = $value['ShippingLine']['id'] . " " . $value['ShippingLine']['business_name'] . " ";
        }
        $this->set('maritimeLines', $listLines);
        $dataCustom = $this->Custom->find('all');
        foreach ($dataCustom as $key => $value) {
            $listCustom[$value['Custom']['id']] = $value['Custom']['id'] . " " . $value['Custom']['cia_name'] . " ";
        }
        $this->set(compact('listCustom'));
        $dataNavyAgent = $this->NavyAgent->find('all');
        foreach ($dataNavyAgent as $key => $value) {
            $listNavyAgent[$value['NavyAgent']['id']] = $value['NavyAgent']['id'] . " " . $value['NavyAgent']['name'] . " ";
        }
        $this->set(compact('listNavyAgent'));
        $this->set('operator', $usersSamplers);
        $this->set('driver', $usersDriver);
        $dataDeparament = $this->Departaments->find('all', array('conditions' => array('Departaments.cod_city' => $this->Session->read('User.jetty'))));
        foreach ($dataDeparament as $key => $value) {
            $listDeparaments[$value['Departaments']['name']] = $value['Departaments']['name'] . " ";
        }
        //debug($listDeparaments);exit;
        $this->set('citys', $listDeparaments);
        $this->set('servicePackages', $this->ServicePackage->find('list', array('conditions' => array('ServicePackage.name_package LIKE' => '%CTG EMBALAJE%'), 'fields' => array('ServicePackage.name_package'))));
    }


    public function edit($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->PackagingCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid info navy'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('InfoNavy');
        $this->loadModel('Clients');
        $this->loadModel('User');
        $this->loadModel('Custom');
        $this->loadModel('ShippingLine');
        $this->loadModel('ReadyContainer');
        $this->loadModel('NavyAgent');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $dataPackagingCaffee = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $id)));
        $dataJetty = $dataPackagingCaffee['PackagingCaffee']['jetty'];
        if ($this->request->is(array('post', 'put'))) {
            if (!empty($dataPackagingCaffee)) {
                if ($dataJetty == 'BUN') {
                    $infoNavies = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.proforma' => $this->request->data['PackagingCaffee']['info_navy_id'])));
                    if ($infoNavies) {
                        $this->request->data['PackagingCaffee']['info_navy_id'] = $infoNavies['InfoNavy']['id'];
                        if ($this->PackagingCaffee->save($this->request->data)) {
                            $this->Flash->success(__('Se actualizo el registro exitosamente'));
                            return $this->redirect(array('action' => 'allOie'));
                        } else {
                            $this->Flash->error(__('No se registro la información. Por favor intentelo nuevamente.'));
                        }
                    } else {
                        $this->Flash->error(__('La Proforma ingresada no existe'));
                    }
                } else if ($dataJetty == 'SPRC' ||  $dataJetty == 'CONTECAR' || $dataJetty == 'COMPAS' || $dataJetty == 'STM' || $dataJetty == 'SPIA') {
                    $result1 = $this->ReadyContainer->updateAll(
                        array(
                            'ReadyContainer.bic_container' => "'" . $this->request->data['PackagingCaffee']['bic_container'] . "'"
                        ),
                        array('ReadyContainer.id' => $this->request->data['PackagingCaffee']['bic_container_id'])
                    );
                    $result2 = $this->InfoNavy->updateAll(
                        array(
                            'InfoNavy.booking' => "'" . $this->request->data['PackagingCaffee']['info_navy_booking'] . "'",
                            'InfoNavy.travel_num' => "'" . $this->request->data['PackagingCaffee']['info_navy_trip'] . "'",
                            'InfoNavy.long_container' => "'" . $this->request->data['PackagingCaffee']['long_container'] . "'",
                            'InfoNavy.motorship_name' => "'" . $this->request->data['PackagingCaffee']['info_navy_motonave'] . "'",
                            'InfoNavy.shipping_lines_id' => "'" . $this->request->data['PackagingCaffee']['info_navy_line'] . "'",
                            'InfoNavy.customs_id' => "'" . $this->request->data['PackagingCaffee']['info_navy_customid'] . "'",
                            'InfoNavy.navy_agent_id' => "'" . $this->request->data['PackagingCaffee']['info_navy_navy'] . "'",
                            'InfoNavy.destiny' => "'" . $this->request->data['PackagingCaffee']['info_navy_trip'] . "'",
                            'InfoNavy.packaging_type' => "'" . $this->request->data['PackagingCaffee']['mode_packaging'] . "'",
                            'InfoNavy.packaging_mode' => "'" . $this->request->data['PackagingCaffee']['type_packaging'] . "'",
                            'InfoNavy.elements_adicional' => "'" . $this->request->data['PackagingCaffee']['info_navy_adicional_element'] . "'",
                            'InfoNavy.packaging_mode' => "'" . $this->request->data['PackagingCaffee']['type_packaging'] . "'"
                        ),
                        array('InfoNavy.id' => $this->request->data['PackagingCaffee']['info_navy_id'])
                    );
                    $result3 = $this->PackagingCaffee->updateAll(
                        array(
                            'PackagingCaffee.jetty' => "'" . $this->request->data['PackagingCaffee']['jetty'] . "'",
                            'PackagingCaffee.user_tarja' => "'" . $this->request->data['PackagingCaffee']['user_tarja'] . "'",
                            'PackagingCaffee.user_driver' => "'" . $this->request->data['PackagingCaffee']['user_driver'] . "'",
                            'PackagingCaffee.iso_ctn' => "'" . $this->request->data['PackagingCaffee']['iso_ctn'] . "'",
                            'PackagingCaffee.exporter_cod' => "'" . $this->request->data['PackagingCaffee']['exporter_cod'] . "'",
                            'PackagingCaffee.observation' => "'" . $this->request->data['PackagingCaffee']['observation'] . "'",
                            'PackagingCaffee.weight_net_cont' => "'" . $this->request->data['PackagingCaffee']['weight_container'] . "'",
                            'PackagingCaffee.qta_bags_packaging' => "'" . $this->request->data['PackagingCaffee']['qta_unit'] . "'",
                            'PackagingCaffee.autorization' => "'" . $this->request->data['PackagingCaffee']['autorizacion'] . "'",
                            'PackagingCaffee.seal_3' => "'" . $this->request->data['PackagingCaffee']['seal_3'] . "'",
                            'PackagingCaffee.seal_4' => "'" . $this->request->data['PackagingCaffee']['seal_4'] . "'",
                            'PackagingCaffee.cooperativa_ctg' => "'" . $this->request->data['PackagingCaffee']['cooperativa_ctg'] . "'",
                            'PackagingCaffee.time_start' => "'" . $this->request->data['PackagingCaffee']['time_start']['hour'] . ":" . $this->request->data['PackagingCaffee']['time_start']['min'] . "'",
                            'PackagingCaffee.time_end' => "'" . $this->request->data['PackagingCaffee']['time_end']['hour'] . ":" . $this->request->data['PackagingCaffee']['time_end']['min'] . "'",
                            'PackagingCaffee.created_date' => "'" . $this->request->data['PackagingCaffee']['packaging_date']['year'] . "-" . $this->request->data['PackagingCaffee']['packaging_date']['month'] . "-" . $this->request->data['PackagingCaffee']['packaging_date']['day'] . " " . $this->request->data['PackagingCaffee']['packaging_date']['hour'] . ":" . $this->request->data['PackagingCaffee']['packaging_date']['min'] . ":00'",
                            'PackagingCaffee.packaging_date' => "'" . $this->request->data['PackagingCaffee']['packaging_date']['year'] . "-" . $this->request->data['PackagingCaffee']['packaging_date']['month'] . "-" . $this->request->data['PackagingCaffee']['packaging_date']['day'] . " " . $this->request->data['PackagingCaffee']['packaging_date']['hour'] . ":" . $this->request->data['PackagingCaffee']['packaging_date']['min'] . ":00'",
                        ),
                        array('PackagingCaffee.id' => $id)
                    );
                    if ($result1 && $result2 && $result3) {
                        $this->Flash->success(__('Se actualizó el registro correctamente'));
                        //Se borran los registros anteriores
                        $detailPackagingCaffees = $this->DetailPackagingCaffee->find('all', ['conditions' => ['packaging_caffee_id' => $id]]);
                        foreach($detailPackagingCaffees as $detailItem){
                            $dataToUpdate = ['state_operation_id' => 2];
                            $this->RemittancesCaffee->updateAll($dataToUpdate, ['RemittancesCaffee.id' => intval($detailItem["DetailPackagingCaffee"]['remittances_caffee_id'])]);
                        }
                        $this->DetailPackagingCaffee->deleteAll(['DetailPackagingCaffee.packaging_caffee_id' => $id], false);
                        //Se crean los nuevos registros
                        $remittancesToPack = $this->request->data['PackagingCaffee']['lots'];
                        foreach ($remittancesToPack as $remittance) {
                            if (strpos($remittance, '-')) {
                                $infoRemittance = explode("-", $remittance);
                                $detailPackagingCaffee = [
                                    'remittances_caffee_id' => intval($infoRemittance[0]),
                                    'packaging_caffee_id' => $id,
                                    'quantity_radicated_bag_out' => intval($infoRemittance[1]),
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'updated_date' => date('Y-m-d H:i:s'),
                                    'state' => '2',
                                    'tara_packaging' => 0,
                                ];
                                if ($this->DetailPackagingCaffee->save($detailPackagingCaffee)) {
                                    $dataToUpdate = ['state_operation_id' => 4];
                                    $this->RemittancesCaffee->updateAll($dataToUpdate, ['RemittancesCaffee.id' => intval($infoRemittance[0])]);
                                }
                            }
                        }
                        return $this->redirect(array('controller' => 'ViewPackaging2s', 'action' => 'view', $id));
                    } else {
                        $this->Flash->error(__('No se registro la información. Por favor intentelo nuevamente.'));
                    }
                }
            }
        } else {
            $options = array('conditions' => array('PackagingCaffee.' . $this->PackagingCaffee->primaryKey => $id));
            $this->request->data = $this->PackagingCaffee->find('first', $options);
            $infoNavies = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.id' => $this->request->data['PackagingCaffee']['info_navy_id'])));
            $this->request->data['PackagingCaffee']['info_navy_id'] = $infoNavies['InfoNavy']['proforma'];
            $this->request->data['PackagingCaffee']['info_navy_proforma'] = $infoNavies['InfoNavy']['proforma'];
            $this->set('infoNavies', $this->InfoNavy->find('list', array('fields' => array('InfoNavy.proforma'))));
            $this->set('dataPackagingCaffee', $dataPackagingCaffee);
            $dataLines = $this->ShippingLine->find('all');
            foreach ($dataLines as $key => $value) {
                $listLines[$value['ShippingLine']['id']] = $value['ShippingLine']['id'] . " " . $value['ShippingLine']['business_name'] . " ";
            }
            $this->set('maritimeLines', $listLines);
            $dataCustom = $this->Custom->find('all');
            foreach ($dataCustom as $key => $value) {
                $listCustom[$value['Custom']['id']] = $value['Custom']['id'] . " " . $value['Custom']['cia_name'] . " ";
            }
            $this->set(compact('listCustom'));
            $dataNavyAgent = $this->NavyAgent->find('all');
            foreach ($dataNavyAgent as $key => $value) {
                $listNavyAgent[$value['NavyAgent']['id']] = $value['NavyAgent']['id'] . " " . $value['NavyAgent']['name'] . " ";
            }
            $this->set(compact('listNavyAgent'));
            $this->set('dataClient', $this->Clients->find('first', array('conditions' => array('Clients.' . $this->Clients->primaryKey => $dataPackagingCaffee['PackagingCaffee']['exporter_cod']))));
            $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 4))));
            $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 3))));
            $this->set('operator', $usersSamplers);
            $this->set('driver', $usersDriver);
            $detailPackagingCaffees = [];
            foreach ($this->request->data['DetailPackagingCaffee'] as $detailItem) {
                $item['id'] = $detailItem['remittances_caffee_id'] . "-" . $detailItem['quantity_radicated_bag_out'];
                $item['text'] = "Remesa = " . $detailItem['remittances_caffee_id'] . " , Cantidad = " . $detailItem['quantity_radicated_bag_out'];
                array_push($detailPackagingCaffees, $item);
            }
            $this->set('detailPackagingCaffees', json_encode($detailPackagingCaffees));
        }
    }

    public function report()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profile') == "Basculero") {
            $this->layout = 'basculero';
        } else {
            $this->layout = 'colaborador';
        }
        if ($this->request->is('post')) {
        }
        $this->loadModel('InfoNavy');
        $this->loadModel('StoresCaffee');
        $this->set('infoNavies', $this->InfoNavy->find('list', array('fields' => array('InfoNavy.proforma'))));
        $this->set('warehouse', $this->StoresCaffee->find('list', array('fields' => array('StoresCaffee.id', 'StoresCaffee.store_name')), array('conditions' => array('StoresCaffee.city_location' => 'BUN'))));
    }

    public function search()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('Client');
        $searchInfoList = array();
        if ($this->request->is('post')) {
            $this->PackagingCaffee->recursive = 3;
            if ($this->request->data['PackagingCaffee']['oie']) {
                $searchInfoList = array($this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $this->request->data['PackagingCaffee']['oie']))));
            } else if ($this->request->data['PackagingCaffee']['proforma']) {
                $searchInfoList = $this->PackagingCaffee->find('all', array('conditions' => array('InfoNavy.proforma' => $this->request->data['PackagingCaffee']['proforma'])));
            } else if ($this->request->data['PackagingCaffee']['booking']) {
                $searchInfoList = $this->PackagingCaffee->find('all', array('conditions' => array('InfoNavy.booking' => $this->request->data['PackagingCaffee']['booking'])));
            } else if ($this->request->data['PackagingCaffee']['remesa']) {
                $dateDetail = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['PackagingCaffee']['remesa'])));
                $searchInfoList = $this->PackagingCaffee->find('all', array('conditions' => array('PackagingCaffee.id' => $dateDetail[0]['DetailPackagingCaffee']['packaging_caffee_id'])));
            } else if ($this->request->data['PackagingCaffee']['lote']) {
                $loteClient = $this->request->data['PackagingCaffee']['lote'];
                $porciones = explode("-", $loteClient);
                $client = $this->Client->find('first', array('conditions' => array('Client.exporter_code' => $porciones[0])));
                $remesa = $this->RemittancesCaffee->find('all', array('conditions' => array(
                    'RemittancesCaffee.lot_caffee' => $porciones[1],
                    'RemittancesCaffee.client_id' => $client['Client']['id']
                )));
                $dateDetail = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.remittances_caffee_id' => $remesa[0]['RemittancesCaffee']['id'])));
                $searchInfoList = $this->PackagingCaffee->find('all', array('conditions' => array('PackagingCaffee.id' => $dateDetail[0]['DetailPackagingCaffee']['packaging_caffee_id'])));
            }
            $this->loadModel('ShippingLine');
            $this->loadModel('NavyAgent');
            $this->loadModel('StatePackaging');
            $this->set('shippingLines', $this->ShippingLine->find('list', array('fields' => array('ShippingLine.id', 'ShippingLine.business_name'))));
            $this->set('navyAgent', $this->NavyAgent->find('list', array('fields' => array('NavyAgent.id', 'NavyAgent.name'))));
            $this->set('statePackaging', $this->StatePackaging->find('list', array('fields' => array('StatePackaging.id', 'StatePackaging.name'))));
        }
        $this->set('searchInfoList', $searchInfoList);
    }

    public function unpack($oie = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            date_default_timezone_set('America/Bogota');
            if ($this->PackagingCaffee->updateAll(
                array(
                    'PackagingCaffee.cause_emptied_ctn_id' => $this->request->data['PackagingCaffee']['cause_emptied_ctn_id'],
                    'PackagingCaffee.observation' => "'" . $this->request->data['PackagingCaffee']['observation'] . "'",
                    'PackagingCaffee.state_packaging_id' => 4,
                    'PackagingCaffee.cancelled_date' => "'" . date('Y-m-d H:i:s') . "'"
                ),
                array('PackagingCaffee.id' => $oie)
            )) {
                $this->Flash->success(__('Se vació el contenedor exitosamente'));
                return $this->redirect(array('action' => 'returnRemittances', $oie));
            }
        } else {
            $infoOIE = array($this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie))));
            $this->set('OIE', $oie);
            $this->set('infoOIE', $infoOIE);
            $this->loadModel('NoveltysCoffee');
            $this->set('noveltysCoffee', $this->NoveltysCoffee->find('list', array('fields' => array('NoveltysCoffee.id', 'NoveltysCoffee.name'))));
        }
    }

    function findInfoNaviesByCreateDateOIE($date)
    {
        $shippingLines = $this->PackagingCaffee->find('all', array(
            'conditions' => array('DATE(PackagingCaffee.created_date)' => date('Y-m-d', strtotime($date))),
            'joins' => array(array(
                'table' => 'shipping_lines',
                'alias' => 'ShippingLines',
                'type' => 'LEFT',
                'conditions' => array('ShippingLines.id = InfoNavy.shipping_lines_id')
            )),
            'fields' => array(
                'ShippingLines.id',
                'ShippingLines.business_name'
            ),
            'group' => 'ShippingLines.business_name',
        ));
        $this->autoRender = false;
        if ($shippingLines) {
            return json_encode($shippingLines);
        } else {
            return "";
        }
    }

    function findMotoShipByCreateDateOIE($date)
    {
        $motoShips = $this->PackagingCaffee->find('all', array(
            'conditions' => array('DATE(PackagingCaffee.created_date)' => date('Y-m-d', strtotime($date))),
            'fields' => array(
                'InfoNavy.id',
                'InfoNavy.motorship_name'
            ),
            'group' => 'InfoNavy.motorship_name',
        ));
        $this->autoRender = false;
        if ($motoShips) {
            return json_encode($motoShips);
        } else {
            return "";
        }
    }

    public function returnRemittances($oie = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->loadModel('RemittancesCaffee');
            date_default_timezone_set('America/Bogota');
            $remittance = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['PackagingCaffee']['remittance'])));
            if ($this->RemittancesCaffee->updateAll(
                array(
                    'RemittancesCaffee.state_operation_id' => 2,
                    'RemittancesCaffee.quantity_bag_in_store' => ($remittance['RemittancesCaffee']['quantity_bag_in_store'] + $this->request->data['PackagingCaffee']['bags_out']),
                    'RemittancesCaffee.quantity_bag_out_store' => ($remittance['RemittancesCaffee']['quantity_bag_out_store'] - $this->request->data['PackagingCaffee']['bags_out']),
                    'RemittancesCaffee.quantity_in_pallet_caffee' => ($remittance['RemittancesCaffee']['quantity_in_pallet_caffee'] + $this->request->data['PackagingCaffee']['pallets_out']),
                    'RemittancesCaffee.quantity_out_pallet_caffee' => ($remittance['RemittancesCaffee']['quantity_out_pallet_caffee'] - $this->request->data['PackagingCaffee']['pallets_out']),
                    'RemittancesCaffee.is_active' => 1,
                    'RemittancesCaffee.tare_packaging' => 0,
                    'RemittancesCaffee.ref_packaging' => "''",
                    'RemittancesCaffee.total_rad_bag_out' => ($remittance['RemittancesCaffee']['total_rad_bag_out'] - $this->request->data['PackagingCaffee']['bags_out']),
                    'RemittancesCaffee.slot_store_id' => $this->request->data['PackagingCaffee']['warehouse_position'],
                    'RemittancesCaffee.updated_dated' => "'" . date('Y-m-d H:i:s') . "'"
                ),
                array('RemittancesCaffee.id' => $this->request->data['PackagingCaffee']['remittance'])
            )) {
                $this->loadModel('DetailPackagingCaffee');
                $this->DetailPackagingCaffee->updateAll(
                    array(
                        'DetailPackagingCaffee.state' => 3
                    ),
                    array(
                        'DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['PackagingCaffee']['remittance'],
                        'DetailPackagingCaffee.packaging_caffee_id' => $oie
                    )
                );
                $this->loadModel('WeighingPackagingCaffee');
                $this->WeighingPackagingCaffee->updateAll(
                    array(
                        'WeighingPackagingCaffee.seq_weight_pallet' => 0
                    ),
                    array(
                        'WeighingPackagingCaffee.remittances_cafee_id' => $this->request->data['PackagingCaffee']['remittance'],
                        'WeighingPackagingCaffee.id_oie' => $oie
                    )
                );
                $this->Flash->success(__('Se regreso la remesa ' . $this->request->data['PackagingCaffee']['remittance']));
                return $this->redirect(array('action' => 'returnRemittances', $oie));
            }
        } else {
            PackagingCaffeesController::renderReturnRemittance($oie);
        }
    }

    public function renderReturnRemittance($oie)
    {
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('StoresCaffee');
        $dataToReturn = $this->DetailPackagingCaffee->find('all', array(
            'conditions' => array(
                'DetailPackagingCaffee.packaging_caffee_id' => $oie,
                'WeighingPackagingCaffee.id_oie' => $oie,
                'DetailPackagingCaffee.state' => 2,
                'OR' => array(array('RemittancesCaffee.state_operation_id' => 4), array('RemittancesCaffee.state_operation_id' => 7))
            ),
            'joins' => array(
                array(
                    'table' => 'weighing_packaging_caffee',
                    'alias' => 'WeighingPackagingCaffee',
                    'type' => 'LEFT',
                    'conditions' =>
                    array('WeighingPackagingCaffee.remittances_cafee_id = DetailPackagingCaffee.remittances_caffee_id')
                )
            ),
            'fields' => array(
                'WeighingPackagingCaffee.remittances_cafee_id',
                'COUNT(WeighingPackagingCaffee.id)',
                'SUM(WeighingPackagingCaffee.quantity_bag_pallet)'
            )
        ));
        if ($dataToReturn[0]['WeighingPackagingCaffee']['remittances_cafee_id'] != null) {
            $dataToReturnRemittance = array();
            foreach ($dataToReturn as $data) {
                array_push($dataToReturnRemittance, array(
                    'remittance' => $data['WeighingPackagingCaffee']['remittances_cafee_id'],
                    'quantityBags' => $data[0]['SUM(`WeighingPackagingCaffee`.`quantity_bag_pallet`)'],
                    'quantityPallets' => $data[0]['COUNT(`WeighingPackagingCaffee`.`id`)']
                ));
            }
            $warehouse = $this->StoresCaffee->find('list', array('fields' => array('StoresCaffee.id', 'StoresCaffee.store_name')));
            $this->set('DataToReturn', $dataToReturnRemittance);
            $this->set('oie', $oie);
            $this->set('warehouse', $warehouse);
        } else {
            return $this->redirect(array('action' => 'allOie'));
        }
    }


















    public function empty_loaded_oie()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('ReadyContainer');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('ViewPackaging');
        $this->loadModel('Client');

        $searchInfoList = array();
        if ($this->request->is('post')) {
            $this->PackagingCaffee->recursive = 3;
            if ($this->request->data['PackagingCaffee']['oie']) {
                $searchInfoList = array($this->ViewPackaging->find('first', array('conditions' => array('ViewPackaging.oie' => $this->request->data['PackagingCaffee']['oie']))));
            } else if ($this->request->data['PackagingCaffee']['lote']) {
                $loteClient = $this->request->data['PackagingCaffee']['lote'];
                $searchInfoList = $this->ViewPackaging->find('all', array('conditions' => array('ViewPackaging.lotes LIKE' => '%' . $loteClient . '%'), 'order' => array('oie' => 'desc')));
            } else if ($this->request->data['PackagingCaffee']['bic_container']) {
                $searchInfoList = $this->ViewPackaging->find('all', array('conditions' => array('ViewPackaging.bic_ctn' => $this->request->data['PackagingCaffee']['bic_container']), 'order' => array('oie' => 'desc')));
            }
            $this->loadModel('ShippingLine');
            $this->loadModel('NavyAgent');
            $this->loadModel('StatePackaging');
            $this->set('shippingLines', $this->ShippingLine->find('list', array('fields' => array('ShippingLine.id', 'ShippingLine.business_name'))));
            $this->set('navyAgent', $this->NavyAgent->find('list', array('fields' => array('NavyAgent.id', 'NavyAgent.name'))));
            $this->set('statePackaging', $this->StatePackaging->find('list', array('fields' => array('StatePackaging.id', 'StatePackaging.name'))));
        }
        $this->set('searchInfoList', $searchInfoList);
    }


    public function search_packaging()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'terminal';
        $this->loadModel('ReadyContainer');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('ViewPackaging');
        $this->loadModel('Client');

        $searchInfoList = array();
        if ($this->request->is('post')) {
            $this->PackagingCaffee->recursive = 3;
            if ($this->request->data['PackagingCaffee']['oie']) {
                $searchInfoList = array($this->ViewPackaging->find('first', array('conditions' => array('and' => array(array('ViewPackaging.oie' => $this->request->data['PackagingCaffee']['oie']), array('ViewPackaging.packaging_date > ' => '2019-02-03 00:00:00'))))));
            } else if ($this->request->data['PackagingCaffee']['lote']) {
                $loteClient = $this->request->data['PackagingCaffee']['lote'];
                $searchInfoList = $this->ViewPackaging->find('all', array('conditions' => array('and' => array(array('ViewPackaging.lotes LIKE' => '%' . $loteClient . '%'), array('ViewPackaging.packaging_date > ' => '2019-02-03 00:00:00'))), 'order' => array('oie' => 'desc')));
            } else if ($this->request->data['PackagingCaffee']['bic_container']) {
                $searchInfoList = $this->ViewPackaging->find('all', array('conditions' => array('and' => array(array('ViewPackaging.bic_ctn' => $this->request->data['PackagingCaffee']['bic_container']), array('ViewPackaging.packaging_date > ' => '2019-02-03 00:00:00'))), 'order' => array('oie' => 'desc')));
            }
            $this->loadModel('ShippingLine');
            $this->loadModel('NavyAgent');
            $this->loadModel('StatePackaging');
            $this->set('shippingLines', $this->ShippingLine->find('list', array('fields' => array('ShippingLine.id', 'ShippingLine.business_name'))));
            $this->set('navyAgent', $this->NavyAgent->find('list', array('fields' => array('NavyAgent.id', 'NavyAgent.name'))));
            $this->set('statePackaging', $this->StatePackaging->find('list', array('fields' => array('StatePackaging.id', 'StatePackaging.name'))));
        }
        $this->set('searchInfoList', $searchInfoList);
    }


    function processEmptyOIE()
    {
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('WeighingPackagingCaffee');
        $this->autoRender = false;
        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;
        $observacion =  isset($this->request->query['observation']) ? $this->request->query['observation'] : null;

        $infoOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        if ($infoOIE['PackagingCaffee']['state_packaging_id'] == 3) {
            date_default_timezone_set('America/Bogota');

            $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 4, 'PackagingCaffee.empty_date' => "'" . date('Y-m-d H:i:s') . "'", 'PackagingCaffee.observation' => "'" . $observacion . "'"), array('PackagingCaffee.id' => $oie));

            $infoDetailPackaging = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie)));
            $infoWeighingPackaging = $this->WeighingPackagingCaffee->find('first', array('conditions' => array('WeighingPackagingCaffee.id_oie' => $oie), 'order' => array('WeighingPackagingCaffee.id DESC')));
            foreach ($infoDetailPackaging as $infoDetail) {
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 3), array('DetailPackagingCaffee.packaging_caffee_id' => $oie));

                $this->RemittancesCaffee->updateAll(array(
                    //'RemittancesCaffee.quantity_bag_in_store' => $infoDetail['RemittancesCaffee']['quantity_bag_in_store'] + $infoDetail['DetailPackagingCaffee']['quantity_radicated_bag_out'], 
                    //'RemittancesCaffee.quantity_bag_out_store' => $infoDetail['RemittancesCaffee']['quantity_bag_out_store'] - $infoDetail['DetailPackagingCaffee']['quantity_radicated_bag_out'], 
                    //'RemittancesCaffee.quantity_in_pallet_caffee' => $infoDetail['RemittancesCaffee']['quantity_in_pallet_caffee'] + $infoWeighingPackaging['WeighingPackagingCaffee']['seq_weight_pallet'], 
                    //'RemittancesCaffee.quantity_out_pallet_caffee' => $infoDetail['RemittancesCaffee']['quantity_out_pallet_caffee'] - $infoWeighingPackaging['WeighingPackagingCaffee']['seq_weight_pallet'], 
                    'RemittancesCaffee.state_operation_id' => 11,
                    'RemittancesCaffee.total_rad_bag_out' => $infoDetail['RemittancesCaffee']['total_rad_bag_out'] - $infoDetail['DetailPackagingCaffee']['quantity_radicated_bag_out'],
                ), array('RemittancesCaffee.id' => $infoDetail['DetailPackagingCaffee']['remittances_caffee_id']));
            }

            $result = array('id_msj' => 1, 'msj' => 'Se ha Vaciado la OIE ' . $oie . ' exitosamente.');
            return json_encode($result);
        } else {
            $result = array('id_msj' => 2, 'msj' => 'No puede vaciar la OIE ' . $oie);
            return json_encode($result);
        }
    }

    function emptyOIE()
    {
        $this->loadModel('ReadyContainer');
        $this->loadModel('WeighingPackagingCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->autoRender = false;
        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;
        $datePack = isset($this->request->query['date']) ? $this->request->query['date'] : null;
        $jornada =  isset($this->request->query['jornally']) ? $this->request->query['jornally'] : null;
        $contenedor =  isset($this->request->query['ready_cantainer_new']) ? $this->request->query['ready_cantainer_new'] : null;
        $observacion =  isset($this->request->query['observation']) ? $this->request->query['observation'] : null;

        $infoOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        if ($infoOIE['PackagingCaffee']['state_packaging_id'] == 3) {
            date_default_timezone_set('America/Bogota');

            $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 9, 'PackagingCaffee.empty_date' => "'" . date('Y-m-d H:i:s') . "'", 'PackagingCaffee.observation' => "'" . $observacion . "'"), array('PackagingCaffee.id' => $oie));

            $this->ReadyContainer->create();
            $registerContainer = array(
                'bic_container' => $contenedor,
                'date_registre' => date('Y-m-d H:i:s'),
                'shipping_lines_id' => 39
            );
            $this->ReadyContainer->save($registerContainer);
            $infoContainer = $this->ReadyContainer->find('first', array('conditions' => array('ReadyContainer.bic_container' => $contenedor), 'order' => array('ReadyContainer.id' => 'desc')));
            $this->PackagingCaffee->create();
            $registerOIENew = array(
                'created_date' => date('Y-m-d H:i:s'),
                'weight_to_out' => $infoOIE['PackagingCaffee']['weight_to_out'],
                'state_packaging_id' => 3,
                'ready_container_id' => $infoContainer['ReadyContainer']['id'],
                'active' => $infoOIE['PackagingCaffee']['active'],
                'cause_emptied_ctn_id' => $infoOIE['PackagingCaffee']['cause_emptied_ctn_id'],
                'info_navy_id' => $infoOIE['PackagingCaffee']['info_navy_id'],
                'observation' => $infoOIE['PackagingCaffee']['observation'],
                'packaging_date' => $datePack,
                'cancelled_date' => NULL,
                'iso_ctn' => NULL,
                'seal_1' => NULL,
                'seal_2' => NULL,
                'users_id' => $this->Session->read('User.id'),
                'seal_date' => NULL,
                'seal1_path' => NULL,
                'seal2_path' => NULL,
                'img_ctn' => NULL,
                'checking_ctn_id' => NULL,
                'traceability_packaging_id' => $infoOIE['PackagingCaffee']['traceability_packaging_id'],
                'exporter_cod' => $infoOIE['PackagingCaffee']['exporter_cod'],
                'weight_net_cont' => $infoOIE['PackagingCaffee']['weight_net_cont'],
                'time_start' => $infoOIE['PackagingCaffee']['time_start'],
                'time_end' => $infoOIE['PackagingCaffee']['time_end'],
                'autorization' => $infoOIE['PackagingCaffee']['autorization'],
                'qta_bags_packaging' => $infoOIE['PackagingCaffee']['qta_bags_packaging'],
                'rem_ref' => $infoOIE['PackagingCaffee']['rem_ref'],
                'emptied_date' => $infoOIE['PackagingCaffee']['emptied_date'],
                'seal_3' => $infoOIE['PackagingCaffee']['seal_3'],
                'seal_4' => $infoOIE['PackagingCaffee']['seal_4'],
                'jetty' => $infoOIE['PackagingCaffee']['jetty'],
                'jornally' => $jornada
            );
            $this->PackagingCaffee->save($registerOIENew);

            $infoOIENew = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.ready_container_id' => $infoContainer['ReadyContainer']['id'])));
            $infoWeighingPacks = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('WeighingPackagingCaffee.id_oie' => $oie)));
            $infoWeighingPacksRemittances = $this->WeighingPackagingCaffee->find('first', array('conditions' => array('WeighingPackagingCaffee.id_oie' => $oie)));
            $infoRemittances = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $infoWeighingPacksRemittances['WeighingPackagingCaffee']['remittances_cafee_id'])));

            foreach ($infoWeighingPacks as $infoWeighingPack) {
                $registerWeighingPack = array(
                    'weight_pallet' => $infoWeighingPack['WeighingPackagingCaffee']['weight_pallet'],
                    'quantity_bag_pallet' => $infoWeighingPack['WeighingPackagingCaffee']['quantity_bag_pallet'],
                    'weighing_date' => $infoWeighingPack['WeighingPackagingCaffee']['weighing_date'],
                    'remittances_cafee_id' => $infoWeighingPack['WeighingPackagingCaffee']['remittances_cafee_id'],
                    'is_fraction' => $infoWeighingPack['WeighingPackagingCaffee']['is_fraction'],
                    'seq_weight_pallet' => $infoWeighingPack['WeighingPackagingCaffee']['seq_weight_pallet'],
                    'id_oie' => $infoOIENew['PackagingCaffee']['id'],
                );
                $this->WeighingPackagingCaffee->create();
                $this->WeighingPackagingCaffee->save($registerWeighingPack);
            }

            $infoDetailPackaging = $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie)));
            //            debug($infoDetailPackaging);exit;
            foreach ($infoDetailPackaging as $infoDetailPack) {
                $registerDetailPack = array(
                    'remittances_caffee_id' => $infoDetailPack['RemittancesCaffee']['id'],
                    'packaging_caffee_id' => $infoOIENew['PackagingCaffee']['id'],
                    'quantity_radicated_bag_out' => $infoDetailPack['DetailPackagingCaffee']['quantity_radicated_bag_out'],
                    'created_date' => date('Y-m-d H:i:s'),
                    'updated_date' => date('Y-m-d H:i:s'),
                    'state' => 2,
                    //                                'tara_packaging' => $infoDetailPack['DetailPackagingCaffee']['tare_packaging'],
                    'users_id' => $infoDetailPack['DetailPackagingCaffee']['users_id'],
                );
                $this->DetailPackagingCaffee->create();
                $this->DetailPackagingCaffee->save($registerDetailPack);
            }

            $result = array('id_msj' => 1, 'msj' => 'Se ha Vaciado y Llenado de la OIE ' . $oie);
            return json_encode($result);
        } else {
            $result = array('id_msj' => 2, 'msj' => 'No puede vaciar y llenar la OIE ' . $oie);
            return json_encode($result);
        }
    }

    function updateContainer()
    {
        $this->loadModel('ReadyContainer');
        $this->autoRender = false;

        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;
        $contenedor =  isset($this->request->query['ready_cantainer_new']) ? $this->request->query['ready_cantainer_new'] : null;

        $infoOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        //        debug($infoOIE);exit;
        if ($infoOIE['PackagingCaffee']['ready_container_id']) {
            date_default_timezone_set('America/Bogota');
            $this->ReadyContainer->updateAll(array('ReadyContainer.bic_container' => "'" . $contenedor . "'"), array('ReadyContainer.id' => $infoOIE['PackagingCaffee']['ready_container_id']));

            $result = array('id_msj' => 1, 'msj' => 'Se ha Modificado el Contenedor ' . $contenedor . ' con la OIE ' . $oie);
            return json_encode($result);
        } else {
            $result = array('id_msj' => 2, 'msj' => 'No puede Modificado el Contenedor ' . $contenedor . ' con la OIE ' . $oie);
            return json_encode($result);
        }
    }


    function breakfreeLoteOIE()
    {
        $this->autoRender = false;
        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;

        $infoPackagingOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        //        debug($infoPackagingOIE);exit;
        if ($infoPackagingOIE['PackagingCaffee']['state_packaging_id'] == 1) {
            date_default_timezone_set('America/Bogota');
            $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 2, 'PackagingCaffee.ready_container_id' => null), array('PackagingCaffee.id' => $oie));

            $result = array('id_msj' => 1, 'msj' => 'Se ha Liberado el Contenedor ' . $infoPackagingOIE['ReadyContainer']['bic_container'] . ' con la OIE ' . $oie);
            return json_encode($result);
        } else {
            $result = array('id_msj' => 2, 'msj' => 'No puede Liberar el Contenedor ' . $infoPackagingOIE['ReadyContainer']['bic_container'] . ' con la OIE ' . $oie);
            return json_encode($result);
        }
    }

    public function delete($oie)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('ReadyContainer');
        $this->loadModel('WeighingPackagingCaffee');
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('InfoNavy');
        $this->autoRender = false;
        $dataPackagingOIE = $this->PackagingCaffee->find('first', array('conditions' => array('PackagingCaffee.id' => $oie)));
        if ($dataPackagingOIE['PackagingCaffee']['jetty'] != 'BUN') {
            $this->DetailPackagingCaffee->deleteAll(['DetailPackagingCaffee.packaging_caffee_id' => $oie]);
            $this->WeighingPackagingCaffee->deleteAll(['WeighingPackagingCaffee.id_oie' => $oie]);
            $this->PackagingCaffee->deleteAll(['PackagingCaffee.id' => $oie]);
            $this->InfoNavy->deleteAll(['InfoNavy.id' => $dataPackagingOIE['PackagingCaffee']['info_navy_id']]);
            $this->ReadyContainer->deleteAll(['ReadyContainer.id' => $dataPackagingOIE['PackagingCaffee']['ready_container_id']]);
            $this->Flash->success(__('Se elimino correctamente el registro! '));
            return $this->redirect($this->referer());
        }
    }
}
