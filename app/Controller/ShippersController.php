<?php
App::uses('AppController', 'Controller');
/**
 * Shippers Controller
 *
 * @property Shipper $Shipper
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 */
class ShippersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash');

}
