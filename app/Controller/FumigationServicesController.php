<?php
App::uses('AppController', 'Controller');
/**
 * FumigationServices Controller
 *
 * @property FumigationService $FumigationService
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class FumigationServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('User');
            $this->loadModel('RemittancesCaffeeHasFumigationService');
            $listUserByCenterOperation = $this->User->find('list',array('conditions' => array('and'=>array('User.departaments_id' => $this->Session->read('User.departaments_id')),array('User.profiles_id' => 1))));
            $this->Paginator->settings = array(
            'limit' => 20,
            //'conditions' => array('FumigationService.departament_id' => $this->Session->read('User.departaments_id'))
            'order' => array('HookupStatus.id' => 'ASC')
        );
        //debug($data);exit;
		$this->FumigationService->recursive = 3;
		$this->set('fumigationServices', $this->Paginator->paginate());
    }
    
    /**
 * index method
 *
 * @return void
 */
	public function indexByLotCoffee() {
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->loadModel('RemittancesCaffeeHasFumigationService');
        $data = $this->RemittancesCaffeeHasFumigationService->find('all',[
            'limit'=>20
        ]);
        $this->set('fumigationServices', $data);
    }

    /**
 * index method
 *
 * @return void
 */
	public function indexByLotCoffeeSchedule() {
        if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->loadModel('RemittancesCaffeeHasFumigationService');
        $this->loadModel('ScheduleCoffeeFumigation'); 
        $data = $this->ScheduleCoffeeFumigation->find('all',[
            'limit'=>20
        ]);
        $this->set('fumigationServices', $this->Paginator->paginate());
    }
        
        
         /**
 * index method
 *
 * @return void
 */
	public function complete($id = null) {
            $this->loadModel('RemittancesCaffeeHasFumigationService');
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
            $db = $this->FumigationService->getDataSource();
            $this->FumigationService->updateAll(
                array(
                    'FumigationService.finished_date'=> $db->value(date('Y-m-d h:i:s'),'string'),
					'FumigationService.hookup_status_id'=> 4,
                    'FumigationService.completed'=> true),
                    array('FumigationService.id' => $id));
            /*$remittancesCaffees = $this->RemittancesCaffeeHasFumigationService->find('all',array('conditions' => array('RemittancesCaffeeHasFumigationService.fumigation_services_id' =>$id)));
            foreach($remittancesCaffees as $remittancesCaffee){
                $this->RemittancesCaffeeHasNoveltysCaffee->updateAll(array('RemittancesCaffeeHasNoveltysCaffee.active'=>0),array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id' => $remittancesCaffee['RemittancesCaffee']['id'],'RemittancesCaffeeHasNoveltysCaffee.noveltys_caffee_id' =>1));
            }*/
            return $this->redirect(array('controller' => 'FumigationServices','action' => 'index'));
    }
    
    /**
     * index method
     *
     * @return void
     */
	public function process($id = null) {
        $this->loadModel('RemittancesCaffeeHasFumigationService');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('ScheduleCoffeeFumigation');
        $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
        $db = $this->FumigationService->getDataSource();
        $this->FumigationService->updateAll(
            array(
                'FumigationService.hookup_status_id'=> 3),
                array('FumigationService.id' => $id));
        /*$remittancesCaffees = $this->RemittancesCaffeeHasFumigationService->find('all',array('conditions' => array('RemittancesCaffeeHasFumigationService.fumigation_services_id' =>$id)));
        foreach($remittancesCaffees as $remittancesCaffee){
            $this->RemittancesCaffeeHasNoveltysCaffee->create();
            $data = [
                'remittances_caffee_id'=>$remittancesCaffee['RemittancesCaffee']['id'],
                'noveltys_caffee_id'=>1,
                'RemittancesCaffeeHasNoveltysCaffee.created_date'=>"'".date('Y-m-d h:i:s')."'",
                'active'=>1
            ];
            //debug($data);exit;
            $this->RemittancesCaffeeHasNoveltysCaffee->save($data);
        }*/
        $this->ScheduleCoffeeFumigation->updateAll(['ScheduleCoffeeFumigation.status'=>2],['ScheduleCoffeeFumigation.fumigation_services_id'=>$id]);
        return $this->redirect(array('controller' => 'FumigationServices','action' => 'index'));
}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FumigationService->exists($id)) {
			throw new NotFoundException(__('Invalid fumigation service'));
		}
                $this->layout = 'colaborador';
		$options = array('conditions' => array('FumigationService.' . $this->FumigationService->primaryKey => $id));
		$this->set('fumigationService', $this->FumigationService->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('ServicesOrder');
		if ($this->request->is('post')) {
			$this->FumigationService->create();
                        $this->ServicesOrder->create();
                        $servicesOrder = array();
                        $servicesOrder['ServicesOrder']['id']=null;
                        $servicesOrder['ServicesOrder']['created_date']=date('Y-m-d h:i:s');
                        //$servicesOrder['ServicesOrder']['closed_date']=date('Y-m-d h:i:s');
                        $servicesOrder['ServicesOrder']['exporter_code']=$this->request->data['FumigationService']['exporter_id'];
                        $servicesOrder['ServicesOrder']['total_valor']=0.0;
                        $servicesOrder['ServicesOrder']['instructions']=$this->request->data['FumigationService']['instructions'];;
                        $servicesOrder['ServicesOrder']['create_user']=$this->Session->read('User.id');
                        $servicesOrder['ServicesOrder']['closed']=false;
                        $this->ServicesOrder->save($servicesOrder);
                        $this->request->data['FumigationService']['start_date']=$this->request->data['FumigationService']['start_date'];
                        $this->request->data['FumigationService']['quantity_poison_used']=0;
                        $this->request->data['FumigationService']['finished_date']=0;
                        $this->request->data['FumigationService']['resquest_service_date']=date('Y-m-d h:i:s');
                        $this->request->data['FumigationService']['bandejas_used']=0;
                        $this->request->data['FumigationService']['ppm_teory']=0;
                        $this->request->data['FumigationService']['exporter_id']=$this->request->data['FumigationService']['client_id'];
                        $this->request->data['FumigationService']['created_user']=$this->Session->read('User.id');
                        $this->request->data['FumigationService']['services_orders_id']=$this->ServicesOrder->id;
			if ($this->FumigationService->save($this->request->data)) {
				$this->Flash->success(__('Paso #1 existoso...'));
				return $this->redirect(array('controller' => 'RemittancesCaffeeHasFumigationServices','action' => 'add','?' => ['services_orders_id' => $this->ServicesOrder->id,'exporter_id' =>$this->request->data['FumigationService']['exporter_id'],'fumigation_services_id' => $this->FumigationService->id]));
			} else {
				$this->Flash->error(__('No se pudo registrar. Intentelo nuevamente.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FumigationService->exists($id)) {
			throw new NotFoundException(__('Invalid fumigation service'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FumigationService->save($this->request->data)) {
				$this->Flash->success(__('The fumigation service has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The fumigation service could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FumigationService.' . $this->FumigationService->primaryKey => $id));
			$this->request->data = $this->FumigationService->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FumigationService->id = $id;
		if (!$this->FumigationService->exists()) {
			throw new NotFoundException(__('Invalid fumigation service'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FumigationService->delete()) {
			$this->Flash->success(__('The fumigation service has been deleted.'));
		} else {
			$this->Flash->error(__('The fumigation service could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function searchServices(){
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('RemittancesCaffee');
            $this->loadModel('Client');
            $this->loadModel('RemittancesCaffeeHasFumigationService');
            $fumigationServices = array();
            
            if($this->request->is('post')){
                $this->FumigationService->recursive = 3;
                if ($this->request->data['FumigationService']['codigo']) {
                    $fumigationServices = $this->FumigationService->find('all', array('conditions' => array('FumigationService.id' => $this->request->data['FumigationService']['codigo'])));
                } else if ($this->request->data['FumigationService']['remision']){
                    $porciones = explode("-", $this->request->data['FumigationService']['remision']);
                    $fumigationServices = $this->FumigationService->find('all', array('conditions' => array('FumigationService.services_orders_id' => $porciones[1])));
                }else if($this->request->data['FumigationService']['lote']){
                    $porciones = explode("-", $this->request->data['FumigationService']['lote']);
                    $client = $this->Client->find('all',array('conditions' => array('Client.exporter_code'=>$porciones[0])));
                    $dataRemittance = $this->RemittancesCaffee->find('list',array('fields'=>array('RemittancesCaffee.id'), 'conditions' => array('AND' =>array(
                                                                                                    'RemittancesCaffee.lot_caffee' => $porciones[1],
                                                                                                    'RemittancesCaffee.client_id' => $client[0]['Client']['id']
                                                                                                ))));
                    $remittancesHasFumigation = $this->RemittancesCaffeeHasFumigationService->find('first', array( 'conditions' => array('RemittancesCaffeeHasFumigationService.remittances_caffee_id' => $dataRemittance)));
                    $fumigationServices = $this->FumigationService->find('all', array('conditions' => array('FumigationService.id' => $remittancesHasFumigation['RemittancesCaffeeHasFumigationService']['fumigation_services_id'])));
                }elseif($this->request->data['FumigationService']['remesa']){
                    $remittancesHasFumigation = $this->RemittancesCaffeeHasFumigationService->find('first', array( 'conditions' => array('RemittancesCaffeeHasFumigationService.remittances_caffee_id' => $this->request->data['FumigationService']['remesa'])));
                    $fumigationServices = $this->FumigationService->find('all', array('conditions' => array('FumigationService.id' => $remittancesHasFumigation['RemittancesCaffeeHasFumigationService']['fumigation_services_id'])));
                }
            }
            $this->set('fumigationServices', $fumigationServices);
        }
        
}
