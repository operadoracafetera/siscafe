<?php
App::uses('AppController', 'Controller');
/**
 * MaterialCoffees Controller
 *
 * @property MaterialCoffee $MaterialCoffee
 * @property PaginatorComponent $Paginator
 */
class MaterialCoffeesController extends AppController {

    
    public function getListMaterialCodes($isFNC){
        $this->autoRender = false;
        $dataMaterialCodes= $this->MaterialCoffee->find('all',array('conditions'=>array('MaterialCoffee.itis_fnc'=>$isFNC)));
        return json_encode($dataMaterialCodes);
    }
    
    public function getMaterialCode($idMaterialCode){
        $this->autoRender = false;
        $dataMaterialCode= $this->MaterialCoffee->find('first',array('conditions'=>array('MaterialCoffee.id'=>$idMaterialCode)));
        return json_encode($dataMaterialCode);
    }

}
