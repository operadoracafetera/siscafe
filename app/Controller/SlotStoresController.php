<?php

App::uses('AppController', 'Controller');

/**
 * SlotStores Controller
 *
 * @property SlotStore $SlotStore
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SlotStoresController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->SlotStore->recursive = 0;
        $this->set('slotStores', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SlotStore->exists($id)) {
            throw new NotFoundException(__('Invalid slot store'));
        }
        $options = array('conditions' => array('SlotStore.' . $this->SlotStore->primaryKey => $id));
        $this->set('slotStore', $this->SlotStore->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SlotStore->create();
            if ($this->SlotStore->save($this->request->data)) {
                $this->Flash->success(__('The slot store has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The slot store could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SlotStore->exists($id)) {
            throw new NotFoundException(__('Invalid slot store'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->SlotStore->save($this->request->data)) {
                $this->Flash->success(__('The slot store has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The slot store could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SlotStore.' . $this->SlotStore->primaryKey => $id));
            $this->request->data = $this->SlotStore->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SlotStore->id = $id;
        if (!$this->SlotStore->exists()) {
            throw new NotFoundException(__('Invalid slot store'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->SlotStore->delete()) {
            $this->Flash->success(__('The slot store has been deleted.'));
        } else {
            $this->Flash->error(__('The slot store could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
    public function findAvailableSlotStoreByStoreId($storeId = null){
        $this->loadModel('RemittancesCaffee');
        $slotUsed = $this->RemittancesCaffee->find('list', array('fields' => array('RemittancesCaffee.slot_store_id', 'RemittancesCaffee.slot_store_id'), 'conditions' => array('AND'=>array('RemittancesCaffee.slot_store_id !='=> null,'RemittancesCaffee.state_operation_id !=' => 4))));
	$options = array('fields' => array('SlotStore.id', 'SlotStore.name_space'), 'conditions' => array('AND'=>array('SlotStore.active'=>true,"NOT" => array("SlotStore.id" => $slotUsed), 'SlotStore.stores_caffee_id' =>$storeId )));
        $slotsEmpty = $this->SlotStore->find('list', $options);
        $this->autoRender = false;
        if ($slotsEmpty) {
            return json_encode($slotsEmpty);
        } else {
            return "";
        }
    }
    
    public function findBusySlotStoreByStoreId($storeId = null){
        $this->loadModel('RemittancesCaffee');
        $slotUsedIds = $this->RemittancesCaffee->find('list', array('fields' => array('RemittancesCaffee.slot_store_id'), 'conditions' => array('RemittancesCaffee.state_operation_id !=' => 4)));
        $slotsUsed = $this->SlotStore->find('list', array('conditions'=>array('AND' => array('SlotStore.stores_caffee_id'=>$storeId,'SlotStore.id'=>$slotUsedIds))));
        $this->autoRender = false;
        if ($slotsUsed) {
            return json_encode($slotsUsed);
        } else {
            return "";
        }
    }
    
    public function findDisableSlotStoreByStoreId($storeId = null){
        $this->loadModel('RemittancesCaffee');
        $slotsDisables = $this->SlotStore->find('list', array('conditions'=>array('AND' => array('SlotStore.stores_caffee_id'=>$storeId,'SlotStore.active'=>false))));
        $this->autoRender = false;
        if ($slotsDisables) {
            return json_encode($slotsDisables);
        } else {
            return "";
        }
    }
    
    public function slotDisabled() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';       
        if($this->request->is('post')){
        $name_space =$this->request->data['SlotStore']['name_space'];             
        $slotSelected = $this->SlotStore->find('first', array('conditions' => array('SlotStore.name_space' => $name_space)));
          
            if ($slotSelected['SlotStore']['id'] != null && $this->SlotStore->updateAll(
                    array(
                        'SlotStore.active' => $this->request->data['SlotStore']['active']),
                        array('SlotStore.id' => $slotSelected['SlotStore']['id']))) {             
                    $this->Flash->success(__('Se actualizo el registro exitosamente'));
                    return $this->redirect(array('action' => 'slotDisabled'));
                } else {
                    $this->Flash->error(__('Posición ocupada por favor seleccione otro lugar. '));
                    return $this->redirect(array('action' => 'slotDisabled'));
                }        
        }
    }
        

}
