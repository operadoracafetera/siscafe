<?php

App::uses('AppController', 'Controller');

/**
 * Clients Controller
 * ShippingLines
 */
class ShippingLinesController extends AppController {

    public function findById($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $shippingLine = $this->ShippingLine->find('first', array('conditions' => array('ShippingLine.id' => $id)));
        return json_encode($shippingLine);
    }

}
