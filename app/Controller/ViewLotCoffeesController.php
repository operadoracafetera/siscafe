<?php
App::uses('AppController', 'Controller');
/**
 * ViewLotCoffees Controller
 *
 * @property ViewLotCoffee $ViewLotCoffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ViewLotCoffeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index($flag=null) {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
		$this->ViewLotCoffee->recursive = 0;
                $this->Paginator->settings = array(
                        'limit' => 15,
                        'conditions' => array('ViewLotCoffee.departaments_id' =>$this->Session->read('User.departaments_id')),
                        'order' => array('ViewLotCoffee.download_caffee_date' => 'DESC'));
		$this->set('viewLotCoffees', $this->Paginator->paginate());
                $this->set('departaments_id',$this->Session->read('User.departaments_id'));
                $this->set('option_index',$flag);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($st_lot_coffee = null) {
                if (!$this->Session->read('User.id')) {
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $parameters = explode("-", $st_lot_coffee);
                $lot_coffee = $parameters[0];
                $client_id = $parameters[1];
                $this->layout = 'colaborador';
                $this->loadModel('RemittancesCaffee');
                $remittancesCaffees = $this->RemittancesCaffee->find('all',
                        array('conditions' => array('AND' => 
                            array(
                                'RemittancesCaffee.lot_caffee' =>$lot_coffee,
                                'RemittancesCaffee.client_id' =>$client_id,
                            ))));
                $this->set('remittancesCaffees',$remittancesCaffees);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ViewLotCoffee->create();
			if ($this->ViewLotCoffee->save($this->request->data)) {
				$this->Flash->success(__('The view lot coffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The view lot coffee could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ViewLotCoffee->exists($id)) {
			throw new NotFoundException(__('Invalid view lot coffee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ViewLotCoffee->save($this->request->data)) {
				$this->Flash->success(__('The view lot coffee has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The view lot coffee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ViewLotCoffee.' . $this->ViewLotCoffee->primaryKey => $id));
			$this->request->data = $this->ViewLotCoffee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ViewLotCoffee->id = $id;
		if (!$this->ViewLotCoffee->exists()) {
			throw new NotFoundException(__('Invalid view lot coffee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ViewLotCoffee->delete()) {
			$this->Flash->success(__('The view lot coffee has been deleted.'));
		} else {
			$this->Flash->error(__('The view lot coffee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
