<?php

App::uses('AppController', 'Controller');
App::uses('RemittancesCaffeeHasFumigationServicesController', 'Controller');
App::uses('DetailPackakgingCrossdockingController', 'Controller');
/**
 * RemittancesCaffees Controller
 *
 * @property RemittancesCaffee $RemittancesCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RemittancesCaffeesController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function index($idColaborate = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profiles_id') == 1) {
            $this->layout = 'colaborador';
        } else if ($this->Session->read('User.profiles_id') == 9) {
            $this->layout = 'invitado';
        } else if ($this->Session->read('User.profile') == "Funcionario FNC") {
            $this->layout = 'fnc';
        }
        $this->loadModel('User');
        $dateUser = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        if ($idColaborate == null) {
            $this->RemittancesCaffee->recursive = 3;
            $this->Paginator->settings = array(
                'limit' => 30,
                'order' => array('RemittancesCaffee.created_date' => 'DESC'),
                'conditions' => array('RemittancesCaffee.jetty' => $dateUser['User']['departaments_id'])
            );
            $data = $this->Paginator->paginate('RemittancesCaffee');
            $this->set('remittancesCaffees', $data);
        } else {
            $this->RemittancesCaffee->recursive = 3;
            $this->Paginator->settings = array(
                'limit' => 30,
                'order' => array('RemittancesCaffee.created_date' => 'DESC'),
                'conditions' => array('RemittancesCaffee.jetty' => $dateUser['User']['departaments_id'])
            );
            $data = $this->Paginator->paginate('RemittancesCaffee');
            $this->set('remittancesCaffees', $data);
            $this->set('idColaborate', $idColaborate);
        }
        $this->set(array(
            'remittancesCaffees' => $data,
            '_serialize' => array('remittancesCaffees')
        ));
    }

    /**
     * index method
     *
     * @return void
     */
    public function return_index()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->loadModel('User');
        $this->loadModel('SlotStore');
        $this->loadModel('WeighingReturnCoffee');
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $this->RemittancesCaffeeReturnsCaffee->recursive = 2;
        //debug($this->Session->read('User.store'));exit;
        $this->Paginator->settings = array(
            'conditions' => array('and' => array('RemittancesCaffeeReturnsCaffee.state' => 1, 'ReturnsCaffee.jetty' => $this->Session->read('User.centerId'))),
            'order' => array('RemittancesCaffee.created_date' => 'DESC'),
            'limit' => 15
        );
        $data = $this->Paginator->paginate('RemittancesCaffeeReturnsCaffee');
        $remittancesCaffeeReturnsCaffees = $this->Paginator->paginate('RemittancesCaffeeReturnsCaffee');
        $arrayCantBagsOut = array();
        foreach ($remittancesCaffeeReturnsCaffees as $remittancesCaffeeReturnsCaffee) {
            $id_detalle = $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['id'];
            $bagReturned = $this->WeighingReturnCoffee->find('all', array('fields' => array('sum(WeighingReturnCoffee.quantity_bag_pallet) AS sacosdevueltos'), 'conditions' => array('and' => array(array('WeighingReturnCoffee.return_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id']), array('WeighingReturnCoffee.remittances_caffee_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['remittances_caffee_id'])))));
            if ($bagReturned[0][0]['sacosdevueltos'] != null) {
                $arrayCantBagsOut[] = array($id_detalle => $bagReturned[0][0]['sacosdevueltos']);
            } else {
                $arrayCantBagsOut[] = array($id_detalle => 0);
            }
        }
        $datatotal = array('data' => $data, 'cantBag' => $arrayCantBagsOut);
        //debug($datatotal);exit;
        $this->set('remittancesCaffees', $datatotal);
    }

    /**
     * reweight_index method
     *
     * @return void
     */
    public function reweight_index()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $this->loadModel('SlotStore');
        //$userLogin = $this->User->find('first',array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        //$listUserByCenterOperation = $this->User->find('list',array('conditions' => array('and'=>array('User.departaments_id' => $userLogin['User']['departaments_id']),array('User.profiles_id' => 1))));
        $slotStores = $this->SlotStore->find('list', array('conditions' => array('SlotStore.stores_caffee_id' => $this->Session->read('User.store'))));
        $this->RemittancesCaffee->recursive = 3;
        $this->Paginator->settings = array(
            'conditions' => array('and' =>
            array(
                'RemittancesCaffee.slot_store_id' => $slotStores,
                'RemittancesCaffee.state_operation_id' => 10
            )),
            'order' => array('RemittancesCaffee.created_date' => 'DESC'),
            'limit' => 10
        );
        $data = $this->Paginator->paginate('RemittancesCaffee');
        $this->set('remittancesCaffees', $data);
        $this->set('departaments_id', $this->Session->read('User.departaments_id'));
    }



    /**
     * index method
     *
     * @return void
     */
    public function sampler_index()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $userLogin = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        $listUserByCenterOperation = $this->User->find('list', array('conditions' => array('and' => array('User.departaments_id' => $userLogin['User']['departaments_id']), array('User.profiles_id' => 1))));
        $this->RemittancesCaffee->recursive = 3;
        $this->Paginator->settings = array(
            'conditions' => array('RemittancesCaffee.user_register' => $listUserByCenterOperation),
            'order' => array('RemittancesCaffee.created_date' => 'DESC'),
            'limit' => 10
        );
        $data = $this->Paginator->paginate('RemittancesCaffee');
        $this->set('remittancesCaffees', $data);
        $this->set('departaments_id', $this->Session->read('User.departaments_id'));
    }

    /**
     * reweight method
     *
     * @return void
     */
    public function reweight($idRem = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('Client');
        $this->loadModel('WeighingDownloadCaffee');
        if ($this->RemittancesCaffee->updateAll(
            array(
                'RemittancesCaffee.state_operation_id' => 10,
                'RemittancesCaffee.quantity_bag_in_store' => 0,
                'RemittancesCaffee.quantity_in_pallet_caffee' => 0,
                'RemittancesCaffee.total_weight_net_real' => 0
            ),
            array('RemittancesCaffee.id' => $idRem)
        )) {
            $this->WeighingDownloadCaffee->deleteAll(array('WeighingDownloadCaffee.remittances_caffee_id' => $idRem), true);
            $this->Flash->success(__('Se procede a realizar el repesaje de la remesa ' . $idRem));
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function store($idRem = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('Client');
        $this->loadModel('TypeUnit');
        $this->loadModel('Custom');
        $this->loadModel('MarkCaffee');
        $this->loadModel('Shipper');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('ServicesOrder');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('CitySource');
        $this->loadModel('ServicePackage');
        $this->loadModel('SlotStore');
        $this->loadModel('User');
        $this->loadModel('PackingCaffee');
        $db = $this->RemittancesCaffee->getDataSource();
        if ($this->request->is('post')) {
            //debug($this->request->data);exit;
            $unit_selected = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $this->request->data['RemittancesCaffee']['units_cafee_id'])));
            if ($this->RemittancesCaffee->updateAll(
                array(
                    'RemittancesCaffee.slot_store_id' => $this->request->data['RemittancesCaffee']['slot_store_id'],
                    'RemittancesCaffee.staff_sample_id' => $this->request->data['RemittancesCaffee']['staff_sample_id'],
                    'RemittancesCaffee.staff_driver_id' => $this->request->data['RemittancesCaffee']['staff_driver_id'],
                    'RemittancesCaffee.units_cafee_id' => $this->request->data['RemittancesCaffee']['units_cafee_id'],
                    'RemittancesCaffee.observation' => $db->value($this->request->data['RemittancesCaffee']['observation'], 'string'),
                    'RemittancesCaffee.total_weight_net_nominal' => ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity'])
                ),
                array('RemittancesCaffee.id' => $idRem)
            )) {
                if ($this->request->data['RemittancesCaffee']['exporter_code'] == '001') {
                    $this->ServicesOrder->create();
                    $servicesOrderDescargue = array(
                        'created_date' => date('Y-m-d h:i:s'),
                        'exporter_code' => $this->request->data['RemittancesCaffee']['exporter_code'],
                        'closed_date' => date('Y-m-d h:i:s'),
                        'approve_user' => $this->Session->read('User.id'),
                        'closed' => 1,
                        'create_user' => $this->Session->read('User.id'),
                    );
                    $this->ServicesOrder->save($servicesOrderDescargue);
                    if ($this->request->data['RemittancesCaffee']['microlot'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00136')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => 1,
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE CAFE MICRO LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 5) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00068')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE CAFE BIG BAG LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 2) {
                        $unitsDownload = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                        if ($unitsDownload >= 1 && $unitsDownload <= 10) {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00003')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAFE CAJAS LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        } else if ($unitsDownload >= 11 && $unitsDownload <= 100) {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00004')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAFE CAJAS LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        } else {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00005')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAFE CAJAS LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00002')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE SACOS CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00002')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE CAFE GRAINPRO LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    }
                    //muestreo
                    if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = ($itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.40);
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.40),
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE GRAINPRO LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.40);
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.40,
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 5) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = ($itemServices['valor'] * ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 14.23) * 0.40);
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.40),
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    }
                } else {
                    $this->ServicesOrder->create();
                    $servicesOrderDescargue = array(
                        'created_date' => date('Y-m-d h:i:s'),
                        'exporter_code' => $this->request->data['RemittancesCaffee']['exporter_code'],
                        'closed_date' => date('Y-m-d h:i:s'),
                        'approve_user' => $this->Session->read('User.id'),
                        'closed' => 1,
                        'create_user' => $this->Session->read('User.id'),
                    );
                    $this->ServicesOrder->save($servicesOrderDescargue);
                    if ($this->request->data['RemittancesCaffee']['microlot'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00137')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => 1,
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE MICRO LOTE CAFE' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00069')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE SACOS CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 2) {
                        $unitsDownload = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                        if ($unitsDownload >= 1 && $unitsDownload <= 10) {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00070')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAJAS CAFE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        } else if ($unitsDownload >= 11 && $unitsDownload <= 100) {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00071')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAJAS CAFE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        } else {
                            $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00072')));
                            $totalServiceOrder = 0;
                            foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                                $this->DetailsServicesToCaffee->create();
                                $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                                $detailsServicesToCaffee = array(
                                    'services_orders_id' => $this->ServicesOrder->id,
                                    'service_package_id' => $servicePackage['ServicePackage']['id'],
                                    'remittances_caffee_id' => $idRem,
                                    'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'items_services_id' => $itemServices['items_services_id'],
                                    'value' => $valueItem,
                                    'completed' => true,
                                    'observation' => 'DESCARGUE CAJAS CAFE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                        $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                    'created_date' => date('Y-m-d h:i:s'),
                                    'document' => ''
                                );
                                $totalServiceOrder += $valueItem;
                                $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                                $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                            }
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00069')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE CAFE GRAINPRO LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 5) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00135')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'DESCARGUE CAFE BIG BAG LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                            $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                        }
                    }
                    //muestreo particula
                    $this->ServicesOrder->create();
                    $servicesMuestreAlmacafe = array(
                        'created_date' => date('Y-m-d h:i:s'),
                        'exporter_code' => '001',
                        'closed_date' => date('Y-m-d h:i:s'),
                        'approve_user' => $this->Session->read('User.id'),
                        'closed' => 1,
                        'create_user' => $this->Session->read('User.id'),
                    );
                    $this->ServicesOrder->save($servicesMuestreAlmacafe);
                    if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in']),
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE GRAINPRO LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 1) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    } else if ($this->request->data['RemittancesCaffee']['type_units_id'] == 5) {
                        $servicePackage = $this->ServicePackage->find('first', array('conditions' => array('ServicePackage.id' => '00006')));
                        $totalServiceOrder = 0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices) {
                            $this->DetailsServicesToCaffee->create();
                            $valueItem = $itemServices['valor'] * ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 14.23);
                            $detailsServicesToCaffee = array(
                                'services_orders_id' => $this->ServicesOrder->id,
                                'service_package_id' => $servicePackage['ServicePackage']['id'],
                                'remittances_caffee_id' => $idRem,
                                'qta_bag_to_work' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 14.23,
                                'items_services_id' => $itemServices['items_services_id'],
                                'value' => $valueItem,
                                'completed' => true,
                                'observation' => 'MUESTREO ALMCAFE CAFE VERDE LOTE ' . '3-' . $this->request->data['RemittancesCaffee']['exporter_code'] . '-' .
                                    $this->request->data['RemittancesCaffee']['lot_caffee'] . ' POR ' . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                                'created_date' => date('Y-m-d h:i:s'),
                                'document' => ''
                            );
                            $totalServiceOrder += $valueItem;
                            $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        }
                        $serviceOrder = $this->ServicesOrder->find('first', array('conditions' => array('ServicesOrder.id' => $this->ServicesOrder->id)));
                        $totalServiceOrder += $serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor' => $totalServiceOrder), array('ServicesOrder.id' => $this->ServicesOrder->id));
                    }
                }
                $this->Flash->success(__('Se guardo correctamente la informacion'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $idRem)));
        $this->set('remittancesCaffee', $remittancesCaffee);
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 4))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 3))));
        $this->set(compact('usersSamplers'));
        $this->set(compact('usersDriver'));
        $this->set('unitsCoffees', $this->UnitsCaffee->find('list', array('fields' => array('UnitsCaffee.id', 'UnitsCaffee.name'))));
    }
    /**
     * find method
     *
     * @return void
     */
    public function findByRemesa($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        return json_encode($dataRemittancesCaffee);
    }

    /* zip method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function zip($remittancesCaffeeId = null)
    {
        $this->autoRender = false;
        $targzname = "fotos-novedades-descargue-" . $remittancesCaffeeId . ".tar.gz";
        shell_exec("cd " . WWW_ROOT . DS . "img" . DS . "data/noveltyCaffee/;tar -cvf ./targz/" . $targzname . " " . $remittancesCaffeeId);
        $this->response->file(
            WWW_ROOT . DS . 'img' . DS . 'data/noveltyCaffee/targz/' . $targzname,
            array('download' => true, 'name' => $targzname)
        );
    }

    public function completeDownloadCoffee($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        if (is_null($dataRemittancesCaffee['RemittancesCaffee']['document_number_sapmigo_fnc'])) {
            if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] == $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']) {
                $dataRemittancesCaffee['RemittancesCaffee']['state_operation_id'] = 12; //pesaje finalizado por bascula
                $this->RemittancesCaffee->save($dataRemittancesCaffee);
                return json_encode($dataRemittancesCaffee);
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function completeDownloadCoffeeSPB($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] == $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']) {
            $dataRemittancesCaffee['RemittancesCaffee']['state_operation_id'] = 2; //descargado y almacenado por radicador
            $dataRemittancesCaffee['RemittancesCaffee']['download_caffee_date'] = date('Y-m-d H:i:s');
            $this->RemittancesCaffee->save($dataRemittancesCaffee);
            $wieghtNetCoffee = $dataRemittancesCaffee['RemittancesCaffee']['total_weight_net_real'] - $dataRemittancesCaffee['RemittancesCaffee']['tare_download'];
            return json_encode(array('cargolot' => $dataRemittancesCaffee['RemittancesCaffee']['cargolot_id'], 'weight' => $wieghtNetCoffee));
        } else {
            return "";
        }
    }

    public function validateCrossdockingByRemittance($remittanceId)
    {
        $this->loadModel('DetailPackagingCaffee');
        $this->loadModel('DetailPackakgingCrossdocking');
        $this->loadModel('Client');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->autoRender = false;
        $allInfoRemittancesCaffee = $this->RemittancesCaffee->find('first', ['conditions' => ['RemittancesCaffee.id' => $remittanceId]]);
        $dataRemittancesCaffee = $allInfoRemittancesCaffee['RemittancesCaffee'];
        $dataClientRemittancesCaffee = $allInfoRemittancesCaffee['Client'];
        $detailsCrossdocking = $this->DetailPackakgingCrossdocking->find('first', ['conditions' => ['DetailPackakgingCrossdocking.lot_coffee' => $dataClientRemittancesCaffee['exporter_code'] . "-" . $dataRemittancesCaffee['lot_caffee'], 'status' => 1]])['DetailPackakgingCrossdocking'];
        if ($detailsCrossdocking) {
            //se adiciona OIE a la remesa que tiene crossdocking
            $dataRemittancesCaffee['packaging_coffee_id'] = $detailsCrossdocking['packaging_caffee_id'];
            $dataRemittancesCaffee['state_operation_id'] = 3;
            $message = '';
            if ($this->RemittancesCaffee->save($dataRemittancesCaffee)) {
                $message = $message . ' / Se actualiza la remesa';
                //se agrega detalle de embalaje
                $this->DetailPackagingCaffee->create();
                $detailPackagingCaffee['remittances_caffee_id'] = $dataRemittancesCaffee['id'];
                $detailPackagingCaffee['packaging_caffee_id'] = $detailsCrossdocking['packaging_caffee_id'];
                $detailPackagingCaffee['quantity_radicated_bag_out'] = $dataRemittancesCaffee['quantity_radicated_bag_in'];
                $detailPackagingCaffee['created_date'] = date('Y-m-d H:i:s');
                $detailPackagingCaffee['updated_date'] = date('Y-m-d H:i:s');
                $detailPackagingCaffee['state'] = 2;
                $detailPackagingCaffee['tara_packaging'] = 0;
                if ($this->DetailPackagingCaffee->save($detailPackagingCaffee)) {
                    $message = $message . ' / Se guarda el detalle';
                    //se consultan las remesas que tienen la OIE con crosdocking y se valida si la cantidad cumple con lo registrado en crossdocking
                    $quantity = $this->RemittancesCaffee->find('first', ['fields' => ['sum(RemittancesCaffee.quantity_radicated_bag_in) as quantity_sum'], 'conditions' => [
                        'RemittancesCaffee.packaging_coffee_id' => $detailsCrossdocking['packaging_caffee_id']
                    ]]);
                    $message = $message . ' / calculos '.intval($quantity[0]['quantity_sum']).' '.intval($detailsCrossdocking['qta_coffee']);
                    if (intval($quantity[0]['quantity_sum']) == intval($detailsCrossdocking['qta_coffee'])) {
                        $detailsCrossdocking['status'] = 2;
                        $message = $message . ' / Son iguales se actualiza crossdocking';
                        if(!$this->DetailPackakgingCrossdocking->save($detailsCrossdocking)){
                            CakeLog::error("Ocurrio un error al actualizar el crossdoking".json_encode($detailsCrossdocking));
                        }
                        CakeLog::error(json_encode($detailsCrossdocking));
                    }
                } else {
                    CakeLog::error("Ocurrio un error al guardar el detalle " . json_encode($detailPackagingCaffee));
                }
            }
            return $message;
        } else {
            return "";
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function download_caffee($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->loadModel('SlotStore');
        $this->RemittancesCaffee->recursive = 2;
        if ($id) {
            if (!$this->RemittancesCaffee->exists($id)) {
                throw new NotFoundException(__('Invalid remittances caffee'));
            } else {
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
                if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] == $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']) {
                    $dataRemittancesCaffee['RemittancesCaffee']['state_operation_id'] = 2;
                    $dataRemittancesCaffee['RemittancesCaffee']['download_caffee_date'] = date('Y-m-d h:i:s');
                    $this->RemittancesCaffee->save($dataRemittancesCaffee);
                    $this->Flash->success(__('Se finalizó el proceso de descargue de la remesa ' . $id));
                } else {
                    $this->Flash->error(__('No se puede finalizar porque hay sacos por pesar de la remesa ' . $id));
                }
                return $this->redirect(array('action' => 'download_caffee'));
            }
        } else {

            $slotStores = $this->SlotStore->find('list', array('conditions' => array('SlotStore.stores_caffee_id' => $this->Session->read('User.store'))));
            //debug($slotStores);
            $this->Paginator->settings = array(
                'conditions' =>
                array('and' =>
                array(
                    'or' => array(array('RemittancesCaffee.state_operation_id' => 10), array('RemittancesCaffee.state_operation_id' => 1)),
                    array('RemittancesCaffee.slot_store_id' => $slotStores)
                )),
                'order' => array('RemittancesCaffee.download_caffee_date desc'),
                'limit' => 100
            );
            $data = $this->Paginator->paginate('RemittancesCaffee');
            $this->set('remittancesCaffees', $data);
        }
    }

    /**
     * search method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function search()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.perfil_name') == "Colaborador") {
            $this->layout = 'colaborador';
        }
        if ($this->Session->read('User.perfil_name') == "Basculero") {
            $this->layout = 'basculero';
        }
        if ($this->Session->read('User.perfil_name') == "Invitado") {
            $this->layout = 'invitado';
        }
    }

    /**
     * search method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function report()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.centerId') == 1) {
            $this->layout = 'colaborador';
        }
        if ($this->Session->read('User.centerId') == 2) {
            $this->layout = 'basculero';
        }
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function options($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profiles_id') == 1) {
            $this->layout = 'colaborador';
        } else if ($this->Session->read('User.profiles_id') == 2) {
            $this->layout = 'basculero';
        }
        $this->loadModel('Client');
        $this->loadModel('User');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('SlotStore');
        $this->loadModel('TypeUnit');
        $this->loadModel('MaterialCoffee');
        if ($this->request->is('post')) {
            //debug($this->request->data['RemittancesCaffees']);exit;
            if ($this->RemittancesCaffee->updateAll(
                array(
                    'RemittancesCaffee.packing_cafee_id' => $this->request->data['RemittancesCaffees']['packing_cafee_id'],
                    'RemittancesCaffee.nota_entrega' => $this->request->data['RemittancesCaffees']['nota_entrega'],
                    'RemittancesCaffee.ref_driver' => "'" . $this->request->data['RemittancesCaffees']['ref_driver'] . "'",
                    'RemittancesCaffee.type_units_id' => $this->request->data['RemittancesCaffees']['type_units_id'],
                    'RemittancesCaffee.tare_packing_coffee_anormal' => $this->request->data['RemittancesCaffees']['tare_packing_coffee_anormal'],
                    'RemittancesCaffee.nota_entrega' => "'" . $this->request->data['RemittancesCaffees']['nota_entrega'] . "'",
                    'RemittancesCaffee.material' => $this->request->data['RemittancesCaffees']['material'],
                    'RemittancesCaffee.coffee_year' => $this->request->data['RemittancesCaffees']['coffee_year'],
                    'RemittancesCaffee.staff_sample_id' => $this->request->data['RemittancesCaffees']['staff_sample_id'],
                    'RemittancesCaffee.staff_driver_id' => $this->request->data['RemittancesCaffees']['staff_driver_id'],
                    'RemittancesCaffee.observation' => "'" . $this->request->data['RemittancesCaffees']['observation'] . "'",
                    'RemittancesCaffee.details_weight' => "'" . $this->request->data['RemittancesCaffees']['details_weight'] . "'"
                ),
                array('RemittancesCaffee.id' => $id)
            )) {
                $this->Flash->success(__('Se actualizo correctamente! ' . $id));
                return $this->redirect(array('action' => 'download_caffee'));
            }
        }
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $itis_fnc = ($dataRemittancesCaffee['RemittancesCaffee']['client_id'] == 1) ? true : false;
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $slotStores = $this->SlotStore->find('list', array('fields' => array('SlotStore.id', 'name_space')));
        $materialCoffee = $this->MaterialCoffee->find('list', array('conditions' => array('MaterialCoffee.itis_fnc' => $itis_fnc), 'fields' => array('cod_material')));
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 4, 'active' => '1'))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 3, 'active' => '1'))));
        $this->set('bagWeigth', $bagWeigth);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set(compact('usersSamplers'));
        $this->set(compact('usersDriver'));
        $this->set('slotStores', $slotStores);
        $this->set('idRemittanceCoffee', $id);
        $this->set('user', $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id')))));
        $this->loadModel('PackingCaffee');
        $packingCaffee = $this->PackingCaffee->find('list', array('fields' => array('PackingCaffee.id', 'PackingCaffee.name')));
        $this->set(compact('packingCaffee'));
        $typesUnits = $this->TypeUnit->find('list');
        $this->set(compact('typesUnits'));
        $this->set('materialCoffee', $materialCoffee);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profiles_id') == 1) {
            $this->layout = 'colaborador';
        } else if ($this->Session->read('User.profiles_id') == 2) {
            $this->layout = 'basculero';
        }
        $this->request->data = $id;
        $this->loadModel('WeighingDownloadCaffee');
        $this->loadModel('Client');
        $this->loadModel('User');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('SlotStore');
        $this->loadModel('Bascule');
        $this->loadModel('TypeUnit');
        $this->loadModel('RemittancesCaffeeHasNoveltysCaffee');
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $dataWeighingDownloadCaffee = $this->WeighingDownloadCaffee->find('all', array('conditions' => array('remittances_caffee_id' => $id)));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $slotStores = $this->SlotStore->find('list', array('fields' => array('SlotStore.id', 'name_space')));
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 4, 'active' => '1'))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('and' => array('User.cod_city_operation' => 'BUN', 'User.profiles_id' => 3, 'active' => '1'))));
        $userCurrent = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        $bascule = $this->Bascule->find('list', array('conditions' => array('and' => array('Bascule.in_operation' => 1, 'Bascule.packging_anormal' => $dataRemittancesCaffee['RemittancesCaffee']['coffee_with_packging_anormal'], 'Bascule.store_id' => $userCurrent['User']['ref_store'])), 'fields' => array('Bascule.bascule', 'Bascule.name')));
        $this->set('bagWeigth', $bagWeigth);
        $options1 = array('conditions' => array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id' => $id));
        $dataRemittancesCaffeeHasNoveltysCaffee = $this->RemittancesCaffeeHasNoveltysCaffee->find('all', $options1);
        $this->set('weighingDownloadCaffees', $dataWeighingDownloadCaffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set(compact('usersSamplers'));
        $this->set(compact('usersDriver'));
        $this->set('slotStores', $slotStores);
        $this->set('remittancesCaffeeHasNoveltysCaffee', $dataRemittancesCaffeeHasNoveltysCaffee);
        $this->set('bascules', $bascule);
        $this->set('user', $userCurrent);
        $this->loadModel('PackingCaffee');
        $packingCaffee = $this->PackingCaffee->find('list', array('fields' => array('PackingCaffee.id', 'PackingCaffee.name')));
        $this->set(compact('packingCaffee'));
        $typesUnits = $this->TypeUnit->find('list');
        $this->set(compact('typesUnits'));
    }

    public function split($idPallet = null)
    {
        $this->loadModel('WeighingDownloadCaffee');
        $this->layout = 'basculero';
        $this->autoRender = false;
        $dataWeighingDownloadCaffee = $this->WeighingDownloadCaffee->find('first', array('conditions' => array('WeighingDownloadCaffee.id' => $idPallet)));
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $dataWeighingDownloadCaffee['WeighingDownloadCaffee']['remittances_caffee_id'])));
        $bagIn = $remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'];
        $weighTotal = $remittancesCaffee['RemittancesCaffee']['total_weight_net_real'];
        $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.total_weight_net_real' => ($weighTotal - $dataWeighingDownloadCaffee['WeighingDownloadCaffee']['weight_pallet']), 'RemittancesCaffee.quantity_bag_in_store' => ($bagIn - $dataWeighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet'])), array('RemittancesCaffee.id' => $remittancesCaffee['RemittancesCaffee']['id']));
        $this->WeighingDownloadCaffee->delete($dataWeighingDownloadCaffee['WeighingDownloadCaffee']['id']);
        $this->Flash->success(__('Peso borrado correctamente. Por favor agregar los demas pallets!.'));
        return $this->redirect(array('action' => 'view', $remittancesCaffee['RemittancesCaffee']['id']));
    }

    public function weights($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profiles_id') == 1) {
            $this->layout = 'colaborador';
        } else if ($this->Session->read('User.profiles_id') == 9) {
            $this->layout = 'invitado';
        } else if ($this->Session->read('User.profiles_id') == 2) {
            $this->layout = 'basculero';
        }
        $this->request->data = $id;
        $this->loadModel('WeighingDownloadCaffee');
        $this->loadModel('Client');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('SlotStore');
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $dataWeighingDownloadCaffee = $this->WeighingDownloadCaffee->find('all', array('conditions' => array('remittances_caffee_id' => $id)));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $slotStores = $this->SlotStore->find('list', array('fields' => array('SlotStore.id', 'name_space')));
        $this->set('bagWeigth', $bagWeigth);
        $this->set('weighingDownloadCaffees', $dataWeighingDownloadCaffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set('slotStores', $slotStores);
    }

    public function savePallet($idRem = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        if ($this->request->is('post')) {
            $id = $this->request['data']["WeighingDownloadCaffee"]["remittances_caffee"];
            if (!$this->RemittancesCaffee->exists($id)) {
                throw new NotFoundException(__('Invalid remittances caffee'));
            } else {
                //debug($this->request->data);exit;
                if ($this->request->data['WeighingDownloadCaffee']['weight_pallet'] > 0) {
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
                    if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] >= ($this->request->data['total_bags'] + $this->request->data['WeighingDownloadCaffee']['quantity_bag_pallet'])) {
                        $this->loadModel('WeighingDownloadCaffee');
                        $dataWeighingDownloadCaffee = $this->WeighingDownloadCaffee->find('first', array('conditions' => array('remittances_caffee_id' => $id), 'order' => array('seq_weight_pallet DESC')));
                        $this->WeighingDownloadCaffee->create();
                        date_default_timezone_set('America/Bogota');
                        $this->request->data['WeighingDownloadCaffee']['weighing_date'] = date('Y-m-d h:i:s');
                        $this->request->data['WeighingDownloadCaffee']['remittances_caffee_id'] = $id;
                        if ($dataWeighingDownloadCaffee) {
                            $this->request->data['WeighingDownloadCaffee']['seq_weight_pallet'] = $dataWeighingDownloadCaffee['WeighingDownloadCaffee']['seq_weight_pallet'] + 1;
                        } else {
                            $this->request->data['WeighingDownloadCaffee']['seq_weight_pallet'] = 1;
                        }
                        if ($this->WeighingDownloadCaffee->save($this->request->data)) {
                            $this->RemittancesCaffee->updateAll(
                                array(
                                    'RemittancesCaffee.staff_wt_in_id' => $this->Session->read('User.id'), 'RemittancesCaffee.quantity_in_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] + 1),
                                    'RemittancesCaffee.quantity_bag_in_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] + $this->request->data['WeighingDownloadCaffee']['quantity_bag_pallet']),
                                    'RemittancesCaffee.details_weight' => "'" . $this->request->data['details_weight'] . "'",
                                    'RemittancesCaffee.total_weight_net_real' => ($dataRemittancesCaffee['RemittancesCaffee']['total_weight_net_real'] + $this->request->data['WeighingDownloadCaffee']['weight_pallet'])
                                ),
                                array('RemittancesCaffee.id' => $this->request->data['WeighingDownloadCaffee']['remittances_caffee'])
                            );

                            $this->Flash->success(__('Peso guardado existoso!. ' . $this->request->data['WeighingDownloadCaffee']['remittances_caffee']));
                            return $this->redirect(array('action' => 'download_caffee'));
                        } else {
                            $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
                        }
                    } else {
                        $this->Flash->error(__('No se pueden agregar más sacos a este descargue'));
                    }
                } else {
                    $this->Flash->error(__('El peso no puede ser 0!'));
                }
                return $this->redirect(array('action' => 'view', $id));
            }
        }
        if ($this->Session->read('User.centerId') != 1) { //Cartagena y Santa Marta
            $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $idRem)));
            $this->set(compact('remittancesCaffee'));
        }
    }

    public function reweighPallet($idBagWeigh = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $parameter = explode("-", $idBagWeigh);
        $this->autoRender = false;
        $this->layout = 'basculero';
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('WeighingDownloadCaffee');
        $idPallet = $parameter[0];
        $bags = $parameter[1];
        $weigh = $parameter[2];
        $weighingDownloadCaffee = $this->WeighingDownloadCaffee->find('first', array('conditions' => array('WeighingDownloadCaffee.id' => $idPallet)));
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $weighingDownloadCaffee['WeighingDownloadCaffee']['remittances_caffee_id'])));
        $totalBags = $remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet'];
        $isUpdated = $remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_in'] >= ($totalBags + $bags);
        if ($isUpdated) {
            $weighRemittances = $remittancesCaffee['RemittancesCaffee']['total_weight_net_real'] - $weighingDownloadCaffee['WeighingDownloadCaffee']['weight_pallet'];
            $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.quantity_bag_in_store' => ($totalBags + $bags), 'RemittancesCaffee.total_weight_net_real' => ($weighRemittances + $weigh)), array('RemittancesCaffee.id' => $remittancesCaffee['RemittancesCaffee']['id']));
            $this->WeighingDownloadCaffee->updateAll(array('WeighingDownloadCaffee.quantity_bag_pallet' => $bags, 'WeighingDownloadCaffee.weighing_date' => "'" . date('Y-m-d h:i:s') . "'", 'WeighingDownloadCaffee.weight_pallet' => $weigh), array('WeighingDownloadCaffee.id' => $weighingDownloadCaffee['WeighingDownloadCaffee']['id']));
            return json_decode(1);
        } else {
            return json_decode(0);
        }
    }

    public function return_pallet()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->Session->read('User.profiles_id') == 1) {
            $this->layout = 'colaborador';
        } else if ($this->Session->read('User.profiles_id') == 2) {
            $this->layout = 'basculero';
        }
        $this->loadModel('WeighingReturnCoffee');
        $this->loadModel('Client');
        $this->loadModel('User');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('SlotStore');
        $this->loadModel('ReturnsCaffee');
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $detalle_id = isset($this->request->query['detalle_id']) ? $this->request->query['detalle_id'] : null;
        $remittancesCaffeeReturnsCaffee = $this->RemittancesCaffeeReturnsCaffee->find('first', array('conditions' => array('RemittancesCaffeeReturnsCaffee.id' => $detalle_id)));
        //debug($remittancesCaffeeReturnsCaffee);exit;
        $this->request->data = $remittancesCaffeeReturnsCaffee['RemittancesCaffee']['id'];
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffee']['id'])));
        $dataWeighingReturnCoffee = $this->WeighingReturnCoffee->find('all', array('conditions' => array('and' => array(array('remittances_caffee_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffee']['id']), array('return_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id'])))));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $dataReturnsCaffee = $this->ReturnsCaffee->find('first', array('conditions' => array('ReturnsCaffee.id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id'])));
        $this->set('bagWeigth', $bagWeigth);
        $this->set('returnCaffee', $dataReturnsCaffee);
        $this->set('weighingReturnCoffees', $dataWeighingReturnCoffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set('user', $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id')))));
        $this->set('remittancesCaffeeReturnsCaffee', $remittancesCaffeeReturnsCaffee);
    }

    public function returnPallet()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $this->loadModel('ReturnsCaffee');
        $this->loadModel('WeighingReturnCoffee');
        $this->layout = 'basculero';
        if ($this->request->is('post')) {
            //debug($this->request->data['WeighingReturnCoffee']);exit;
            if (isset($this->request->data['WeighingReturnCoffee']['weight'])) { //PESO MANUAL
                $this->request->data['WeighingReturnCoffee']['weight_pallet'] = $this->request->data['WeighingReturnCoffee']['weight'];
                $this->request->data['WeighingReturnCoffee']['quantity_bag_pallet'] = $this->request->data['WeighingReturnCoffee']['bags'];
            }
            if ($this->request->data['WeighingReturnCoffee']['weight_pallet'] > 0) { //DIRECTO BASCULA
                $id = $this->request->data['WeighingReturnCoffee']['remittances_caffee'];
                //debug($this->request['data']);exit;
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
                if ($this->request->data['WeighingReturnCoffee']['qta_bag_return'] >= ($this->request->data['WeighingReturnCoffee']['total_bags'] + $this->request->data['WeighingReturnCoffee']['quantity_bag_pallet'])) {

                    $idReturn = $this->request->data['WeighingReturnCoffee']['return_id'];
                    $dataWeighingReturnCoffee = $this->WeighingReturnCoffee->find('first', array('conditions' => array('remittances_caffee_id' => $id), 'order' => array('seq_weight_pallet DESC')));
                    $this->WeighingReturnCoffee->create();
                    date_default_timezone_set('America/Bogota');
                    $this->request->data['WeighingReturnCoffee']['weighing_date'] = date('Y-m-d H:i:s');
                    $this->request->data['WeighingReturnCoffee']['remittances_caffee_id'] = $id;
                    if ($dataWeighingReturnCoffee) {
                        $this->request->data['WeighingReturnCoffee']['seq_weight_pallet'] = $dataWeighingReturnCoffee['WeighingReturnCoffee']['seq_weight_pallet'] + 1;
                    } else {
                        $this->request->data['WeighingReturnCoffee']['seq_weight_pallet'] = 1;
                        $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.user_weight' => $this->Session->read('User.id')), array('ReturnsCaffee.id' => $idReturn));
                    }
                    if ($this->WeighingReturnCoffee->save($this->request->data)) {
                        $this->RemittancesCaffee->updateAll(
                            array(
                                'RemittancesCaffee.staff_wt_in_id' => $this->Session->read('User.id'),
                                'RemittancesCaffee.quantity_in_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] - 1),
                                'RemittancesCaffee.quantity_out_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] + 1),
                                'RemittancesCaffee.quantity_bag_in_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $this->request->data['WeighingReturnCoffee']['quantity_bag_pallet']),
                                'RemittancesCaffee.quantity_bag_out_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] + $this->request->data['WeighingReturnCoffee']['quantity_bag_pallet']),
                                'RemittancesCaffee.total_weight_net_real' => ($dataRemittancesCaffee['RemittancesCaffee']['total_weight_net_real'] - $this->request->data['WeighingReturnCoffee']['weight_pallet'])
                            ),
                            array('RemittancesCaffee.id' => $id)
                        );
                        $this->Flash->success(__('Se guardo el peso de menera correcta!'));
                        return $this->redirect(array('action' => 'return_index'));
                    } else {
                        $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
                    }
                } else {
                    $this->Flash->error(__('No se pueden agregar más sacos a este descargue'));
                    return $this->redirect(array('action' => 'return_index'));
                }
            } else {
                $this->Flash->error(__('El peso no puede ser 0!'));
                return $this->redirect(array('action' => 'return_index'));
            }
        }
        if ($this->Session->read('User.centerId') != 1) { //Cartagena y Santa Marta
            $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $idRem)));
            $this->set(compact('remittancesCaffee'));
        }
    }

    public function reopening($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->autoRender = false;
        date_default_timezone_set('America/Bogota');
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $textAppenDetailtWeight = $remittancesCaffee['RemittancesCaffee']['details_weight'];
        $textAppenDetailtWeight .= "\n Intención repesaje Café generado: " . date('Y-m-d H:i:s') . ' por usuario con ID: ' . $this->Session->read('User.id');
        if ($this->RemittancesCaffee->updateAll(
            array(
                'RemittancesCaffee.details_weight' => "'" . $textAppenDetailtWeight . "'",
                'RemittancesCaffee.state_operation_id' => 1
            ),
            array('RemittancesCaffee.id' => $id)
        )) {
            $this->Flash->success(__('Remesa ' . $id . " con REPESAJE activado. Café visualizado en Bascula Bod. " . $remittancesCaffee['SlotStore']['name_space']));
            return $this->redirect(array('action' => 'weights', $id));
        }
    }

    public function edit($idRem = null)
    {
        $this->loadModel('Client');
        $this->loadModel('TypeUnit');
        $this->loadModel('Custom');
        $this->loadModel('MarkCaffee');
        $this->loadModel('Shipper');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('CoffeeSample');
        $this->loadModel('ServicesOrder');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('CitySource');
        $this->loadModel('Bill');
        $this->loadModel('Departament');
        $this->loadModel('SampleCoffeeHasTypeSample');
        $this->loadModel('ServicePackage');
        $this->loadModel('SlotStore');
        $this->loadModel('User');
        $this->loadModel('PackingCaffee');
        $this->loadModel('MaterialCoffee');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if (!$this->RemittancesCaffee->exists($idRem)) {
            throw new NotFoundException(__('Invalid info RemittancesCaffee'));
        }
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $idRem)));
        if ($remittancesCaffee['RemittancesCaffee']['jetty'] == 1 && $remittancesCaffee['RemittancesCaffee']['state_operation_id'] != 1) {
            $this->Flash->error(__('No se puede editar la remesa por el estado!'));
            return $this->redirect(array('action' => 'index'));
        }
        $userRegister = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        if ($this->request->is(array('post', 'put'))) {
            date_default_timezone_set('America/Bogota');
            $db = $this->RemittancesCaffee->getDataSource();
            $unit_selected = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $this->request->data['RemittancesCaffee']['units_cafee_id'])));
            $dataCreatedDate = "'" . $this->request->data['RemittancesCaffee']['created_date']['year'] . "-" . $this->request->data['RemittancesCaffee']['created_date']['month'] . "-" . $this->request->data['RemittancesCaffee']['created_date']['day'] . "'";
            $dateIniTime = (isset($this->request->data['RemittancesCaffee']['ini_ctg_download'])) ? "'" . $this->request->data['RemittancesCaffee']['ini_ctg_download']['hour'] . ":" . $this->request->data['RemittancesCaffee']['ini_ctg_download']['min'] . "'" : null;
            $dateEndTime = (isset($this->request->data['RemittancesCaffee']['end_ctg_download'])) ? "'" . $this->request->data['RemittancesCaffee']['end_ctg_download']['hour'] . ":" . $this->request->data['RemittancesCaffee']['end_ctg_download']['min'] . "'" : null;
            $fieldSampleReq = (isset($this->request->data['RemittancesCaffee']['sample_req_ctg'])) ?  "'" . $this->request->data['RemittancesCaffee']['sample_req_ctg'] . "'" : null;
            $fieldPlagueControl = (isset($this->request->data['RemittancesCaffee']['sample_req_ctg'])) ?  "'" . $this->request->data['RemittancesCaffee']['plague_control_ctg'] . "'" : null;
            $fieldMachineDownload = (isset($this->request->data['RemittancesCaffee']['sample_req_ctg'])) ?  "'" . $this->request->data['RemittancesCaffee']['machine_download_ctg'] . "'" : null;
            $fieldCooperativa = (isset($this->request->data['RemittancesCaffee']['sample_req_ctg'])) ?  "'" . $this->request->data['RemittancesCaffee']['cooperativa_cta'] . "'" : null;
            $dataRemittancesCaffee = array(
                'client_id' => $this->request->data['RemittancesCaffee']['client_id'],
                'lot_caffee' => $db->value($this->request->data['RemittancesCaffee']['lot_caffee'], 'string'),
                'quantity_radicated_bag_in' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                'coffee_with_packging_anormal' => ($this->request->data['RemittancesCaffee']['coffee_with_packging_anormal'] == 'on') ? 1 : 0,
                'nota_entrega' => "'" . $this->request->data['RemittancesCaffee']['nota_entrega'] . "'",
                'material' => "'" . $this->request->data['RemittancesCaffee']['material'] . "'",
                'seals' => "'" . $this->request->data['RemittancesCaffee']['seals'] . "'",
                'packing_cafee_id' => "'" . $this->request->data['RemittancesCaffee']['packing_cafee_id'] . "'",
                'type_units_id' => "'" . $this->request->data['RemittancesCaffee']['type_units_id'] . "'",
                'units_cafee_id' => "'" . $this->request->data['RemittancesCaffee']['units_cafee_id'] . "'",
                'vehicle_plate' => "'" . $this->request->data['RemittancesCaffee']['vehicle_plate'] . "'",
                'guide_id' => "'" . $this->request->data['RemittancesCaffee']['guide'] . "'",
                'microlot' => "'" . $this->request->data['RemittancesCaffee']['microlot'] . "'",
                'bill_id' => "'" . $this->request->data['RemittancesCaffee']['bill_id'] . "'",
                'cargolot_id' => "'" . $this->request->data['RemittancesCaffee']['cargolot_id'] . "'",
                'city_source_id' => "'" . $this->request->data['RemittancesCaffee']['city_source_id'] . "'",
                'shippers_id' => "'" . $this->request->data['RemittancesCaffee']['shippers_id'] . "'",
                'slot_store_id' => "'" . $this->request->data['RemittancesCaffee']['slot_store_id'] . "'",
                'staff_sample_id' => "'" . $this->request->data['RemittancesCaffee']['staff_sample_id'] . "'",
                'staff_driver_id' => "'" . $this->request->data['RemittancesCaffee']['staff_driver_id'] . "'",
                'cooperativa_cta' => $fieldCooperativa,
                'created_date' => $dataCreatedDate,
                'machine_download_ctg' => $fieldMachineDownload,
                'custom_id' => "'" . $this->request->data['RemittancesCaffee']['custom_id'] . "'",
                'plague_control_ctg' => $fieldPlagueControl,
                'sample_req_ctg' => $fieldSampleReq,
                'ini_ctg_download' => $dateIniTime,
                'end_ctg_download' => $dateEndTime,
                'observation' => "'" . $this->request->data['RemittancesCaffee']['observation'] . "'",
                'details_weight' => "'" . $this->request->data['RemittancesCaffee']['details_weight'] . "'",
                'mark_cafee_id' => "'" . $this->request->data['RemittancesCaffee']['mark_cafee_id'] . "'",
                'total_weight_net_nominal' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity'],
                'updated_dated' => "'" . date('Y-m-d H:i:s') . "'"
            );
            $db = $this->RemittancesCaffee->getDataSource();
            if ($this->RemittancesCaffee->updateAll(
                $dataRemittancesCaffee,
                array('RemittancesCaffee.id' => $idRem)
            )) {

                if (isset($this->request->data['dataFumigation'])) {
                    $dataToFumigation = $this->request->data['dataFumigation'];

                    foreach ($dataToFumigation as $key => $value) {
                        $request = ($value . "-" . $idRem . "-" . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in']);
                        //debug($request);exit;
                        $controller = new RemittancesCaffeeHasFumigationServicesController();
                        $controller->addCoffeeFumigation($request);
                    }
                }

                /*if (isset($this->request->data['dataCrossdocking'])) {
                    $dataCrossdocking = $this->request->data['dataCrossdocking'];
                    foreach ($dataCrossdocking as $key => $value) {
                        $controller = new DetailPackakgingCrossdockingController();
                        $controller->updateDetailPackakgingCrossdocking($value, $idRem);
                    }
                }*/

                $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $this->request->data['RemittancesCaffee']['client_id'])));
                $dataJetty = $this->Departament->find('first', array('conditions' => array('Departament.id' => $remittancesCaffee['RemittancesCaffee']['jetty'])));
                $dataSlotStore = $this->SlotStore->find('first', array('conditions' => array('SlotStore.id' => $this->request->data['RemittancesCaffee']['slot_store_id'])));

                $dataExporterAuth = array(61, 510, 55);
                if (in_array($dataClient['Client']['id'], $dataExporterAuth)) {
                    $dataCoffeeSample = $this->CoffeeSample->find('first', array('conditions' => array('CoffeeSample.rem_coffee_id' => $idRem)));

                    $arrayCoffeeSample = array(
                        'qta_bag' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                        'status' => $db->value("REGISTRADA", 'string'),
                        'created_date' => "'" . date('Y-m-d h:i:s') . "'",
                        'register_users_id' => $this->Session->read('User.id'),
                        'observation' => $db->value("CONTRAMUESTRA", 'string'),
                        'operation_center' => $db->value($dataJetty['Departament']['cod_terminal'], 'string'),
                        'client_id' => $dataClient['Client']['id'],
                        'terminal' => $db->value($dataSlotStore['SlotStore']['name_space'], 'string'),
                        'lot_coffee_ref' => $db->value($dataClient['Client']['exporter_code'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'], 'string'),
                        'get_sample' => 1,
                        'rem_coffee_id' => $idRem
                    );

                    if ($this->CoffeeSample->updateAll($arrayCoffeeSample, array('CoffeeSample.id' => $dataCoffeeSample['CoffeeSample']['id']))) {
                        $typeCoffee = 0;
                        if ($dataClient['Client']['id'] == 61) {
                            $typeCoffee = 15;
                        } else if ($dataClient['Client']['id'] == 510) {
                            $typeCoffee = 16;
                        } else if ($dataClient['Client']['id'] == 55) {
                            $typeCoffee = 13;
                        }
                        $arrayDataSampleCoffeeHasTypeSample = array(
                            'sample_coffee_id' => $dataCoffeeSample['CoffeeSample']['id'],
                            'type_sample_coffee_id' => $typeCoffee
                        );
                    }
                    $this->SampleCoffeeHasTypeSample->updateAll($arrayDataSampleCoffeeHasTypeSample, $arrayDataSampleCoffeeHasTypeSample);
                }

                if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                    $options = array(
                        'joins' => array(
                            array(
                                'table' => 'sample_coffee_has_type_sample',
                                'alias' => 'SampleHasType',
                                'type' => 'INNER',
                                'conditions' => array(
                                    'SampleHasType.sample_coffee_id = CoffeeSample.id'
                                )
                            )
                        ),
                        'conditions' => array(
                            'and' =>
                            array(
                                'CoffeeSample.rem_coffee_id' => $idRem,
                                array(
                                    'or' => array(array('type_sample_coffee_id' => 17), array('type_sample_coffee_id' => 18))
                                )
                            )
                        )
                    );
                    $coffeeSample = $this->CoffeeSample->find('first', $options);
                    $typeCoffee = 0;
                    $qtaCoffee = 0;
                    if ($dataClient['Client']['id'] == 1) {
                        $typeCoffee = 18;
                        $qtaCoffee = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.4;
                    } else {
                        $typeCoffee = 17;
                        $qtaCoffee = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                    }
                    $dataCoffeeSample = array(
                        'qta_bag' => $qtaCoffee,
                        'status' => $db->value("COMPLETADO", 'string'),
                        'created_date' => "'" . date('Y-m-d h:i:s') . "'",
                        'updated_date' => "'" . date('Y-m-d h:i:s') . "'",
                        'register_users_id' => $this->Session->read('User.id'),
                        'observation' => $db->value("CONTRAMUESTRA HERMETICA", 'string'),
                        'operation_center' => $db->value($dataJetty['Departament']['cod_terminal'], 'string'),
                        'client_id' => $dataClient['Client']['id'],
                        'terminal' => $db->value($dataSlotStore['SlotStore']['name_space'], 'string'),
                        'lot_coffee_ref' => $db->value($dataClient['Client']['exporter_code'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'], 'string'),
                        'get_sample' => 1,
                        'rem_coffee_id' => $idRem
                    );
                    if ($coffeeSample) {
                        $this->CoffeeSample->updateAll(
                            $dataCoffeeSample,
                            array('CoffeeSample.id' => $coffeeSample['CoffeeSample']['id'])
                        );
                    } else {
                        $this->CoffeeSample->create();
                        if ($this->CoffeeSample->save($dataCoffeeSample)) {
                            $arrayDataSampleCoffeeHasTypeSample = array(
                                'sample_coffee_id' => $this->CoffeeSample->id,
                                'type_sample_coffee_id' => $typeCoffee
                            );
                        }
                        $this->SampleCoffeeHasTypeSample->create();
                        $this->SampleCoffeeHasTypeSample->save($arrayDataSampleCoffeeHasTypeSample);
                    }
                }

                $this->Flash->success(__('Se actualizo el registro exitosamente'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('No se registro la informaci�n. Por favor intentelo nuevamente.'));
            }
        } else {
            $options = array('conditions' => array('RemittancesCaffee.' . $this->RemittancesCaffee->primaryKey => $idRem));
            $remmitancesCaffee = $this->RemittancesCaffee->find('first', $options);
            $this->request->data = $remmitancesCaffee;
            $this->request->data['RemittancesCaffee']['codigo'] = $remmitancesCaffee['Client']['exporter_code'];
            $this->request->data['RemittancesCaffee']['id_cliente'] = $remmitancesCaffee['Client']['id'];
            $this->request->data['RemittancesCaffee']['client'] = $remmitancesCaffee['Client']['exporter_code'] . "-" . $remmitancesCaffee['Client']['business_name'];
            $this->request->data['RemittancesCaffee']['guide'] = $remmitancesCaffee['RemittancesCaffee']['guide_id'];
            $this->request->data['RemittancesCaffee']['staff_sample_id'] = $remmitancesCaffee['RemittancesCaffee']['staff_sample_id'];
        }
        $citys = $this->CitySource->find('list', array('fields' => array('CitySource.id', 'CitySource.city_name')));
        $units = $this->UnitsCaffee->find('list', array('fields' => array('UnitsCaffee.id', 'UnitsCaffee.name_unit')));
        $marks_caffee = $this->MarkCaffee->find('list');
        $typesUnits = $this->TypeUnit->find('list');
        $customns = $this->Custom->find('list', array('fields' => array('Custom.id', 'Custom.cia_name')));
        $shippers = $this->Shipper->find('list', array('fields' => array('Shipper.id', 'Shipper.business_name')));
        $slotUsed = $this->RemittancesCaffee->find('list', array('fields' => array('RemittancesCaffee.slot_store_id', 'RemittancesCaffee.slot_store_id'), 'conditions' => array('RemittancesCaffee.state_operation_id' => 2)));
        $options = array('fields' => array('SlotStore.id', 'SlotStore.name_space'), 'conditions' => array("NOT" => array("SlotStore.id" => $slotUsed)));
        $slotsEmpty = $this->SlotStore->find('list', $options);
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 4))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 3))));
        $packingCaffee = $this->PackingCaffee->find('list', array('fields' => array('PackingCaffee.id', 'PackingCaffee.name')));
        $materialCoffee = $this->MaterialCoffee->find('all');
        $typeBills = $this->Bill->find('list');
        $this->set(compact('typeBills'));
        foreach ($materialCoffee as $key => $value) {
            $listMaterials[$value['MaterialCoffee']['id']] = $value['MaterialCoffee']['cod_material'] . " " . $value['MaterialCoffee']['name'] . " ";
        }
        $this->set('materialCoffee', $listMaterials);
        $this->set(compact('marks_caffee'));
        $this->set(compact('packingCaffee'));
        $this->set(compact('units'));
        $this->set(compact('shippers'));
        $this->set(compact('citys'));
        $this->set(compact('customns'));
        $this->set(compact('userRegister'));
        $this->set(compact('slotsEmpty'));
        $this->set(compact('usersSamplers'));
        $this->set(compact('usersDriver'));
        $this->set(compact('typesUnits'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        $this->loadModel('Client');
        $this->loadModel('Custom');
        $this->loadModel('TypeUnit');
        $this->loadModel('MarkCaffee');
        $this->loadModel('Shipper');
        $this->loadModel('Departament');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('CoffeeSample');
        $this->loadModel('CitySource');
        $this->loadModel('SlotStore');
        $this->loadModel('Bill');
        $this->loadModel('ServicePackage');
        $this->loadModel('ServicesOrder');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('SampleCoffeeHasTypeSample');
        $this->loadModel('RemittancesCaffeeHasFumigationServices');
        $this->loadModel('User');
        $this->loadModel('PackingCaffee');
        $this->loadModel('MaterialCoffee');
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $db = $this->RemittancesCaffee->getDataSource();
        $userRegister = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        if ($this->request->is('post')) {
            $this->RemittancesCaffee->create();
            $unit_selected = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $this->request->data['RemittancesCaffee']['units_cafee_id'])));
            $this->request->data['RemittancesCaffee']['guide_id'] = $this->request->data['RemittancesCaffee']['guide'];
            $this->request->data['RemittancesCaffee']['state_operation_id'] = 1;
            $this->request->data['RemittancesCaffee']['user_register'] = $this->Session->read('User.id');
            $this->request->data['RemittancesCaffee']['jetty'] = $userRegister['User']['departaments_id'];
            if ($userRegister['User']['departaments_id'] > 1 && $userRegister['User']['departaments_id'] < 8) {
                $this->request->data['RemittancesCaffee']['state_operation_id'] = 2;
                $this->request->data['RemittancesCaffee']['created_date'] = $this->request->data['RemittancesCaffee']['created_date'];
                $this->request->data['RemittancesCaffee']['download_caffee_date'] = $this->request->data['RemittancesCaffee']['created_date'];
                $this->request->data['RemittancesCaffee']['quantity_bag_in_store'] = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                $this->request->data['RemittancesCaffee']['quantity_in_pallet_caffee'] = ($this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity']) / 1750;
                $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                $this->request->data['RemittancesCaffee']['total_weight_net_real'] = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity'];
            }
            $this->request->data['RemittancesCaffee']['client_id'] = $this->request->data['RemittancesCaffee']['client_id'];
            $this->request->data['RemittancesCaffee']['total_weight_net_nominal'] = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity'];
            unset($this->request->data['RemittancesCaffee']['guide']);

            $findCoffeeSample = $this->CoffeeSample->find('first', array(
                'joins' => array(
                    array(
                        'table' => 'sample_coffee_has_type_sample',
                        'alias' => 'SampleHasType',
                        'type' => 'INNER',
                        'conditions' => array(
                            'SampleHasType.sample_coffee_id = CoffeeSample.id'
                        )
                    )
                ),
                'conditions' => array(
                    'and' => [
                        ['SampleHasType.type_sample_coffee_id' => 12],
                        ['CoffeeSample.lot_coffee_ref LIKE ' => "%" . $this->request->data['RemittancesCaffee']['codigo_exp'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'] . "%"],
                        ['CoffeeSample.status LIKE ' => "%REGISTRADA%"],
                        ['CoffeeSample.get_sample' => 1]
                    ]
                )
            ));

            $findCoffeeSample1 = $this->CoffeeSample->find('first', array(
                'joins' => array(
                    array(
                        'table' => 'sample_coffee_has_type_sample',
                        'alias' => 'SampleHasType',
                        'type' => 'INNER',
                        'conditions' => array(
                            'SampleHasType.sample_coffee_id = CoffeeSample.id'
                        )
                    )
                ),
                'conditions' => array(
                    'and' => [
                        ['SampleHasType.type_sample_coffee_id' => 12],
                        ['CoffeeSample.lot_coffee_ref LIKE ' => "%" . $this->request->data['RemittancesCaffee']['codigo_exp'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'] . "%"],
                        ['CoffeeSample.status LIKE ' => "%REGISTRADA%"]
                    ]
                )
            ));

            if ($this->RemittancesCaffee->save($this->request->data)) {
                if (isset($this->request->data['dataFumigation'])) {
                    $dataToFumigation = $this->request->data['dataFumigation'];
                    foreach ($dataToFumigation as $key => $value) {
                        $request = ($value . "-" . $this->RemittancesCaffee->id . "-" . $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in']);
                        $controller = new RemittancesCaffeeHasFumigationServicesController();
                        $controller->addCoffeeFumigation($request);
                    }
                }

                /*if (isset($this->request->data['dataCrossdocking'])) {
                    $dataCrossdocking = $this->request->data['dataCrossdocking'];
                    foreach ($dataCrossdocking as $key => $value) {
                        $controller = new DetailPackakgingCrossdockingController();
                        $controller->updateDetailPackakgingCrossdocking($value, $this->RemittancesCaffee->id);
                    }
                }*/

                $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $this->request->data['RemittancesCaffee']['client_id'])));
                $dataJetty = $this->Departament->find('first', array('conditions' => array('Departament.id' => $this->request->data['RemittancesCaffee']['jetty'])));
                $dataSlotStore = $this->SlotStore->find('first', array('conditions' => array('SlotStore.id' => $this->request->data['RemittancesCaffee']['slot_store_id'])));

                $seTomaMuestra = 1;
                if ($findCoffeeSample1) {
                    $seTomaMuestra = $findCoffeeSample1['CoffeeSample']['get_sample'];
                }

                $dataExporterAuth = array(61, 510, 55);
                if (in_array($dataClient['Client']['id'], $dataExporterAuth)) {
                    $dataCoffeeSample = array(
                        'qta_bag' => $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'],
                        'status' => 'REGISTRADA',
                        'created_date' => date('Y-m-d h:i:s'),
                        'register_users_id' => $this->Session->read('User.id'),
                        'observation' => 'CONTRAMUESTRA',
                        'operation_center' => $dataJetty['Departament']['cod_terminal'],
                        'client_id' => $dataClient['Client']['id'],
                        'terminal' => $dataSlotStore['SlotStore']['name_space'],
                        'lot_coffee_ref' => $dataClient['Client']['exporter_code'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'],
                        'get_sample' => $seTomaMuestra,
                        'rem_coffee_id' => $this->RemittancesCaffee->id
                    );
                    $this->CoffeeSample->create();
                    if ($this->CoffeeSample->save($dataCoffeeSample)) {
                        $typeCoffee = 0;
                        if ($dataClient['Client']['id'] == 61) {
                            $typeCoffee = 15;
                        } else if ($dataClient['Client']['id'] == 510) {
                            $typeCoffee = 16;
                        } else if ($dataClient['Client']['id'] == 55) {
                            $typeCoffee = 13;
                        }
                        $arrayDataSampleCoffeeHasTypeSample = array(
                            'sample_coffee_id' => $this->CoffeeSample->id,
                            'type_sample_coffee_id' => $typeCoffee
                        );
                    }
                    $this->SampleCoffeeHasTypeSample->create();
                    $this->SampleCoffeeHasTypeSample->save($arrayDataSampleCoffeeHasTypeSample);
                }

                if ($this->request->data['RemittancesCaffee']['type_units_id'] == 4) {
                    $typeCoffee = 0;
                    $qtaCoffee = 0;
                    if ($dataClient['Client']['id'] == 1) {
                        $typeCoffee = 18;
                        $qtaCoffee = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * 0.4;
                    } else {
                        $typeCoffee = 17;
                        $qtaCoffee = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'];
                    }

                    $dataCoffeeSample = array(
                        'qta_bag' => $qtaCoffee,
                        'status' => 'COMPLETADA',
                        'created_date' => date('Y-m-d h:i:s'),
                        'end_date' => date('Y-m-d h:i:s'),
                        'register_users_id' => $this->Session->read('User.id'),
                        'observation' => 'CONTRAMUESTRA HERMETICA',
                        'operation_center' => $dataJetty['Departament']['cod_terminal'],
                        'client_id' => $dataClient['Client']['id'],
                        'terminal' => $dataSlotStore['SlotStore']['name_space'],
                        'lot_coffee_ref' => $dataClient['Client']['exporter_code'] . "-" . $this->request->data['RemittancesCaffee']['lot_caffee'],
                        'get_sample' => 1,
                        'rem_coffee_id' => $this->RemittancesCaffee->id
                    );
                    $this->CoffeeSample->create();
                    if ($this->CoffeeSample->save($dataCoffeeSample)) {
                        $arrayDataSampleCoffeeHasTypeSample = array(
                            'sample_coffee_id' => $this->CoffeeSample->id,
                            'type_sample_coffee_id' => $typeCoffee
                        );
                    }
                    $this->SampleCoffeeHasTypeSample->create();
                    $this->SampleCoffeeHasTypeSample->save($arrayDataSampleCoffeeHasTypeSample);
                }
                $this->Flash->success(__('Se agrego exitosamente la remesa ' . $this->RemittancesCaffee->id . '.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('No se pudo registrar la remesa'));
            }
        }
        $citys = $this->CitySource->find('list', array('fields' => array('CitySource.id', 'CitySource.city_name')));
        $units = $this->UnitsCaffee->find('list', array('fields' => array('UnitsCaffee.id', 'UnitsCaffee.name_unit')));
        $marks_caffee = $this->MarkCaffee->find('list');
        $typesUnits = $this->TypeUnit->find('list');
        $customns = $this->Custom->find('list', array('fields' => array('Custom.id', 'Custom.cia_name')));
        $shippers = $this->Shipper->find('list', array('fields' => array('Shipper.id', 'Shipper.business_name')));
        $slotUsed = $this->RemittancesCaffee->find('list', array('fields' => array('RemittancesCaffee.slot_store_id', 'RemittancesCaffee.slot_store_id'), 'conditions' => array('RemittancesCaffee.state_operation_id !=' => 4)));
        if ($userRegister['User']['departaments_id'] != 1) {
            $options = array('fields' => array('SlotStore.id', 'SlotStore.name_space'), 'conditions' => array('and' => array('SlotStore.ref_departament' => $userRegister['User']['departaments_id'])));
            $slotsEmpty = $this->SlotStore->find('list', $options);
        } else {
            $options = array('fields' => array('SlotStore.id', 'SlotStore.name_space'), 'conditions' => array('and' => array('SlotStore.ref_departament' => $userRegister['User']['departaments_id'], array('NOT' => array("SlotStore.id" => $slotUsed)))));
            $slotsEmpty = $this->SlotStore->find('list', $options);
        }
        $usersSamplers = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 4))));
        $usersDriver = $this->User->find('list', array('fields' => array('User.id', 'User.last_name'), 'conditions' => array('and' => array('User.cod_city_operation' => $this->Session->read('User.jetty'), 'User.profiles_id' => 3))));
        $packingCaffee = $this->PackingCaffee->find('list', array('fields' => array('PackingCaffee.id', 'PackingCaffee.name')));
        $materialCoffee = $this->MaterialCoffee->find('all');
        $typeBills = $this->Bill->find('list');
        $this->set(compact('typeBills'));
        foreach ($materialCoffee as $key => $value) {
            $listMaterials[$value['MaterialCoffee']['id']] = $value['MaterialCoffee']['cod_material'] . " " . $value['MaterialCoffee']['name'] . " ";
        }
        $this->set('materialCoffee', $listMaterials);
        $this->set(compact('marks_caffee'));
        $this->set(compact('packingCaffee'));
        $this->set(compact('units'));
        $this->set(compact('userRegister'));
        $this->set(compact('shippers'));
        $this->set(compact('citys'));
        $this->set(compact('customns'));
        $this->set(compact('slotsEmpty'));
        $this->set(compact('usersSamplers'));
        $this->set(compact('usersDriver'));
        $this->set(compact('typesUnits'));
    }


    public function addremote()
    {
        $this->loadModel('Client');
        $this->loadModel('Custom');
        $this->loadModel('TypeUnit');
        $this->loadModel('MarkCaffee');
        $this->loadModel('Shipper');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('CitySource');
        $this->loadModel('SlotStore');
        $this->loadModel('ServicePackage');
        $this->loadModel('ServicesOrder');
        $this->loadModel('DetailsServicesToCaffee');
        $this->loadModel('User');
        $this->loadModel('PackingCaffee');
        $userSPB = $this->User->find('first', array('conditions' => array('User.id' => 71)));
        $stringBase64Ok = $userSPB['User']['username'] . ":spb.2018";
        $hashOk = base64_encode($stringBase64Ok);
        if (!isset($_GET['token']) || ($hashOk != $_GET['token'])) {
            $this->set(array(
                'message' => array('msg' => "Datos de acceso incorrectos ", 'id-msg' => 3),
                '_serialize' => array('message')
            ));
        } else {
            if (
                !isset($_GET['guide_id']) || !isset($_GET['lot_caffee']) || !isset($_GET['quantity_radicated_bag_in']) || !isset($_GET['vehicle_plate'])
                || !isset($_GET['nit']) || !isset($_GET['packing_cafee_id']) || !isset($_GET['mark_cafee_id'])
                || !isset($_GET['custom_id']) || !isset($_GET['city_source_id']) || !isset($_GET['shippers_id']) || !isset($_GET['type_units_id'])
                || !isset($_GET['seals']) || !isset($_GET['cargolot_id']) || !isset($_GET['type_packaging_coffee']) || !isset($_GET['coffee_to_fumigation'])
            ) {
                $this->set(array(
                    'message' => array('msg' => "Datos incompletos!", 'id-msg' => 5),
                    '_serialize' => array('message')
                ));
            } else {
                $client = $this->Client->find('first', array('conditions' => array('Client.nit' => $_GET['nit'])));
                if (empty($client)) {
                    $this->set(array(
                        'message' => array('msg' => "Cliente no existe o Nit Errada", 'id-msg' => 6),
                        '_serialize' => array('message')
                    ));
                } else if ($_GET['guide_id'] == 0 || $_GET['guide_id'] == "") {
                    $this->set(array(
                        'message' => array('msg' => "Numero de Guia de transito Vacia", 'id-msg' => 7),
                        '_serialize' => array('message')
                    ));
                } else if ($_GET['guide_id'] == 0 || $_GET['lot_caffee'] == "") {
                    $this->set(array(
                        'message' => array('msg' => "N�mero de lote de caf� Vacio", 'id-msg' => 8),
                        '_serialize' => array('message')
                    ));
                } else if ($_GET['quantity_radicated_bag_in'] == "" || $_GET['quantity_radicated_bag_in'] == 0) {
                    $this->set(array(
                        'message' => array('msg' => "N�mero de sacos de ingreso vacios o en Cero", 'id-msg' => 9),
                        '_serialize' => array('message')
                    ));
                } else if ($_GET['vehicle_plate'] == "") {
                    $this->set(array(
                        'message' => array('msg' => "Placa vehiculo Vacia", 'id-msg' => 10),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->PackingCaffee->find('first', array('conditions' => array('PackingCaffee.id' => $_GET['packing_cafee_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Empaque vacio o en 0", 'id-msg' => 12),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->MarkCaffee->find('first', array('conditions' => array('MarkCaffee.id' => $_GET['mark_cafee_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Marca de cafe vacio o en 0", 'id-msg' => 14),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->Custom->find('first', array('conditions' => array('Custom.id' => $_GET['custom_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Agencia aduanas vacio o en 0", 'id-msg' => 15),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->CitySource->find('first', array('conditions' => array('CitySource.id' => $_GET['city_source_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Ciudad de origen vacio o en 0 ", 'id-msg' => 16),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->Shipper->find('first', array('conditions' => array('Shipper.id' => $_GET['shippers_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Empresa de transporte o en 0 ", 'id-msg' => 17),
                        '_serialize' => array('message')
                    ));
                } else if (empty($this->TypeUnit->find('first', array('conditions' => array('TypeUnit.id' => $_GET['type_units_id']))))) {
                    $this->set(array(
                        'message' => array('msg' => "id Tipo de unidades no existe o en 0", 'id-msg' => 18),
                        '_serialize' => array('message')
                    ));
                } else if ($_GET['cargolot_id'] == "" || $_GET['cargolot_id'] == 0) {
                    $this->set(array(
                        'message' => array('msg' => "cargolot id Vacio o en 0", 'id-msg' => 19),
                        '_serialize' => array('message')
                    ));
                } else {
                    $db = $this->RemittancesCaffee->getDataSource();
                    $this->RemittancesCaffee->create();
                    $this->request->data['RemittancesCaffee']['created_date'] = date('Y-m-d h:i:s');
                    $this->request->data['RemittancesCaffee']['guide_id'] = $_GET['guide_id'];
                    $this->request->data['RemittancesCaffee']['state_operation_id'] = 1;
                    $this->request->data['RemittancesCaffee']['lot_caffee'] = $_GET['lot_caffee'];
                    $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] = $_GET['quantity_radicated_bag_in'];
                    $this->request->data['RemittancesCaffee']['vehicle_plate'] = $_GET['vehicle_plate'];
                    $this->request->data['RemittancesCaffee']['user_register'] = 71;
                    $client = $this->Client->find('first', array('conditions' => array('Client.nit' => $_GET['nit'])));
                    $this->request->data['RemittancesCaffee']['client_id'] = $client['Client']['id'];
                    $this->request->data['RemittancesCaffee']['units_cafee_id'] = 1;
                    $unit_selected = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => 1)));
                    $this->request->data['RemittancesCaffee']['packing_cafee_id'] = $_GET['packing_cafee_id'];
                    $this->request->data['RemittancesCaffee']['mark_cafee_id'] = $_GET['mark_cafee_id'];
                    $this->request->data['RemittancesCaffee']['custom_id'] = $_GET['custom_id'];
                    $this->request->data['RemittancesCaffee']['city_source_id'] = $_GET['city_source_id'];
                    $this->request->data['RemittancesCaffee']['shippers_id'] = $_GET['shippers_id'];
                    $this->request->data['RemittancesCaffee']['type_units_id'] = $_GET['type_units_id'];
                    $this->request->data['RemittancesCaffee']['seals'] = $_GET['seals'];
                    $this->request->data['RemittancesCaffee']['jetty'] = 1;
                    $this->request->data['RemittancesCaffee']['type_packaging_coffee'] = $_GET['type_packaging_coffee'];
                    $this->request->data['RemittancesCaffee']['coffee_to_fumigation'] = $_GET['coffee_to_fumigation'];
                    $this->request->data['RemittancesCaffee']['cargolot_id'] = $_GET['cargolot_id'];
                    $this->request->data['RemittancesCaffee']['total_weight_net_nominal'] = $this->request->data['RemittancesCaffee']['quantity_radicated_bag_in'] * $unit_selected['UnitsCaffee']['quantity'];
                    if ($this->RemittancesCaffee->save($this->request->data)) {
                        $msg = "Se agrego exitosamente la remesa " . $this->RemittancesCaffee->id . '.';
                        $this->set(array(
                            'message' => array('msg' => $msg, 'id-msg' => 1, 'strtotime' => strtotime($this->request->data['RemittancesCaffee']['created_date']), 'transaction-id' => md5(strtotime($this->request->data['RemittancesCaffee']['created_date']))),
                            '_serialize' => array('message')
                        ));
                    } else {
                        $msg = "No se pudo registrar la remesa";
                        $this->set(array(
                            'message' => array($msg, 'id-msg' => 2),
                            '_serialize' => array('message')
                        ));
                    }
                }
            }
        }
    }


    public function updateTare()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->autoRender = false;
        $id = $this->request->data["RemittancesCaffee"]["id"];
        if (!$this->RemittancesCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid remittances caffee'));
        } else {
            if ($this->request->data['RemittancesCaffee']['fraction']) {
                $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
                $nuevaTara = $remittancesCaffee['RemittancesCaffee']['tare_download'] + $this->request->data['RemittancesCaffee']['tare_download'];
                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.tare_download' => $nuevaTara), array('RemittancesCaffee.id' => $id));
                $this->Flash->success(__('Se actualizo la tara correctamente.'));
                return $this->redirect(array('action' => 'tare', $id));
            } else if ($this->RemittancesCaffee->save($this->request->data)) {
                $this->Flash->success(__('Se registro la tara correctamente.'));
                return $this->redirect(array('action' => 'download_caffee'));
            } else {
                $this->Flash->error(__('The remittances caffee could not be saved. Please, try again.'));
            }
        }
    }

    public function updateTare2()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->loadModel('DetailPackagingCaffee');
        $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.tara_packaging' => $this->request->data['RemittancesCaffee']['tare_download']), array(
            'and' => array(
                array('DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['RemittancesCaffee']['id']),
                array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['RemittancesCaffee']['oie'])
            )
        ));
        return $this->redirect(array('controller' => 'DetailPackagingCaffees', 'action' => 'packaging_empty'));
    }

    public function updateTare3()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $remittancesCaffeeReturnsCaffee = $this->RemittancesCaffeeReturnsCaffee->find(
            'first',
            array('conditions' =>
            array('and' => array(
                array('RemittancesCaffeeReturnsCaffee.remittances_caffee_id' => $this->request->data['RemittancesCaffee']['id']),
                array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $this->request->data['RemittancesCaffee']['return_id'])
            )))
        );
        if ($this->RemittancesCaffeeReturnsCaffee->updateAll(
            array('RemittancesCaffeeReturnsCaffee.tare_return' => $this->request->data['RemittancesCaffee']['weigth']),
            array('RemittancesCaffeeReturnsCaffee.id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['id'])
        )) {
            $this->Flash->success(__('Se registro la tara correctamente.'));
            return $this->redirect(array('controller' => 'RemittancesCaffees', 'action' => 'return_index'));
        } else {
            $this->Flash->error(__('No se realizo la operaci?n. Intentelo nuevamente'));
        }
    }


    public function tare($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('User');
        $this->loadModel('Bascule');
        $this->layout = 'basculero';
        $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $this->request->data = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $id)));
        $userCurrent = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
        $bascule = $this->Bascule->find('list', array('conditions' => array('and' => array('Bascule.in_operation' => 1, 'Bascule.packging_anormal' => $dataRemittancesCaffee['RemittancesCaffee']['coffee_with_packging_anormal'], 'Bascule.store_id' => $userCurrent['User']['ref_store'])), 'fields' => array('Bascule.bascule', 'Bascule.name')));
        $this->set('user', $userCurrent);
        $this->set('bascule', $bascule);
    }



    public function tare2()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $oie = isset($this->request->query['oie']) ? $this->request->query['oie'] : null;
        $this->loadModel('User');
        $this->layout = 'basculero';
        $this->request->data = $remesa;
        $this->set('oie', $oie);
        $this->set('remesa', $remesa);
        $this->set('user', $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id')))));
    }

    public function tare3()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('User');
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $this->layout = 'basculero';
        $this->RemittancesCaffeeReturnsCaffee->recursive = 2;
        $remesa = isset($this->request->query['remittances_coffee_id']) ? $this->request->query['remittances_coffee_id'] : null;
        $return_id = isset($this->request->query['return_id']) ? $this->request->query['return_id'] : null;
        $this->request->data = $this->RemittancesCaffeeReturnsCaffee->find('first', array('conditions' => array('and' => array('RemittancesCaffeeReturnsCaffee.remittances_caffee_id' => $remesa), array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $return_id))));
        $this->set('user', $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id')))));
    }



    public function endUnload($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
    }

    public function delete($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->id = $id;
        if (!$this->RemittancesCaffee->exists()) {
            throw new NotFoundException(__('Invalid remittances caffee'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RemittancesCaffee->delete()) {
            $this->Flash->success(__('The remittances caffee has been deleted.'));
        } else {
            $this->Flash->error(__('The remittances caffee could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * find method
     *
     * @return void
     */
    public function findRemittancesByExportCode($exportCode = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.exporter_code' => $exportCode)));
        if ($dataClient) {
            $this->Paginator->settings = array(
                'conditions' => array('RemittancesCaffee.client_id' => $dataClient['Client']['id']),
                'limit' => 10
            );
            $dataRemittance = $this->Paginator->paginate('RemittancesCaffee');
            $data = array(
                'clientData' => $dataClient,
                'remittancesData' => $dataRemittance
            );
            return json_encode($data);
        } else {
            return "";
        }
    }

    public function findByRemittancesClient($clientRemittances = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $porciones = explode("-", $clientRemittances);
        $client = $this->Client->find('first', array('conditions' => array('Client.exporter_code' => $porciones[1])));
        //debug($client);exit;
        $dataRemittance = $this->RemittancesCaffee->find(
            'all',
            array('conditions' => array(
                'AND' => array(
                    'RemittancesCaffee.id' => $porciones[0],
                    'RemittancesCaffee.client_id' => $client['Client']['id']
                )
            ))
        );
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByRemittanceId($remittanceId = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $dataRemittance = $this->RemittancesCaffee->find('all', array('conditions' => array('RemittancesCaffee.id' => $remittanceId)));
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByCargoLotId($cargolotid = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $dataRemittance = $this->RemittancesCaffee->find('all', array('conditions' => array('RemittancesCaffee.cargolot_id' => $cargolotid)));
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByCargoLotIdClient($clientLotCoffee = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $porciones = explode("_", $clientLotCoffee);
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $client = $this->Client->find('all', array('conditions' => array('Client.exporter_code' => $porciones[0])));
        $dataRemittance = $this->RemittancesCaffee->find(
            'all',
            array('conditions' => array(
                'AND' => array(
                    'RemittancesCaffee.cargolot_id' => $porciones[1],
                    'RemittancesCaffee.client_id' => $client[0]['Client']['id']
                )
            ))
        );
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByLot($lot = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $dataRemittance = $this->RemittancesCaffee->find('all', array('conditions' => array('RemittancesCaffee.lot_caffee' => $lot)));
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesBySlotStore($slotStore = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $dataRemittance = $this->RemittancesCaffee->find('all', array('conditions' => array('and' => array('RemittancesCaffee.slot_store_id' => $slotStore, 'RemittancesCaffee.jetty' => 1, 'RemittancesCaffee.is_active' => true))));
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByLotClient($lotClient = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $porciones = explode("-", $lotClient);
        $client = $this->Client->find('all', array('conditions' => array('Client.exporter_code' => $porciones[0])));
        $dataRemittance = $this->RemittancesCaffee->find(
            'all',
            array('conditions' => array(
                'AND' => array(
                    'RemittancesCaffee.lot_caffee' => $porciones[1],
                    'RemittancesCaffee.client_id' => $client[0]['Client']['id'],
                    'RemittancesCaffee.slot_store_id != ' => null,
                    'RemittancesCaffee.state_operation_id BETWEEN 1 AND 2',

                )
            ))
        );
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findRemittancesByLotClientMultiple()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $lotClient = $this->request->query['q'];
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $porciones = explode("-", $lotClient);
        $client = $this->Client->find('all', array('conditions' => array('Client.exporter_code' => $porciones[0])));
        $dataRemittance = $this->RemittancesCaffee->find(
            'all',
            array('conditions' => array(
                'AND' => array(
                    'RemittancesCaffee.lot_caffee' => $porciones[1],
                    'RemittancesCaffee.client_id' => $client[0]['Client']['id'],
                    'RemittancesCaffee.slot_store_id != ' => null,
                    'RemittancesCaffee.state_operation_id BETWEEN 1 AND 2',

                )
            ))
        );
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function findByLotClient($lotClient = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->RemittancesCaffee->recursive = 2;
        $this->autoRender = false;
        $this->loadModel('Client');
        $porciones = explode("-", $lotClient);
        $client = $this->Client->find('all', array('conditions' => array('Client.exporter_code' => $porciones[0])));
        $dataRemittance = $this->RemittancesCaffee->find(
            'all',
            array('conditions' => array(
                'AND' => array(
                    'RemittancesCaffee.lot_caffee' => $porciones[1],
                    'RemittancesCaffee.client_id' => $client[0]['Client']['id']
                )
            ))
        );
        if ($dataRemittance) {
            return json_encode($dataRemittance);
        } else {
            return "";
        }
    }

    public function return_final($id = null)
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $this->loadModel('WeighingReturnCoffee');
        $this->loadModel('ReturnsCaffee');
        $this->layout = 'colaborador';
        $remittancesCaffeeReturnsCaffee =  $this->RemittancesCaffeeReturnsCaffee->find('first', array('conditions' => array('RemittancesCaffeeReturnsCaffee.id' => $id)));
        $remesa = $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['remittances_caffee_id'];
        $return_id = $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id'];
        $dataReturnCoffee = $this->ReturnsCaffee->find('first', array('conditions' => array('ReturnsCaffee.id' => $return_id)));
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
        $bagReturned = $this->WeighingReturnCoffee->find('all', array('fields' => array('sum(WeighingReturnCoffee.quantity_bag_pallet) AS sacosdevueltos'), 'conditions' => array('and' => array(array('WeighingReturnCoffee.return_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['returns_caffee_id']), array('WeighingReturnCoffee.remittances_caffee_id' => $remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['remittances_caffee_id'])))));
        if ($remittancesCaffeeReturnsCaffee['RemittancesCaffeeReturnsCaffee']['qta_bags'] == $bagReturned[0][0]['sacosdevueltos']) {
            if ($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] == 0) {
                if ($dataReturnCoffee['ReturnsCaffee']['return_type'] == 'FIQUE') {
                    $this->RemittancesCaffee->updateAll(
                        array(
                            'RemittancesCaffee.state_operation_id' => 2,
                            'RemittancesCaffee.quantity_bag_out_store' => 0,
                            'RemittancesCaffee.quantity_out_pallet_caffee' => 0,
                            'RemittancesCaffee.quantity_radicated_bag_in' => 0,
                            'RemittancesCaffee.quantity_radicated_bag_out' => 0,
                            'RemittancesCaffee.total_weight_net_real' => 0,
                            'RemittancesCaffee.total_rad_bag_out' => 0
                        ),
                        array('RemittancesCaffee.id' => $remesa)
                    );
                } else {
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id' => 9), array('RemittancesCaffee.id' => $remesa));
                }
            } else {
                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id' => 2), array('RemittancesCaffee.id' => $remesa));
            }
            $this->RemittancesCaffeeReturnsCaffee->updateAll(array('RemittancesCaffeeReturnsCaffee.state' => 2), array('RemittancesCaffeeReturnsCaffee.id' => $id));
            $isCompletedReturn = $this->RemittancesCaffeeReturnsCaffee->find('all', array('conditions' => array('and' => array(
                array('RemittancesCaffeeReturnsCaffee.returns_caffee_id' => $return_id),
                array('RemittancesCaffeeReturnsCaffee.state' => 1)
            ))));
            if ($isCompletedReturn == null) {
                $this->ReturnsCaffee->updateAll(array('ReturnsCaffee.state_return' => "'" . "COMPLETADA" . "'"), array('ReturnsCaffee.id' => $return_id));
            }
        } else {
            $this->Flash->error(__('Faltan sacos por devolver de la remsa ' . $remesa . '. Por favor verificar'));
            return $this->redirect(array('action' => 'return_index'));
        }
        $this->Flash->success(__('Se finalizo el pesaje de la remesa ' . $remesa));
        return $this->redirect(array('action' => 'return_index'));
    }

    public function returnCaffeeRemittances()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $this->loadModel('ReturnsCaffee');
        $this->layout = 'colaborador';
        $this->RemittancesCaffee->recursive = 2;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $return_id = isset($this->request->query['return_id']) ? $this->request->query['return_id'] : null;
        $qta_bags = isset($this->request->query['qta_bags']) ? $this->request->query['qta_bags'] : null;
        $type = isset($this->request->query['type']) ? $this->request->query['type'] : null;
        if ($type == "FIQUE") {
            $this->RemittancesCaffee->updateAll(
                array(
                    'quantity_bag_in_store' => $qta_bags,
                    'quantity_radicated_bag_in' => $qta_bags,
                    'total_rad_bag_out' => 0,
                    'quantity_radicated_bag_out' => 0,
                    'state_operation_id' => 9
                ),
                array('RemittancesCaffee.id' => $remesa)
            );
        }
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
        $qtaReturnCoffee = ($remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $qta_bags >= 0) ? true : false;
        if ($remesa && $qtaReturnCoffee) {
            $this->RemittancesCaffeeReturnsCaffee->create();
            $dataRemittancesCaffeeReturns = array(
                'remittances_caffee_id' => $remesa,
                'returns_caffee_id' => $return_id,
                'qta_bags' => $qta_bags
            );
            $this->RemittancesCaffeeReturnsCaffee->save($dataRemittancesCaffeeReturns);
            $this->RemittancesCaffee->updateAll(array('total_rad_bag_out' => ($remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] + $qta_bags), 'quantity_radicated_bag_out' => ($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_out'] + $qta_bags), 'state_operation_id' => 9), array('RemittancesCaffee.id' => $remesa));
            //$this->Flash->success(__('Se registro correctamente la devoluci�n la remesa almacenada ' . $remesa));
            return json_encode($dataRemittancesCaffeeReturns);
        } else {
            //$this->Flash->error(__('No se pudo realizar la devoluci�n de la remesa almacenada ' . $remesa. 'por saldos insuficientes.'));
            return "";
        }
        //return $this->redirect(array('controller'=>'ReturnsCaffees','action' => 'return_lot',$return_id));
    }

    public function deleteReturnCaffeeRemittances()
    {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender = false;
        $this->loadModel('RemittancesCaffeeReturnsCaffee');
        $detalle_id = isset($this->request->query['detalle_id']) ? $this->request->query['detalle_id'] : null;
        $dataReturnsCaffee = $this->RemittancesCaffeeReturnsCaffee->find('first', array('conditions' =>  array('RemittancesCaffeeReturnsCaffee.id' => $detalle_id)));
        $remittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('and' => array(array('RemittancesCaffee.id' => $dataReturnsCaffee['RemittancesCaffee']['id']), array('or' => array('RemittancesCaffee.quantity_bag_out_store' == null), array('RemittancesCaffee.quantity_bag_out_store' == 0))))));
        if ($dataReturnsCaffee) {
            $qta_bags = $dataReturnsCaffee['RemittancesCaffeeReturnsCaffee']['qta_bags'];
            $this->RemittancesCaffeeReturnsCaffee->delete($dataReturnsCaffee['RemittancesCaffeeReturnsCaffee']['id']);
            if ($remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] == $qta_bags) {
                $this->RemittancesCaffee->updateAll(array('total_rad_bag_out' => ($remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] - $qta_bags), 'quantity_radicated_bag_out' => ($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_out'] - $qta_bags), 'state_operation_id' => 2), array('RemittancesCaffee.id' => $remittancesCaffee['RemittancesCaffee']['id']));
            } else {
                $this->RemittancesCaffee->updateAll(array('total_rad_bag_out' => ($remittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] - $qta_bags), 'quantity_radicated_bag_out' => ($remittancesCaffee['RemittancesCaffee']['quantity_radicated_bag_out'] - $qta_bags), 'state_operation_id' => 9), array('RemittancesCaffee.id' => $remittancesCaffee['RemittancesCaffee']['id']));
            }
            //$this->Flash->success(__('Se elimino la devolución de la remesa ' . $remesa));
            return json_encode($dataReturnsCaffee);
        } else {
            //$this->Flash->error(__('No se puede eliminar la remesa almacenada ' . $remesa));
            return "";
        }
        //return $this->redirect(array('controller'=>'ReturnsCaffees','action' => 'return_lot',$return_id));
    }
}
