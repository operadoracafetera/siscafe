<?php
App::uses('AppController', 'Controller');
require_once("../Vendor/aws/aws-autoloader.php");
 
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
/**
 * OperationTrackings Controller
 *
 * @property OperationTracking $OperationTracking
 * @property PaginatorComponent $Paginator
 */
class OperationTrackingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($typeTracking = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
        $dateUser = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id'))));
        if($dateUser['User']['cod_city_operation'] == 'BUN'){
            $this->paginate = array(
            'limit' => 50,
            'order' => array('OperationTracking.id'=>'desc'),
            'conditions' =>
                    array('and' => array(
                        array('OperationTracking.type_tracking' => array(1,4,6,7)),
                        array('OperationTracking.jetty = ' => "BUN"),
			array('OperationTracking.active' => true)
                        )
            ));
        }
        else if($dateUser['User']['cod_city_operation'] == 'STM'){
            $this->paginate = array(
            'limit' => 50,
            'order' => array('OperationTracking.id'=>'desc'),
            'conditions' =>
                    array('and' => array(
                        array('OperationTracking.type_tracking' => array(1,4,6,7)),
                        array('OperationTracking.jetty' => "STM"),
			array('OperationTracking.active' => true)
                        )
            ));
        }
	else if($dateUser['User']['cod_city_operation'] == 'SPIA'){
            $this->paginate = array(
            'limit' => 50,
            'order' => array('OperationTracking.id'=>'desc'),
            'conditions' =>
                    array('and' => array(
                        array('OperationTracking.type_tracking' => array(1,4,6,7)),
                        array('OperationTracking.jetty' => "SPIA"),
			array('OperationTracking.active' => true)
                        )
            ));
        }
        else if($dateUser['User']['cod_city_operation'] == 'IMP'){
            $this->paginate = array(
            'limit' => 50,
            'order' => array('OperationTracking.id'=>'desc'),
            'conditions' =>
                    array('and' => array(
                        array('OperationTracking.type_tracking' => array(1,4,6,7)),
                        array('OperationTracking.jetty' => "IMP"),
			array('OperationTracking.active' => true)
                        )
            ));
        }
        else{
            $this->paginate = array(
            'limit' => 50,
            'order' => array('OperationTracking.id'=>'desc'),
            'conditions' =>
                    array('and' => array(
                        array('OperationTracking.type_tracking' => array(1,4,6,7)),
                        array('OperationTracking.jetty != ' => "BUN"),
                        array('OperationTracking.jetty != ' => "STM"),
			array('OperationTracking.jetty != ' => "SPIA"),
			array('OperationTracking.active' => true)
                        )
            ));
        }
        $this->OperationTracking->recursive = 0;
        $this->set('operationTrackings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
            	return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            if($this->Session->read('User.profiles_id') == 1){
                $this->layout = 'colaborador';
            }
            else if($this->Session->read('User.profiles_id') == 6){
                $this->layout = 'terminal';
            }
            $this->loadModel('User');
            $this->loadModel('TrackingHasPhoto');
            $this->loadModel('ViewPackaging');
	    $this->loadModel('ViewPackaging2');
            if (!$this->OperationTracking->exists($id)) {
                    throw new NotFoundException(__('Invalid operation tracking'));
            }
            $options = array('conditions' => array('OperationTracking.' . $this->OperationTracking->primaryKey => $id));
            $dataOperationTracking = $this->OperationTracking->find('first', $options);
            $this->set('operationTracking', $dataOperationTracking);
            
            $optionsPhotos = array('conditions' =>
                    array('and' => array(
                        array('TrackingHasPhoto.tracking_id = ' => $id),
						array('TrackingHasPhoto.active' => true)
                        )));
            $dataPhotos = $this->TrackingHasPhoto->find('all', $optionsPhotos);
            
            $options1 = array('conditions' => array('ViewPackaging.' . $this->ViewPackaging->primaryKey => $dataOperationTracking['OperationTracking']['ref_text']));
	    
            $dataTarja = $this->ViewPackaging->find('first', $options1);
	
            if($dataTarja){
                $this->set('tarja', $dataTarja);
            }
            else{
                $this->set('tarja', null);
            }
            
            $this->set('dataPhotos', $dataPhotos);
	}
        
     /**
 * downloadAllPhotos method
 *
 * @return void
 */   
     public function downloadAllPhotos($id = null ){
         if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
         }
         $this->layout = 'colaborador';
         $this->loadModel('TrackingHasPhoto');
         $optionsPhotos = array('conditions' =>
                    array('or' => array(
                        array('TrackingHasPhoto.tracking_id = ' => $id)
                        )));
         $dataPhotos = $this->TrackingHasPhoto->find('all',$optionsPhotos);
         
         
 
         $zip = new ZipArchive();
         $nameZip = "/tmp/".$id.".zip";
         $zip->open($nameZip, \ZipArchive::CREATE);
 
         foreach ($dataPhotos as $photo){
             $zip->addFromString($photo['TrackingHasPhoto']['name_file'], file_get_contents("https://trackingphotos.s3.amazonaws.com/".$id."/".$photo['TrackingHasPhoto']['name_file']));
         }
          
         $zip->close();
 
         header('Content-disposition: attachment; filename='.$id.'.zip');
         header('Content-type: application/zip');
         readfile("/tmp/".$id.".zip");
 
     }

/**
 * add method
 *
 * @return void
 */
	public function add() {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('User');
		$this->loadModel('Client');
		if ($this->request->is('post')) {
            date_default_timezone_set('America/Bogota');
			$this->OperationTracking->create();
            $this->request->data['OperationTracking']['created_date'] = date('Y-m-d H:i:s');
			$this->request->data['OperationTracking']['trackaing_fnc'] = $this->request->data['OperationTracking']['trackaing_fnc'];
			$this->request->data['OperationTracking']['break_seal'] = $this->request->data['OperationTracking']['break_seal'];
			$this->request->data['OperationTracking']['user_reg'] = $this->Session->read('User.id');
			if ($this->OperationTracking->save($this->request->data)) {
                            $bucket = 'trackingphotos';
                            $keyname = $this->OperationTracking->id."/";
                            
                            $s3Client = new S3Client([
                            'version' => 'latest',
                            'region'  => 'us-east-1',
                            'credentials' => [
                            'key'    => "AKIAJZHFB4GIIF3FDWMA",
                            'secret' => "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",]]);
                            
                             $s3Client->putObject(array( 
                                'Bucket' => $bucket,
                                'Key'    => $keyname
                               ));
                            
		             $this->Flash->success(__('Registro creado exitosamente!'));
		             return $this->redirect(array('action' => 'index',1));
			} else {
				$this->Flash->error(__('The operation tracking could not be saved. Please, try again.'));
			}
		}
		$dataClients = $this->Client->find('all');
		
		foreach ($dataClients as $key => $value) {
           $listClients[$value['Client']['id']] = $value['Client']['exporter_code'] . " " . $value['Client']['business_name'] . " " ;
        }
						
		$this->set('clients',$listClients);
	}


	/**
 * add method
 *
 * @return void
 */
	public function addfumigation($id = null) {
        if (!$this->Session->read('User.id')) {
        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
    $this->autoRender=false;
    $this->loadModel('FumigationService');
        date_default_timezone_set('America/Bogota');
        $this->OperationTracking->create();
        $dataOperationTracking = $this->OperationTracking->find('first',array('conditions'=>['AND'=>['OperationTracking.ref_text LIKE '=>"%".$id."%"],['OperationTracking.type_tracking'=>'10']]));
        if(!$dataOperationTracking){
            $this->request->data['OperationTracking']['created_date'] = date('Y-m-d H:i:s');
            $this->request->data['OperationTracking']['type_tracking'] = 10;//fumigaciones
            $this->request->data['OperationTracking']['ref_text'] = $id;
            if ($this->OperationTracking->save($this->request->data)) {
                $bucket = 'trackingphotos';
                $keyname = $this->OperationTracking->id."/";
                $this->FumigationService->updateAll(array('FumigationService.tracking_id'=>"'".$this->OperationTracking->id."'"),array('FumigationService.id'=>$id));

                $s3Client = new S3Client([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'credentials' => [
                'key'    => "AKIAJZHFB4GIIF3FDWMA",
                'secret' => "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",]]);

                 $s3Client->putObject(array( 
                    'Bucket' => $bucket,
                    'Key'    => $keyname
                   ));

                 $this->Flash->success(__('Registro creado exitosamente!'));
                 return $this->redirect(array('action' => 'view',$this->OperationTracking->id));
            }
        }
        else{
            return $this->redirect(array('action' => 'view',$dataOperationTracking['OperationTracking']['id']));
        }
}
        
        
    /**
 * add method
 *
 * @return void
 */
	public function addctns($idOie = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender=false;
        $this->loadModel('PackagingCaffee');
            date_default_timezone_set('America/Bogota');
            $this->OperationTracking->create();
            $dataOperationTracking = $this->OperationTracking->find('first',array('conditions'=>array('OperationTracking.ref_text LIKE '=>"%".$idOie."%")));
            if(!$dataOperationTracking){
                $this->PackagingCaffee->updateAll(array('PackagingCaffee.upload_tracking_photos_date'=>"'".date('Y-m-d H:i:s')."'"),array('PackagingCaffee.id'=>$idOie));
                $this->request->data['OperationTracking']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['OperationTracking']['type_tracking'] = 2;
                $this->request->data['OperationTracking']['ref_text'] = $idOie;
                if ($this->OperationTracking->save($this->request->data)) {
                    $bucket = 'trackingphotos';
                    $keyname = $this->OperationTracking->id."/";

                    $s3Client = new S3Client([
                    'version' => 'latest',
                    'region'  => 'us-east-1',
                    'credentials' => [
                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                    'secret' => "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",]]);

                     $s3Client->putObject(array( 
                        'Bucket' => $bucket,
                        'Key'    => $keyname
                       ));

                     $this->Flash->success(__('Registro creado exitosamente!'));
                     return $this->redirect(array('action' => 'view',$this->OperationTracking->id));
                }
            }
            else{
                return $this->redirect(array('action' => 'view',$dataOperationTracking['OperationTracking']['id']));
            }
	}


	/**
 * addreturn method
 *
 * @return void
 */
	public function addreturn($idReturn = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->autoRender=false;
            date_default_timezone_set('America/Bogota');
            $this->OperationTracking->create();
            $dataOperationTracking = $this->OperationTracking->find('first',array('conditions'=>['and'=>[['OperationTracking.type_tracking'=>3],['OperationTracking.ref_text LIKE '=>"%".$idReturn."%"]]]));
            if(!$dataOperationTracking){
                $this->request->data['OperationTracking']['created_date'] = date('Y-m-d H:i:s');
                $this->request->data['OperationTracking']['type_tracking'] = 3;
                $this->request->data['OperationTracking']['ref_text'] = $idReturn;
                if ($this->OperationTracking->save($this->request->data)) {
                    $bucket = 'trackingphotos';
                    $keyname = $this->OperationTracking->id."/";

                    $s3Client = new S3Client([
                    'version' => 'latest',
                    'region'  => 'us-east-1',
                    'credentials' => [
                    'key'    => "AKIAJZHFB4GIIF3FDWMA",
                    'secret' => "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",]]);

                     $s3Client->putObject(array( 
                        'Bucket' => $bucket,
                        'Key'    => $keyname
                       ));

                     $this->Flash->success(__('Registro creado exitosamente!'));
                     return $this->redirect(array('action' => 'view',$this->OperationTracking->id));
                }
            }
            else{
                return $this->redirect(array('action' => 'view',$dataOperationTracking['OperationTracking']['id']));
            }
	}
        
        
        /**
 * savePhoto method
 *
 * @return void
 */
	public function savePhoto() {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('TrackingHasPhoto');
            if ($this->request->is('post')) {
                    $this->TrackingHasPhoto->create();   
                    date_default_timezone_set('America/Bogota');
                    $fileUploaded = $this->request->data['TrackingHasPhoto']['name_file'];
                    $bucket = 'trackingphotos';
                    
                    $keyname = $fileUploaded['name'];
                    $temp_file_location = $fileUploaded['tmp_name'];
                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region'  => 'us-east-1',
                        'credentials' => [
                        'key'    => "AKIAJZHFB4GIIF3FDWMA",
                        'secret' => "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",]]);

                    $result = $s3->putObject([
                            'Bucket' => 'trackingphotos',
                            'Key'    => $this->request->data['TrackingHasPhoto']['trackging_id']."/".$keyname,
                            'SourceFile' => $temp_file_location
                    ]);
                    
                    $this->request->data['TrackingHasPhoto']['name_file'] = $keyname;
                    $this->request->data['TrackingHasPhoto']['created_date'] = date('Y-m-d H:i:s');
                    $this->request->data['TrackingHasPhoto']['tracking_id'] = $this->request->data['TrackingHasPhoto']['trackging_id'];
                    
                    if ($this->TrackingHasPhoto->save($this->request->data)) {
                            $this->Flash->success(__('Fotografia cargada exitosamente!'));
                            return $this->redirect(array('action' => 'view',$this->request->data['TrackingHasPhoto']['trackging_id']));
                    } else {
                            $this->Flash->error(__('The operation tracking could not be saved. Please, try again.'));
                            return $this->redirect(array('action' => 'view',$this->request->data['TrackingHasPhoto']['trackging_id']));
                    }
            }
        
	}
        
        
         /**
        * getTrackingRequest method
        *
        * @return void
        */
       public function getTrackingRequest($idRequest = null){
           if (!$this->Session->read('User.id')) {
               //return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->autoRender=false;
           $this->layout = 'colaborador';
           $this->loadModel('TrackingHasPhoto');
           $dataOperationTracking = $this->TrackingHasPhoto->find('first',array('conditions' => array('TrackingHasPhoto.' . $this->TrackingHasPhoto->primaryKey => $idRequest)));
           $bucket = 'trackingphotos';
           $keyname = $dataOperationTracking['TrackingHasPhoto']['name_file'];
           $folder = $dataOperationTracking['TrackingHasPhoto']['tracking_id'];

           $s3 = new S3Client([
               'version' => 'latest',
               'region'  => 'us-east-1',
               'credentials' => [
               'key'    => "AKIAJZHFB4GIIF3FDWMA",
               'secret' => "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",
           ]
           ]);

           try {
               $result = $s3->getObject([
                   'Bucket' => $bucket,
                   'Key'    => $folder."/".$keyname,
               ]);

               header("Content-Type: {$result['ContentType']}");
               header('Content-Disposition: attachment; filename='.$keyname);
               echo $result['Body'];
           } catch (S3Exception $e) {
               $this->Flash->error(__($e));
           }

       }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
             if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->layout = 'colaborador';
		   $this->loadModel('Client');
		if (!$this->OperationTracking->exists($id)) {
			throw new NotFoundException(__('Invalid operation tracking'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['OperationTracking']['ref_text']="'".$this->request->data['OperationTracking']['ref_text']."'";
            $this->request->data['OperationTracking']['observation']="'".$this->request->data['OperationTracking']['observation']."'";
            $this->request->data['OperationTracking']['driver_name']="'".$this->request->data['OperationTracking']['driver_name']."'";
            $this->request->data['OperationTracking']['driver_id']="'".$this->request->data['OperationTracking']['driver_id']."'";
            $this->request->data['OperationTracking']['seals']="'".$this->request->data['OperationTracking']['seals']."'";
            $this->request->data['OperationTracking']['jetty']="'".$this->request->data['OperationTracking']['jetty']."'";
			$this->request->data['OperationTracking']['trackaing_fnc'] = "'".$this->request->data['OperationTracking']['trackaing_fnc']."'";
			$this->request->data['OperationTracking']['break_seal'] = "'".$this->request->data['OperationTracking']['break_seal']."'";
			$this->request->data['OperationTracking']['ref_send'] = "'".$this->request->data['OperationTracking']['ref_send']."'";
			$this->request->data['OperationTracking']['item1'] = "'".$this->request->data['OperationTracking']['item1']."'";
			$this->request->data['OperationTracking']['item2'] = "'".$this->request->data['OperationTracking']['item2']."'";
			$this->request->data['OperationTracking']['item4'] = "'".$this->request->data['OperationTracking']['item4']."'";
			$this->request->data['OperationTracking']['item5'] = "'".$this->request->data['OperationTracking']['item5']."'";

			if ($this->OperationTracking->updateAll(
                                $this->request->data['OperationTracking'],
                                array('OperationTracking.id'=>$this->request->data['OperationTracking']['id'])
                                )) {
				$this->Flash->success(__('El registro se actualizo correctemente'));
				return $this->redirect(array('action' => 'index',1));
			} else {
				$this->Flash->error(__('The operation tracking could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OperationTracking.' . $this->OperationTracking->primaryKey => $id));
			$this->request->data = $this->OperationTracking->find('first', $options);
			
			$dataClients = $this->Client->find('all');
		
			foreach ($dataClients as $key => $value) {
			   $listClients[$value['Client']['id']] = $value['Client']['exporter_code'] . " " . $value['Client']['business_name'] . " " ;
			}
						
			$this->set('clients',$listClients);
		}
	}
        
        /**
        * search method
        *
        * @throws NotFoundException
        * @param string $id
        * @return void
        */
        public function search(){
            if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->layout = 'colaborador';
        }
        
        
        /**
        * search findByPlate
        *
        * @throws NotFoundException
        * @param string $id
        * @return void
        */
        public function findByPlate($plateCafe = null){
            if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->layout = 'colaborador';
           $this->autoRender=false;
           $dataTracking = $this->OperationTracking->find('all',
                    array('conditions' => array('and'=>
                        //array('OperationTracking.ref_text LIKE'=>"%".$plateCafe."%"),array('YEAR(OperationTracking.created_date)'=>date("Y")))));
			array('OperationTracking.ref_text LIKE'=>"%".$plateCafe."%"))));
           return json_encode($dataTracking);
           //debug($dataTracking);
        }
        
        /**
        * search findBylotCoffee
        *
        * @throws NotFoundException
        * @param string $id
        * @return void
        */
        public function findBylotCoffee($lotCoffee = null){
            if (!$this->Session->read('User.id')) {
               return $this->redirect(array('controller' => 'users', 'action' => 'login'));
           }
           $this->layout = 'colaborador';
           $this->autoRender=false;
           $dataTracking = $this->OperationTracking->find('all',
                    array('conditions' => array('and'=>
                        array('OperationTracking.observation LIKE '=>"%".$lotCoffee."%"),array('YEAR(OperationTracking.created_date)'=>date("Y")))));
           return json_encode($dataTracking);
           //debug($dataTracking);
        }
        
        /**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function deletePhoto($id = null) {
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
		$this->loadModel('TrackingHasPhoto');
		if ($this->TrackingHasPhoto->updateAll(['TrackingHasPhoto.active'=>false],['TrackingHasPhoto.id'=>$id])) {
			$this->Flash->success(__('Se elimino el registro correctamente'));
		} else {
			$this->Flash->error(__('No se pudo realizar la operación.Intentar nuevamente'));
		}
		return $this->redirect($this->referer());
	}
        

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->OperationTracking->id = $id;
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
		$this->loadModel('TrackingHasPhoto');
		$disableTrackingHasPhoto = $this->TrackingHasPhoto->updateAll(['TrackingHasPhoto.active'=>false],['TrackingHasPhoto.tracking_id'=>$id]);
		$disableOperationTracking = $this->OperationTracking->updateAll(['OperationTracking.active'=>false],['OperationTracking.id'=>$id]);
		if ($disableOperationTracking && $disableTrackingHasPhoto) {
			$this->Flash->success(__('Se elimino el registro correctamente'));
		} else {
			$this->Flash->error(__('No se pudo realizar la operación.Intentar nuevamente'));
		}
		return $this->redirect(array('action' => 'index'));
	}

}
