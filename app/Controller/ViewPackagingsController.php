<?php
App::uses('AppController', 'Controller');
/**
 * ViewPackagings Controller
 *
 * @property ViewPackaging $ViewPackaging
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ViewPackagingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
        var $helpers = array('Csv');

	/**
         * view method
         *
         * @throws NotFoundException
         * @param string $id
         * @return void
         */
	public function view($id = null) {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }

            if($this->Session->read('User.profiles_id') == 1){
                $this->layout = 'colaborador';
            }
            else if($this->Session->read('User.profiles_id') == 6){
                $this->layout = 'terminal';
            }
            if (!$this->ViewPackaging->exists($id)) {
                    throw new NotFoundException(__('Invalid view packaging'));
            }
            $options = array('conditions' => array('ViewPackaging.' . $this->ViewPackaging->primaryKey => $id));
            $this->set('viewPackaging', $this->ViewPackaging->find('first', $options));
	}
        
        public function download(){
            $this->layout = null;
            $dateIni = isset($this->request->query['dateIni']) ? $this->request->query['dateIni'] : null;
            $dateFin = isset($this->request->query['dateFin']) ? $this->request->query['dateFin'] : null;
	    $options = array(
                array(
                    'fields'=> array('ViewPackaging.oie','ViewPackaging.estado_embalaje',
                    'ViewPackaging.estado_embalaje','ViewPackaging.bic_ctn','ViewPackaging.lotes','ViewPackaging.total_sacos','ViewPackaging.booking','ViewPackaging.proforma',
                    'ViewPackaging.travel_num','ViewPackaging.business_name','ViewPackaging.exportador','ViewPackaging.expotador_code',
                    'ViewPackaging.agente_naviero','ViewPackaging.total_sacos','ViewPackaging.packaging_mode','ViewPackaging.packaging_type','ViewPackaging.seal_1',
                    'ViewPackaging.seal_2','ViewPackaging.packaging_date','ViewPackaging.iso_ctn','ViewPackaging.total_cafe_empaque','ViewPackaging.tare_estibas_descargue',
                    'ViewPackaging.tare_empaque','ViewPackaging.elementos_adicionales','ViewPackaging.motorship_name')),
                    array('conditions' => array(
                        'and' =>(array(
                            array("DATE(ViewPackaging.packaging_date) BETWEEN DATE('".$dateIni."') AND DATE('".$dateFin."')"),
                            array("ViewPackaging.jetty" => "BUN"))))));
	    //debug($options);exit;
            $dataViewPackaging = $this->ViewPackaging->find('all', $options);
	    
            $this->set('viewPackagings', $dataViewPackaging);
            $this->autoLayout = false;
            Configure::write('debug', '0');
        }
        
        public function result() {
            if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $dateIni = isset($this->request->query['dateIni']) ? $this->request->query['dateIni'] : null;
            $dateFin = isset($this->request->query['dateFin']) ? $this->request->query['dateFin'] : null;
            if($this->Session->read('User.profiles_id') == 1){
                $this->layout = 'colaborador';
            }
            else if($this->Session->read('User.profiles_id') == 6){
                $this->layout = 'terminal';
            }
            $this->Paginator->settings = array(
            'conditions' => array(
                'and' =>
                array('or' => array(
                        array("ViewPackaging.packaging_date BETWEEN DATE('".$dateIni."') AND DATE('".$dateFin."')")),
                    //array('RemittancesCaffee.staff_driver_id' => $this->Session->read('User.id')),
                )),
            'limit' => 50);
            $this->set('viewPackagings', $this->Paginator->paginate());
            $this->set('dateIni',$dateIni);
            $this->set('dateFin',$dateFin);
	}
        
        function findByOIE($oie = null) {
            $this->autoRender = false;
            $viewPackaging = $this->ViewPackaging->find('first', array('conditions' => array('AND'=> array('ViewPackaging.oie' => $oie))));
            if ($viewPackaging) {
                return json_encode($viewPackaging);
            } else {
                return "";
            }
        }
    
        function findByBic($bic = null) {
            $this->autoRender = false;
            $viewPackaging = $this->ViewPackaging->find('first', array('conditions' => array('AND'=> array('ViewPackaging.bic_ctn' => $bic))));
            if ($viewPackaging) {
                return json_encode($viewPackaging);
            } else {
                return "";
            }
        }
        
        function findByAutorizacion($autorizacion = null) {
            $this->autoRender = false;
            $viewPackaging = $this->ViewPackaging->find('first', array('conditions' => array('AND'=> array('ViewPackaging.autorization' => $autorizacion))));
            if ($viewPackaging) {
                return json_encode($viewPackaging);
            } else {
                return "";
            }
        }
        
        function findByLotClient($lotClient = null) {
            $this->autoRender = false;
            if($lotClient != null){
                $viewPackaging = $this->ViewPackaging->find('all', array('conditions' => array('ViewPackaging.lotes LIKE ' => '%'.$lotClient.'%')));
                if ($viewPackaging) {
                    return json_encode($viewPackaging);
                } else {
                    return "";
                }
            }
            else {
                    return "";
                }
        }
        
         function findByDates() {
            $this->autoRender = false;
            $dateIni = isset($this->request->query['dateIni']) ? $this->request->query['dateIni'] : null;
            $dateFin = isset($this->request->query['dateFin']) ? $this->request->query['dateFin'] : null;
            $dataViewPackaging = $this->ViewPackaging->find('all', array(
                'conditions' => array('AND' =>array(
                    array("ViewPackaging.packaging_date BETWEEN DATE('".$dateIni."') AND DATE('".$dateFin."')"),
                    array("ViewPackaging.jetty"=>"BUN")))
                ));
            if ($dataViewPackaging) {
                return json_encode($dataViewPackaging);
            } else {
                return "";
            }
           
        }
}
