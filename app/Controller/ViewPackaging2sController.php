<?php
App::uses('AppController', 'Controller');
/**
 * ViewPackaging2s Controller
 *
 * @property ViewPackaging2 $ViewPackaging2
 * @property PaginatorComponent $Paginator
 */
class ViewPackaging2sController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ViewPackaging2->recursive = 0;
		$this->set('viewPackaging2s', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		$options = array('conditions' => array('ViewPackaging2.oie' => $id));
		$this->set('viewPackaging2', $this->ViewPackaging2->find('first', $options));
	}

}
