<?php
App::uses('AppController', 'Controller');
/**
 * TrackingHasPhotos Controller
 *
 * @property TrackingHasPhoto $TrackingHasPhoto
 * @property PaginatorComponent $Paginator
 */
class TrackingHasPhotosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TrackingHasPhoto->recursive = 0;
		$this->set('trackingHasPhotos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TrackingHasPhoto->exists($id)) {
			throw new NotFoundException(__('Invalid tracking has photo'));
		}
		$options = array('conditions' => array('TrackingHasPhoto.' . $this->TrackingHasPhoto->primaryKey => $id));
		$this->set('trackingHasPhoto', $this->TrackingHasPhoto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TrackingHasPhoto->create();
                        
			if ($this->TrackingHasPhoto->save($this->request->data)) {
				$this->Flash->success(__('The tracking has photo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tracking has photo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TrackingHasPhoto->exists($id)) {
			throw new NotFoundException(__('Invalid tracking has photo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TrackingHasPhoto->save($this->request->data)) {
				$this->Flash->success(__('The tracking has photo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tracking has photo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TrackingHasPhoto.' . $this->TrackingHasPhoto->primaryKey => $id));
			$this->request->data = $this->TrackingHasPhoto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TrackingHasPhoto->id = $id;
                $this->autoRender=false;
		if (!$this->TrackingHasPhoto->exists()) {
			throw new NotFoundException(__('Invalid tracking has photo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TrackingHasPhoto->delete()) {
			$this->Flash->success(__('Se elimino la fotografia correctamente.'));
		} else {
			$this->Flash->error(__('The tracking has photo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller'=>'OperationTrackings','action' => 'index',1));
	}
}
