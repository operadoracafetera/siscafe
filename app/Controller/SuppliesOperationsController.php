<?php
App::uses('AppController', 'Controller');
/**
 * SuppliesOperations Controller
 *
 * @property SuppliesOperation $SuppliesOperation
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SuppliesOperationsController extends AppController {
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index($idSupply=null) {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->SuppliesOperation->recursive = 0;
            $this->Paginator->settings = array(
                'limit' => 30,
                'conditions' => array('SuppliesOperation.supplies_id' => $idSupply),
                'order' => array('SuppliesOperation.created_date' => 'DESC')
            );
            $supply = $this->SuppliesOperation->Supply->find('first',array('conditions' => array('Supply.id' => $idSupply)));
            $this->set('suppliesOperations', $this->Paginator->paginate('SuppliesOperation'));
            $this->set('supply',$supply);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		if (!$this->SuppliesOperation->exists($id)) {
			throw new NotFoundException(__('Invalid supplies operation'));
		}
		$options = array('conditions' => array('SuppliesOperation.' . $this->SuppliesOperation->primaryKey => $id));
		$this->set('suppliesOperation', $this->SuppliesOperation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idSupply=null) {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('InfoNavy');
            $supply = $this->SuppliesOperation->Supply->find('first',array('conditions' => array('Supply.id' => $idSupply)));
            
		if ($this->request->is('post')) {
			$this->SuppliesOperation->create();
                        $supply = $this->SuppliesOperation->Supply->find('first',array('conditions' => array('Supply.id' => $idSupply)));
                        $operation = $this->request->data['SuppliesOperation']['operation'];
                        $qtaOperation = $this->request->data['SuppliesOperation']['qta'];
                        $infoNavies = $this->InfoNavy->find('first', array('conditions' => array('InfoNavy.proforma' => $this->request->data['SuppliesOperation']['info_navy_id'])));
                        $this->request->data['SuppliesOperation']['users_id']=$this->Session->read('User.id');
                        $this->request->data['SuppliesOperation']['supplies_id']=$idSupply;
                        $this->request->data['SuppliesOperation']['saldo_before']=$supply['Supply']['saldo'];
                        if($operation == "SALIDA" && $supply['Supply']['saldo'] != 0 && $qtaOperation <= $supply['Supply']['saldo']){
                            if ($this->SuppliesOperation->save($this->request->data)) {
                                    $saldoOld = $supply['Supply']['saldo'];
                                    $saldoNew = $saldoOld - $qtaOperation;
                                    $this->SuppliesOperation->Supply->updateAll(array('Supply.saldo'=>$saldoNew),array('Supply.id'=>$idSupply));
                                    //elementos Vdry
                                    $isVdry = (strpos($supply['Supply']['name'],'MANTAS'));
                                    if($infoNavies && $isVdry == 0){
                                        $dataRxSuppliesOperation = $this->SuppliesOperation->find('first',array('conditions' => array('SuppliesOperation.info_navy_id' => $infoNavies['InfoNavy']['id']),'fields' => array('sum(SuppliesOperation.qta) AS ctotal')));
                                        $sumaCtnTotal = $dataRxSuppliesOperation[0]['ctotal'];
                                        $sumaCtnTotal = $sumaCtnTotal + $qtaOperation;
                                        $fieldInfoNavy = 'InfoNavy.'.$supply['Supply']['ref1_field_infonavy'];
                                        $this->InfoNavy->updateAll(['InfoNavy.num_ctn_real'=>$sumaCtnTotal,$fieldInfoNavy=>1],['InfoNavy.id'=>$infoNavies['InfoNavy']['id']]);
                                        $this->SuppliesOperation->updateAll(
                                            ['SuppliesOperation.info_navy_id'=>$infoNavies['InfoNavy']['id'],
                                            'SuppliesOperation.observation'=>'"Uso en proforma de embalaje "'.'"'.$infoNavies['InfoNavy']['proforma'].'"'],
                                            ['SuppliesOperation.id'=>$this->SuppliesOperation->id]);
                                    }

                                    $this->Flash->success(__('Se registro el movimiento correctamente!'));
                                    return $this->redirect(array('action' => 'index',$idSupply));
                            } else {
                                    $this->Flash->error(__('The supplies operation could not be saved. Please, try again.'));
                            }
                        }
                        else if($operation == "INGRESO"){
                            if ($this->SuppliesOperation->save($this->request->data)) {
                                    $saldoOld = $supply['Supply']['saldo'];
                                    $saldoNew = $saldoOld + $qtaOperation;
                                    $this->SuppliesOperation->Supply->updateAll(array('Supply.saldo'=>$saldoNew),array('Supply.id'=>$idSupply));
                                    $this->Flash->success(__('Se registro el movimiento correctamente!'));
                                    return $this->redirect(array('action' => 'index',$idSupply));
                            } else {
                                    $this->Flash->error(__('The supplies operation could not be saved. Please, try again.'));
                            }
                        }
                        else{
                            $this->Flash->error(__('Saldo insuficiente en el inventario!.'));
                        }
        }
        $this->set('infoNavies', $this->InfoNavy->find('list', array('fields' => array('InfoNavy.proforma'))));
		$this->set('supply',$supply);
	}
        
        
/**
 * add method
 *
 * @return void
 */
	public function element($oie_id=null) {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $this->loadModel('ServicesOrder');
            $this->loadModel('ServicePackage');
            $this->loadModel('PackagingCaffee');
            $this->loadModel('DetailsServicesToCaffee');
            $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions' => array('PackagingCaffee.id' => $oie_id)));
            if ($this->request->is('post')) {
                $this->SuppliesOperation->create();
                $idSupply = $this->request->data['SuppliesOperation']['supplies_id'];
                $this->request->data['SuppliesOperation']['operation']="SALIDA";
                $this->request->data['SuppliesOperation']['oie_id']=$oie_id;
                $this->request->data['SuppliesOperation']['observation']='EMBALAJE LOTES: '.$packagingCaffee['PackagingCaffee']['observation'];
                $supply = $this->SuppliesOperation->Supply->find('first',array('conditions' => array('Supply.id' => $idSupply)));
                $qtaOperation = $this->request->data['SuppliesOperation']['qta'];
                $this->request->data['SuppliesOperation']['saldo_before']=$supply['Supply']['saldo'];
                $this->request->data['SuppliesOperation']['created_date']=date('Y-m-d H:i:s');
                $this->request->data['SuppliesOperation']['users_id']=$this->Session->read('User.id');
                //$this->request->data['SuppliesOperation']['supplies_id']=$idSupply;
                if($supply['Supply']['saldo'] != 0 && $qtaOperation <= $supply['Supply']['saldo']){
                    if ($this->SuppliesOperation->save($this->request->data)) {
                        $saldoOld = $supply['Supply']['saldo'];
                        $saldoNew = $saldoOld - $qtaOperation;
                        $this->SuppliesOperation->Supply->updateAll(array('Supply.saldo'=>$saldoNew),array('Supply.id'=>$idSupply));
                        $this->DetailsServicesToCaffee->create();
                        $servicePackage = $this->ServicePackage->find('first',array('conditions'=>array('ServicePackage.id' =>$supply['Supply']['ref_package_service'])));
                        $totalServiceOrder=0;
                        foreach ($servicePackage['ServicePackageHasItemsService'] as $itemServices){
                            $valueItem = $itemServices['valor']*$qtaOperation;
                            $this->DetailsServicesToCaffee->create();
                            $detailsServicesToCaffee = array(
                            'services_orders_id'=> $packagingCaffee['PackagingCaffee']['rem_ref'],
                            'service_package_id'=> $servicePackage['ServicePackage']['id'],
                            'items_services_id' => $itemServices['items_services_id'],
                            'remittances_caffee_id'=>NULL,
                            'qta_bag_to_work'=>$qtaOperation,
                            'observation' => $supply['Supply']['observation'].' CANT: '.$qtaOperation,
                            'value'=> $valueItem,
                            'created_date'=>date('Y-m-d H:i:s'),
                            'completed' => true,
                            'document'=> ''
                            );
                            $totalServiceOrder+=$valueItem;
                        }
                        $this->DetailsServicesToCaffee->save($detailsServicesToCaffee);
                        $this->Flash->success(__('Se registro el movimiento correctamente!'));
                        $serviceOrder = $this->ServicesOrder->find('first',array('conditions'=>array('ServicesOrder.id' =>$packagingCaffee['PackagingCaffee']['rem_ref'])));
                        $totalServiceOrder+=$serviceOrder['ServicesOrder']['total_valor'];
                        $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor'=>$totalServiceOrder),array('ServicesOrder.id' =>$packagingCaffee['PackagingCaffee']['rem_ref']));
                        return $this->redirect(array('action' => 'element',$oie_id));
                    } else {
                            $this->Flash->error(__('The supplies operation could not be saved. Please, try again.'));
                    }
                }
                else{
                    $this->Flash->error(__('Saldo insuficiente en el inventario!.'));
                }
            }
            $supplys = $this->SuppliesOperation->Supply->find('list');
            $supplysOnPackaging = $this->SuppliesOperation->find('all',array('conditions' => 
                array('AND' => 
                    array(
                            'SuppliesOperation.operation LIKE'=> '%SALIDA%',
                            'SuppliesOperation.oie_id' => $oie_id))));
            $this->set('packagingCaffee',$packagingCaffee);
            $this->set('supplys',$supplys);
            $this->set('supplysOnPackaging',$supplysOnPackaging);
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
            if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
		if (!$this->SuppliesOperation->exists($id)) {
			throw new NotFoundException(__('Invalid supplies operation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SuppliesOperation->save($this->request->data)) {
				$this->Flash->success(__('The supplies operation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The supplies operation could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SuppliesOperation.' . $this->SuppliesOperation->primaryKey => $id));
			$this->request->data = $this->SuppliesOperation->find('first', $options);
		}
		$supplies = $this->SuppliesOperation->Supply->find('list');
		$this->set(compact('supplies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
            if (!$this->Session->read('User.id')) {
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->layout = 'colaborador';
            $servicesPackage_id = isset($this->request->query['servicesPackage_id']) ? $this->request->query['servicesPackage_id']: null;
            $oie_id = isset($this->request->query['oie_id']) ? $this->request->query['oie_id']: null;
            $idSuppliesOperation = isset($this->request->query['supplies_operation_id']) ? $this->request->query['supplies_operation_id']: null;
            $supply_id = isset($this->request->query['supply_id']) ? $this->request->query['supply_id']: null;
            //debug($servicesPackage_id);debug($oie_id);debug($idSuppliesOperation);exit;
            $this->loadModel('ServicesOrder');
            $this->loadModel('ServicePackage');
            $this->loadModel('PackagingCaffee');
            $this->loadModel('DetailsServicesToCaffee');
            $this->loadModel('Supply');
            $packagingCaffee = $this->PackagingCaffee->find('first',array('conditions' => array('PackagingCaffee.id'=>$oie_id)));
            if ($this->SuppliesOperation->deleteAll(['SuppliesOperation.id'=>$idSuppliesOperation],false)) {
                $detailsServicesToCaffee = $this->DetailsServicesToCaffee->find('first',
                    array('conditions' => array(
                    'AND'=>array(
                        'DetailsServicesToCaffee.service_package_id'=>$servicesPackage_id,
                        'DetailsServicesToCaffee.services_orders_id'=>$packagingCaffee['PackagingCaffee']['rem_ref'],
                    ))));
                $supply=$this->Supply->find('first',array('conditions' => array('Supply.id'=>$supply_id)));
                $newQtaSupply=$supply['Supply']['saldo']+$detailsServicesToCaffee['DetailsServicesToCaffee']['qta_bag_to_work'];
                $this->Supply->updateAll(array('Supply.saldo'=>$newQtaSupply),array('Supply.id'=>$supply_id));
                $valueServicePackage = $detailsServicesToCaffee['DetailsServicesToCaffee']['value'];
                $servicesOrder = $this->ServicesOrder->find('first',array('conditions' => array('ServicesOrder.id'=>$packagingCaffee['PackagingCaffee']['rem_ref'])));
                $valueTotalServiceOrder = $servicesOrder['ServicesOrder']['total_valor'];
                $newTotalServiceOrder=$valueTotalServiceOrder-$valueServicePackage;
                if($this->DetailsServicesToCaffee->deleteAll(['DetailsServicesToCaffee.id'=>$detailsServicesToCaffee['DetailsServicesToCaffee']['id']],false)){
                    $this->ServicesOrder->updateAll(array('ServicesOrder.total_valor'=>$newTotalServiceOrder),array('ServicesOrder.id'=>$servicesOrder['ServicesOrder']['id']));
                    $this->Flash->success(__('Se elimino el movimiento del suministro al embalaje.'));
                }
            } else {
                    $this->Flash->error(__('The supplies operation could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'element',$oie_id));
    }
    
    public function deleteOperation($id = null){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $this->loadModel('Supply');
        $this->loadModel('InfoNavy');
        $this->autoRender = false;
        
        $dataOldSuppliesOperation = $this->SuppliesOperation->find('first',['conditions'=>['SuppliesOperation.id'=>$id]]);
        $dataSupply = $this->Supply->find('first',['conditions'=>['Supply.id'=>$dataOldSuppliesOperation['SuppliesOperation']['supplies_id']]]);
        $this->SuppliesOperation->deleteAll(['SuppliesOperation.id'=>$id],false);
        //elementos Vdry
        $isVdry = (strpos($dataSupply['Supply']['name'],'MANTAS'));
        if($isVdry == 0){
            $dataRxSuppliesOperation = $this->SuppliesOperation->find('first',array('conditions' => array('SuppliesOperation.info_navy_id' => $dataOldSuppliesOperation['SuppliesOperation']['info_navy_id']),'fields' => array('sum(SuppliesOperation.qta) AS ctotal')));
            $fieldInfoNavy = 'InfoNavy.'.$dataSupply['Supply']['ref1_field_infonavy'];
            $llevaManta=0;
            if($dataRxSuppliesOperation[0]['ctotal'] != 0){$llevaManta = 1;}
            $this->InfoNavy->updateAll(['InfoNavy.num_ctn_real'=>$dataRxSuppliesOperation[0]['ctotal'],$fieldInfoNavy=>$llevaManta],['InfoNavy.id'=>$dataOldSuppliesOperation['SuppliesOperation']['info_navy_id']]);
        }
                                        
        $saldoNew = $dataSupply['Supply']['saldo'] + $dataOldSuppliesOperation['SuppliesOperation']['qta'];
        $this->SuppliesOperation->Supply->updateAll(array('Supply.saldo'=>$saldoNew),array('Supply.id'=>$dataSupply['Supply']['id']));
        $this->Flash->success(__('Se elimino el movimiento del suministro correctamente'));
        return $this->redirect(array('action' => 'index',$dataSupply['Supply']['id']));
    }
}
