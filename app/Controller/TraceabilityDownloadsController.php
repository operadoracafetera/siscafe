<?php
App::uses('AppController', 'Controller');
/**
 * TraceabilityDownloads Controller
 *
 * @property TraceabilityDownload $TraceabilityDownload
 * @property PaginatorComponent $Paginator
 */
class TraceabilityDownloadsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            if(!$this->Session->read('User.id')){
            return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
		$this->TraceabilityDownload->recursive = 0;
		$this->set('traceabilityDownloads', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TraceabilityDownload->exists($id)) {
			throw new NotFoundException(__('Invalid traceability download'));
		}
		$options = array('conditions' => array('TraceabilityDownload.' . $this->TraceabilityDownload->primaryKey => $id));
		$this->set('traceabilityDownload', $this->TraceabilityDownload->find('first', $options));
	}
        
                /**
 * zip method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function zip($remittancesCaffeeId = null) {
		$this->autoRender = false;
                $this->loadModel('RemittancesCaffee');
                $targzname = "fotos-trazabilidad-descargue-".$remittancesCaffeeId.".tar.gz";
                shell_exec("cd ".WWW_ROOT.DS.'img'.DS.'data/trazabilidad/in'.";tar -cvf ./targz/".$targzname." ".$remittancesCaffeeId);
                $this->response->file(
                    WWW_ROOT.DS.'img'.DS.'data/trazabilidad/in/targz/'.$targzname,
                    array('download' => true, 'name' => $targzname)
                );       
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idRemittancesCaffee = null) {
                $this->loadModel('RemittancesCaffee');
                $options = array('conditions' => array('RemittancesCaffee.' . $this->RemittancesCaffee->primaryKey => $idRemittancesCaffee));
                $remittancesCaffee = $this->RemittancesCaffee->find('first',$options);
                $this->set('remittancesCaffee', $remittancesCaffee);
                $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->TraceabilityDownload->create();
                        $db = $this->TraceabilityDownload->getDataSource();
                        $this->request->data['TraceabilityDownload']['remittances_caffee_id']=$remittancesCaffee['RemittancesCaffee']['id'];
                        $file_img1_path = $this->request->data['TraceabilityDownload']['img1'];
                        $file_img2_path = $this->request->data['TraceabilityDownload']['img2'];
                        $file_img3_path = $this->request->data['TraceabilityDownload']['img3'];
                        $file_img4_path = $this->request->data['TraceabilityDownload']['img4'];
                        $file_img5_path = $this->request->data['TraceabilityDownload']['img5'];
                        $pathDirectory='data/trazabilidad/in'."/".$remittancesCaffee['RemittancesCaffee']['id'];
                        if (!is_dir(WWW_ROOT.'img/'.$pathDirectory)) {
                            mkdir(WWW_ROOT.'img/'.$pathDirectory, 0777, true);
                        }
                        $pathImg1=$pathDirectory."/".$file_img1_path['name'];
                        $resultImg1 = move_uploaded_file($file_img1_path['tmp_name'], WWW_ROOT.'img/'.$pathImg1);
                        $this->request->data['TraceabilityDownload']['img1']=$pathImg1;
                        $pathImg2=$pathDirectory."/".$file_img2_path['name'];
                        $resultImg2 = move_uploaded_file($file_img2_path['tmp_name'], WWW_ROOT.'img/'.$pathImg2);
                        $this->request->data['TraceabilityDownload']['img2']=$pathImg2;
                        $pathImg3=$pathDirectory."/".$file_img3_path['name'];
                        $resultImg3 = move_uploaded_file($file_img3_path['tmp_name'], WWW_ROOT.'img/'.$pathImg3);
                        $this->request->data['TraceabilityDownload']['img3']=$pathImg3;
                        $pathImg4=$pathDirectory."/".$file_img4_path['name'];
                        $resultImg4 = move_uploaded_file($file_img4_path['tmp_name'], WWW_ROOT.'img/'.$pathImg4);
                        $this->request->data['TraceabilityDownload']['img4']=$pathImg4;
                        $pathImg5=$pathDirectory."/".$file_img5_path['name'];
                        $resultImg5 = move_uploaded_file($file_img5_path['tmp_name'], WWW_ROOT.'img/'.$pathImg5);
                        $this->request->data['TraceabilityDownload']['img5']=$pathImg5;
                        //$sealsVehicule = $this->request->data['TraceabilityDownload']['seals'];
			if ($this->TraceabilityDownload->save($this->request->data)) {
                                    $this->RemittancesCaffee->updateAll(
                                        array(
                                            //'RemittancesCaffee.seals' => $db($sealsVehicule,'string'),
                                            'RemittancesCaffee.traceability_download_id' => $this->TraceabilityDownload->id),
                                        array('RemittancesCaffee.id' => $idRemittancesCaffee)
                                    );
				$this->Flash->success(__('Se agrego la trazabilidad de descargue exitosamente.'));
				return $this->redirect(array('controller'=>'RemittancesCaffees','action' => 'sampler_index'));
			} else {
				$this->Flash->error(__('No se registro los datos. Por favor intentolo nuevamente.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TraceabilityDownload->exists($id)) {
			throw new NotFoundException(__('Invalid traceability download'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TraceabilityDownload->save($this->request->data)) {
				$this->Flash->success(__('The traceability download has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The traceability download could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TraceabilityDownload.' . $this->TraceabilityDownload->primaryKey => $id));
			$this->request->data = $this->TraceabilityDownload->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TraceabilityDownload->id = $id;
		if (!$this->TraceabilityDownload->exists()) {
			throw new NotFoundException(__('Invalid traceability download'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TraceabilityDownload->delete()) {
			$this->Flash->success(__('The traceability download has been deleted.'));
		} else {
			$this->Flash->error(__('The traceability download could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
