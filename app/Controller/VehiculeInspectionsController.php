<?php
App::uses('AppController', 'Controller');
/**
 * VehiculeInspections Controller
 *
 * @property VehiculeInspection $VehiculeInspection
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class VehiculeInspectionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
		$this->VehiculeInspection->recursive = 0;
		$this->set('vehiculeInspections', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                $this->layout = 'colaborador';
		if (!$this->VehiculeInspection->exists($id)) {
			throw new NotFoundException(__('Invalid vehicule inspection'));
		}
		$options = array('conditions' => array('VehiculeInspection.' . $this->VehiculeInspection->primaryKey => $id));
		$this->set('vehiculeInspection', $this->VehiculeInspection->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->VehiculeInspection->create();
                        $this->request->data['VehiculeInspection']['user_id']=$this->Session->read('User.id');
                        $this->request->data['VehiculeInspection']['date_inspection']=date('Y-m-d h:i:s');
                        //debug($this->request->data);exit;
			if ($this->VehiculeInspection->save($this->request->data)) {
				$this->Flash->success(__('Inspección registrada correctamente'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Se genero un error. Por favor intentelo nuevamente'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->VehiculeInspection->exists($id)) {
			throw new NotFoundException(__('Invalid vehicule inspection'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->VehiculeInspection->save($this->request->data)) {
				$this->Flash->success(__('The vehicule inspection has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The vehicule inspection could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('VehiculeInspection.' . $this->VehiculeInspection->primaryKey => $id));
			$this->request->data = $this->VehiculeInspection->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->VehiculeInspection->id = $id;
		if (!$this->VehiculeInspection->exists()) {
			throw new NotFoundException(__('Invalid vehicule inspection'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->VehiculeInspection->delete()) {
			$this->Flash->success(__('The vehicule inspection has been deleted.'));
		} else {
			$this->Flash->error(__('The vehicule inspection could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
