<?php
App::uses('AppController', 'Controller');
App::uses('AuditeventusersController', 'Controller');
App::uses('SessionusersController', 'Controller');
App::uses('TypeCtnsController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
            
            $this->layout = 'administrador';
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}
        
        function redirectByGroup($group) {
            if(strcmp($group, "administrador") == 0) {
                            return $this->redirect(
                                array('controller' => 'pages', 'action' => 'administrador')
                            );
                        }
                        else if(strcmp($group, "cliente") == 0) {
                            return $this->redirect(
                                array('controller' => 'pages', 'action' => 'cliente')
                            );
                        }
                        else {
                            return $this->redirect(
                                array('controller' => 'pages', 'action' => 'colaborador')
                            );
                        }
                        
        }
        
        public function login() {
            $this->layout = 'login';
            if($this->Session->read('User.id')){
                $user = $this->User->find('first',array('conditions' => array('User.id' => $this->Session->read('User.id'))));
                            if($user ["Profiles"]["name"] == 'Colaborador'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'colaborador')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'terminal'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'terminal')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Basculero'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'basculero')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Invitado'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'invitado')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Funcionario FNC'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'fnc')
			      );
                            }
            }
            if ($this->request->is('post')) {
                        $passwdIn = md5($this->request->data['User']['password']);
                        $configution = array(
                            'conditions' => array(
                                'username' => $this->request->data['User']['username'],
                                'password' => $passwdIn,
                                'active' => true
                            )
                            );
                        $this->loadModel('Departament');
                        $user = $this->User->find('first',$configution);
                        
                        if($user != null) {
			    $departament = $this->Departament->find('first',array('conditions' => array('Departament.id' => $user['User']['departaments_id'])));
                            $user['User']['updated_date'] = date("Y/m/d H:i:s");
                            $this->Session->write('User.first_name', $user['User']['first_name']);
                            $this->Session->write('User.last_name', $user['User']['last_name']);
                            $this->Session->write('User.profiles_id', $user["User"]["profiles_id"]);
                            $this->Session->write('User.id', $user['User']['id']);
                            $this->Session->write('User.store', $user['User']['ref_store']);
		            $this->Session->write('User.cod_ext2', $user['User']['cod_ext2']);
                            $this->Session->write('User.departaments_id', $user['User']['departaments_id']);
                            $this->Session->write('User.cod_ext1', $user['User']['cod_ext1']);
			    $this->Session->write('User.city_alt1', $user['User']['city_alt1']);
                            $this->Session->write('User.perfil_name', $user ["Profiles"]["name"]);
                            $this->Session->write('User.perfil_name2', $user ["Profiles"]["description"]);
                            $this->Session->write('User.center', $departament ["Departament"]["description"]);
                            $this->Session->write('User.center_city', $departament ["Departament"]["city_operation"]);
                            $this->Session->write('User.center_departament', $departament ["Departament"]["departament_operation"]);
                            $this->Session->write('User.centerId', $departament ["Departament"]["id"]);
                            $this->Session->write('User.jetty', $user['User']['cod_city_operation']);
			    $this->Session->write('User.opera', $departament ["Departament"]["all_operations"]);
                            $this->User->create();
                            $this->User->save($user);
                            
                            if($user ["Profiles"]["name"] == 'Colaborador'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'colaborador')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'terminal'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'terminal')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Basculero'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'basculero')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Invitado'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'invitado')
			      );
                            }
                            else if($user ["Profiles"]["name"] == 'Funcionario FNC'){
			      return $this->redirect(
                                array('controller' => 'pages', 'action' => 'fnc')
			      );
                            }
                        }
                        else {
                            $this->Flash->error(__('Datos de acceso invalidos, intentelo nuevamente!'));
                        }
                    }
        }
        
        public function logout($idUser) {
            $this->Session->destroy();
            //$this->Flash->success(__('Hasta luego!'));
            return $this->redirect(array('controller'=>'users', 'action' => 'login'));
        }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            $this->layout = 'administrador';
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
        
        public function viewperuser($id = null) {
                $this->layout = 'cliente';
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
                
                $AuditeventusersController = new AuditeventusersController;
                $AuditeventusersController->register(7, $id);
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
        
        public function getTime(){
            date_default_timezone_set('America/Bogota');
            $this->autoRender = false;
            return date('Y-m-d H:i:m:s');
        }

/**
 * add method
 *
 * @return void
 */
	public function add() {
            $this->layout = 'administrador';
		if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
		if ($this->request->is('post')) {
			$this->User->create();
			$this->request->data['User']['password']=sha1($this->request->data['User']['password']);
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('Se registro correctamente el nuevo usuario.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
                $this->layout = 'basculero';
                if (!$this->Session->read('User.id')) {
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                $this->loadModel('Bascule');
		$this->loadModel('Departaments');
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->updateAll(
                                array(
                                    'User.first_name'=>"'".$this->request->data['User']['first_name']."'",
                                    'User.last_name'=>"'".$this->request->data['User']['last_name']."'",
                                    'User.bascule_id'=>"'".$this->request->data['User']['bascule_id']."'",
				    'User.departaments_id'=>"'".$this->request->data['User']['departaments_id']."'",
                                    'User.ref_store'=>"".$this->request->data['User']['ref_store']),
                                array('User.id'=>$id))) {
                                $this->Session->write('User.centerId', $this->request->data['User']['departaments_id']);
				$this->Flash->success(__('Se modifico correctamente los datos'));
				return $this->redirect(array('controller'=>'Users','action' =>'edit', $this->Session->read('User.id')));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
			$this->set('departaments',$this->Departaments->find('list',array('conditions'=>['Departaments.cod_city LIKE'=>$this->Session->read('User.jetty')])));
                        $this->set('bascules',$this->Bascule->find('list',array('fields'=>array('Bascule.id','Bascule.name'))));
		}
	}
        
        public function editperuser($id = null) {
                $this->layout = 'cliente';
                $AuditeventusersController = new AuditeventusersController;
                $AuditeventusersController->register(8, $id);
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
                    
                    $CurrentUser = $this->User->findById($id);
                    $CurrentUser['User']['password'] = sha1($this->request->data['User']['password']);
                    $CurrentUser['User']['name'] = $this->request->data['User']['name'];
                    $CurrentUser['User']['email'] = $this->request->data['User']['email'];
			if ($this->User->save($CurrentUser)) {
				$this->Flash->success(__('Usuario Actualizado'));
				return $this->redirect(array('controller' => 'users','action' => 'viewperuser/'.$id));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
                $user = $this->User->findById($id);
                $this->set('user', $user);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
                $this->layout = 'administrador';
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('Ha sido exitosamente el usuario.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
