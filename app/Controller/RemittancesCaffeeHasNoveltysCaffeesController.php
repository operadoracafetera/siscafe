<?php

App::uses('AppController', 'Controller');

/**
 * RemittancesCaffeeHasNoveltysCaffees Controller
 *
 * @property RemittancesCaffeeHasNoveltysCaffee $RemittancesCaffeeHasNoveltysCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class RemittancesCaffeeHasNoveltysCaffeesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->RemittancesCaffeeHasNoveltysCaffee->recursive = 0;
        $this->set('remittancesCaffeeHasNoveltysCaffees', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->RemittancesCaffeeHasNoveltysCaffee->exists($id)) {
            throw new NotFoundException(__('Invalid remittances caffee has noveltys caffee'));
        }
        $options = array('conditions' => array('RemittancesCaffeeHasNoveltysCaffee.' . $this->RemittancesCaffeeHasNoveltysCaffee->primaryKey => $id));
        $this->set('remittancesCaffeeHasNoveltysCaffee', $this->RemittancesCaffeeHasNoveltysCaffee->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($idRemittances = null) {
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('NoveltysCoffee');
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->RemittancesCaffeeHasNoveltysCaffee->create();
            $this->request->data['RemittancesCaffeeHasNoveltysCaffee']['remittances_caffee_id']=$idRemittances;
            $this->request->data['RemittancesCaffeeHasNoveltysCaffee']['active']=true;
            $this->request->data['RemittancesCaffeeHasNoveltysCaffee']['created_date']=date('Y-m-d h:i:s');
            //debug($this->request->data);exit;
            if ($this->RemittancesCaffeeHasNoveltysCaffee->save($this->request->data)) {
                $this->Flash->success(__('Se agrego la novedad correctamente!'));
                return $this->redirect(array('action' => 'add',$idRemittances));
            } else {
                $this->Flash->error(__('No se pudo registrar la novedad. Por favor validar.'));
            }
        }
        $options1=array('conditions' => array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id' => $idRemittances));
        $this->RemittancesCaffeeHasNoveltysCaffee->recursive=0;
	$remittancesCaffeeHasNoveltysCaffee = $this->RemittancesCaffeeHasNoveltysCaffee->find('all',$options1);
        $options2 = array('conditions' => array('RemittancesCaffee.' . $this->RemittancesCaffee->primaryKey => $idRemittances));
        $this->set('remittancesCaffee',$this->RemittancesCaffee->find('first', $options2));
        $this->set('noveltysCoffee',$this->NoveltysCoffee->find('list'));
        $flagBlock=false;
        foreach ($remittancesCaffeeHasNoveltysCaffee as $item) {
            if($item['RemittancesCaffeeHasNoveltysCaffee']['active'] == 1){
                $flagBlock = true;
                break;
            }
        }
        $this->set('flagBlock',$flagBlock);
        $this->set('remittancesCaffeeHasNoveltysCaffee',$remittancesCaffeeHasNoveltysCaffee);
        //debug($remittancesCaffeeHasNoveltysCaffee);exit;
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete() {
        if(!$this->Session->read('User.id')){
                return $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        
        $this->layout = 'colaborador';
        $remittances_caffee_id = isset($this->request->query['remittances_caffee_id']) ? $this->request->query['remittances_caffee_id']: null;
        $noveltys_caffee_id = isset($this->request->query['noveltys_caffee_id']) ? $this->request->query['noveltys_caffee_id']: null;
        
        if ($this->RemittancesCaffeeHasNoveltysCaffee->updateAll(
                array('RemittancesCaffeeHasNoveltysCaffee.active'=>false),
                array('RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id' => $remittances_caffee_id,
                      'RemittancesCaffeeHasNoveltysCaffee.noveltys_caffee_id' => $noveltys_caffee_id)
                )) {
            $this->Flash->success(__('Se desactivo el bloqueo a la remesa correctamente!'));
            return $this->redirect(array('action' => 'add',$remittances_caffee_id));
        } else {
            $this->Flash->error(__('No se pudo desbloquear la remesa. Por favor validar.'));
            return $this->redirect(array('action' => 'add',$remittances_caffee_id));
        }
        
    }

    public function getCoffeeNoveltysByOIE($oie = null) {
        if ($oie) {
            $this->autoRender = false;
            $this->loadModel('PackagingCaffee');
            $this->loadModel('DetailPackagingCaffee');
            $this->paginate = array(
                'conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $oie),
                'joins' => array(
                    array(
                        'table' => 'detail_packaging_caffee',
                        'alias' => 'DetailPackagingCaffee',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'DetailPackagingCaffee.remittances_caffee_id = RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id'
                        ),
                    ),
                    array(
                        'table' => 'noveltys_caffee',
                        'alias' => 'NoveltysCaffee',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'NoveltysCaffee.id = RemittancesCaffeeHasNoveltysCaffee.noveltys_caffee_id',
                        ),
                    ),
                ),
                'fields' => array(
                    'RemittancesCaffeeHasNoveltysCaffee.remittances_caffee_id',
                    'NoveltysCaffee.name',
                    'RemittancesCaffeeHasNoveltysCaffee.active'
                )
            );
            return $this->paginate($this->RemittancesCaffeeHasNoveltysCaffee);
        }
        return "";
    }
    
    public function ajaxCoffeeNoveltysByOIE($oie = null) {
        if ($oie) {
            $this->autoRender = false;
            return json_encode(RemittancesCaffeeHasNoveltysCaffeesController::getCoffeeNoveltysByOIE($oie));
        }
    }
    

}
