<?php
App::uses('AppController', 'Controller');
/**
 * BasculeVerifications Controller
 *
 * @property BasculeVerification $BasculeVerification
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BasculeVerificationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
                $this->layout = 'basculero';
		$this->BasculeVerification->recursive = 0;
		$this->set('basculeVerifications', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
                $this->layout = 'basculero';
		if (!$this->BasculeVerification->exists($id)) {
			throw new NotFoundException(__('Invalid bascule verification'));
		}
		$options = array('conditions' => array('BasculeVerification.' . $this->BasculeVerification->primaryKey => $id));
		$this->set('basculeVerification', $this->BasculeVerification->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
                $this->layout = 'basculero';
                $this->loadModel('User');
		if ($this->request->is('post')) {
			$this->BasculeVerification->create();
                        $this->request->data['BasculeVerification']['tolerancy']=$this->request->data['BasculeVerification']['tolerancyHidden'];
                        $this->request->data['BasculeVerification']['basc_id']=$this->request->data['BasculeVerification']['basc'];
                        $this->request->data['BasculeVerification']['datetime_verification']=date('Y-m-d h:i:s');
                        unset($this->request->data['BasculeVerification']['basc'],$this->request->data['BasculeVerification']['tolerancyHidden']);
                        //debug($this->request->data);exit;
			if ($this->BasculeVerification->save($this->request->data)) {
				$this->Flash->success(__('Registro Padron guardado exitosamente!'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('No se pudo guardar el registro. Por favor intentelo nuevamente.'));
			}
		}
                $this->set('user', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
	}

}
