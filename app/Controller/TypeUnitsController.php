<?php
App::uses('AppController', 'Controller');
/**
 * TypeUnits Controller
 *
 * @property TypeUnit $TypeUnit
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TypeUnitsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TypeUnit->recursive = 0;
		$this->set('typeUnits', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TypeUnit->exists($id)) {
			throw new NotFoundException(__('Invalid type unit'));
		}
		$options = array('conditions' => array('TypeUnit.' . $this->TypeUnit->primaryKey => $id));
		$this->set('typeUnit', $this->TypeUnit->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TypeUnit->create();
			if ($this->TypeUnit->save($this->request->data)) {
				$this->Flash->success(__('The type unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The type unit could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TypeUnit->exists($id)) {
			throw new NotFoundException(__('Invalid type unit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TypeUnit->save($this->request->data)) {
				$this->Flash->success(__('The type unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The type unit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TypeUnit.' . $this->TypeUnit->primaryKey => $id));
			$this->request->data = $this->TypeUnit->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TypeUnit->id = $id;
		if (!$this->TypeUnit->exists()) {
			throw new NotFoundException(__('Invalid type unit'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TypeUnit->delete()) {
			$this->Flash->success(__('The type unit has been deleted.'));
		} else {
			$this->Flash->error(__('The type unit could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
