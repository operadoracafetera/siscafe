<?php

App::uses('AppController', 'Controller');

/**
 * DetailPackagingCaffees Controller
 *
 * @property DetailPackagingCaffee $DetailPackagingCaffee
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DetailPackagingCaffeesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function packaging_processing() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->DetailPackagingCaffee->recursive = 2;
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        if ($idOIE && $remesa) {
            $this->loadModel('WeighingPackagingCaffee');
            $this->loadModel('RemittancesCaffee');
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
            $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('and' => array(
                        array('WeighingPackagingCaffee.remittances_cafee_id' => $remesa),
                        array('WeighingPackagingCaffee.id_oie' => $idOIE)))));
            $quantityBags = 0;
            foreach ($dataWeighingPackagingCaffee as $weighingPackagingCaffeee) {
                $quantityBags = $quantityBags + $weighingPackagingCaffeee['WeighingPackagingCaffee']['quantity_bag_pallet'];
            }
            $isComplete = $quantityBags == $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'];
            if ($isComplete) {
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 2, 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE,
                    'DetailPackagingCaffee.remittances_caffee_id' => $remesa));
                $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('and' => array(
                            array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)), 'NOT' => array('DetailPackagingCaffee.state' => 2))));
                $changeData;
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] == 0) {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 4, 'RemittancesCaffee.is_active' => '0');
                } else {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 7);
                }
                $this->RemittancesCaffee->updateAll($changeData, array('RemittancesCaffee.id' => $remesa));
                if ($dataDetailPackagingCaffee) {
                    $this->Flash->success(__('Se finalizó el proceso de emebalaje de la remesa ' . $remesa . ' con OIE ' . $idOIE));
                } else {
                    $this->loadModel('PackagingCaffee');
                    date_default_timezone_set('America/Bogota');
                    $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 3, 'PackagingCaffee.packaging_date' => "'" . date('Y-m-d h:i:s') . "'"), array('PackagingCaffee.id' => $idOIE));
                    $this->Flash->success(__('Se finalizó el proceso de embalaje de la OIE ' . $idOIE));
                }
            } else {
                $this->Flash->error(__('No se puede finalizar porque hay sacos por embalar de la remesa ' . $remesa));
            }
            return $this->redirect(array('action' => 'packaging_processing'));
        } else {
            $this->DetailPackagingCaffee->recursive = 3;
            $this->loadModel('SlotStore');
            $slotStores = $this->SlotStore->find('list',array('conditions'=> array('SlotStore.stores_caffee_id'=>$this->Session->read('User.store'))));
            $this->Paginator->settings = array(
                'conditions' => array(
                    'and' =>
                    array(
                        array('DetailPackagingCaffee.state' => 7),
                        array('RemittancesCaffee.slot_store_id' => $slotStores),
                        //array('SlotStore.staff_driver_id' => $this->Session->read('User.id')))
                        array('DetailPackagingCaffee.users_id' => $this->Session->read('User.id')))
                ),
                'limit' => 50);
            $this->set('detailPackagingCaffees', $this->Paginator->paginate());
        }
    }

    
    public function findContainerByOie(){
        $this->loadModel('ViewPackaging');
        $this->layout = 'basculero';
        $this->autoRender = false;
        $dataIdOie = $this->request['data']["OIE"];
        $viewPackaging = $this->ViewPackaging->find('first', array('conditions' => array('ViewPackaging.oie' => $dataIdOie)));
        if(!empty($viewPackaging)){
            return json_encode($viewPackaging);
        }
        else{
            return json_encode("");
        }
    }
    
    public function completeRemmitancesCoffee(){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
	$this->loadModel('PackagingCaffee');
        $this->layout = 'basculero';
        $this->autoRender = false;
        $this->DetailPackagingCaffee->recursive = 2;
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
	$packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
        $containerRegistered = $packagingCaffee['PackagingCaffee']['ready_container_id'];
        if ($idOIE && $remesa && $containerRegistered) {
            $this->loadModel('WeighingPackagingCaffee');
            $this->loadModel('RemittancesCaffee');
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
            $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('and' => array(
                        array('WeighingPackagingCaffee.remittances_cafee_id' => $remesa),
                        array('WeighingPackagingCaffee.id_oie' => $idOIE)))));
            $quantityBags = 0;
            foreach ($dataWeighingPackagingCaffee as $weighingPackagingCaffeee) {
                $quantityBags = $quantityBags + $weighingPackagingCaffeee['WeighingPackagingCaffee']['quantity_bag_pallet'];
            }
            $isComplete = $quantityBags == $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'];
            if ($isComplete) {
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 2, 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE,
                    'DetailPackagingCaffee.remittances_caffee_id' => $remesa));
                $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('and' => array(
                            array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)), 'NOT' => array('DetailPackagingCaffee.state' => 2))));
                $changeData;
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] == 0) {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 4, 'RemittancesCaffee.is_active' => '0');
                } else {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 7);
                }
                $this->RemittancesCaffee->updateAll($changeData, array('RemittancesCaffee.id' => $remesa));
                if ($dataDetailPackagingCaffee) {
                    return "";
                } else {
                    $this->loadModel('PackagingCaffee');
                    $this->loadModel('ViewPackaging');
                    $viewPackaging = $this->ViewPackaging->find('first',array('conditions'=>array('ViewPackaging.oie' => $idOIE)));
                    $this->loadModel('PackagingCaffee');
                    date_default_timezone_set('America/Bogota');
                    $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 3, 'PackagingCaffee.packaging_date' => "'" . date('Y-m-d H:i:s') . "'"), array('PackagingCaffee.id' => $idOIE));
                    return json_encode($viewPackaging);
                }
            } else {
                return "";
            }
        }
        return json_encode($idOIE." ".$remesa);
    }

    public function completeRemmitancesCoffeeToOut(){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->loadModel('WeighingPackagingCaffee');
        $this->loadModel('WeighingDownloadCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('PackagingCaffee');
        $this->loadModel('ViewPackaging');
        $this->autoRender = false;
        $this->DetailPackagingCaffee->recursive = 2;
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        date_default_timezone_set('America/Bogota');
	$packagingCaffee = $this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE)));
        $containerRegistered = $packagingCaffee['PackagingCaffee']['ready_container_id'];
        if ($idOIE && $remesa && $containerRegistered) {            
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
            $dataWeighingDownloadCaffee = $this->WeighingDownloadCaffee->find('all', array('conditions' => array('and' => array(
                        array('WeighingDownloadCaffee.remittances_caffee_id' => $remesa)))));
            
            $quantityBags = 0;
            $quantityPallet = 0;
            foreach ($dataWeighingDownloadCaffee as $weighingDownloadCaffee) {
                $quantityBags = $quantityBags + $weighingDownloadCaffee['WeighingDownloadCaffee']['quantity_bag_pallet'];
                $quantityPallet = $quantityPallet + 1;
            }
            $isComplete = $quantityBags == $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'];
            
            if ($isComplete) {
                
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 2, 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE,'DetailPackagingCaffee.remittances_caffee_id' => $remesa)); 

                foreach ($dataWeighingDownloadCaffee as $infoWeighingPack) {
                        $registerWeighingPack = array(
                                    'weight_pallet' => $infoWeighingPack['WeighingDownloadCaffee']['weight_pallet'],
                                    'quantity_bag_pallet' => $infoWeighingPack['WeighingDownloadCaffee']['quantity_bag_pallet'],
                                    'weighing_date' => date('Y-m-d H:i:s'),
                                    'remittances_cafee_id' => $remesa,
                                    'is_fraction' => 0,
                                    'seq_weight_pallet' => $infoWeighingPack['WeighingDownloadCaffee']['seq_weight_pallet'],
                                    'id_oie' => $idOIE,
                            );
                    $this->WeighingPackagingCaffee->create();
                    $this->WeighingPackagingCaffee->save($registerWeighingPack);
                }    
                
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.quantity_bag_in_store' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']
                                                      ,'RemittancesCaffee.quantity_bag_out_store' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] + $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out']  
                                                      ,'RemittancesCaffee.quantity_in_pallet_caffee' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] - $quantityPallet
                                                      ,'RemittancesCaffee.quantity_out_pallet_caffee' => $dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] + $quantityPallet ), array('RemittancesCaffee.id' => $remesa));
                
                $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('and' => array(
                            array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)), 'NOT' => array('DetailPackagingCaffee.state' => 2))));
                
                $changeData;                                
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
                if ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] == 0) {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 4, 'RemittancesCaffee.is_active' => '0');
                } else {
                    $changeData = array('RemittancesCaffee.state_operation_id' => 7);
                }
                $this->RemittancesCaffee->updateAll($changeData, array('RemittancesCaffee.id' => $remesa));
                if ($dataDetailPackagingCaffee) {
                    return "";
                } else {
                    $viewPackaging = $this->ViewPackaging->find('first',array('conditions'=>array('ViewPackaging.oie' => $idOIE)));
                    $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 3, 'PackagingCaffee.packaging_date' => "'" . date('Y-m-d H:i:s') . "'"), array('PackagingCaffee.id' => $idOIE));
                    return json_encode($viewPackaging);
                }
            } else {
                return "";
            }
      }
        return json_encode($idOIE." ".$remesa);
    }
    
    public function packaging_empty() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->DetailPackagingCaffee->recursive = 2;
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        if ($idOIE && $remesa) {
            $this->loadModel('WeighingEmptyCoffee');
            $this->loadModel('RemittancesCaffee');
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
            $dataWeighingEmptyCoffee = $this->WeighingEmptyCoffee->find('all', array('conditions' => array('and' => array(
                        array('WeighingEmptyCoffee.remittances_caffee_id' => $remesa),
                        array('WeighingEmptyCoffee.oie_id' => $idOIE)
                ))));
            $quantityBags = 0;
            foreach ($dataWeighingEmptyCoffee as $weighingEmptyCoffee) {
                $quantityBags = $quantityBags + $weighingEmptyCoffee['WeighingEmptyCoffee']['quantity_bag_pallet'];
            }
	    
            $isComplete = $quantityBags == $dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'];
            if ($isComplete) {
                $this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.state' => 9, 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE,
                    'DetailPackagingCaffee.remittances_caffee_id' => $remesa));
                $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('all', array('conditions' => array('and' => array(
                            array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)), 'NOT' => array('DetailPackagingCaffee.state' => 8))));
                $changeData = array(
                    'RemittancesCaffee.state_operation_id'=>2,
                    'RemittancesCaffee.is_active'=>1
                );
                $this->RemittancesCaffee->updateAll($changeData, array('RemittancesCaffee.id' => $remesa));
                if($dataDetailPackagingCaffee) {
                    $this->Flash->success(__('Se finaliz� el proceso de vaciado de la remesa ' . $remesa . ' con OIE ' . $idOIE));
		    return $this->redirect(array('action' => 'packaging_empty'));
                } else {
                    $this->Flash->success(__('Se finaliz� el proceso de vaciado de la OIE ' . $idOIE));
		    return $this->redirect(array('action' => 'packaging_empty'));
                }
            }
            else{
                $this->Flash->error(__('No se puede finalizar porque hay sacos por vaciar de la remesa ' . $remesa));
		return $this->redirect(array('action' => 'packaging_empty'));
            }
            
        }
        else{
            $this->Paginator->settings = array(
                'conditions' => array(
                    'and' =>
                    array(
                        array('DetailPackagingCaffee.state' => 3)
                        //array('RemittancesCaffee.staff_driver_id' => $this->Session->read('User.id')))
                        //array('DetailPackagingCaffee.users_id' => $this->Session->read('User.id')))
                    )),
                'limit' => 15);
            $this->set('detailPackagingCaffees', $this->Paginator->paginate());
        }
    }


    public function process() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        //debug($detailPackagingCaffee);exit;
        if ($this->DetailPackagingCaffee->updateAll(
                        array('DetailPackagingCaffee.state' => 7, 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE, 'DetailPackagingCaffee.remittances_caffee_id' => $remesa))) {
            $this->loadModel('PackagingCaffee');
            $this->PackagingCaffee->updateAll(array('PackagingCaffee.state_packaging_id' => 5,'PackagingCaffee.packaging_date' => "'" . date('Y-m-d H:i:s') . "'"), array('PackagingCaffee.id' => $idOIE));
            $this->Flash->success(__('Se paso el detalle embalaje a en proceso. '));
            return $this->redirect(array('action' => 'packaging_pendient'));
        } else {
            $this->Flash->error(__('No se pudo procesar el registro de embalaje. Por favor intentelo nuevamente.'));
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function packaging_pendient() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->DetailPackagingCaffee->recursive = 2;
        $this->loadModel('SlotStore');
        $slotStores = $this->SlotStore->find('list',array('conditions'=> array('SlotStore.stores_caffee_id'=>$this->Session->read('User.store'))));
        $this->Paginator->settings = array(
            'conditions' => array(
                'and' => array(
                        array('DetailPackagingCaffee.state' => 1)),
                        array('RemittancesCaffee.slot_store_id' => $slotStores),
                ),
            'limit' => 50);
        $this->set('detailPackagingCaffees', $this->Paginator->paginate());
    }

    public function view() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $this->loadModel('WeighingPackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('Client');
        $this->loadModel('UnitsCaffee');
        $this->loadModel('User');
	$this->loadModel('Bascule');
        $this->loadModel('PackagingCaffee');
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
        $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                    array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                    array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
        $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('and' => array(
                    array('WeighingPackagingCaffee.remittances_cafee_id' => $remesa),
                    array('WeighingPackagingCaffee.id_oie' => $idOIE)))));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $userCurrent = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id'))));
	$bascule = $this->Bascule->find('list',array('conditions'=>array('and'=> array('Bascule.in_operation'=>0,'Bascule.packging_anormal'=>$dataRemittancesCaffee['RemittancesCaffee']['coffee_with_packging_anormal'],'Bascule.store_id'=>$userCurrent['User']['ref_store'])),'fields' => array('Bascule.bascule','Bascule.name')));
	$this->set('bagWeigth', $bagWeigth);
	$this->set('bascules', $bascule);
        $this->set('weighingPackagingCaffee', $dataWeighingPackagingCaffee);
        $this->set('detailPackagingCaffee', $dataDetailPackagingCaffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set('user', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
        $this->set('packagingCaffee',$this->PackagingCaffee->find('first',array('conditions'=>array('PackagingCaffee.id'=>$idOIE))));
    }

    public function view2() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $this->loadModel('WeighingPackagingCaffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('Client');
        $this->loadModel('UnitsCaffee');
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
        $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                    array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                    array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
        $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('all', array('conditions' => array('and' => array(
                    array('WeighingPackagingCaffee.remittances_cafee_id' => $remesa),
                    array('WeighingPackagingCaffee.id_oie' => $idOIE)))));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $this->set('bagWeigth', $bagWeigth);
        $this->set('weighingPackagingCaffee', $dataWeighingPackagingCaffee);
        $this->set('detailPackagingCaffee', $dataDetailPackagingCaffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
    }
    
    public function view3() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $idOIE = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
        $remesa = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $this->loadModel('WeighingEmptyCoffee');
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('Client');
        $this->loadModel('User');
        $this->loadModel('UnitsCaffee');
        $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $remesa)));
        $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                    array('DetailPackagingCaffee.remittances_caffee_id' => $remesa),
                    array('DetailPackagingCaffee.packaging_caffee_id' => $idOIE)))));
        $dataWeighingEmptyCoffee = $this->WeighingEmptyCoffee->find('all', array('conditions' => array('and' => array(
                    array('WeighingEmptyCoffee.remittances_caffee_id' => $remesa)))));
        $dataClient = $this->Client->find('first', array('conditions' => array('Client.id' => $dataRemittancesCaffee['RemittancesCaffee']['client_id'])));
        $bagWeigth = $this->UnitsCaffee->find('first', array('conditions' => array('UnitsCaffee.id' => $dataRemittancesCaffee['RemittancesCaffee']['units_cafee_id'])));
        $this->set('bagWeigth', $bagWeigth);
        $this->set('WeighingEmptyCoffee', $dataWeighingEmptyCoffee);
        $this->set('detailPackagingCaffee', $dataDetailPackagingCaffee);
        $this->set('remittancesCaffee', $dataRemittancesCaffee);
        $this->set('client', $dataClient);
        $this->set('user', $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('User.id')))));
    }

    public function packaging_tare() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'basculero';
        $this->request->data['RemittancesCaffeeId'] = isset($this->request->query['remesa']) ? $this->request->query['remesa'] : null;
        $this->request->data['OIE'] = isset($this->request->query['OIE']) ? $this->request->query['OIE'] : null;
    }

    public function savePackagingTare() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->request->data['DetailPackagingCaffee']['tara_packaging'] > 0) {
            date_default_timezone_set('America/Bogota');
            if ($this->DetailPackagingCaffee->updateAll(array('DetailPackagingCaffee.tara_packaging' => $this->request->data['DetailPackagingCaffee']['tara_packaging'],
                        'DetailPackagingCaffee.updated_date' => "'" . date('Y-m-d h:i:s') . "'", 'DetailPackagingCaffee.users_id' => $this->Session->read('User.id')), array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id'],
                        'DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id']))) {
                $this->loadModel('RemittancesCaffee');
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'])));
                $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.tare_packaging' => ($dataRemittancesCaffee['RemittancesCaffee']['tare_packaging'] + $this->request->data['DetailPackagingCaffee']['tara_packaging'])), array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id']));
                $this->Flash->success(__('Peso guardado existoso!.'));
            } else {
                $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
            }
        } else {
            $this->Flash->error(__('El peso no puede ser 0!'));
        }
        return $this->redirect(array('action' => 'packaging_processing'));
    }

    public function savePackagingPallet() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        //debug($this->request->data);exit;
        if ($this->request->data['WeighingPackagingCaffee']['weight_pallet'] > 0) {
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['WeighingPackagingCaffee']['id_oie'])))));
            if ($dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'] >= ($this->request->data['quantity_bags'] + $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet'])) {
                date_default_timezone_set('America/Bogota');
                $this->loadModel('WeighingPackagingCaffee');
                $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('first', array('conditions' => array('and' => array(
                            array('remittances_cafee_id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']),
                            array('id_oie' => $this->request->data['WeighingPackagingCaffee']['id_oie']))),
                    'order' => array('seq_weight_pallet DESC')));
                $this->request->data['WeighingPackagingCaffee']['weighing_date'] = date('Y-m-d h:i:s');
                if ($dataWeighingPackagingCaffee) {
                    $this->request->data['WeighingPackagingCaffee']['seq_weight_pallet'] = $dataWeighingPackagingCaffee['WeighingPackagingCaffee']['seq_weight_pallet'] + 1;
                } else {
                    $this->request->data['WeighingPackagingCaffee']['seq_weight_pallet'] = 1;
                }
                if ($this->WeighingPackagingCaffee->save($this->request->data)) {
                    $this->loadModel('RemittancesCaffee');
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id'])));
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.quantity_out_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] + 1),
                        'RemittancesCaffee.quantity_in_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] - 1),
                        'RemittancesCaffee.ref_driver'=>"'".$this->request->data['WeighingPackagingCaffee']['ref_driver']."'",
                        'RemittancesCaffee.quantity_bag_out_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] + $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet']),
                        'RemittancesCaffee.quantity_bag_in_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet'])), array('RemittancesCaffee.id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']));
                    $this->Flash->success(__('Peso guardado existoso!.'));
                    return $this->redirect(array('action' => 'packaging_processing'));
                } else {
                    $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
                }
            } else {
                $this->Flash->error(__('No se pueden agregar más sacos a este embalaje'));
            }
        } else {
            $this->Flash->error(__('El peso no puede ser 0!'));
        }
        return $this->redirect(array('action' => 'view', '?' => array('OIE' => $this->request->data['WeighingPackagingCaffee']['id_oie'], 'remesa' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id'])));
    }

    public function savePackagingPallet2() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if ($this->request->data['WeighingPackagingCaffee']['weight_pallet'] > 0) {
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['WeighingPackagingCaffee']['id_oie'])))));
            if ($dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'] >= ($this->request->data['quantity_bags'] + $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet'])) {
                date_default_timezone_set('America/Bogota');
                $this->loadModel('WeighingPackagingCaffee');
                $dataWeighingPackagingCaffee = $this->WeighingPackagingCaffee->find('first', array('conditions' => array('and' => array(
                            array('remittances_cafee_id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']),
                            array('id_oie' => $this->request->data['WeighingPackagingCaffee']['id_oie']))),
                    'order' => array('seq_weight_pallet DESC')));
                $this->request->data['WeighingPackagingCaffee']['weighing_date'] = date('Y-m-d h:i:s');
                if ($dataWeighingPackagingCaffee) {
                    $this->request->data['WeighingPackagingCaffee']['seq_weight_pallet'] = $dataWeighingPackagingCaffee['WeighingPackagingCaffee']['seq_weight_pallet'] + 1;
                } else {
                    $this->request->data['WeighingPackagingCaffee']['seq_weight_pallet'] = 1;
                }
                if ($this->WeighingPackagingCaffee->save($this->request->data)) {
                    $this->loadModel('RemittancesCaffee');
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id'])));
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.quantity_out_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] + 1),
                        'RemittancesCaffee.quantity_in_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] - 1),
                        'RemittancesCaffee.quantity_bag_out_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] + $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet']),
                        'RemittancesCaffee.quantity_bag_in_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] - $this->request->data['WeighingPackagingCaffee']['quantity_bag_pallet'])), array('RemittancesCaffee.id' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id']));
                    $this->Flash->success(__('Peso guardado existoso!.'));
                } else {
                    $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
                }
            } else {
                $this->Flash->error(__('No se pueden agregar más sacos a este embalaje'));
            }
        } else {
            $this->Flash->error(__('El peso no puede ser 0!'));
        }
        return $this->redirect(array('action' => 'view2', '?' => array('OIE' => $this->request->data['WeighingPackagingCaffee']['id_oie'], 'remesa' => $this->request->data['WeighingPackagingCaffee']['remittances_cafee_id'])));
    }
    
    public function savePackagingPallet3() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('WeighingEmptyCoffee');
        if($this->request->data['WeighingEmptyCoffee']['slot_store_id'] != null){
            $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.slot_store_id'=>$this->request->data['WeighingEmptyCoffee']['slot_store_id']),array('RemittancesCaffee.id'=>$this->request->data['WeighingEmptyCoffee']['remittances_caffee_id']));
            $this->Flash->success(__('Posici�n actualizada!.'));
        }
        if ($this->request->data['WeighingEmptyCoffee']['weight_pallet'] > 0) {
            $dataDetailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('and' => array(
                        array('DetailPackagingCaffee.remittances_caffee_id' => $this->request->data['WeighingEmptyCoffee']['remittances_caffee_id']),
                        array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['WeighingEmptyCoffee']['oie_id'])))));
            if ($dataDetailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'] >= ($this->request->data['quantity_bags'] + $this->request->data['WeighingEmptyCoffee']['quantity_bag_pallet'])) {
                date_default_timezone_set('America/Bogota');
                $dataWeighingEmptyCoffee = $this->WeighingEmptyCoffee->find('first', array('conditions' => array('and' => array(
                            array('WeighingEmptyCoffee.remittances_caffee_id' => $this->request->data['WeighingEmptyCoffee']['remittances_caffee_id']))),
                    'order' => array('seq_weight_pallet DESC')));
                $this->request->data['WeighingEmptyCoffee']['weighing_date'] = "".date('Y-m-d h:i:s')."";
                //debug($dataWeighingDownloadCaffee);
                if ($dataWeighingEmptyCoffee) {
                    $this->request->data['WeighingEmptyCoffee']['seq_weight_pallet'] = $dataWeighingEmptyCoffee['WeighingEmptyCoffee']['seq_weight_pallet'] + 1;
                } else {
                    $this->request->data['WeighingEmptyCoffee']['seq_weight_pallet'] = 1;
                }
                //debug($this->request->data);exit;
                if ($this->WeighingEmptyCoffee->save($this->request->data)) {
                    $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['WeighingEmptyCoffee']['remittances_caffee_id'])));
                    $this->RemittancesCaffee->updateAll(array(
                        'RemittancesCaffee.quantity_out_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_out_pallet_caffee'] - 1),
                        'RemittancesCaffee.quantity_in_pallet_caffee' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_in_pallet_caffee'] + 1),
                        'RemittancesCaffee.quantity_bag_out_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_out_store'] - $this->request->data['WeighingEmptyCoffee']['quantity_bag_pallet']),
                        'RemittancesCaffee.quantity_bag_in_store' => ($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] + $this->request->data['WeighingEmptyCoffee']['quantity_bag_pallet'])), array('RemittancesCaffee.id' => $this->request->data['WeighingEmptyCoffee']['remittances_caffee_id']));
                    $this->Flash->success(__('Peso guardado existoso!.'));
                } else {
                    $this->Flash->error(__('No se pudo guardar el peso. Intentelo nuevamente!'));
                }
            } else {
                $this->Flash->error(__('No hay mas sacos del embalaje a retorna de la remesa.'));
            }
        } else {
            $this->Flash->error(__('El peso no puede ser 0!'));
        }
        return $this->redirect(array('action' => 'view3', '?' => array('OIE' => $this->request->data['WeighingEmptyCoffee']['oie_id'], 'remesa' => $this->request->data['WeighingEmptyCoffee']['remittances_caffee_id'])));
    }

    public function add() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
	$this->loadModel('RemittancesCaffee');
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->DetailPackagingCaffee->create();
            date_default_timezone_set('America/Bogota');
            $this->request->data['DetailPackagingCaffee']['created_date'] = date('Y-m-d h:i:s');
            $this->request->data['DetailPackagingCaffee']['updated_date'] = date('Y-m-d h:i:s');
            $this->request->data['DetailPackagingCaffee']['state'] = 0;
            $this->request->data['DetailPackagingCaffee']['tara_packaging'] = 0;
            $detailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('DetailPackagingCaffee.remittances_caffee_id ' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'],
                    'DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id'])));
            if (!$detailPackagingCaffee) {
                $qtaCoffeetoOut=$this->request->data['DetailPackagingCaffee']['quantity_radicated_bag_out'];
                $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'])));
                if($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']>=$qtaCoffeetoOut){
                    if ($this->DetailPackagingCaffee->save($this->request->data)) {
                        $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.state_operation_id'=>3,'RemittancesCaffee.total_rad_bag_out' => ($dataRemittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] + $this->request->data['DetailPackagingCaffee']['quantity_radicated_bag_out'])), array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id']));
                        $this->Flash->success(__('La remesa amparada se ha guardado'));
                        return $this->redirect(array('action' => 'add', '?' => ['oie_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id']]));
                    } else {
                        $this->Flash->error(__('La remesa amparada no puedo ser guardada'));
                    }
                }
                else{
                    $this->Flash->error(__('Saldo insuficiente en la Remesa '.$dataRemittancesCaffee['RemittancesCaffee']['id'].' de Caf�. Favor verificar '));
                }
            } else {
                $this->Flash->error(__('Ya exite una remesa ampara a la OIE'));
            }
        }
        $this->DetailPackagingCaffee->recursive = 2;
        $this->set('detailPackagingCaffees', $this->DetailPackagingCaffee->find('all', array('conditions' => array('DetailPackagingCaffee.packaging_caffee_id' => $this->request->query['oie_id']))));
        $this->set('oie_id', $this->request->query['oie_id']);
    }

    public function edit($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            date_default_timezone_set('America/Bogota');
            $this->loadModel('RemittancesCaffee');
            $dataRemittancesCaffee = $this->RemittancesCaffee->find('first', array('conditions' => array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'])));
            $qtaCoffeetoOut=$this->request->data['DetailPackagingCaffee']['quantity_radicated_bag_out'];
            if($dataRemittancesCaffee['RemittancesCaffee']['quantity_bag_in_store']>=$qtaCoffeetoOut){
                $detailPackagingCaffee = $this->DetailPackagingCaffee->find('first', array('conditions' => array('DetailPackagingCaffee.remittances_caffee_id ' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'],
                        'DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id'])));
                if ($this->DetailPackagingCaffee->updateAll(
                                array('DetailPackagingCaffee.quantity_radicated_bag_out' => $this->request->data['DetailPackagingCaffee']['quantity_radicated_bag_out'],
                            'DetailPackagingCaffee.updated_date' => "'" . date('Y-m-d h:i:s') . "'"), array('and' =>
                            array('DetailPackagingCaffee.remittances_caffee_id' => (int) $this->request->data['DetailPackagingCaffee']['remittances_caffee_id'],
                                'DetailPackagingCaffee.packaging_caffee_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id'])))) {
                    $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.total_rad_bag_out' => ($dataRemittancesCaffee['RemittancesCaffee']['total_rad_bag_out'] - $detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'] + $this->request->data['DetailPackagingCaffee']['quantity_radicated_bag_out'])), array('RemittancesCaffee.id' => $this->request->data['DetailPackagingCaffee']['remittances_caffee_id']));
                    $this->Flash->success(__('Se actualizo el registro exitosamente'));
                    return $this->redirect(array('action' => 'add', '?' => ['oie_id' => $this->request->data['DetailPackagingCaffee']['packaging_caffee_id']]));
                } else {
                    $this->Flash->error(__('No se registro la informaci�n. Por favor intentelo nuevamente.'));
                }
            }
            else{
                $this->Flash->error(__('Saldo insuficiente en la Remesa '.$dataRemittancesCaffee['RemittancesCaffee']['id'].' de Caf�. Favor verificar '));
            }
        }
        $this->set('oie_id', $this->request->query['oie_id']);
        $this->set('remittance_id', $this->request->query['remittance_id']);
    }
    
    public function reweighPallet($idBagWeigh = null){
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $parameter = explode("-", $idBagWeigh);
        $this->autoRender=false;
        $this->layout = 'basculero';
        $this->loadModel('RemittancesCaffee');
        $this->loadModel('WeighingPackagingCaffee');
        $idPallet = $parameter[0];
        $bags = $parameter[1];
        $weigh = $parameter[2];
        $weighingPackagingCaffee = $this->WeighingPackagingCaffee->find('first',array('conditions'=>array('WeighingPackagingCaffee.id' => $idPallet)));
        $detailPackagingCaffee = $this->DetailPackagingCaffee->find('first',array('conditions'=>array('and'=>array(
            array('DetailPackagingCaffee.remittances_caffee_id'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['remittances_cafee_id']),
            array('DetailPackagingCaffee.packaging_caffee_id'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['id_oie'])
        ))));
        $totalOutBag = $this->WeighingPackagingCaffee->find('all',
                array('fields'=>'sum(WeighingPackagingCaffee.quantity_bag_pallet)','conditions'=>array('and'=>array(
                    array('WeighingPackagingCaffee.remittances_cafee_id'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['remittances_cafee_id']),
                    array('WeighingPackagingCaffee.id_oie'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['id_oie'])))));
        $remittancesCaffee = $this->RemittancesCaffee->find('first',array('conditions'=>array('RemittancesCaffee.id'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['remittances_cafee_id'])));
        $quantity_bag_pallet = $weighingPackagingCaffee['WeighingPackagingCaffee']['quantity_bag_pallet'];
        $bagsBeforeWeight = $remittancesCaffee['RemittancesCaffee']['quantity_bag_in_store'] + $quantity_bag_pallet;
        $totalBags = ($totalOutBag[0][0]['sum(`WeighingPackagingCaffee`.`quantity_bag_pallet`)'] - $quantity_bag_pallet);
        $isUpdated = $detailPackagingCaffee['DetailPackagingCaffee']['quantity_radicated_bag_out'] >= ($totalBags + $bags);
        if($isUpdated){
            $this->RemittancesCaffee->updateAll(array('RemittancesCaffee.quantity_bag_in_store'=>($bagsBeforeWeight - $bags),'RemittancesCaffee.quantity_bag_out_store'=>($totalBags + $bags)),array('RemittancesCaffee.id'=>$remittancesCaffee['RemittancesCaffee']['id']));
            $this->WeighingPackagingCaffee->updateAll(array('WeighingPackagingCaffee.quantity_bag_pallet'=>$bags,'WeighingPackagingCaffee.weighing_date'=>"'".date('Y-m-d h:i:s')."'",'WeighingPackagingCaffee.weight_pallet'=>$weigh),array('WeighingPackagingCaffee.id'=>$weighingPackagingCaffee['WeighingPackagingCaffee']['id']));
            return json_decode($totalBags);
        }
        else{
            return "";
        }
    }

}
