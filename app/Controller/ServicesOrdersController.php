<?php

App::uses('AppController', 'Controller');

/**
 * ServicesOrders Controller
 *
 * @property ServicesOrder $ServicesOrder
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ServicesOrdersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('User');
        $listUserByCenterOperation = $this->User->find('list',array('conditions' => array('and'=>array('User.departaments_id' => $this->Session->read('User.departaments_id')),array('User.profiles_id' => 1))));
        $this->layout = 'colaborador';
        $this->ServicesOrder->recursive = 1;
        $this->Paginator->settings = array(
            'limit' => 10,
            'conditions' => array('ServicesOrder.create_user' => $listUserByCenterOperation)
            , 'order' => array('ServicesOrder.created_date' => 'DESC')
        );
        $this->set('servicesOrders', $this->Paginator->paginate('ServicesOrder'));
        $this->loadModel('Client');
        $this->set('dataClients', $this->Client->find('list', array('fields' => array('Client.exporter_code', 'Client.business_name'))));
        $this->loadModel('DetailsServicesToCaffee');
        $this->set('detailsServicesToCaffee', $this->DetailsServicesToCaffee->find('list', array('fields' => array('DetailsServicesToCaffee.services_orders_id'),'group'=>array('DetailsServicesToCaffee.services_orders_id'))));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->ServicesOrder->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = 'colaborador';
        $options = array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id));
        $this->set('servicesOrder', $this->ServicesOrder->find('first', $options));
        $this->loadModel('DetailsServicesToCaffee');
        $this->set('detailsServicesToCaffees', $this->paginate('DetailsServicesToCaffee', array('DetailsServicesToCaffee.services_orders_id' => $id)));
        $this->set('detailsServicesPackage', $this->DetailsServicesToCaffee->find('all', ['conditions'=>['DetailsServicesToCaffee.services_orders_id' => $id],'group'=>['DetailsServicesToCaffee.service_package_id']]));
        $this->loadModel('Client');
        $this->set('dataClients', $this->Client->find('list', array('fields' => array('Client.exporter_code', 'Client.business_name'))));
    }
    
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function search($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        $options = array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id));
        $this->set('servicesOrder', $this->ServicesOrder->find('first', $options));
        $this->loadModel('DetailsServicesToCaffee');
        $this->set('detailsServicesToCaffees', $this->paginate('DetailsServicesToCaffee', array('DetailsServicesToCaffee.services_orders_id' => $id)));
        $this->set('detailsServicesPackage', $this->DetailsServicesToCaffee->find('all', ['conditions'=>['DetailsServicesToCaffee.services_orders_id' => $id],'group'=>['DetailsServicesToCaffee.service_package_id']]));
        $this->loadModel('Client');
        $this->set('dataClients', $this->Client->find('list', array('fields' => array('Client.exporter_code', 'Client.business_name'))));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is('post')) {
            $this->ServicesOrder->create();
            date_default_timezone_set('America/Bogota');
            $this->request->data['ServicesOrder']['created_date'] = date('Y-m-d h:i:s');
            $this->request->data['ServicesOrder']['total_valor'] = 0;
            $this->request->data['ServicesOrder']['create_user'] = $this->Session->read('User.id');
            if ($this->ServicesOrder->save($this->request->data)) {
                $this->Flash->success(__('The services order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        if (!$this->ServicesOrder->exists($id)) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = 'colaborador';
        if ($this->request->is(array('post', 'put'))) {
            if ($this->ServicesOrder->save($this->request->data)) {
                $this->Flash->success(__('The services order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id));
            $this->request->data = $this->ServicesOrder->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->ServicesOrder->id = $id;
        if (!$this->ServicesOrder->exists()) {
            throw new NotFoundException(__('Invalid services order'));
        }
        $this->layout = 'colaborador';
        $servicesOrder = $this->ServicesOrder->find('first',array('conditions' => array('ServicesOrder.' . $this->ServicesOrder->primaryKey => $id)));
        $servicesOrder['ServicesOrder']['active']=false;
        $servicesOrder['ServicesOrder']['approve_user'] = $this->Session->read('User.id');
        if ($this->ServicesOrder->save($servicesOrder)) {
                $this->Flash->success(__('Desactiva la OS # '.$id));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The services order could not be saved. Please, try again.'));
            }
        return $this->redirect(array('action' => 'index'));
    }

}
