<?php
App::uses('AppController', 'Controller');
/**
 * SampleCoffeeSends Controller
 *
 * @property SampleCoffeeSend $SampleCoffeeSend
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SampleCoffeeSendsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
                $this->layout = 'colaborador';
		$this->SampleCoffeeSend->recursive = 0;
		$this->set('sampleCoffeeSends', $this->Paginator->paginate());
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
                $this->layout = 'colaborador';
		if ($this->request->is('post')) {
			$this->SampleCoffeeSend->create();
			if ($this->SampleCoffeeSend->save($this->request->data)) {
				$this->Flash->success(__('Envio generado correctamente.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Se genero un error al generar el envio. Por favor intentolo nuavemente.'));
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SampleCoffeeSend->id = $id;
		if (!$this->SampleCoffeeSend->exists()) {
			throw new NotFoundException(__('Invalid sample coffee send'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SampleCoffeeSend->delete()) {
			$this->Flash->success(__('The sample coffee send has been deleted.'));
		} else {
			$this->Flash->error(__('The sample coffee send could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
