<?php

App::uses('AppController', 'Controller');

/**
 * Clients Controller
 * 
 * @property Client $Client
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ClientsController extends AppController {

    public $components = array('Paginator', 'Flash', 'Session');


    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';

        $this->Client->recursive = 0;
        $this->set('clients', $this->Paginator->paginate());
    }

	
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';

		if (!$this->Client->exists($id)) {
            throw new NotFoundException(__('Invalid Client'));
        }
        $options = array('conditions' => array('Client.' . $this->Client->primaryKey => $id));
        $this->set('clients', $this->Client->find('first', $options));
    }


    /**
     * add method
     *
     * @return void
     */
    public function add() {
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';

		if ($this->request->is('post')) {
            $this->Client->create();

				$this->request->data['Client']['created_date'] = date('Y-m-d H:i:s');
				$this->request->data['Client']['updated_date'] = date('Y-m-d H:i:s');
				$this->request->data['Client']['isBig'] = 0;
				$this->request->data['Client']['billing_manual'] = 0;
				$this->request->data['Client']['billing_dayly'] = 1;
				$this->request->data['Client']['user_register'] =$this->Session->read('User.id');

			if ($this->Client->save($this->request->data)) {
                $this->Flash->success(__('El Cliente se ha guardado con exito.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('No se pudo guardar el Cliente. Inténtalo de nuevo.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
		if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';

		if (!$this->Client->exists($id)) {
            throw new NotFoundException(__('Error al modificar el cliente'));
        }
        if ($this->request->is(array('post', 'put'))) {
			
			$dataClient = $this->Client->findById($id);
			$dataClient['Client']['business_name'] = $this->request->data['Client']['business_name'];
			$dataClient['Client']['nit'] = $this->request->data['Client']['nit'];
			$dataClient['Client']['bank_account'] = $this->request->data['Client']['bank_account'];
			$dataClient['Client']['emails'] = $this->request->data['Client']['emails'];
			$dataClient['Client']['contact_name'] = $this->request->data['Client']['contact_name'];
			$dataClient['Client']['phone'] = $this->request->data['Client']['phone'];
			$dataClient['Client']['city_location'] = $this->request->data['Client']['city_location'];
			$dataClient['Client']['updated_date'] = date("Y/m/d H:i:s");
			$dataClient['Client']['user_register'] = $this->Session->read('User.id');

		
            if ($this->Client->save($dataClient)) {
                $this->Flash->success(__('Cliente Actualizado.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('Error el cliente no puede actualizarce. Por favor, vuelve a intentar.'));
            }
        } else {
            $options = array('conditions' => array('Client.' . $this->Client->primaryKey => $id));
            $this->request->data = $this->Client->find('first', $options);
        }
    }

    public function search() {
        if (!$this->Session->read('User.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->layout = 'colaborador';
            
        $searchClients = array();
            if ($this->request->is('post')) {
                $this->Client->recursive = 3;
                if ($this->request->data['Client']['exporter_code']) {
                    $searchClients = array($this->Client->find('first', array('conditions' => array('Client.exporter_code' => $this->request->data['Client']['exporter_code']))));                       
                } else if ($this->request->data['Client']['business_name']) {
                    $businessName = $this->request->data['Client']['business_name'];                    
                    $searchClients = $this->Client->find('all', array('conditions' => array('Client.business_name LIKE' => '%'.$businessName.'%'),'order' => array('id' => 'asc')));                                           
                }else if ($this->request->data['Client']['nit']) {
                    $searchClients = $this->Client->find('all', array('conditions' => array('Client.nit' => $this->request->data['Client']['nit'])));
                }                 
            }
        $this->set('searchClients', $searchClients);
            
    }









    	public function findByExpo($codExpo = null) {
        	if (!$this->Session->read('User.id')) {
            		return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        	}
        	$this->autoRender = false;
        	$client = $this->Client->find('first', array('conditions' => array('Client.exporter_code' => $codExpo)));
        	return json_encode($client);
    	}

	public function addremote(){
		$this->loadModel('User');
		$userSPB = $this->User->find('first',array('conditions'=>array('User.id'=>71)));
        	$stringBase64Ok = $userSPB['User']['username'].":spb.2018";
        	$hashOk = base64_encode($stringBase64Ok);
        	if(!isset($_GET['token']) || ($hashOk != $_GET['token'])){
				$this->set(array(
            		'message' => array('msg'=> "Datos de acceso incorrectos ",'id-msg'=>3),
            		'_serialize' => array('message')
        		));
			}else{
				if(!isset($_GET['business_name']) || !isset($_GET['emails']) || !isset($_GET['contact_name']) || !isset($_GET['exporter_code']) 
				|| !isset($_GET['city_location']) || !isset($_GET['nit']) || !isset($_GET['phone'])){
					$this->set(array(
            			'message' => array('msg'=>"Datos incompletos!",'id-msg'=>5),
            			'_serialize' => array('message')
        			));
				}else{
					$clientOld = $this->Client->find('first',array('conditions'=>array('Client.nit'=>$_GET['nit'])));
					if(empty($clientOld)){
						$this->request->data['Client']['created_date'] = date('Y-m-d h:i:s');
						$this->request->data['Client']['business_name'] = $_GET['business_name'];
						$this->request->data['Client']['emails'] = $_GET['emails'];
						$this->request->data['Client']['contact_name'] = $_GET['contact_name'];
						$this->request->data['Client']['exporter_code'] = $_GET['exporter_code'];
						$this->request->data['Client']['phone'] = $_GET['phone'];
						$this->request->data['Client']['city_location'] = $_GET['city_location'];
						$this->request->data['Client']['updated_date'] = date('Y-m-d h:i:s');
						$this->request->data['Client']['nit'] = $_GET['nit'];
						if ($this->Client->save($this->request->data)) {
								$this->set(array(
								'message' => array('msg'=>"Cliente registrado correctamente".$this->request->data['Client']['nit'],'id-msg'=>1),
								'_serialize' => array('message')
								));
						}
						else{
								$this->set(array(
								'message' => array('msg'=>"No se puedo registrar los datos del cliente",'id-msg'=>2),
								'_serialize' => array('message')
								));
						}
					}
					else{
						$this->set(array(
								'message' => array('msg'=>"Cliente ya existe",'id-msg'=>4),
								'_serialize' => array('message')
								));
					}
				}
			}
		}

}
