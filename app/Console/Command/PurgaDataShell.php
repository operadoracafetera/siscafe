<?php

    App::uses('EmbalajesController', 'Controller');
    App::uses('ExportersController', 'Controller');
    App::uses('TareRemesasController', 'Controller');
    App::uses('AdictionalElementsController', 'Controller');
    App::uses('TypeCtnsController', 'Controller');
    App::uses('TallyAdictionalInfosController', 'Controller');
    App::uses('TallyExposController', 'Controller');
    App::uses('AdictionalElementsHasProformasController', 'Controller');
    App::uses('TallyPackagingsController', 'Controller');
    App::uses('AdictionalElementsHasTallyPackagingsController', 'Controller');
    
    
class PurgaDataShell extends AppShell {

    function exportDataPackaging(){
	 $connection_expocafe = "192.168.35.213";
	 $username = "sop_user";
	 $password = "123";
	 $dbname = "schema_siscafe";
	 $date_var = date("Y-m-d", strtotime('-1 day'));
	 echo $date_var;
	 $date_time = date("Y-m-d H:i:s");
	 //$date_var = date("2017-03-08");
	 echo $date_time."\n";
	 $conn_expocafe = new mysqli($connection_expocafe, $username, $password, $dbname);
	 //$query1="select te.id,te.seq_tally,te.booking from tally_expo te where te.booking in (
	//	    select ta.booking from tally_adictional_info ta
	//	    inner join adictional_elements_has_proforma aep on ta.id=aep.tally_adictional_info_id
	//	    where ta.updated=true)";
	$query1="select te.id FROM tally_expo te WHERE te.packaging_date like '%".$date_var."%'";
	 if ($result1 = $conn_expocafe->query($query1)) {
	    echo "Purgando idTally sin elementos adicionales..."."\n";
	    if($result1->num_rows == 0) {
		echo "La consulta No arrojo resultados"."\n";
	    }
	    else{
	      $i=0;
	      while($obj1 = $result1->fetch_object()){
		$id=$obj1->id;
		echo "Id Tally_Expo: " . $id."\n";
		$this->purgeDataEmpty($id);
		//$this->exportarData($idTally);
		$i++;
	      }
	      echo "Total registros borrados: ".$i."\n";
	    }
	 }
	 $query2 = "SELECT tp.id FROM tally_packaging tp WHERE tp.created_date like '%".$date_var."%' and
	    tp.id NOT IN (select te.seq_tally FROM tally_expo te WHERE te.packaging_date like '%".$date_var."%')";
	    echo $query2;
	 if ($result2 = $conn_expocafe->query($query2)) {
	    echo "Exportando Data a tabla Tally_Expo (Excel)..."."\n";
	    if($result2->num_rows == 0) {
		echo "La consulta No arrojo resultados"."\n";
	    }
	    else{
	       $i=0;
	      while($obj2 = $result2->fetch_object()){
		$idTally=$obj2->id;
		$this->exportarData($idTally);
		echo $idTally."\n";
		$i++;
	      }
	      echo "Total registros creados: ".$i."\n";
	    }
	 }
    }
    
    
    function exportarData($idTally) {
	$EmbalajesController = new EmbalajesController;
	$TallyExposController = new TallyExposController;
	$TallyExposController->TallyExpo->create();
	$adictionalElementsHasProformasController = new AdictionalElementsHasProformasController;
	$TallyAdictionalInfosController = new TallyAdictionalInfosController;
	$tallyPackagingsController = new TallyPackagingsController;
	$TypeCtnsController = new TypeCtnsController;
	$TareRemesasController = new TareRemesasController;
	$ExportersController = new ExportersController;
	$tallyPackaging = $tallyPackagingsController->findById($idTally);
	$bic_container = $tallyPackaging['TallyPackaging']['bic_container'];
	$date_packaging = substr($tallyPackaging['TallyPackaging']['created_date'], 0, 10);
	$adictionalElementsController = new AdictionalElementsController;
	$embalajes = $EmbalajesController->findWeightByContainer($bic_container,$date_packaging);
	if($embalajes == null){
	  echo "Contenedor sin REM ".$bic_container."\n";
	}
	else{
	  echo "Contenedor con REM ".$bic_container."\n";
	  $cod_exportador = $embalajes[0]['Embalaje']['id_exportador'];
	  $loteCafe=0;
	  $loteCafeFrm=0;
	  foreach ($embalajes as $embalaje){
	      $loteCafetmp=$embalaje["Embalaje"]["lote"];
	      if($loteCafe != $loteCafetmp){
		$loteCafeFrm.="3-".$cod_exportador."-".$loteCafetmp." | ";
		$loteCafe = $loteCafetmp;
	      }
	  }
	  
	  $totalSacos=0;
	  foreach ($embalajes as $embalaje){
	      $totalSacos+=$embalaje["Embalaje"]["rpssacos"];
	  }
	  $TaraEstiba = $TareRemesasController->tarePallet($embalajes);
	  $totalWeight = $EmbalajesController->totalWeightContainer($embalajes);
	  $Exporter=$ExportersController->exporterBycode($embalajes);
	  $weightNet = $totalWeight-($TaraEstiba["tareEstiba"]);
	  $proforma = $embalajes[0]['Embalaje']['proforma'];
	  $dateExportation=$EmbalajesController->findDateByContainer($bic_container);
	  $idIsoCtn = $tallyPackaging['TallyPackaging']['type_ctn_id'];
	  $TypeCtn = $TypeCtnsController->findById($idIsoCtn);
	  $TallyAdictionalInfos = $TallyAdictionalInfosController->findByProforma($proforma);
	  $elementsCtn="";
	  $booking="";
	  $destino="";
	  $viaje="";
	  if($TallyAdictionalInfos == null){
	      echo "Contenedor ".$bic_container." sin Booking; Proforma: ".$proforma." NO existe o no Coincide\n";
	  }
	  else{
	      $booking = $TallyAdictionalInfos['TallyAdictionalInfo']['booking'];
	      $destino = $TallyAdictionalInfos['TallyAdictionalInfo']['destino'];
	      $viaje = $TallyAdictionalInfos['TallyAdictionalInfo']['viaje'];
	      if($TallyAdictionalInfos['AdictionalElementsHasProforma'] != null){
		$tally_adictional_info_id = $TallyAdictionalInfos['AdictionalElementsHasProforma'][0]['tally_adictional_info_id'];
		$allElements = $adictionalElementsHasProformasController->findByTallyAdictional($tally_adictional_info_id);
		$adictionalElements = $adictionalElementsController->findAll();
		$elementsCtn="";
		foreach ($allElements as $element){
		  foreach($adictionalElements as $adictionalElement){
		    if($adictionalElement['AdictionalElement']['id'] == $element['AdictionalElementsHasProforma']['adictional_elements_id']){
		      $elementsCtn .= $adictionalElement['AdictionalElement']['name']." - ";
		      $valor = $element['AdictionalElementsHasProforma']['type_unit']; 
			if($valor == 0)
			  $elementsCtn.="CAPA | ";
			else if($valor == 1)
			  $elementsCtn.="UNIDAD | ";
			  else
			  $elementsCtn .= "N/A | ";
		      $elementsCtn .= "CTA: ".$element['AdictionalElementsHasProforma']['quantity']." | ";
		    }
		  }
		}
	      }
	      else{
		  echo "Contenedor ".$bic_container." con Booking: ".$booking." y Proforma: ".$proforma." sin Elementos Adicionales\n";
	      }
	      
	  }
	  $totalCtnLoad = $weightNet+$TypeCtn['TypeCtn']['weight_ctn'];
	  $lineaNaviera = $embalajes[0]["Embalaje"]["linea"];
	  $naviera = $embalajes[0]["Embalaje"]["agente_maritimo"];
	  $embalajes[0]["Embalaje"]["agente_maritimo"];
	  $modalidad_embalaje = $embalajes[0]["Embalaje"]["modalidad_embalaje"];
	  $tipo_embalaje = $embalajes[0]["Embalaje"]["tipo_embalaje"];
	  $seal1 = $tallyPackaging['TallyPackaging']['seals_line_code'];
	  $seal2 = $tallyPackaging['TallyPackaging']['seals_policy_code'];
	  $TareCtn = $TypeCtn['TypeCtn']['weight_ctn'];
	  $datePackaging = substr($embalajes[0]['Embalaje']['fecha'], 0,10)." ".$embalajes[0]['Embalaje']['rpshora'];
	  $tally_caffee = array(
	    null,
	    'lotes' => $loteCafeFrm,
	    'booking' => trim($booking),
	    'destino' => trim($destino),
	    'viaje' => trim($viaje),
	    'linea' => $lineaNaviera,
	    'agt_maritimo' => $naviera,
	    'exportador' => $Exporter['Exporter']['name'],
	    'cod_exportador' => $cod_exportador,
	    'total_sacos' => $totalSacos,
	    'modalidad_embalaje' => $modalidad_embalaje,
	    'tipo_embalaje' => $tipo_embalaje,
	    'bic_ctn' => $bic_container,
	    'iso_ctn' => $TypeCtn['TypeCtn']['type_ctn'],
	    'seal1' => $seal1,
	    'seal2' => $seal2,
	    'weight_ctn' => $TareCtn,
	    'weight_load_net' => $weightNet,
	    'weight_load_total' => $totalWeight,
	    'packaging_date' => $datePackaging,
	    'adictional_elements' => $elementsCtn,
	    'seq_tally' => $idTally,
	    'motorship' => $embalajes[0]["Embalaje"]["motonave"]
	  );
	  $TallyExposController->TallyExpo->save($tally_caffee);
	  //var_dump($tally_caffee);
	}
    }
    
    function purgeDataEmpty($id){
	/*$TallyAdictionalInfosController = new TallyAdictionalInfosController;
	$InfoReserva = $TallyAdictionalInfosController->findByBooking($booking);
	$InfoReserva['TallyAdictionalInfo']['updated']=false;
	if($TallyAdictionalInfosController->TallyAdictionalInfo->save($InfoReserva)){
	  $InfoProforma = $TallyAdictionalInfosController->findByBooking($booking);
	  debug($InfoProforma);
	  echo "update booking!\n";
	}
	else{
	  echo "not update!\n";
	}*/
	$TallyExposController = new TallyExposController;
	if($TallyExposController->TallyExpo->delete($id)){
	  echo "delete!\n";
	}
	else{
	  echo "not delete!\n";
	}
    }
    
    public function main() {
	    $this->exportDataPackaging();
    }
}
